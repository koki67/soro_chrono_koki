# -*- coding: utf-8 -*-
"""
Created on Wed Oct 30 04:16:14 2019

@author: dmulr
"""

# In[Header]

"""
author: declan mulroy
project: JAMoEBA
email: dmulroy@hawk.iit.edu
date: 10/9/19
"""

# In[import libraries]
import pychrono.core as chrono
import pychrono.irrlicht as chronoirr
import pychrono.postprocess as postprocess
import os
import numpy as np
import timeit

sim=0
start = timeit.default_timer()
record=0
run=10
# In[Set Path]
chrono.SetChronoDataPath("C:/Users/dmulr/Documents/data")


# In[Create sysem and other misselanous things]
my_system = chrono.ChSystemNSC()
my_system.Set_G_acc(chrono.ChVectorD(0, -9.81, 0))
chrono.ChCollisionModel.SetDefaultSuggestedEnvelope(0.0001)
chrono.ChCollisionModel.SetDefaultSuggestedMargin(0.00001)
my_system.SetMaxItersSolverSpeed(1000)
#my_system.SetMinBounceSpeed(.2)

# In[Define frictional properties]
mu_f=.4
mu_b=.01
mu_r=.4
mu_s=.9

# In[create material properties]
material = chrono.ChMaterialSurfaceNSC()
material.SetFriction(mu_f)
material.SetDampingF(mu_b)
material.SetCompliance (0.0001)
material.SetComplianceT(0.0001)
material.SetRollingFriction(mu_r)
material.SetSpinningFriction(mu_s)
material.SetComplianceRolling(0.0001)
material.SetComplianceSpinning(0.0001)
# In[Create floor]
body_floor = chrono.ChBody()
body_floor.SetBodyFixed(True)
body_floor.SetPos(chrono.ChVectorD(0, -1, 0 ))
body_floor.SetMaterialSurface(material)

# In[Collision shape]
body_floor.GetCollisionModel().ClearModel()
body_floor.GetCollisionModel().AddBox(3, 1, 3) # hemi sizes
body_floor.GetCollisionModel().BuildModel()
body_floor.SetCollide(True)

# In[Visualization shape]
body_floor_shape = chrono.ChBoxShape()
body_floor_shape.GetBoxGeometry().Size = chrono.ChVectorD(3, 1, 3)
body_floor.GetAssets().push_back(body_floor_shape)
body_floor_texture = chrono.ChTexture()
body_floor_texture.SetTextureFilename(chrono.GetChronoDataPath() + 'aluminum.jpg')
body_floor.GetAssets().push_back(body_floor_texture)
my_system.Add(body_floor)

# In[Create wall to enclose the robot]

# Geometry of walls
wwidth=.1
wheight=.1
wlength=1.5

meh=np.array([1,-1])

for i in(meh):
    # Create walls 
    wall = chrono.ChBody()
    wall.SetBodyFixed(True)
    wall.SetPos(chrono.ChVectorD(1.5*i, wheight/2, 0))
    wall.SetMaterialSurface(material)
    # Collision shape
    wall.GetCollisionModel().ClearModel()
    wall.GetCollisionModel().AddBox(wwidth, wheight, wlength) # hemi sizes
    wall.GetCollisionModel().BuildModel()
    wall.SetCollide(True)
    # Visualization shape
    wall_shape = chrono.ChBoxShape()
    wall_shape.GetBoxGeometry().Size = chrono.ChVectorD(wwidth, wheight, wlength)
    wall_shape.SetColor(chrono.ChColor(0.4,0.4,0.5))
    wall.GetAssets().push_back(wall_shape)
    wall_texture = chrono.ChTexture()
    wall_texture.SetTextureFilename(chrono.GetChronoDataPath() + 'aluminum.jpg')
    wall.GetAssets().push_back(wall_texture)
    my_system.Add(wall)

for i in(meh):
    # Create walls
    wall = chrono.ChBody()
    wall.SetBodyFixed(True)
    wall.SetPos(chrono.ChVectorD(0, wheight/2, 1.5*i))
    wall.SetMaterialSurface(material)
    # Collision shape
    wall.GetCollisionModel().ClearModel()
    wall.GetCollisionModel().AddBox(wlength, wheight, wwidth) # hemi sizes
    wall.GetCollisionModel().BuildModel()
    wall.SetCollide(True)
    # Visualization shape
    wall_shape = chrono.ChBoxShape()
    wall_shape.GetBoxGeometry().Size = chrono.ChVectorD(wlength,wheight, wwidth)
    wall_shape.SetColor(chrono.ChColor(0.4,0.4,0.5))
    wall.GetAssets().push_back(wall_shape)
    wall_texture = chrono.ChTexture()
    wall_texture.SetTextureFilename(chrono.GetChronoDataPath() + 'aluminum.jpg')
    wall.GetAssets().push_back(wall_texture)
    my_system.Add(wall)

height=.12

diameter=.08 
nb=8
R1=(diameter*nb/(np.pi*2))+.055


obj=[]
Springs=[]

# spring constant
k=0

# damping
b=0

# resting length
rl=.05

# max resting length
rlmax=.051

# Inertias
Ixx=1.92*10**-4
Izz=.000524
Iyy=.000529


F=0
# In[External forces]
constfun = chrono.ChFunction_Const(F)
constfun0 = chrono.ChFunction_Const(0) 


  
# Empty matrix      
botcall=np.zeros((1,nb))

# ID number of robots to be active
active=np.array([])

# For robots that are active fill botcall==1
for i in (active):
    botcall[:,i]=1

# In[Create bots]
for i in range(nb):

    # Initial postion of each bot
    theta=i*2*np.pi/nb
    x=R1*np.cos(theta)
    y=5*height
    z=R1*np.sin(theta)
    

    # Create Body
    bot = chrono.ChBody()
    my_system.AddBody(bot)
    bot.SetIdentifier(i)
    bot.SetName("bot")
    
    # Set certain properties
    bot.SetMass(.128)
    bot.SetInertiaXX(chrono.ChVectorD(Ixx, Iyy, Izz))
    bot.SetPos(chrono.ChVectorD(x, y, z))
    rotation1 = chrono.ChQuaternionD()
    rotation1.Q_from_AngAxis(-theta, chrono.ChVectorD(0, 1, 0));  
    bot.SetRot(rotation1)
    
    # Create collision shape
    cyl_r = chrono.ChCylinderShape()
    cyl_r.GetCylinderGeometry().p1 = chrono.ChVectorD(0, height/2, 0)
    cyl_r.GetCylinderGeometry().p2 = chrono.ChVectorD(0, -height/2, 0)
    cyl_r.GetCylinderGeometry().rad = diameter/2
    bot.AddAsset(cyl_r)
    
    # collision model
    bot.GetCollisionModel().ClearModel()
    bot.GetCollisionModel().AddCylinder(diameter/2,diameter/2,height/2) # hemi sizes
    bot.GetCollisionModel().BuildModel()
    bot.SetCollide(True)
    bot.SetBodyFixed(False)
    
    # Distance model

    
    
    # Attatch springs    
    if i>=1:
        ground=chrono.ChLinkDistance()
        ground.SetName("ground")
        p1=0
        p2=diameter/2
        p3=0
        p4=-diameter/2
        ground.Initialize(obj[i-1], bot,True, chrono.ChVectorD(0,0,0), chrono.ChVectorD(0,0,0))
        my_system.AddLink(ground)
        Springs.append(ground)

    my_system.Add(bot)
    obj.append(bot)        




# In[Irrlecht Simulation]    
myapplication = chronoirr.ChIrrApp(my_system, 'PyChrono example', chronoirr.dimension2du(1024,768))

myapplication.AddTypicalSky()
myapplication.AddTypicalLogo(chrono.GetChronoDataPath() + 'logo_pychrono_alpha.png')
myapplication.AddTypicalCamera(chronoirr.vector3df(0.75,0.75,1.5))
myapplication.AddLightWithShadow(chronoirr.vector3df(2,4,2),chronoirr.vector3df(0,0,0),9,1,9,50)                

            # ==IMPORTANT!== Use this function for adding a ChIrrNodeAsset to all items
                        # in the system. These ChIrrNodeAsset assets are 'proxies' to the Irrlicht meshes.
                        # If you need a finer control on which item really needs a visualization proxy in
                        # Irrlicht, just use application.AssetBind(myitem); on a per-item basis.

myapplication.AssetBindAll();

                        # ==IMPORTANT!== Use this function for 'converting' into Irrlicht meshes the assets
                        # that you added to the bodies into 3D shapes, they can be visualized by Irrlicht!

myapplication.AssetUpdateAll();

            # If you want to show shadows because you used "AddLightWithShadow()'
            # you must remember this:
myapplication.AddShadowAll();




# In[ Run the simulation]

count=0

h=.001

myapplication.SetTimestep(h)
myapplication.SetTryRealtime(True)



while(myapplication.GetDevice().run()):
    
    
    myapplication.BeginScene()
    myapplication.DrawAll()
    ## Render the distance constraint.
    chronoirr.ChIrrTools.drawSegment(
    myapplication.GetVideoDriver(),
    ground.GetEndPoint1Abs(),
    ground.GetEndPoint2Abs(),
    chronoirr.SColor(255, 200, 20, 0), True)      
    
    myapplication.DoStep()
    myapplication.EndScene()
    

    
    
    
    