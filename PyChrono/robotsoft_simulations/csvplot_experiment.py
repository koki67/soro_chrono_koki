
# In[Import libraries]
import matplotlib.pyplot as plt
import numpy as np
import sys
import os

# In[File paths]
#filepath= "C:/Users/dmulr/Documents/bouncing-balls/PyChrono/Experiment_process/data/"
filepath="C:/Users/dmulr/OneDrive/Documents/bouncing-balls/PyChrono/robotsoft_simulations/experiments_grasping/"


#filename= ["2019-10-22 10 37","2019-10-22 11 07","2019-10-22 20 34",'2019-10-22 20 38','2019-10-22 20 44','2019-10-22 20 46','2019-10-22 20 48','2019-10-22 20 50',"2019-10-22 20 52",
   #        "2019-10-22 20 56","2019-10-22 20 59","2019-10-22 21 01","2019-10-22 21 03","2019-10-22 21 14"]

filename=["2019-10-23 21 25","2019-10-23 21 39","2019-10-23 21 44","2019-10-23 21 48","2019-10-23 21 51"]



w=5
for W in (filename):    
    
    csv=np.genfromtxt(filepath+W+".csv",delimiter=',')
    csv=np.delete(csv,0,0)

    x=np.array([])
    z=np.array([])
    theta=np.array([])

    nr=8
    count=0
    for i in range((csv.shape[1]-1)):
        if i%3==0:
            x=np.append(x,csv[:,i])
            z=np.append(z,csv[:,i+1])
            
    
    count=csv.shape[0]
    time=count/5
    X=np.zeros((nr,count))
    Z=np.zeros((nr,count))
   


    for i in range(nr):
        X[i,:]=np.transpose(x[count*i:count*i+count])
        Z[i,:]=np.transpose(z[count*i:count*i+count])



  #  results= "C:/Users/dmulr/Documents/bouncing-balls/PyChrono/Experiment_process/processed/"
    results="C:/Users/dmulr/OneDrive/Documents/bouncing-balls/PyChrono/robotsoft_simulations/processed_grasping/"  
    X=np.delete(X,0,axis=1)
    Z=np.delete(Z,0,axis=1)

    plt.figure(1)

    for i in range(nr):

            plt.plot(X[i,:],Z[i,:])

    plt.title('Positon of the bots in the x-z plane')
    plt.xlabel('x position (m)')
    plt.ylabel('z position (m)')
    plt.grid(True)
    plt.savefig(results+W+".pdf")
    plt.close('all')


    np.savez(results+W+".npz",allow_pickle=True,X=X,Z=Z,time=time)

