
# In[Header]

"""
author: declan mulroy
project: JAMoEBA
email: dmulroy@hawk.iit.edu
date: 10/9/19
"""

# In[import libraries]
import pychrono.core as chrono
import pychrono.irrlicht as chronoirr
import pychrono.postprocess as postprocess
import os
import numpy as np
import timeit

sim=0
start = timeit.default_timer()
record=0
run=10
# In[Set Path]
#chrono.SetChronoDataPath("../../../data/")

chrono.SetChronoDataPath("C:/Users/dmulr/OneDrive/Documents/data")
# In[Create sysem and other misselanous things]
my_system = chrono.ChSystemNSC()
my_system.Set_G_acc(chrono.ChVectorD(0, -9.81, 0))
chrono.ChCollisionModel.SetDefaultSuggestedEnvelope(0.00001)
chrono.ChCollisionModel.SetDefaultSuggestedMargin(0.00001)
my_system.SetMaxItersSolverSpeed(1000)
my_system.SetMinBounceSpeed(.1)

# In[Define frictional properties]
mu_f=.4
mu_b=.01
mu_r=.4
mu_s=.4

# In[create material properties]
material = chrono.ChMaterialSurfaceNSC()
material.SetFriction(mu_f)
material.SetDampingF(mu_b)
material.SetCompliance (0.00001)
material.SetComplianceT(0.00001)
material.SetRollingFriction(mu_r)
material.SetSpinningFriction(mu_s)
material.SetComplianceRolling(0.00001)
material.SetComplianceSpinning(0.0001)
# In[Create floor]
body_floor = chrono.ChBody()
body_floor.SetBodyFixed(True)
body_floor.SetPos(chrono.ChVectorD(0, -1, 0 ))
body_floor.SetMaterialSurface(material)

# In[Collision shape]
body_floor.GetCollisionModel().ClearModel()
body_floor.GetCollisionModel().AddBox(3, 1, 3) # hemi sizes
body_floor.GetCollisionModel().BuildModel()
body_floor.SetCollide(True)

# In[Visualization shape]
body_floor_shape = chrono.ChBoxShape()
body_floor_shape.GetBoxGeometry().Size = chrono.ChVectorD(3, 1, 3)
body_floor.GetAssets().push_back(body_floor_shape)
body_floor_texture = chrono.ChTexture()
body_floor_texture.SetTextureFilename(chrono.GetChronoDataPath() + 'aluminum.jpg')
body_floor.GetAssets().push_back(body_floor_texture)
my_system.Add(body_floor)

# In[Create wall to enclose the robot]

# Geometry of walls
wwidth=.1
wheight=.1
wlength=1.5

meh=np.array([1,-1])

for i in(meh):
    # Create walls 
    wall = chrono.ChBody()
    wall.SetBodyFixed(True)
    wall.SetPos(chrono.ChVectorD(1.5*i, wheight/2, 0))
    wall.SetMaterialSurface(material)
    # Collision shape
    wall.GetCollisionModel().ClearModel()
    wall.GetCollisionModel().AddBox(wwidth, wheight, wlength) # hemi sizes
    wall.GetCollisionModel().BuildModel()
    wall.SetCollide(True)
    # Visualization shape
    wall_shape = chrono.ChBoxShape()
    wall_shape.GetBoxGeometry().Size = chrono.ChVectorD(wwidth, wheight, wlength)
    wall_shape.SetColor(chrono.ChColor(0.4,0.4,0.5))
    wall.GetAssets().push_back(wall_shape)
    wall_texture = chrono.ChTexture()
    wall_texture.SetTextureFilename(chrono.GetChronoDataPath() + 'aluminum.jpg')
    wall.GetAssets().push_back(wall_texture)
    my_system.Add(wall)

for i in(meh):
    # Create walls
    wall = chrono.ChBody()
    wall.SetBodyFixed(True)
    wall.SetPos(chrono.ChVectorD(0, wheight/2, 1.5*i))
    wall.SetMaterialSurface(material)
    # Collision shape
    wall.GetCollisionModel().ClearModel()
    wall.GetCollisionModel().AddBox(wlength, wheight, wwidth) # hemi sizes
    wall.GetCollisionModel().BuildModel()
    wall.SetCollide(True)
    # Visualization shape
    wall_shape = chrono.ChBoxShape()
    wall_shape.GetBoxGeometry().Size = chrono.ChVectorD(wlength,wheight, wwidth)
    wall_shape.SetColor(chrono.ChColor(0.4,0.4,0.5))
    wall.GetAssets().push_back(wall_shape)
    wall_texture = chrono.ChTexture()
    wall_texture.SetTextureFilename(chrono.GetChronoDataPath() + 'aluminum.jpg')
    wall.GetAssets().push_back(wall_texture)
    my_system.Add(wall)

# In[cylinder dimmensions/geometries]

# number of robots
nb=8

# number of interior robots
n=np.array([])

# sum them for total number of interior
ni=np.sum(n)

# total number of bots and particles
nt=nb+ni

# diameter of cylinder and robots
diameter=.08 

# Radius of postions
R1=(diameter*nb/(np.pi*2))+.055

# mass
mr=.18

# mass of particles
mp=.04

# height of cylinder
height=0.12

# half height of cylinder
hhalf=.06

# calculate volume
volume=np.pi*.25*height*(diameter)**2

# calculate density of robot
rowr=mr/volume

# calculate density of particles
rowp=mp/volume

# spring constant
k=100

# damping
b=.1

# resting length
rl=.05

# max resting length
rlmax=.051

# empty matrix of bots and particles
obj=[]

# empty matrix of springs
Springs=[]

F=.4
# In[External forces]
constfun = chrono.ChFunction_Const(F)
constfun0 = chrono.ChFunction_Const(0) 


  
# Empty matrix      
botcall=np.zeros((1,nb))

# ID number of robots to be active
active=np.array([3])

# For robots that are active fill botcall==1
for i in (active):
    botcall[:,i]=1

# In[Create Bots]
for i in range (nb):
    
    # Initial postion of each bot
    theta=i*2*np.pi/nb
    x=R1*np.cos(theta)
    y=.5*height
    z=R1*np.sin(theta)
    
    # Create bots    
    bot = chrono.ChBody()
    bot = chrono.ChBodyEasyCylinder(diameter/2, height,rowr)
    bot.SetPos(chrono.ChVectorD(x,y,z))
    bot.SetMaterialSurface(material)
    
    # rotate them
#    rotation1 = chrono.ChQuaternionD()
#    rotation1.Q_from_AngAxis(-theta, chrono.ChVectorD(0, 1, 0));  
#    bot.SetRot(rotation1)
#    
    # give ID number
    bot.SetId(i)
    
    # collision model
    bot.GetCollisionModel().ClearModel()
    bot.GetCollisionModel().AddCylinder(diameter/2,diameter/2,hhalf) # hemi sizes
    bot.GetCollisionModel().BuildModel()
    bot.SetCollide(True)
    bot.SetBodyFixed(False)
    
    # Apply forces to active bots
    if botcall[:,i]==1:
        myforcex = chrono.ChForce()
        bot.AddForce(myforcex)
        myforcex.SetMode(chrono.ChForce.FORCE)
        myforcex.SetF_x(constfun)
        myforcex.SetVrelpoint(chrono.ChVectorD(x,-.05*y,.01*z))
        myforcex.SetDir(chrono.ChVectorD(1,0,0))
        
        
        myforcex2 = chrono.ChForce()
        bot.AddForce(myforcex2)
        myforcex2.SetMode(chrono.ChForce.FORCE)
        myforcex2.SetF_x(constfun)
        myforcex2.SetVrelpoint(chrono.ChVectorD(x,-.05*y,-.01*z))

        
        myforcex2.SetDir(chrono.ChVectorD(1,0,0))
        col_y = chrono.ChColorAsset()
        col_y.SetColor(chrono.ChColor(0.44, .11, 52))
        bot.AddAsset(col_y)
    
#    # passive robots
#    else:
#        myforcex = chrono.ChForce()
#        bot.AddForce(myforcex)
#        myforcex.SetMode(chrono.ChForce.FORCE)
#        myforcex.SetF_x(constfun0)
#        myforcex.SetVrelpoint(chrono.ChVectorD(x,-.03*y,z))
#        myforcex.SetDir(chrono.ChVectorD(1,0,0))
#        col_g = chrono.ChColorAsset()
#        col_g.SetColor(chrono.ChColor(0, 1, 0))
#        bot.AddAsset(col_g)
        
#    # Attatch springs    
#    if i>=1:
#        ground=chrono.ChLinkSpring()
#        ground.SetName("ground")
#        p1=0
#        p2=diameter/2
#        p3=0
#        p4=-diameter/2
#
#        ground.Initialize(obj[i-1], bot,True,chrono.ChVectorD(p1,0,p2), chrono.ChVectorD(p3,0,p4),False)
##        ground.Set_SpringK(k)
##        ground.Set_SpringR(b)
#        ground.Set_springF(1)
#        ground.Set_SpringRestLength(rl)
#        col1=chrono.ChColorAsset()
#        col1.SetColor(chrono.ChColor(0,0,1))
#        ground.AddAsset(col1)
#        ground.AddAsset(chrono.ChPointPointSpring(.01,80,15))
#        my_system.AddLink(ground)
#        Springs.append(ground)
#    # Last spring
#        if i==nb-1:        
#            ground=chrono.ChLinkSpring()
#            ground.SetName("ground")
#            ground.Initialize(bot, obj[0], True, chrono.ChVectorD(p1,0,p2), chrono.ChVectorD(p3,0,p4),False)
##            ground.Set_SpringK(k)
##            ground.Set_SpringR(b)
#            ground.Set_SpringF(1)
#            ground.Set_SpringRestLength(rl)
#            col1=chrono.ChColorAsset()
#            col1.SetColor(chrono.ChColor(0,0,1))
#            ground.AddAsset(col1)
#        ground.AddAsset(chrono.ChPointPointSpring(.01,80,15))
#        my_system.AddLink(ground)
#        Springs.append(ground) 
    my_system.Add(bot)
    obj.append(bot)        


# In[Create interiors]
for i in range(n.size):
    print(i)
    for j in range(n[i]):
        R2=diameter*n[i]/(np.pi*2)
        x=R2*np.cos(j*2*np.pi/n[i])
        y=.6*height
        z=R2*np.sin(j*2*np.pi/n[i])
        gran = chrono.ChBody()
        gran = chrono.ChBodyEasyCylinder(diameter/2, height,rowp)
        gran.SetPos(chrono.ChVectorD(x,y,z))
        gran.SetMaterialSurface(material)
        gran.SetId(i)
        gran.GetCollisionModel().ClearModel()
        gran.GetCollisionModel().AddCylinder(diameter/2,diameter/2,height/2) # hemi sizes
        gran.GetCollisionModel().BuildModel()
        gran.SetCollide(True)
        col_r = chrono.ChColorAsset()
        col_r.SetColor(chrono.ChColor(1, 0, 0))
        gran.AddAsset(col_r)
        my_system.Add(gran)
        obj.append(gran) 


        

h=.008


        

        

        
        


# In[IRRLICHT code]

myapplication = chronoirr.ChIrrApp(my_system, 'PyChrono example', chronoirr.dimension2du(1024,768))

myapplication.AddTypicalSky()
myapplication.AddTypicalLogo(chrono.GetChronoDataPath() + 'logo_pychrono_alpha.png')
myapplication.AddTypicalCamera(chronoirr.vector3df(0.75,0.75,1.5))
myapplication.AddLightWithShadow(chronoirr.vector3df(2,4,2),chronoirr.vector3df(0,0,0),9,1,9,50)                

            # ==IMPORTANT!== Use this function for adding a ChIrrNodeAsset to all items
                        # in the system. These ChIrrNodeAsset assets are 'proxies' to the Irrlicht meshes.
                        # If you need a finer control on which item really needs a visualization proxy in
                        # Irrlicht, just use application.AssetBind(myitem); on a per-item basis.

myapplication.AssetBindAll();

                        # ==IMPORTANT!== Use this function for 'converting' into Irrlicht meshes the assets
                        # that you added to the bodies into 3D shapes, they can be visualized by Irrlicht!
myapplication.AssetUpdateAll();

            # If you want to show shadows because you used "AddLightWithShadow()'
            # you must remember this:
myapplication.AddShadowAll();

# In[ Run the simulation]

count=0

myapplication.SetTimestep(h)
myapplication.SetTryRealtime(True)



while(myapplication.GetDevice().run()):
    myapplication.BeginScene()
    myapplication.DrawAll()
    myapplication.DoStep()
    myapplication.EndScene()



