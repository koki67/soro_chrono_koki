# -*- coding: utf-8 -*-
"""
Created on Tue Feb  4 21:07:21 2020

@author: dmulr
"""

import pychrono.core as chrono
import pychrono.irrlicht as chronoirr
import pychrono.postprocess as postprocess
import os
import numpy as np
import timeit
from objects import *
from myconfig_torus import *
from scipy.optimize import LinearConstraint, Bounds, minimize, linprog

start = timeit.default_timer()



# In[Set Path]
chrono.SetChronoDataPath("C:/Users/dmulr/OneDrive/Documents/data/")

# In[Create sysem and other misselanous things]
my_system = chrono.ChSystemNSC()
my_system.SetSolverType(chrono.ChSolver.Type_SOR_MULTITHREAD)
#my_system.SetTol(1e-6)
#my_system.SetSolverType(chrono.ChSolver.Type_APGD)
my_system.Set_G_acc(chrono.ChVectorD(0,-9.81, 0))
my_system.SetMaxItersSolverSpeed(300)
material = Material(mu_f, mu_b, mu_r, mu_s, C_, Ct, Cr, Cs)
body_floor = Floor(material, length, tall)
my_system.Add(body_floor)


x1=0
y1=height/2
z1=0

x2=.5
y2=height/2
z2=.5

# robot side 1
bot1 = chrono.ChBody()
bot1 = chrono.ChBodyEasyCylinder(diameter/2, height,rowr) # create square robot
bot1.SetPos(chrono.ChVectorD(x1,y1,z1))     # set position 
bot1.SetMaterialSurface(material)            #apply surface material 
bot1.GetCollisionModel().ClearModel()
bot1.GetCollisionModel().AddCylinder(diameter/2,diameter/2,height/2) # hemi sizes
bot1.GetCollisionModel().BuildModel()
bot1.SetCollide(True)
bot1.SetBodyFixed(False) # decides if the object will be fixed. 
pt = chrono.ChLinkMatePlane()
pt.Initialize(body_floor, bot1, False, chrono.ChVectorD(0, 0, 0), chrono.ChVectorD(0, 0, 0),chrono.ChVectorD(0, 1, 0), chrono.ChVectorD(0, -1, 0))
my_system.AddLink(pt)
my_system.Add(bot1)

# robot side 1
bot2 = chrono.ChBody()
bot2 = chrono.ChBodyEasyCylinder(diameter/2, height,rowr) # create square robot
bot2.SetPos(chrono.ChVectorD(x2,y2,z2))     # set position 
bot2.SetMaterialSurface(material)            #apply surface material 
bot2.GetCollisionModel().ClearModel()
bot2.GetCollisionModel().AddCylinder(diameter/2,diameter/2,height/2) # hemi sizes
bot2.GetCollisionModel().BuildModel()
bot2.SetCollide(True)
bot2.SetBodyFixed(False) # decides if the object will be fixed. 
pt = chrono.ChLinkMatePlane()
pt.Initialize(body_floor, bot2, False, chrono.ChVectorD(0, 0, 0), chrono.ChVectorD(0, 0, 0),chrono.ChVectorD(0, 1, 0), chrono.ChVectorD(0, -1, 0))
my_system.AddLink(pt)
my_system.Add(bot2)



dist_crank_slider = chrono.ChLinkDistance()
dist_crank_slider.SetName("dist_crank_slider")
dist_crank_slider.Initialize(bot1,bot2, False, chrono.ChVectorD(x1, 0, z1), chrono.ChVectorD(z2, 0, z2))
my_system.AddLink(dist_crank_slider)


#wall=chrono.ChBody()
#wall = chrono.ChBodyEasyCylinder(diameter/4, height,rowr) # create square robot
#wall.SetPos(chrono.ChVectorD(x,y,z+height))     # set position 
#wall.SetMaterialSurface(material)            #apply surface material 
#wall.GetCollisionModel().ClearModel()
#wall.GetCollisionModel().AddCylinder(diameter/4,diameter/4,height/2) # hemi sizes
#wall.GetCollisionModel().BuildModel()
#wall.SetCollide(True)
#wall.SetBodyFixed(False) # decides if the object will be fixed. 
##rotation1 = chrono.ChQuaternionD()
##rotation1.Q_from_AngAxis(-np.pi/2, chrono.ChVectorD(1, 0, 0));  
##wall.SetRot(rotation1)
#my_system.Add(wall)
#
#z2y = chrono.ChQuaternionD()
#z2x = chrono.ChQuaternionD()
#z2y.Q_from_AngAxis(-chrono.CH_C_PI / 2, chrono.ChVectorD(1, 0, 0))
#z2x.Q_from_AngAxis(chrono.CH_C_PI / 2, chrono.ChVectorD(0, 1, 0))
#spherical_crank_rod = chrono.ChLinkLockSpherical()
#spherical_crank_rod.SetName("spherical_crank_rod")
#spherical_crank_rod.Initialize(bot1, wall, chrono.ChCoordsysD(chrono.ChVectorD(1, 0, 0), chrono.QUNIT))
#my_system.AddLink(spherical_crank_rod)
#
#prismatic_ground_slider = chrono.ChLinkLockPrismatic()
#prismatic_ground_slider.SetName("prismatic_ground_slider")
#prismatic_ground_slider.Initialize(bot1, wall, chrono.ChCoordsysD(chrono.ChVectorD(2, 0, 0), z2x))
#my_system.AddLink(prismatic_ground_slider)



myapplication = chronoirr.ChIrrApp(my_system,sim, chronoirr.dimension2du(1600,1200))
myapplication.AddTypicalSky()
myapplication.AddTypicalLogo(chrono.GetChronoDataPath() + 'logo_pychrono_alpha.png')
myapplication.AddTypicalCamera(chronoirr.vector3df(0.75,0.75,1.5))
myapplication.AddLightWithShadow(chronoirr.vector3df(2,5,2),chronoirr.vector3df(2,2,2),10,2,10,120)
myapplication.DrawAll               
myapplication.AssetBindAll();
myapplication.AssetUpdateAll();
myapplication.AddShadowAll();
    
count=0
# Time step

myapplication.SetTimestep(tstep)
myapplication.SetTryRealtime(False)
while(myapplication.GetDevice().run()):

    myapplication.BeginScene()
    myapplication.DrawAll()
    #print ('time=', my_system.GetChTime())
    t=tstep*count
    # run step
    myapplication.DoStep()
    myapplication.EndScene()
    count=count+1
# Close the simulation if time ends
    if t > tend:
        myapplication.GetDevice().closeDevice()

# In[Print time out]
#runtime=stop-start
#runtime=runtime*(1/60)
#print("Total runtime: "+str(runtime)+" minutes")



    
    
