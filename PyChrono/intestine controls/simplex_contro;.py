# -*- coding: utf-8 -*-
"""
Created on Tue Jan 28 10:01:39 2020

@author: dmulr
"""
elif self.opt == "simplex":
    res = linprog(sin(self.t) ** 2 - cos(self.t) ** 2, method='simplex', bounds=[(0, 1)] * 12)
    for (i, ids) in enumerate(self.leg_ids):
        if self.at_least_one_stopped:
            if (90 + self.t_a) < theta[ids] < (270 - self.t_a):
                result[i] = -1
            elif 0 <= theta[ids] < (90 - self.t_a) or (270 + self.t_a) < theta[ids] <= 360:
                result[i] = 1
            else:
                result[i] = 0
        else:
            result[i] = -res.x[i] * (abs(self.t[i]) > pi / 2) + res.x[i] * (abs(self.t[i]) < pi / 2)
