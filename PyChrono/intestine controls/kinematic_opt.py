# -*- coding: utf-8 -*-
"""
Created on Wed Feb 26 17:09:08 2020

@author: Amin
"""

import numpy as np
from scipy.optimize import minimize
from scipy.optimize import Bounds
from numpy import pi
from scipy.optimize import LinearConstraint

n = 12
l = 10
x0 = np.arange(0,2*pi,2*pi/n)
bounds = Bounds(np.zeros(n), np.ones(n))
A = np.array([np.cos(x0), np.sin(x0)])
b = np.average(A, 1)
theta_d = np.array([0, 0, 0, 0, 0, 0, pi, pi, pi, pi, pi, pi])
linear_constraint = LinearConstraint(A, b, b)

def cost(x):
    """The Cost function"""
    x = np.array(x)
    return (np.linalg.norm((x[1:-2]-((x[2:-1]+x[0:-3])/2))/l-(theta_d[1:-2]-x0[1:-2])) +  abs((x[-1]-(x[-2]+x[0])/2)/l-(theta_d[-1]-x0[-1])) + abs((x[0]-(x[1]+x[-1])/2)/l-(theta_d[0]-x0[0])))


res = minimize(cost, x0, method='trust-constr', constraints=[linear_constraint], options={'verbose': 1}, bounds=bounds)
print(res.x)
