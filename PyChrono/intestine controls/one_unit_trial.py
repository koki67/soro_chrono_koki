# -*- coding: utf-8 -*-
"""
Created on Mon Jan 20 18:15:07 2020

@author: dmulr
"""

import pychrono.core as chrono
import pychrono.irrlicht as chronoirr
import pychrono.postprocess as postprocess
import os
import numpy as np
import timeit
from objects import Material,Floor
from myconfig6 import *
from scipy import signal


start = timeit.default_timer()

# In[Set Path]
chrono.SetChronoDataPath("C:/Users/dmulr/OneDrive/Documents/data/")

# In[Create sysem and other misselanous things]
my_system = chrono.ChSystemNSC()
my_system.SetSolverType(chrono.ChSolver.Type_SOR_MULTITHREAD)
#my_system.SetTol(1e-6)
#my_system.SetSolverType(chrono.ChSolver.Type_APGD)
my_system.Set_G_acc(chrono.ChVectorD(0,-9.81, 0))
my_system.SetMaxItersSolverSpeed(100)

material=Material(mu_f,mu_b,mu_r,mu_s,C,Ct,Cr,Cs)
body_floor=Floor(material,length,tall)
my_system.Add(body_floor)


x=0
y=height/2
z=0
width=.01 # width of wheel
diameter2=diameter*.5 # diameter of wheel
pen=.001
position=chrono.ChVectorD(x,y+pen,z)

center=chrono.ChVectorD(x,(diameter2/2)-.002,z)
rotation=chrono.ChVectorD(x-.02,diameter2/2,z)
col_p = chrono.ChColorAsset()
col_p.SetColor(chrono.ChColor(0.44, .11, 52))
log=False

# robot side 1
bot1 = chrono.ChBody()
bot1 = chrono.ChBodyEasyCylinder(diameter/2, height,rowr) # create square robot
bot1.SetPos(chrono.ChVectorD(x,y,z-width))     # set position 
bot1.SetMaterialSurface(material)            #apply surface material 
bot1.GetCollisionModel().ClearModel()
bot1.GetCollisionModel().AddCylinder(diameter/2,diameter/2,height/2) # hemi sizes
bot1.GetCollisionModel().BuildModel()
bot1.SetCollide(True)
bot1.SetBodyFixed(False) # decides if the object will be fixed. 




myapplication = chronoirr.ChIrrApp(my_system,sim, chronoirr.dimension2du(1600,1200))
myapplication.AddTypicalSky()
myapplication.AddTypicalLogo(chrono.GetChronoDataPath() + 'logo_pychrono_alpha.png')
myapplication.AddTypicalCamera(chronoirr.vector3df(0.75,0.75,1.5))
myapplication.AddLightWithShadow(chronoirr.vector3df(2,5,2),chronoirr.vector3df(2,2,2),10,2,10,120)
myapplication.DrawAll               
myapplication.AssetBindAll();
myapplication.AssetUpdateAll();
myapplication.AddShadowAll();
    
count=0
# Time step
myapplication.SetTimestep(tstep)
myapplication.SetTryRealtime(False)
while(myapplication.GetDevice().run()):

    myapplication.BeginScene()
    myapplication.DrawAll()
    print ('time=', my_system.GetChTime())
    t=tstep*count  
 
        
    # run step
    myapplication.DoStep()
    myapplication.EndScene()
# Close the simulation if time ends
    if t > tend:
        myapplication.GetDevice().closeDevice()

#myforcex = chrono.ChForce()
#bot1.AddForce(myforcex)
#myforcex.SetMode(chrono.ChForce.FORCE)
#myforcex.SetDir(chrono.ChVectorD(.1,1,z))
#myforcex.SetVrelpoint(chrono.ChVectorD(x,-height/2,z))
## wheel
#wheel=chrono.ChBody()
#wheel=chrono.ChBodyEasyCylinder(diameter2/2,width,rowr)
#wheel.SetPos(center)
#wheel.GetCollisionModel().ClearModel()
#wheel.GetCollisionModel().AddCylinder(diameter2/2,diameter2/2,width) # hemi sizes
#wheel.GetCollisionModel().BuildModel()
#wheel.SetCollide(True)
#wheel.SetBodyFixed(log)
#wheel.SetRot(chrono.Q_ROTATE_Y_TO_Z)
#torque = chrono.ChForce()
#wheel.AddForce(torque)
#torque.SetMode(chrono.ChForce.TORQUE)
#torque.SetMforce(0)
#torque.SetDir(chrono.VECT_Z)
#my_system.Add(wheel)
#wheel.AddAsset(col_p)
#body_floor_texture = chrono.ChTexture()
#body_floor_texture.SetTextureFilename(chrono.GetChronoDataPath() + 'bluwhite.png')
#wheel.GetAssets().push_back(body_floor_texture)
#
## robot side 1
#bot1 = chrono.ChBody()
#bot1 = chrono.ChBodyEasyBox(diameter/2,height,width/2,rowr)   # create square robot
#bot1.SetPos(chrono.ChVectorD(x,y,z-width))     # set position 
#bot1.SetMaterialSurface(material)            #apply surface material 
#bot1.GetCollisionModel().ClearModel()
#bot1.GetCollisionModel().AddBox(diameter/2,height/2,width/2) # hemi sizes
#bot1.GetCollisionModel().BuildModel()
#bot1.SetCollide(True)
#bot1.SetBodyFixed(True) # decides if the object will be fixed. 
##torque = chrono.ChForce()
##bot1.AddForce(torque)
##torque.SetMode(chrono.ChForce.TORQUE)
##torque.SetVrelpoint(chrono.ChVectorD(0,-height/2,0))
##torque.SetMforce(.1)
##torque.SetDir(chrono.VECT_Y)
#my_system.Add(bot1)
#
#
## robot side 2
#bot2 = chrono.ChBody()
#bot2 = chrono.ChBodyEasyBox(diameter/2,height,width,rowr)   # create square robot
#bot2.SetPos(chrono.ChVectorD(x,y,z+width))     # set position 
#bot2.SetMaterialSurface(material)            #apply surface material 
#bot2.GetCollisionModel().ClearModel()
#bot2.GetCollisionModel().AddBox(diameter/2,height/2,width/2) # hemi sizes
#bot2.GetCollisionModel().BuildModel()
#bot2.SetCollide(True)
#bot2.SetBodyFixed(True) # decides if the object will be fixed. 
#my_system.Add(bot2)
#
## robot side 3
#bot3 = chrono.ChBody()
#bot3 = chrono.ChBodyEasyBox(diameter/2,3*width,width,rowr)   # create square robot
#rotation1 = chrono.ChQuaternionD()
#rotation1.Q_from_AngAxis(-np.pi/2, chrono.ChVectorD(1, 0, 0));  
#bot3.SetRot(rotation1)
#bot3.SetPos(chrono.ChVectorD(x,2*y+width/2,z))     # set position 
#bot3.SetMaterialSurface(material)            #apply surface material 
#bot3.GetCollisionModel().ClearModel()
#bot3.GetCollisionModel().AddBox(diameter/2,height/2,width/2) # hemi sizes
#bot3.GetCollisionModel().BuildModel()
#bot3.SetCollide(True)
#bot3.SetBodyFixed(log) # decides if the object will be fixed. 
#my_system.Add(bot3)
#
##my_motor = chrono.ChLinkMotorRotationSpeed()
##my_motor.Initialize(bot1,   # the first connected body
##                    wheel,   # the second connected body
##                    chrono.ChFrameD(rotation)) # where to create the motor in abs.space
##my_angularspeed = chrono.ChFunction_Const(.02) # ang.speed: 180°/s
##my_motor.SetMotorFunction(my_angularspeed)
##my_system.Add(my_motor)
#
## joint a
#mjointA = chrono.ChLinkLockRevolute()
#mjointA.Initialize(bot1,
#                   wheel, 
#                   chrono.ChCoordsysD(center))
#my_system.Add(mjointA)
#
## joint b
#mjointB = chrono.ChLinkLockRevolute()
#mjointB.Initialize(bot2,
#                   wheel, 
#                   chrono.ChCoordsysD(center))
#my_system.Add(mjointB)
#
#myjointC = chrono.ChLinkMateFix()
#myjointC.Initialize(bot3, bot2)
#my_system.Add(myjointC)

# Create piston-truss joint

#width=.05
#diameter2=diameter/2
#x=0
#y=height/2
#z=0
#pen=.001
#y2=y-diameter2
#position=chrono.ChVectorD(x,y+pen,z)
#center=chrono.ChVectorD(x,diameter2/2,z)
#col_p = chrono.ChColorAsset()
#col_p.SetColor(chrono.ChColor(0.44, .11, 52))
## robot
#bot=chrono.ChBody()
#bot.SetPos(chrono.ChVectorD(x-width, y, z))
#bot.SetBodyFixed(False)
#my_system.Add(bot)
#
#box1 = chrono.ChBoxShape()
#box1.GetBoxGeometry().Size = chrono.ChVectorD(diameter,height, diameter)
#bot.AddAsset(box1)

#box1.SetCollide(True)
#box1.GetCollisionModel().ClearModel()
#box1.GetCollisionModel().AddBox(diameter/2,height/2,diameter/2) # hemi sizes
#box1.GetCollisionModel().BuildModel()







#bot = chrono.ChBody()
#bot = chrono.ChBodyEasyCylinder(diameter/2, height,rowr)
#bot.SetPos(position)
#bot.SetMaterialSurface(material)
#bot.SetCollide(True)
#bot.SetBodyFixed(True)
#bot.GetCollisionModel().ClearModel()
#bot.GetCollisionModel().AddCylinder(diameter/2,diameter/2,height/2) # hemi sizes
#bot.GetCollisionModel().BuildModel()
#my_system.Add(bot)
## wheel
#wheel=chrono.ChBody()
#wheel=chrono.ChBodyEasyCylinder(diameter2/2,.01,rowr)
#wheel.SetPos(center)
#wheel.SetCollide(True)
#wheel.SetBodyFixed(True)
#wheel.SetRot(chrono.Q_ROTATE_Y_TO_Z)
#my_system.Add(wheel)
#wheel.AddAsset(col_p)
#body_floor_texture = chrono.ChTexture()
#body_floor_texture.SetTextureFilename(chrono.GetChronoDataPath() + 'bluwhite.png')
#wheel.GetAssets().push_back(body_floor_texture)
#
#mjointB = chrono.ChLinkLockRevolute()
#mjointB.Initialize(wheel,
#                   bot, 
#                   chrono.ChCoordsysD(center))
#my_system.Add(mjointB)


#my_motor = chrono.ChLinkMotorRotationSpeed()
#my_motor.Initialize(wheel,   # the first connected body
#                    bot,   # the second connected body
#                    chrono.ChFrameD(center)) # where to create the motor in abs.space
#my_angularspeed = chrono.ChFunction_Const(chrono.CH_C_PI*100) # ang.speed: 180°/s
#my_motor.SetMotorFunction(my_angularspeed)
#my_system.Add(my_motor)


#mfunZ = chrono.ChFunction_Sine(0,60,.1)# phase, frequency, amplitude
#myforce=chrono.ChForce()
#bot.AddForce(myforce)
#myforce.SetMode(chrono.ChForce.FORCE)
#myforce.SetModulation(mfunZ)
#myforce.SetDir(chrono.VECT_Y)
#myforce.SetVrelpoint(chrono.ChVectorD(x,y/2,z-.02))
#
#myforce2=chrono.ChForce()
#bot.AddForce(myforce2)
#myforce2.SetMode(chrono.ChForce.FORCE)
#myforce2.SetModulation(mfunZ)
#myforce2.SetDir(chrono.VECT_Y)
#myforce2.SetVrelpoint(chrono.ChVectorD(x,y/2,z+.02))

#myforce3=chrono.ChForce()
#bot.AddForce(myforce3)
#myforce3.SetMode(chrono.ChForce.FORCE)
#myforce3.SetMforce(.01)
#myforce3.SetDir(chrono.VECT_X)
#myforce3.SetVrelpoint(chrono.ChVectorD(x,.03*y,z))

#my_system.Add(bot)
#obj.append(bot)




