# In[Header]

"""
author: declan mulroy
project: JAMoEBA
email: dmulroy@hawk.iit.edu
date: 10/9/19
"""

# In[import libraries]
import pychrono.core as chrono
import pychrono.irrlicht as chronoirr
import pychrono.postprocess as postprocess
import os
import numpy as np
import timeit


start = timeit.default_timer()

record=1
# In[Set Path]
chrono.SetChronoDataPath("data/")


# In[Create sysem and other misselanous things]
my_system = chrono.ChSystemNSC()
my_system.Set_G_acc(chrono.ChVectorD(0, -9.81, 0))
chrono.ChCollisionModel.SetDefaultSuggestedEnvelope(0.00001)
chrono.ChCollisionModel.SetDefaultSuggestedMargin(0.00001)
my_system.SetMaxItersSolverSpeed(1000)
my_system.SetMinBounceSpeed(.2)

# In[Define frictional properties]
mu_f=.4
mu_b=.01
mu_r=.4
mu_s=.4

# In[create material properties]
material = chrono.ChMaterialSurfaceNSC()
material.SetFriction(mu_f)
material.SetDampingF(mu_b)
material.SetCompliance (0.00001)
material.SetComplianceT(0.00001)
material.SetRollingFriction(mu_r)
material.SetSpinningFriction(mu_s)
material.SetComplianceRolling(0.00001)
material.SetComplianceSpinning(0.0001)
# In[Create floor]
body_floor = chrono.ChBody()
body_floor.SetBodyFixed(True)
body_floor.SetPos(chrono.ChVectorD(0, -1, 0 ))
body_floor.SetMaterialSurface(material)

# In[Collision shape]
body_floor.GetCollisionModel().ClearModel()
body_floor.GetCollisionModel().AddBox(3, 1, 3) # hemi sizes
body_floor.GetCollisionModel().BuildModel()
body_floor.SetCollide(True)

# In[Visualization shape]
body_floor_shape = chrono.ChBoxShape()
body_floor_shape.GetBoxGeometry().Size = chrono.ChVectorD(3, 1, 3)
body_floor.GetAssets().push_back(body_floor_shape)
body_floor_texture = chrono.ChTexture()
body_floor_texture.SetTextureFilename(chrono.GetChronoDataPath() + 'aluminum.jpg')
body_floor.GetAssets().push_back(body_floor_texture)
my_system.Add(body_floor)

# In[Create wall to enclose the robot]

# Geometry of walls
wwidth=.1
wheight=.1
wlength=1.5

meh=np.array([1,-1])

for i in(meh):
    # Create walls 
    wall = chrono.ChBody()
    wall.SetBodyFixed(True)
    wall.SetPos(chrono.ChVectorD(1.5*i, wheight/2, 0))
    wall.SetMaterialSurface(material)
    # Collision shape
    wall.GetCollisionModel().ClearModel()
    wall.GetCollisionModel().AddBox(wwidth, wheight, wlength) # hemi sizes
    wall.GetCollisionModel().BuildModel()
    wall.SetCollide(True)
    # Visualization shape
    wall_shape = chrono.ChBoxShape()
    wall_shape.GetBoxGeometry().Size = chrono.ChVectorD(wwidth, wheight, wlength)
    wall_shape.SetColor(chrono.ChColor(0.4,0.4,0.5))
    wall.GetAssets().push_back(wall_shape)
    wall_texture = chrono.ChTexture()
    wall_texture.SetTextureFilename(chrono.GetChronoDataPath() + 'aluminum.jpg')
    wall.GetAssets().push_back(wall_texture)
    my_system.Add(wall)

for i in(meh):
    # Create walls
    wall = chrono.ChBody()
    wall.SetBodyFixed(True)
    wall.SetPos(chrono.ChVectorD(0, wheight/2, 1.5*i))
    wall.SetMaterialSurface(material)
    # Collision shape
    wall.GetCollisionModel().ClearModel()
    wall.GetCollisionModel().AddBox(wlength, wheight, wwidth) # hemi sizes
    wall.GetCollisionModel().BuildModel()
    wall.SetCollide(True)
    # Visualization shape
    wall_shape = chrono.ChBoxShape()
    wall_shape.GetBoxGeometry().Size = chrono.ChVectorD(wlength,wheight, wwidth)
    wall_shape.SetColor(chrono.ChColor(0.4,0.4,0.5))
    wall.GetAssets().push_back(wall_shape)
    wall_texture = chrono.ChTexture()
    wall_texture.SetTextureFilename(chrono.GetChronoDataPath() + 'aluminum.jpg')
    wall.GetAssets().push_back(wall_texture)
    my_system.Add(wall)








# In[Load resume file]
#np.savez('resume1str.npz',allow_pickle=True,Fxc=Fxc,Fyc=Fyc,Fzc=Fzc,Fxt=Fxt,Fyt=Fyt,Fzt=Fzt,qx=qx,qy=qy,qz=qz,nb=nb,ni=ni,mr=mr,mp=mp,k=k,rowr=rowr,rowp=rowp,height=height,diameter=diameter,volume=volume,ttemp=ttemp,count=count,rot0=rot0,rot1=rot1,rot2=rot2,rot3=rot3,botcall=botcall,SL=SL)    
    
    
data=np.load('resume0.npz')

nb=data['nb']

ni=data['ni']

qx=data['qx']

qy=data['qy']

qz=data['qz']

rot0=data['rot0']

rot1=data['rot1']

rot2=data['rot2']

rot3=data['rot3']

height=data['height']

mr=data['mr']

mp=data['mp']

volume=data['volume']

count=data['count']

diameter=data['diameter']

rowr=data['rowr']

rowp=data['rowp']

sim=data['sim']

sim=sim+1

count=np.sum(count)

diameter=np.sum(diameter)

height=np.sum(height)

rowr=np.sum(rowr)

rowp=np.sum(rowp)

mr=np.sum(mr)

mp=np.sum(mp)



ni=np.sum(ni)

nb=np.sum(nb)

nt=nb+ni

k=100

# damping
b=.05

# resting length
rl=0.03

# max resting length
rlmax=0.031

# empty matrix
obj=[]

# 
Springs=[]


# In[External forces]
constfun = chrono.ChFunction_Const(1.3)
constfun0 = chrono.ChFunction_Const(0) 
 
# Empty matrix      
botcall=np.zeros((1,nb))

# ID number of robots to be active
active=np.array([0])

# For robots that are active fill botcall==1
for i in (active):
    
    botcall[:,i]=1



# In[Create Bots]
for i in range (nb):
    print(i)
    x=qx[i,count-1]
    y=qy[i,count-1]
    z=qz[i,count-1]
    bot = chrono.ChBody()
    bot = chrono.ChBodyEasyCylinder(diameter/2, height,rowr)
    bot.SetPos(chrono.ChVectorD(x,y,z))
    bot.SetRot(chrono.ChQuaternionD(rot0[i,count-1], rot1[i,count-1], rot2[i,count-1], rot3[i,count-1]))
    bot.SetMaterialSurface(material)
    bot.SetMass(mr)
    bot.SetId(i)
    bot.GetCollisionModel().ClearModel()
    bot.GetCollisionModel().AddCylinder(diameter/2,diameter/2,height/2) # hemi sizes
    bot.GetCollisionModel().BuildModel()
    bot.SetCollide(True)
    bot.SetBodyFixed(False)
    # Apply forces to active bots
    if botcall[:,i]==1:
        myforcex = chrono.ChForce()
        bot.AddForce(myforcex)
        myforcex.SetMode(chrono.ChForce.FORCE)
        myforcex.SetF_x(constfun)
        myforcex.SetVrelpoint(chrono.ChVectorD(x,y-.06,z))
        myforcex.SetRelDir(chrono.ChVectorD(1,0,0))
        col_y = chrono.ChColorAsset()
        col_y.SetColor(chrono.ChColor(0.44, .11, 52))
        bot.AddAsset(col_y)
    
    # passive robots
    else:
        myforcex = chrono.ChForce()
        bot.AddForce(myforcex)
        myforcex.SetMode(chrono.ChForce.FORCE)
        myforcex.SetF_x(constfun0)
        myforcex.SetVrelpoint(chrono.ChVectorD(x,y,z))
        myforcex.SetRelDir(chrono.ChVectorD(1,0,0))
        col_g = chrono.ChColorAsset()
        col_g.SetColor(chrono.ChColor(0, 1, 0))
        bot.AddAsset(col_g)
    
    # Attatch springs    
    if i>=1:
        ground=chrono.ChLinkSpring()
        ground.SetName("ground")
        p1=0
        p2=diameter/2
        p3=0
        p4=-diameter/2

        ground.Initialize(obj[i-1], bot,True,chrono.ChVectorD(p1,0,p2), chrono.ChVectorD(p3,0,p4),False)
        ground.Set_SpringK(k)
        ground.Set_SpringR(b)
        ground.Set_SpringRestLength(rl)
        col1=chrono.ChColorAsset()
        col1.SetColor(chrono.ChColor(0,0,1))
        ground.AddAsset(col1)
        ground.AddAsset(chrono.ChPointPointSpring(.01,80,15))
        my_system.AddLink(ground)
        Springs.append(ground)
    # Last spring
        if i==nb-1:        
            ground=chrono.ChLinkSpring()
            ground.SetName("ground")
            ground.Initialize(bot, obj[0], True, chrono.ChVectorD(p1,0,p2), chrono.ChVectorD(p3,0,p4),False)
            ground.Set_SpringK(k)
            ground.Set_SpringR(b)
            ground.Set_SpringRestLength(rl)
            col1=chrono.ChColorAsset()
            col1.SetColor(chrono.ChColor(0,0,1))
            ground.AddAsset(col1)
        ground.AddAsset(chrono.ChPointPointSpring(.01,80,15))
        my_system.AddLink(ground)
        Springs.append(ground) 
    my_system.Add(bot)
    obj.append(bot)            
       


# In[Create interiors]
for i in range(nb,nb+ni):
    x=qx[i,count-1]
    y=qy[i,count-1]
    z=qz[i,count-1]
    gran = chrono.ChBody()
    gran = chrono.ChBodyEasyCylinder(diameter/2, height,rowp)
    gran.SetPos(chrono.ChVectorD(x,y,z))
    gran.SetRot(chrono.ChQuaternionD(rot0[i,count-1], rot1[i,count-1], rot2[i,count-1], rot3[i,count-1]))
    gran.SetMaterialSurface(material)
    gran.SetMass(mp)
    gran.SetId(i)
    gran.GetCollisionModel().ClearModel()
    gran.GetCollisionModel().AddCylinder(diameter/2,diameter/2,height/2) # hemi sizes
    gran.GetCollisionModel().BuildModel()
    gran.SetCollide(True)
    col_r = chrono.ChColorAsset()
    col_r.SetColor(chrono.ChColor(1, 0, 0))
    gran.AddAsset(col_r)
    my_system.Add(gran)
    obj.append(gran) 


# In[ Create empty matrices to be filled]
Xpos=[]
Ypos=[]
Zpos=[]

Xforce=[]
Yforce=[]
Zforce=[]

Xcontact=[]
Ycontact=[]
Zcontact=[]


# empty temporary velocity matrices
Xvel=[]
Yvel=[]
Zvel=[]

templ=[]
ttemp=[]
rott0=[]
rott1=[]
rott2=[]
rott3=[]

count=0
h=.008
# In[Pov RAY]
if record==1:
    
    script_dir = os.path.dirname("povvideofiles"+str(sim)+"/")
    pov_exporter = postprocess.ChPovRay(my_system)

    # Sets some file names for in-out processes.
    pov_exporter.SetTemplateFile("data/_template_POV.pov")
    pov_exporter.SetOutputScriptFile("rendering"+str(sim)+".pov")
    pov_exporter.SetOutputDataFilebase("my_state")
    pov_exporter.SetPictureFilebase("picture")

    # create folders
    if not os.path.exists("output"+str(sim)):
        os.mkdir("output"+str(sim))
    if not os.path.exists("anim"+str(sim)):
            os.mkdir("anim"+str(sim))
    pov_exporter.SetOutputDataFilebase("output"+str(sim)+"/my_state")
    pov_exporter.SetPictureFilebase("anim"+str(sim)+"/picture")

    # Export out objects
    pov_exporter.AddAll() 
    pov_exporter.SetCamera(chrono.ChVectorD(0,1,0), chrono.ChVectorD(0,0,0), 90)# specifiy camera location
    pov_exporter.ExportScript()
    #In[Run the simulation]
    count=0
    while (my_system.GetChTime() < 10) :
    


        for i in range(nb):
            var1=Springs[i].Get_SpringLength()
        
            if var1<rl:
                Springs[i].Set_SpringK(0)
        
            if var1>rlmax:
                Springs[i].Set_SpringK(10*k)
            else:
                Springs[i].Set_SpringK(k)
            t=h*count
    
        for i in range(nt):
            
            # Tempory named variables
            templ.append(var1)
            temp=obj[i].GetContactForce()
            tempx=obj[i].Get_Xforce()
            tempxx=obj[i].GetPos_dt()
            
            # Fill in the temportary  Total force matrices
            Xforce.append(tempx.x)
            Yforce.append((tempx.y))
            Zforce.append(tempx.z)
            
            # Fill in the temporary contact force matrices
            Xcontact.append(temp.x)
            Ycontact.append(temp.y)
            Zcontact.append(temp.z)
            
            # Fill in the temporary position matrices
            Xpos.append(obj[i].GetPos().x)
            Ypos.append((obj[i].GetPos().y))
            Zpos.append(obj[i].GetPos().z)
            
            # fill in the temporary rotational matrices
            rott0.append(obj[i].GetRot().e0)
            rott1.append(obj[i].GetRot().e1)
            rott2.append(obj[i].GetRot().e2)
            rott3.append(obj[i].GetRot().e3)
            
            # fill in the temporary velocity matrices
            Xvel.append(tempxx.x)
            Yvel.append(tempxx.y)
            Zvel.append(tempxx.z)
        
        
        
        ttemp.append(t)
        count=count+1

        my_system.DoStepDynamics(h)
        print ('time=', my_system.GetChTime())
        if count%4==0:
            pov_exporter.ExportData()


# In[IRRLICHT code]
else:

    myapplication = chronoirr.ChIrrApp(my_system, 'PyChrono example', chronoirr.dimension2du(1024,768))

    myapplication.AddTypicalSky()
    myapplication.AddTypicalLogo(chrono.GetChronoDataPath() + 'logo_pychrono_alpha.png')
    myapplication.AddTypicalCamera(chronoirr.vector3df(0.75,0.75,1.5))
    myapplication.AddLightWithShadow(chronoirr.vector3df(2,4,2),chronoirr.vector3df(0,0,0),9,1,9,50)                

            # ==IMPORTANT!== Use this function for adding a ChIrrNodeAsset to all items
                        # in the system. These ChIrrNodeAsset assets are 'proxies' to the Irrlicht meshes.
                        # If you need a finer control on which item really needs a visualization proxy in
                        # Irrlicht, just use application.AssetBind(myitem); on a per-item basis.

    myapplication.AssetBindAll();

                        # ==IMPORTANT!== Use this function for 'converting' into Irrlicht meshes the assets
                        # that you added to the bodies into 3D shapes, they can be visualized by Irrlicht!

    myapplication.AssetUpdateAll();

            # If you want to show shadows because you used "AddLightWithShadow()'
            # you must remember this:
    myapplication.AddShadowAll();

# In[ Run the simulation]

    count=0

# Time step
    h=.001

    myapplication.SetTimestep(h)
    myapplication.SetTryRealtime(True)



    while(myapplication.GetDevice().run()):
        myapplication.BeginScene()
        myapplication.DrawAll()
    
        for i in range(nb):
            var1=Springs[i].Get_SpringLength()
        
            if var1<rl:
                Springs[i].Set_SpringK(0)
        
            if var1>rlmax:
                Springs[i].Set_SpringK(10*k)
            else:
                Springs[i].Set_SpringK(k)
     
            t=h*count
        for i in range(nt):
            
            templ.append(var1)
            temp=obj[i].GetContactForce()
            tempx=obj[i].Get_Xforce()
            tempxx=obj[i].GetPos_dt()
            
            
            
            Xforce.append(tempx.x)
            Yforce.append((tempx.y))
            Zforce.append(tempx.z)
            
            Xcontact.append(temp.x)
            Ycontact.append(temp.y)
            Zcontact.append(temp.z)
            
            Xpos.append(obj[i].GetPos().x)
            Ypos.append((obj[i].GetPos().y))
            Zpos.append(obj[i].GetPos().z)
            
            rott0.append(obj[i].GetRot().e0)
            rott1.append(obj[i].GetRot().e1)
            rott2.append(obj[i].GetRot().e2)
            rott3.append(obj[i].GetRot().e3)
            
            # fill in the temporary velocity matrices
            Xvel.append(tempxx.x)
            Yvel.append(tempxx.y)
            Zvel.append(tempxx.z)
            
            
        ttemp.append(t)
        count=count+1
        myapplication.DoStep()
        myapplication.EndScene()

        if t > 1000:
            myapplication.GetDevice().closeDevice()

# In[Convert plist to matrices]
Xpos=np.asarray(Xpos)
Ypos=np.asarray(Ypos)
Zpos=np.asarray(Zpos)

rott0=np.asarray(rott0)
rott1=np.asarray(rott1)
rott2=np.asarray(rott2)
rott3=np.asarray(rott3)

templ=np.asarray(templ)

Xforce=np.asarray(Xforce)
Yforce=np.asarray(Yforce)
Zforce=np.asarray(Zforce)

Xcontact=np.asarray(Xcontact)
Ycontact=np.asarray(Ycontact)
Zcontact=np.asarray(Zcontact)

Xvel=np.asarray(Xvel)
Yvel=np.asarray(Yvel)
Zvel=np.asarray(Zvel)
# In[Create empty arrays]
# position
qx=np.zeros((nt,count))
qy=np.zeros((nt,count))
qz=np.zeros((nt,count))

# empty toational matrices
rot0=np.zeros((nt,count))
rot1=np.zeros((nt,count))
rot2=np.zeros((nt,count))
rot3=np.zeros((nt,count))


# contact forces
Fxc=np.zeros((nt,count))
Fyc=np.zeros((nt,count))
Fzc=np.zeros((nt,count))
# total forces
Fxt=np.zeros((nt,count))
Fyt=np.zeros((nt,count))
Fzt=np.zeros((nt,count))
# Spring length
SL=np.zeros((nt,count))

# Velocity empty matrices
Xv=np.zeros((nt,count))
Yv=np.zeros((nt,count))
Zv=np.zeros((nt,count))

for i in range(count):
    print(i)
    
    
    # fill the position matrices
    qx[:,i]=Xpos[nt*i:nt*i+nt]
    qy[:,i]=Ypos[nt*i:nt*i+nt]  
    qz[:,i]=Zpos[nt*i:nt*i+nt]  
  
    # fill the rotational matrices  
    rot0[:,i]=rott0[nt*i:nt*i+nt]
    rot1[:,i]=rott1[nt*i:nt*i+nt]
    rot2[:,i]=rott2[nt*i:nt*i+nt]
    rot3[:,i]=rott3[nt*i:nt*i+nt]
    
    # fill the total force matrices
    Fxt[:,i]=Xforce[nt*i:nt*i+nt] 
    Fyt[:,i]=Yforce[nt*i:nt*i+nt] 
    Fzt[:,i]=Zforce[nt*i:nt*i+nt] 
    
    # fill the contact force matrices
    Fxc[:,i]=Xcontact[nt*i:nt*i+nt] 
    Fyc[:,i]=Ycontact[nt*i:nt*i+nt] 
    Fzc[:,i]=Zcontact[nt*i:nt*i+nt]
    
    Xv[:,i]=Xvel[nt*i:nt*i+nt]
    Yv[:,i]=Yvel[nt*i:nt*i+nt]
    Zv[:,i]=Zvel[nt*i:nt*i+nt]
    
    SL[:,i]=templ[nt*i:nt*i+nt]
    
        
stop = timeit.default_timer()

runtime=stop-start
runtime=runtime/60


variables=([nb,ni,diameter,height,volume,mr,mp,k,b,mu_f,mu_b,mu_r,mu_s,rl,runtime])
description="This is for all robots with a force forward of 1.3 newtons"


texts=["number of boundary(n/a)=","\r\n number of interior(n/a)=","\r\n diameter(m)=","\r\n height of each robot(m)=","\r\n volume(m^3)=","\r\n mass of each robot(kg)=","\r\n mass of each particle(kg)=","\r\n spring constant(N/m)=","\r\n damping coefficent(Ns/m)=","\r\nsliding friction=","\r\nmaterial dampning=","\r\nRolling friction=","\r\nSpinning friction=","\r\nresting length=","\r\nruntime(minutes)=",description]

f= open("simulation"+str(sim)+"variables.txt","w+")

for i in range(np.size(variables)):
    
    f.write(texts[i]+str(variables[i]) )
    
np.savez("resume"+str(sim)+".npz",allow_pickle=True,Fxc=Fxc,Fyc=Fyc,Fzc=Fzc,Fxt=Fxt,Fyt=Fyt,Fzt=Fzt,qx=qx,qy=qy,qz=qz,nb=nb,ni=ni,mr=mr,mp=mp,k=k,rowr=rowr,rowp=rowp,height=height,diameter=diameter,volume=volume,ttemp=ttemp,count=count,rot0=rot0,rot1=rot1,rot2=rot2,rot3=rot3,botcall=botcall,SL=SL,Xv=Xv,Yv=Yv,Zv=Zv,sim=sim)



