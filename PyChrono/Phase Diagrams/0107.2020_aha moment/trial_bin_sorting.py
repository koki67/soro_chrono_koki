# -*- coding: utf-8 -*-
"""
Created on Sun Jan 19 18:24:48 2020

@author: dmulr
"""
import numpy as np
import math as math
import matplotlib.pyplot as plt
import os


# In[Extract one time step]


def extract_instance(qx,qz,i,ballp,nb):
    X=np.zeros((nb,1))
    Z=np.zeros((nb,1))
    
    X=qx[0:nb,i]
    Z=qz[0:nb,i]
    B=ballp[i]
    
    return(X,Z,B)

# In[Determine Gripping angle]


 
#def Gripping_angle(ballp,nb,time,obj,i,xw):    
    
def Groupem(X,Z,B):  
    # First deterime which quardant each robot falls in    
    # z plane    
    orignz=0
    # robots in quadrant 1
    bin1=[]
    # robots in quadrant 2
    bin2=[]
    # x postions 
    binx1=[]
    binx2=[]
    binz1=[]
    binz2=[]
    
    # z positions
    # sort them into different bins
    for i in range(nb):
        q=orignz-Z[i]  # dummy variable 
        # if its positive
        if q>0:
            bin1.append(i)
            binx1.append(X[i])
            binz1.append(Z[i])
        # if its negative
        elif q<0:
            bin2.append(i)
            binx2.append(X[i])
            binz2.append(Z[i])
        # if it equals zero
        elif q==0:
            bin2.append(i)
            binx2.append(X[i])
            binz2.append(Z[i])
    
    return(bin1,bin2,binx1,binz1,binx2,binz2)    
    
# In[Find contact bots]    
def find_contact_bots(X,Z,B,db,tol):
    
    D1=[]
    XD=[]
    ZD=[]
    conbot=[]
    for i in range(len(X)):
        D=np.sqrt((X[i]-B)**2+(Z[i]**2))
        D1.append(D)
        if D1[i]<=db+tol:
            
            conbot.append(i)
    # will remove for final iteration
    for i in(conbot):
        XD.append(X[i])
        ZD.append(Z[i])
    
    return(conbot,D1,XD,ZD)

# In[Find Angle]  
def find_angle(XD,ZD,conbotX,Z,B):
    
    
    
    
# In[Import Data]

#In[Import data]
data=np.load('Experiment 1.npz',allow_pickle=True)

#data2=np.load('compare.npz',allow_pickle=True)

# Positions
qx=data['qx']
qy=data['qy']
qz=data['qz']

# number of robots
nb=data['nb']
ni=data['ni']

# ball position
ballp=data['ballp']
i=500

db=.0902+(.035/2)
tol=.06
(X,Z,B)=extract_instance(qx,qz,i,ballp,nb)
(bin1,bin2,binx1,binz1,binx2,binz2)=Groupem(X,Z,B)

(conbot,D1,XD,ZD)=find_contact_bots(X,Z,B,db,tol)


# plot them 
plt.figure(1)
plt.scatter(binx1,binz1,color='b')
    
plt.scatter(binx2,binz2,color='r')

plt.scatter(XD,ZD,color='k')

plt.scatter(B,0,color='g')
    
    