number of boundary(n/a)=100
 number of interior(n/a)=628
 nb over ni(n/a)=0.1592356687898089
 number of interior(n/a)=
 Spring constant(N/m)=-5
 Magnitude of force on bots(N)=0
 magnitude of force on ball (N)=200
 Ratio of ball to robot=0.15
 Diameter of ball (m)=0.18211269024649013
 diameter of Robot (m)=1.2140846016432676
 Time to pull=6
 Time to jam=5Time end=7# In[Define frictional properties]mu_f=.4     # frictionmu_b=.01    # dampningmu_r=.1     # rolling frictionmu_s=.01     # SPinning fictionCt=.00001C=.00001Cr=.0001Cs=.0001mu_f2=.3     # frictionmu_b2=.01    # dampningmu_r2=.1     # rolling frictionmu_s2=.1     # SPinning fictionCt=.00001C=.00001Cr=.0001Cs=.0001# ID number of robots to be activeactive1=np.arange(0,int(nb/2),1)active2=np.arange(int(nb/2),nb,1)active=np.hstack((active1,active2))sactive1=np.arange(88,99,1)sactive2=np.arange(0,12,1)sactive=np.hstack((active1,active2))