# -*- coding: utf-8 -*-
"""
Created on Mon Dec  9 14:16:43 2019

@author: dmulr
"""

from plotly.offline import plot
import plotly.graph_objs as go
# #import plotly.graph_objects as go

# fig = go.Figure(
#     data=[go.Scatter(x=[0, 1], y=[0, 1])],
#     layout=go.Layout(
#         xaxis=dict(range=[0, 5], autorange=False),
#         yaxis=dict(range=[0, 5], autorange=False),
#         title="Start Title",
#         updatemenus=[dict(
#             type="buttons",
#             buttons=[dict(label="Play",
#                           method="animate",
#                           args=[None])])]
#     ),
#     frames=[go.Frame(data=[go.Scatter(x=[1, 2], y=[1, 2])]),
#             go.Frame(data=[go.Scatter(x=[1, 4], y=[1, 4])]),
#             go.Frame(data=[go.Scatter(x=[3, 4], y=[3, 4])],
#                      layout=go.Layout(title_text="End Title"))]
# )

# fig.show()

# #from plotly.offline import plot
# #import plotly.graph_objs as go

# #fig = go.Figure(data=[go.Bar(y=[1, 3, 2])])
# plot(fig, auto_open=True)



import plotly.express as px

gapminder = px.data.gapminder()

fig = px.bar(gapminder, x="continent", y="pop", color="continent",
  animation_frame="year", animation_group="country", range_y=[0,4000000000])
fig.show()

plot(fig, auto_open=True)
