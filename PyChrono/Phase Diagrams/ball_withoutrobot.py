# -*- coding: utf-8 -*-
"""
Created on Mon Dec  9 14:22:43 2019

@author: dmulr
"""


'''

author: declan mulroy
project: JAMoEBA
email: dmulroy@hawk.iit.edu
date: 11/19/19
Phase diagram generator
'''

# In[import libraries]
import pychrono.core as chrono
import pychrono.irrlicht as chronoirr
import pychrono.postprocess as postprocess
import os
import numpy as np
import timeit
import matplotlib.pyplot as plt
from Sim_objects import Material,Floor,Interior,Ball,Wall,Box





sim="compare"
start = timeit.default_timer()
record=2

tstep=0.003 #Time Step
tset= 0.1 #Settling time
tend=10 #Length of simulation
tp=5 # time to pull
tj=4   # time jamming starts
# In[Set Path]
#chrono.SetChronoDataPath("C:/Chrono/Builds/chrono-develop/bin/data/")
#chrono.SetChronoDataPath("C:/Users/dmulr/Documents/data/")
chrono.SetChronoDataPath("C:/Users/dmulr/OneDrive/Documents/data/")
#chrono.SetChronoDataPath("D:/WaveLab/Soft Robotics/Chrono/data/")
# In[Create sysem and other misselanous things]
my_system = chrono.ChSystemNSC()
#my_system.SetSolverType(chrono.ChSolver.Type_BARZILAIBORWEIN)
my_system.SetSolverType(chrono.ChSolver.Type_SOR_MULTITHREAD)
my_system.Set_G_acc(chrono.ChVectorD(0, -9.81, 0))
#chrono.ChCollisionModel.SetDefaultSuggestedEnvelope(0.0001)
#chrono.ChCollisionModel.SetDefaultSuggestedMargin(0.01)
my_system.SetMaxItersSolverSpeed(150)
#my_system.SetMaxPenetrationRecoverySpeed(5)
#my_system.SetMinBounceSpeed(.5)

# In[Define frictional properties]
mu_f=.4     # friction
mu_b=.01    # dampning
mu_r=.4     # rolling friction
mu_s=.4     # SPinning fiction

Ct=.00001
C=.00001
Cr=.0001
Cs=.0001


length=8    # Length of the body floor
tall=.1     # height of the body floor


# Create The material
material=Material(mu_f,mu_b,mu_r,mu_s,C,Ct,Cr,Cs)

# In[Create Floor]
body_floor=Floor(material,length,tall)
my_system.Add(body_floor)


# In[cylinder dimmensions/geometries]

nb=100             # number of robots
diameter=.07        # diameter of cylinder and robots
R1=(diameter*nb/(np.pi*2))+.1 
    



mb=3              # mass of ball
height=0.12         # height of cylinder
hhalf=.06           # half height of cylinder
k=200                # spring constant (bots)
b=k/50               # damping (bots)


ratio=.25   
Rb=ratio*R1         # Radius of the ball
offset=Rb+R1        # Offset of ball psotion
rx=offset+.05       # x positon of ball
ry=hhalf           # y position of ball
rz=0               # z position of ball


volumeb=np.pi*.25*height*(Rb*2)**2  # volume of ball

rowb=mb/volumeb # calculate volume of ball
# In[External forces]

mag = 0 #[N]- magnitude of external force applied at each bot
magf=(9.81)*mb*mu_f    # Force from friction and weight
magd=0   # Desired force
mag2= magd+magf    # compensated force to account for friction
force=[] #empty array to store force objects
forceb=[] # empty array to store forces on ball
obj=[]












# In[Create Ball]
Balls=[]
Ball(rx,ry,rz,Rb,height,hhalf,rowb,material,obj,my_system,forceb,body_floor,Balls)

#RL=Rb
#ball=[]
#Box(rx,ry,rz,Rb,RL,height,hhalf,rowr,material,obj,my_system,forceb,body_floor,ball)
# In[Create Wall]  
  # x position of wall
z =  0      # Z position of wall
rotate = np.pi/2    # rotate wall
length = 10     # length of wall
height = .25    # height of wall
width = .1      # width of wall
y = height/1.9  # initial y position of wall
x=-R1-width

# In[ Create empty matrices to be filled]

coord=[]
Fm=[]
cpoint=[]
ballp=[]
count=0

ttemp=[]
# In[Pov RAY]
if record==1:
    
    script_dir = os.path.dirname("povvideofiles"+sim+"/")
    pov_exporter = postprocess.ChPovRay(my_system)

    # Sets some file names for in-out processes.
    pov_exporter.SetTemplateFile(chrono.GetChronoDataPath() + "_template_POV.pov")
    pov_exporter.SetOutputScriptFile("rendering"+str(sim)+".pov")
    pov_exporter.SetOutputDataFilebase("my_state")
    pov_exporter.SetPictureFilebase("picture")

    # create folders
    if not os.path.exists("output"+str(sim)):
        os.mkdir("output"+str(sim))
    if not os.path.exists("anim"+str(sim)):
            os.mkdir("anim"+str(sim))
    pov_exporter.SetOutputDataFilebase("output"+str(sim)+"/my_state")
    pov_exporter.SetPictureFilebase("anim"+str(sim)+"/picture")
    pov_exporter.SetCamera(chrono.ChVectorD(0,3,0), chrono.ChVectorD(0,0,0), 90)# specifiy camera location
    pov_exporter.AddAll()
    pov_exporter.ExportScript()

    #In[Run the simulation]

    count=0
    t=tstep*count 
    while (my_system.GetChTime() < tend) :
        
        print ('time=', my_system.GetChTime() )
        t=tstep*count  
        for i in range(len(force)):
            force[i].SetDir(chrono.VECT_X)
        for i in range(nb):
            if t > tset:
               
                for i in range(len(forceb)):
                    forceb[i].SetMforce(-mag2)
                    forceb[i].SetDir(chrono.ChVectorD(1,0,0))
            if t > tj:
    

                for i in range(len(forceb)):
                    forceb[i].SetMforce(-mag2) 

            if t>tp:
  
                for i in range(len(forceb)):
                    forceb[i].SetMforce(1.5*magf)
                    
        
            
        for i in range(len(Balls)):
            ballp.append(Balls[i].GetPos().x)
            


# In[IRRLICHT code]

myapplication = chronoirr.ChIrrApp(my_system,sim, chronoirr.dimension2du(1200,800))
myapplication.AddTypicalSky()
myapplication.AddTypicalLogo(chrono.GetChronoDataPath() + 'logo_pychrono_alpha.png')
myapplication.AddTypicalCamera(chronoirr.vector3df(0.75,0.75,1.5))
myapplication.AddLightWithShadow(chronoirr.vector3df(2,5,2),chronoirr.vector3df(2,2,2),10,2,10,120)
myapplication.DrawAll               
myapplication.AssetBindAll();
myapplication.AssetUpdateAll();
myapplication.AddShadowAll();

count=0
# Time step
myapplication.SetTimestep(tstep)
myapplication.SetTryRealtime(False)

while(myapplication.GetDevice().run()):
    myapplication.BeginScene()
    myapplication.DrawAll()
    print ('time=', my_system.GetChTime() )
    t=tstep*count  
    for i in range(len(force)):
        force[i].SetDir(chrono.VECT_X)

    if t > tset:
               
        for i in range(len(forceb)):
            forceb[i].SetMforce(-mag2)
            forceb[i].SetDir(chrono.ChVectorD(1,0,0))
    if t > tj:
    

        for i in range(len(forceb)):
            forceb[i].SetMforce(-mag2) 

    if t>tp:
  
        for i in range(len(forceb)):
            forceb[i].SetMforce(1.5*magf)
                    
        
            
    for i in range(len(Balls)):
        ballp.append(Balls[i].GetPos().x)
    ttemp.append(t)
    count=count+1
    myapplication.DoStep()
    myapplication.EndScene()
# Close the simulation if time ends
    if t > tend:
        myapplication.GetDevice().closeDevice()




ballp=np.asarray(ballp)
ttemp=np.asarray(ttemp) 

plt.figure(1)

plt.plot(ttemp,ballp,'b')
plt.title('ball position vs time')
plt.xlim(5,10)
plt.xlabel('time')
plt.ylabel('ball position')
plt.grid(True)
plt.savefig('unforced.pdf') 
plt.close('all')
    

stop = timeit.default_timer()

runtime=stop-start
runtime=runtime
print("Total runtime: "+str(runtime)+" seconds")

np.savez(sim+".npz",allow_pickle=True,
ttemp=ttemp,
ballp=ballp)

