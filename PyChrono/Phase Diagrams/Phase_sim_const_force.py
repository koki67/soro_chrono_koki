'''

author: declan mulroy
project: JAMoEBA
email: dmulroy@hawk.iit.edu
date: 11/19/19
Phase diagram generator
'''

# In[import libraries]
import pychrono.core as chrono
import pychrono.irrlicht as chronoirr
import pychrono.postprocess as postprocess
import os
import numpy as np
import timeit
from Sim_objects import Material,Floor,Interior,Ball,Wall,Box,ExtractData,MyReportContactCallback,ExtractData3,PidControl






sim="Experiment 38"
start = timeit.default_timer()
record=2

tstep=0.005 #Time Step
tset= 0.01 #Settling time
tend=6 #Length of simulation
tp=5 # time to pull
tj=4   # time jamming starts
# In[Set Path]
#chrono.SetChronoDataPath("C:/Chrono/Builds/chrono-develop/bin/data/")
#chrono.SetChronoDataPath("C:/Users/dmulr/Documents/data/")
chrono.SetChronoDataPath("C:/Users/dmulr/OneDrive/Documents/data/")
#chrono.SetChronoDataPath("D:/WaveLab/Soft Robotics/Chrono/data/")
# In[Create sysem and other misselanous things]
my_system = chrono.ChSystemNSC()
my_system.SetSolverType(chrono.ChSolver.Type_SOR_MULTITHREAD)
#my_system.SetSolverType(chrono.ChSolver.Type_APGD)
my_system.Set_G_acc(chrono.ChVectorD(0, -9.81, 0))
my_system.SetMaxItersSolverSpeed(150)


# In[Define frictional properties]
mu_f=.4     # friction
mu_b=.01    # dampning
mu_r=.4     # rolling friction
mu_s=.1     # SPinning fiction

Ct=.00001
C=.00001
Cr=.0001
Cs=.0001


length=8    # Length of the body floor
tall=.1     # height of the body floor


# Create The material
material=Material(mu_f,mu_b,mu_r,mu_s,C,Ct,Cr,Cs)
# In[Define frictional properties for second material]
mu_f2=.1     # friction
mu_b2=.01    # dampning
mu_r2=.4     # rolling friction
mu_s2=.2     # SPinning fiction

Ct=.00001
C=.00001
Cr=.0001
Cs=.0001

# Create second material
material2=Material(mu_f2,mu_b2,mu_r2,mu_s2,C,Ct,Cr,Cs)
# In[Create Floor]
body_floor=Floor(material2,length,tall)
my_system.Add(body_floor)


# In[cylinder dimmensions/geometries]

nb=100             # number of robots
diameter=.07        # diameter of cylinder and robots
R1=(diameter*nb/(np.pi*2))+.1 
diameter2=.07        # diameter of cylinder and robots   
#n=np.array([104,97,91,85,78,72,66,60,53,47,41,34,28,22,16,9,3]) # Ring of interiors
n=np.array([71,92,60,77,50,62,40,50,30,38,20,21,10,5])
#n=np.arange(nb,5,-15)
ni=np.sum(n)        # sum them for total number of interior
nt=nb+ni            # total number of bots and particles
nr=nb/ni            # ratio of nb over ni
mr=.18              # mass
mp=.03              # mass of particles
mb=3              # mass of ball
height=0.12         # height of cylinder
hhalf=.06           # half height of cylinder
k=-20                # spring constant (bots)
kj=-40         # Jamming Spring constant
rl=0            # resting length
rlmax=.02          # max resting length
rlj=.000          # desired length jammed
rljmax=.001 # desired length jammed max
obj=[]              # empty matrix of bots and particles
Springs=[]          # empty matrix of springs

volume=np.pi*.25*height*(diameter)**2      # calculate volume
volume2=np.pi*.25*height*(diameter2)**2      # calculate volume

ratio=.25   
Rb=ratio*R1         # Radius of the ball
offset=Rb+R1        # Offset of ball psotion
rx=offset+.05       # x positon of ball
ry=hhalf           # y position of ball
rz=0               # z position of ball


volumeb=np.pi*.25*height*(Rb*2)**2  # volume of ball
rowr=mr/volume # calculate density of robot
rowp=mp/volume2 # calculate density of particles
rowb=mb/volumeb # calculate volume of ball
# In[External forces]

mag = 0 #[N]- magnitude of external force applied at each bot
magf=(9.81)*mb*mu_f2    # Force from friction and weight
magd=100     # Desired force
mag2= magd+magf    # compensated force to account for friction
force=[] #empty array to store force objects
forceb=[] # empty array to store forces on ball

# Empty matrix      
botcall=np.zeros((1,nb))

jamcall=np.zeros((1,nb))

# ID number of robots to be active
active1=np.arange(0,int(nb/2),1)
active2=np.arange(int(nb/2),nb,1)
active=np.hstack((active1,active2))

sactive1=np.arange(int(11*nb/12),0,1)
sactive2=np.arange(0,int(1*nb/12),1)
sactive=np.hstack((active1,active2))

# Active robots 
for i in (sactive):
    jamcall[:,i]=1

# For robots that are active fill botcall==1
for i in (active):
    botcall[:,i]=1



# In[Create txt file]
variables=([nb,ni,nr,k,mag,magd,ratio,Rb,R1,tp,tj])

texts=["number of boundary(n/a)=",
       "\r\n number of interior(n/a)=",
       "\r\n nb over ni(n/a)=",
       "\r\n number of interior(n/a)="
       "\r\n Spring constant(N/m)=",
       "\r\n Magnitude of force on bots(N)=",
       "\r\n magnitude of force on ball (N)=",
       "\r\n Ratio of ball to robot=",
       "\r\n Diameter of ball (m)=",
       "\r\n diameter of Robot (m)=",
       "\r\n Time to pull=",
       "\r\n Time to jam=",
       ]

f= open(sim+"variables.txt","w+")

for i in range(np.size(variables)):
    
    f.write(texts[i]+str(variables[i]) )



# In[Create Robots]
for i in range(nb): 
    theta=i*2*np.pi/nb
    x=R1*np.cos(theta)
    y=.5*height
    z=R1*np.sin(theta)
    # Create bots    
    bot = chrono.ChBody()
    bot = chrono.ChBodyEasyCylinder(diameter/2, height,rowr)
    bot.SetPos(chrono.ChVectorD(x,y,z))
    bot.SetMaterialSurface(material)
    # rotate them
    rotation1 = chrono.ChQuaternionD()
    rotation1.Q_from_AngAxis(-theta, chrono.ChVectorD(0, 1, 0));  
    bot.SetRot(rotation1)
    # give ID number
    bot.SetId(i)
    # collision model
    bot.GetCollisionModel().ClearModel()
    bot.GetCollisionModel().AddCylinder(diameter/2,diameter/2,hhalf) # hemi sizes
    bot.GetCollisionModel().BuildModel()
    bot.SetCollide(True)
    bot.SetBodyFixed(False)
    pt=chrono.ChLinkMatePlane()
    pt.Initialize(body_floor,bot,False,chrono.ChVectorD(0,0,0),chrono.ChVectorD(0,0,0),chrono.ChVectorD(0,1, 0),chrono.ChVectorD(0,-1, 0))
    my_system.AddLink(pt)
    # Apply forces to active bots
    if botcall[:,i]==1:
        myforcex = chrono.ChForce()
        bot.AddForce(myforcex)
        myforcex.SetMode(chrono.ChForce.FORCE)
        myforcex.SetDir(chrono.VECT_X)
        myforcex.SetVrelpoint(chrono.ChVectorD(x,.03*y,z))
        #myforcex.SetMforce(mag)
        force.append(myforcex)
        col_y = chrono.ChColorAsset()
        col_y.SetColor(chrono.ChColor(0.44, .11, 52))
        bot.AddAsset(col_y)
    # passive robots
    else:
        col_g = chrono.ChColorAsset()
        col_g.SetColor(chrono.ChColor(0, 1, 0))
        bot.AddAsset(col_g)   
    # Attach springs    
    if i>=1:
        ground=chrono.ChLinkSpring()
        ground1=chrono.ChLinkSpring()
      # Identify points to be attatched to the springs 
        ground.SetName("ground")
        p1=0
        p2=diameter/2
        p3=0
        p4=-diameter/2
        h=height/4
        
        # Attatches first springs
        ground.Initialize(obj[i-1], bot,True,chrono.ChVectorD(p1,h,p2), chrono.ChVectorD(p3,h,p4),False)
        ground.Set_SpringF(k)
        ground.Set_SpringRestLength(rl)
        col1=chrono.ChColorAsset()
        col1.SetColor(chrono.ChColor(0,0,1))
        ground.AddAsset(col1)
        ground.AddAsset(chrono.ChPointPointSpring(.01,80,15))
        my_system.AddLink(ground)
        Springs.append(ground)
        # Attatch second springs
        ground1.Initialize(obj[i-1], bot,True,chrono.ChVectorD(p1,-h,p2), chrono.ChVectorD(p3,-h,p4),False)
        ground1.Set_SpringK(k)
        ground1.Set_SpringRestLength(rl)
        col1=chrono.ChColorAsset()
        col1.SetColor(chrono.ChColor(0,0,1))
        ground1.AddAsset(col1)
        ground1.AddAsset(chrono.ChPointPointSpring(.01,80,15))
        my_system.AddLink(ground1)
        Springs.append(ground1)
    # Last spring
    if i==nb-1:        
        ground=chrono.ChLinkSpring()
        ground.SetName("ground")
        ground.Initialize(bot, obj[0], True, chrono.ChVectorD(p1,h,p2), chrono.ChVectorD(p3,h,p4),False)
        ground.Set_SpringF(k)
        ground.Set_SpringRestLength(rl)
        col1=chrono.ChColorAsset()
        col1.SetColor(chrono.ChColor(0,0,1))
        ground.AddAsset(col1)
        ground.AddAsset(chrono.ChPointPointSpring(.01,80,15))
        my_system.AddLink(ground)
        Springs.append(ground) 
            
        ground1=chrono.ChLinkSpring()
        ground1.SetName("ground")
        ground1.Initialize(bot, obj[0], True, chrono.ChVectorD(p1,-h,p2), chrono.ChVectorD(p3,-h,p4),False)
        ground1.Set_SpringF(k)
        ground1.Set_SpringRestLength(rl)
        col1=chrono.ChColorAsset()
        col1.SetColor(chrono.ChColor(0,0,1))
        ground1.AddAsset(col1)
        ground1.AddAsset(chrono.ChPointPointSpring(.01,80,15))
        my_system.AddLink(ground1)
        Springs.append(ground1) 
    my_system.Add(bot)
    obj.append(bot)
# In[Create Interior]
for i in range(n.size):
    print(i)
    if i%2==0:
        print('yee')
        diameter3=(2**.5)*diameter2
    else:
        print('no yee')
        diameter3=diameter2
    for j in range(n[i]):
        R2=diameter3*n[i]/(np.pi*2)
        x=R2*np.cos(j*2*np.pi/n[i])
        y=.5*height
        z=R2*np.sin(j*2*np.pi/n[i])
        Interior(x,y,z,i,diameter3,height,rowp,R2,material,obj,my_system,body_floor)
# In[Create Ball]
Balls=[]
Ball(rx,ry,rz,Rb,height,hhalf,rowb,material,obj,my_system,forceb,body_floor,Balls)

#RL=Rb
#ball=[]
#Box(rx,ry,rz,Rb,RL,height,hhalf,rowr,material,obj,my_system,forceb,body_floor,ball)
# In[Create Wall]  
  # x position of wall
z =  0      # Z position of wall
rotate = np.pi/2    # rotate wall
length = 10     # length of wall
height = .25    # height of wall
width = .1      # width of wall
y = height/1.9  # initial y position of wall
x=-R1-width
Wall(x,y,z,rotate,length,height,width,material2,my_system)


#z =  .55      # Z position of wall
#rotate = np.pi    # rotate wall
#length = 2     # length of wall
#height = .25    # height of wall
#width = .1      # width of wall
#y = height/1.9  # initial y position of wall
#x=-R1-width-1.5
#Wall(x,y,-z,rotate,length,height,width,material2,my_system)
#
#Wall(x,y,z,-rotate,length,height,width,material,my_system)
# In[ Create empty matrices to be filled]
Xpos=[]
Ypos=[]
Zpos=[]

Xforce=[]
Yforce=[]
Zforce=[]

# Contact forces 
Xcontact=[]
Ycontact=[]
Zcontact=[]

# empty temporary velocity matrices
Xvel=[]
Yvel=[]
Zvel=[]

templ=[]
ttemp=[]
# Rotation Positions
rott0=[]
rott1=[]
rott2=[]
rott3=[]

xunit=[]
yunit=[]
zuni=[]
coord=[]
Fm=[]
cpoint=[]
ballp=[]
# VERY important
count=0
# Contact points
cx=[]
cy=[]
cz=[]
nc=[]

# Contact forces

Fxct=[]
Fyct=[]
Fzct=[]

# directions
XdirForce=[]
YdirForce=[]
ZdirForce=[]

Kp=.1
Kd=.1
Ki=.1
tarpos=np.zeros((nb,3))

for i in range(nb): 
    theta=i*2*np.pi/nb
    tarpos[i,0]=R1*np.cos(theta)+.2
    tarpos[i,1]=(.5*height)
    tarpos[i,2]=(R1*np.sin(theta)+.2)



my_rep = MyReportContactCallback()
# In[Pov RAY]
if record==1:
    
    script_dir = os.path.dirname("povvideofiles"+sim+"/")
    pov_exporter = postprocess.ChPovRay(my_system)

    # Sets some file names for in-out processes.
    pov_exporter.SetTemplateFile(chrono.GetChronoDataPath() + "_template_POV.pov")
    pov_exporter.SetOutputScriptFile("rendering"+str(sim)+".pov")
    pov_exporter.SetOutputDataFilebase("my_state")
    pov_exporter.SetPictureFilebase("picture")

    # create folders
    if not os.path.exists("output"+str(sim)):
        os.mkdir("output"+str(sim))
    if not os.path.exists("anim"+str(sim)):
            os.mkdir("anim"+str(sim))
    pov_exporter.SetOutputDataFilebase("output"+str(sim)+"/my_state")
    pov_exporter.SetPictureFilebase("anim"+str(sim)+"/picture")
    pov_exporter.SetCamera(chrono.ChVectorD(0,3,0), chrono.ChVectorD(0,0,0), 90)# specifiy camera location
    pov_exporter.AddAll()
    pov_exporter.ExportScript()

    #In[Run the simulation]

    count=0
    t=tstep*count 
    while (my_system.GetChTime() < tend) :
        my_rep.ResetList()
        # time 
        print ('time=', my_system.GetChTime() )
        t=tstep*count
        # Extract data
        #ExtractData(my_system,obj,i,force,forceb,mag,mag2,magf,rl,rlj,rlmax,rljmax,Springs,Fm,k,ballp,Balls,templ,Xforce,Yforce,Zforce,Xcontact,Ycontact,Zcontact,Xpos,Ypos,Zpos,rott0,rott1,rott2,rott3,Xvel,Yvel,Zvel,nb,nt,t,tj,tset,tp,jamcall,kj)  
        ExtractData3(my_system,obj,i,force,forceb,mag,mag2,magf,rl,rlj,rlmax,rljmax,Springs,Fm,k,ballp,Balls,templ,Xforce,Yforce,Zforce,Xcontact,Ycontact,Zcontact,Xpos,Ypos,Zpos,rott0,rott1,rott2,rott3,Xvel,Yvel,Zvel,nb,nt,t,tj,tset,tp,jamcall,kj,XdirForce,YdirForce,ZdirForce,tarpos,Kp,Ki,Kd)
        
        # Track center of robot for RL 
        Xpostemp=[]
        Zpostemp=[]
        for i in range(nb):
            Xpostemp.append(obj[i].GetPos().x)
            Zpostemp.append(obj[i].GetPos().z)
     
        Xpostemp=np.asarray(Xpostemp)
        Zpostemp=np.asarray(Zpostemp)
        
        Xavg=np.mean(Xpostemp)
        Zavg=np.mean(Zpostemp)
        my_system.GetContactContainer().ReportAllContacts(my_rep)

        crt_list = my_rep.GetList()
        nc.append(my_system.GetContactContainer().GetNcontacts())
        cx.append(crt_list[0])
        cy.append(crt_list[1])
        cz.append(crt_list[2])
        Fxct.append(crt_list[3])
        Fyct.append(crt_list[4])
        Fzct.append(crt_list[5])
        ttemp.append(t)
        count=count+1
        my_system.DoStepDynamics(tstep)
        print ('time=', my_system.GetChTime())
        pov_exporter.SetCamera(chrono.ChVectorD(Xavg,3,Zavg), chrono.ChVectorD(Xavg,0,Zavg), 90)# specifiy camera location
        pov_exporter.AddAll()
        pov_exporter.ExportScript()
        # Export every 15th frame 
        if count%15==0:
            pov_exporter.ExportData()  
# In[Irrlecht]


else:
    myapplication = chronoirr.ChIrrApp(my_system,sim, chronoirr.dimension2du(1200,800))
    myapplication.AddTypicalSky()
    myapplication.AddTypicalLogo(chrono.GetChronoDataPath() + 'logo_pychrono_alpha.png')
    myapplication.AddTypicalCamera(chronoirr.vector3df(0.75,0.75,1.5))
    myapplication.AddLightWithShadow(chronoirr.vector3df(2,5,2),chronoirr.vector3df(2,2,2),10,2,10,120)
    myapplication.DrawAll               
    myapplication.AssetBindAll();
    myapplication.AssetUpdateAll();
    myapplication.AddShadowAll();
    
    count=0
# Time step
    myapplication.SetTimestep(tstep)
    myapplication.SetTryRealtime(False)
    while(myapplication.GetDevice().run()):
        my_rep.ResetList()
        myapplication.BeginScene()
        myapplication.DrawAll()
        print ('time=', my_system.GetChTime())
        t=tstep*count  
        tar=tarpos[i,:]
        dirForce = force[i].GetDir()
        XdirForce=dirForce.x
        YdirForce=dirForce.y
        ZdirForce=dirForce.z
        ExtractData3(my_system,obj,i,force,forceb,mag,mag2,magf,rl,rlj,rlmax,rljmax,Springs,Fm,k,ballp,Balls,templ,Xforce,Yforce,Zforce,Xcontact,Ycontact,Zcontact,Xpos,Ypos,Zpos,rott0,rott1,rott2,rott3,Xvel,Yvel,Zvel,nb,nt,t,tj,tset,tp,jamcall,kj,XdirForce,YdirForce,ZdirForce,tarpos,Kp,Ki,Kd)
        #ExtractData2(my_system,obj,i,force,forceb,mag,mag2,magf,rl,rlj,rlmax,rljmax,Springs,Fm,k,ballp,Balls,templ,Xforce,Yforce,Zforce,Xcontact,Ycontact,Zcontact,Xpos,Ypos,Zpos,rott0,rott1,rott2,rott3,Xvel,Yvel,Zvel,nb,nt,t,tj,tset,tp,jamcall,kj)  
        my_system.GetContactContainer().ReportAllContacts(my_rep)

        crt_list = my_rep.GetList()
        nc.append(my_system.GetContactContainer().GetNcontacts())
        cx.append(crt_list[0])
        cy.append(crt_list[1])
        cz.append(crt_list[2])
        Fxct.append(crt_list[3])
        Fyct.append(crt_list[4])
        Fzct.append(crt_list[5])
        ttemp.append(t)
        count=count+1
        myapplication.DoStep()
        myapplication.EndScene()
# Close the simulation if time ends

        if t > tend:
            myapplication.GetDevice().closeDevice()
nc=np.asarray(nc)


lengthm=np.amax(nc)


tryme=cx[1]



# In[Convert plist to matrices]
Xpos=np.asarray(Xpos)
Ypos=np.asarray(Ypos)
Zpos=np.asarray(Zpos)

rott0=np.asarray(rott0)
rott1=np.asarray(rott1)
rott2=np.asarray(rott2)
rott3=np.asarray(rott3)

templ=np.asarray(templ)

Xforce=np.asarray(Xforce)
Yforce=np.asarray(Yforce)
Zforce=np.asarray(Zforce)

Xcontact=np.asarray(Xcontact)
Ycontact=np.asarray(Ycontact)
Zcontact=np.asarray(Zcontact)

Xvel=np.asarray(Xvel)
Yvel=np.asarray(Yvel)
Zvel=np.asarray(Zvel)
Fm=np.asarray(Fm)

ballp=np.asarray(ballp)
# In[Create empty arrays]
# position
qx=np.zeros((nt,count))
qy=np.zeros((nt,count))
qz=np.zeros((nt,count))

# empty toational matrices
rot0=np.zeros((nt,count))
rot1=np.zeros((nt,count))
rot2=np.zeros((nt,count))
rot3=np.zeros((nt,count))


# contact forces
Fxc=np.zeros((nt,count))
Fyc=np.zeros((nt,count))
Fzc=np.zeros((nt,count))
# total forces
Fxt=np.zeros((nt,count))
Fyt=np.zeros((nt,count))
Fzt=np.zeros((nt,count))
# Spring length
SL=np.zeros((nt,count))

# Velocity empty matrices
Xv=np.zeros((nt,count))
Yv=np.zeros((nt,count))
Zv=np.zeros((nt,count))

# Membrane force
Fmem=np.zeros((nb,count))

#Create empty contact matrices
xc=np.zeros((lengthm,count))
yc=np.zeros((lengthm,count))
zc=np.zeros((lengthm,count))
# Contact forces
Fcx=np.zeros((lengthm,count))
Fcy=np.zeros((lengthm,count))
Fcz=np.zeros((lengthm,count))
# In[Fill the matrices]
for i in range(count):    
    # fill the position matrices
    qx[:,i]=Xpos[nt*i:nt*i+nt]
    qy[:,i]=Ypos[nt*i:nt*i+nt]  
    qz[:,i]=Zpos[nt*i:nt*i+nt]  
  
    # fill the rotational matrices  
    rot0[:,i]=rott0[nt*i:nt*i+nt]
    rot1[:,i]=rott1[nt*i:nt*i+nt]
    rot2[:,i]=rott2[nt*i:nt*i+nt]
    rot3[:,i]=rott3[nt*i:nt*i+nt]
    
    # fill the total force matrices
    Fxt[:,i]=Xforce[nt*i:nt*i+nt] 
    Fyt[:,i]=Yforce[nt*i:nt*i+nt] 
    Fzt[:,i]=Zforce[nt*i:nt*i+nt] 
    
    # fill the contact force matrices
    Fxc[:,i]=Xcontact[nt*i:nt*i+nt] 
    Fyc[:,i]=Ycontact[nt*i:nt*i+nt] 
    Fzc[:,i]=Zcontact[nt*i:nt*i+nt]
    
    Xv[:,i]=Xvel[nt*i:nt*i+nt]
    Yv[:,i]=Yvel[nt*i:nt*i+nt]
    Zv[:,i]=Zvel[nt*i:nt*i+nt]
    
    SL[:,i]=templ[nt*i:nt*i+nt]
    Fmem[:,i]=Fm[nb*i:nb*i+nb]       

print(nc[0])
for i in range(count):
    print(i)
    ind=nc[i]
    tryme=cx[i]
    tryme2=cy[i]
    tryme3=cz[i]
    tryme4=Fxct[i]
    tryme5=Fyct[i]
    tryme6=Fzct[i]
    # convert to array
    tryme=np.asarray(tryme)
    tryme2=np.asarray(tryme2)
    tryme3=np.asarray(tryme3)
    tryme4=np.asarray(tryme4)
    tryme5=np.asarray(tryme5)
    tryme6=np.asarray(tryme6)
    
    # fill array position
    xc[0:ind,i]=np.transpose(tryme)
    yc[0:ind,i]=np.transpose(tryme2)
    zc[0:ind,i]=np.transpose(tryme3)

# Fill array forces
    Fcx[0:ind,i]=np.transpose(tryme4)
    Fcy[0:ind,i]=np.transpose(tryme5)
    Fcz[0:ind,i]=np.transpose(tryme6)            
# In[Save and export out to npz file]    
np.savez(sim+".npz",allow_pickle=True,
         Fxc=Fxc,
         Fyc=Fyc,
         Fzc=Fzc,
         Fxt=Fxt,
         Fyt=Fyt,
         Fzt=Fzt,
         qx=qx,
         qy=qy,
         qz=qz,
         nb=nb,
         ni=ni,
         mr=mr,
         mp=mp,
         k=k,
         rowr=rowr,
         rowp=rowp,
         height=height,
         diameter=diameter,
         volume=volume,
         ttemp=ttemp,
         count=count,
         rot0=rot0,
         rot1=rot1,
         rot2=rot2,
         rot3=rot3,
         botcall=botcall,
         SL=SL,
         Xv=Xv,
         Yv=Yv,
         Zv=Zv,
         sim=sim,
         Fmem=Fmem,ballp=ballp,
         xc=xc,
         yc=yc,
         zc=zc,
         nc=nc,
         Fcx=Fcx,
         Fcy=Fcy,
         Fcz=Fcz)
stop = timeit.default_timer()

# In[Print time out]
runtime=stop-start
runtime=runtime
print("Total runtime: "+str(runtime)+" seconds")