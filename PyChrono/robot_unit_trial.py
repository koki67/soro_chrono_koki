# -*- coding: utf-8 -*-
"""
Created on Tue Oct  1 12:43:50 2019

@author: dmulr
"""

# In[import libraries]
import pychrono.core as chrono
import pychrono.irrlicht as chronoirr
import pychrono.postprocess as postprocess
import os
import numpy as np


# In[Set Path]

print ("Example: create a slider crank and plot results");

# Change this path to asset path, if running from other working dir. 
# It must point to the data folder, containing GUI assets (textures, fonts, meshes, etc.)
chrono.SetChronoDataPath("../../../data/")

# ---------------------------------------------------------------------
#
#  Create the simulation system and add items
#

mysystem      = chrono.ChSystemNSC()

# Some data shared in the following
crank_center = chrono.ChVectorD(0,1,0)
crank_rad    = 0.4
crank_thick  = 0.1
rod_length   = 1.5




# In[Create the floor truss]
mfloor = chrono.ChBodyEasyBox(3, 1, 3, 1000)
mfloor.SetPos(chrono.ChVectorD(0,-0.5,0))
mfloor.SetBodyFixed(True)

mfloor.GetCollisionModel().ClearModel()
mfloor.GetCollisionModel().AddBox(1.5, .5, 1.5) # hemi sizes
mfloor.GetCollisionModel().BuildModel()
mfloor.SetCollide(True)


mysystem.Add(mfloor)



# In[Create the flywheel crank]
mcrank = chrono.ChBodyEasyCylinder(crank_rad, crank_thick, 1000)
mcrank.SetPos(crank_center)
mcrank.GetCollisionModel().ClearModel()
mcrank.GetCollisionModel().AddCylinder(crank_rad/2,crank_rad/2,crank_thick/2) # hemi sizes
mcrank.GetCollisionModel().BuildModel()
mcrank.SetCollide(True)


# Since ChBodyEasyCylinder creates a vertical (y up) cylinder, here rotate it:
mcrank.SetRot(chrono.Q_ROTATE_Y_TO_Z)
mysystem.Add(mcrank)


# In[Axis]

ax=chrono.ChBodyBox(.2,.2,.2,1000)

# Create crank-truss joint: a motor that spins the crank flywheel
my_motor = chrono.ChLinkMotorRotationSpeed()
my_motor.Initialize(mcrank,   # the first connected body
                    mfloor,   # the second connected body
                    chrono.ChFrameD(crank_center)) # where to create the motor in abs.space
my_angularspeed = chrono.ChFunction_Const(chrono.CH_C_PI) # ang.speed: 180°/s
my_motor.SetMotorFunction(my_angularspeed)
mysystem.Add(my_motor)


















