# -*- coding: utf-8 -*-
"""
Created on Tue Aug 20 16:07:52 2019

@author: dmulr
"""



# In[import libraries]
import pychrono.core as chrono
import pychrono.irrlicht as chronoirr
import pychrono.postprocess as postprocess
import os
import numpy as np
# In[Set Path]
chrono.SetChronoDataPath("data/")
# In[Create sysem and other misselanous things]
my_system = chrono.ChSystemNSC()
#my_system.SetSolverType(chrono.ChSolver.Type_BARZILAIBORWEIN)
chrono.ChCollisionModel.SetDefaultSuggestedEnvelope(0.001)
chrono.ChCollisionModel.SetDefaultSuggestedMargin(0.001)
my_system.SetMaxItersSolverSpeed(1000)
# In[create material]
material = chrono.ChMaterialSurfaceNSC()
material.SetFriction(.3)
material.SetDampingF(.3)
material.SetCompliance (0.00001)
material.SetComplianceT(0.00001)
material.SetRollingFriction(.3)
material.SetSpinningFriction(.3)
material.SetComplianceRolling(0.00001)
material.SetComplianceSpinning(0.00001)
# In[Create floor]
body_floor = chrono.ChBody()
body_floor.SetBodyFixed(True)
body_floor.SetPos(chrono.ChVectorD(0, -1, 0 ))
body_floor.SetMaterialSurface(material)

# In[Collision shape]
body_floor.GetCollisionModel().ClearModel()
body_floor.GetCollisionModel().AddBox(3, 1, 3) # hemi sizes
body_floor.GetCollisionModel().BuildModel()
body_floor.SetCollide(True)

# In[Visualization shape]
body_floor_shape = chrono.ChBoxShape()
body_floor_shape.GetBoxGeometry().Size = chrono.ChVectorD(3, 1, 3)
body_floor.GetAssets().push_back(body_floor_shape)
body_floor_texture = chrono.ChTexture()
body_floor_texture.SetTextureFilename(chrono.GetChronoDataPath() + 'aluminum.jpg')
body_floor.GetAssets().push_back(body_floor_texture)
my_system.Add(body_floor)

# In[Create wall]

wwidth=.1
wheight=.1
wlength=2.5

meh=np.array([1,-1])
for i in(meh):
    wall = chrono.ChBody()
    wall.SetBodyFixed(True)
    wall.SetPos(chrono.ChVectorD(2.5*i, wheight/2, 0))
    wall.SetMaterialSurface(material)
    # Collision shape
    wall.GetCollisionModel().ClearModel()
    wall.GetCollisionModel().AddBox(wwidth, wheight, wlength) # hemi sizes
    wall.GetCollisionModel().BuildModel()
    wall.SetCollide(True)
    # Visualization shape
    wall_shape = chrono.ChBoxShape()
    wall_shape.GetBoxGeometry().Size = chrono.ChVectorD(wwidth, wheight, wlength)
    wall_shape.SetColor(chrono.ChColor(0.4,0.4,0.5))
    wall.GetAssets().push_back(wall_shape)
    wall_texture = chrono.ChTexture()
    wall_texture.SetTextureFilename(chrono.GetChronoDataPath() + 'aluminum.jpg')
    wall.GetAssets().push_back(wall_texture)
    my_system.Add(wall)

for i in(meh):
    wall = chrono.ChBody()
    wall.SetBodyFixed(True)
    wall.SetPos(chrono.ChVectorD(0, wheight/2, 2.5*i))
    wall.SetMaterialSurface(material)
    # Collision shape
    wall.GetCollisionModel().ClearModel()
    wall.GetCollisionModel().AddBox(wlength, wheight, wwidth) # hemi sizes
    wall.GetCollisionModel().BuildModel()
    wall.SetCollide(True)
    # Visualization shape
    wall_shape = chrono.ChBoxShape()
    wall_shape.GetBoxGeometry().Size = chrono.ChVectorD(wlength,wheight, wwidth)
    wall_shape.SetColor(chrono.ChColor(0.4,0.4,0.5))
    wall.GetAssets().push_back(wall_shape)
    wall_texture = chrono.ChTexture()
    wall_texture.SetTextureFilename(chrono.GetChronoDataPath() + 'aluminum.jpg')
    wall.GetAssets().push_back(wall_texture)
    my_system.Add(wall)

# In[cylinder dimmensions]
# run 1 97, k=100, n=np.array([93,86,77,69,61,52,44,33,22,12,4])

# run 2 94, k=100, n=np.array([93,86,77,69,61,52,44,33,22,12,4])

data=np.load('resumesqr1.npz')
nb=data['nb']
ni=data['ni']
qx=data['qx']
qy=data['qy']
qz=data['qz']

rot0=data['rot0']
rot1=data['rot1']
rot2=data['rot2']
rot3=data['rot3']

height=data['height']
mass=data['mass']
Inertia=data['Inertia']
volume=data['volume']
count=data['count']
diameter=data['diameter']
density=data['density']

count=np.sum(count)
diameter=np.sum(diameter)
height=np.sum(height)
density=np.sum(density)
mass=np.sum(mass)
ni=np.sum(ni)
nb=np.sum(nb)
k=1000
# damping
b=0


# empty matrix
obj=[]
# constant function for external forces



# ..create the function for imposed x horizontal motion, etc.
constfun = chrono.ChFunction_Const(3)

botcall=np.zeros((1,nb))


active=np.array([39,40,41,42,43,44,45,46,47,48,49,50,51])

for i in (active):
    botcall[:,i]=1

# In[Create Bots]
for i in range (nb):
    print(i)
    x=qx[i,count-1]
    y=qy[i,count-1]
    z=qz[i,count-1]
    bot = chrono.ChBody()
    bot = chrono.ChBodyEasyBox(diameter, height,diameter,density)
    bot.SetPos(chrono.ChVectorD(x,y,z))
    bot.SetRot(chrono.ChQuaternionD(rot0[i,count-1], rot1[i,count-1], rot2[i,count-1], rot3[i,count-1]))
    bot.SetMaterialSurface(material)
    bot.SetMass(mass)
    bot.SetId(i)
    bot.GetCollisionModel().ClearModel()
    bot.GetCollisionModel().AddBox(diameter/2,height/2,diameter/2) # hemi sizes
    bot.GetCollisionModel().BuildModel()
    bot.SetCollide(True)  
    if botcall[:,i]==1:
        myforcex = chrono.ChForce()
        bot.AddForce(myforcex)
        myforcex.SetMode(chrono.ChForce.FORCE)
        myforcex.SetF_x(constfun)
        myforcex.SetDir(chrono.ChVectorD(1,0,0))
        col_g = chrono.ChColorAsset()
        col_g.SetColor(chrono.ChColor(1, 1, 0))
        bot.AddAsset(col_g)
    
    else:
        print('passive')

        col_y = chrono.ChColorAsset()
        col_y.SetColor(chrono.ChColor(0, 1, 0))
        bot.AddAsset(col_y)
    if i>=1:
        ground=chrono.ChLinkSpring()
        ground.SetName("ground")
        ground.Initialize(obj[i-1], bot, False, chrono.ChVectorD(obj[i-1].GetPos().x,obj[i-1].GetPos().y ,obj[i-1].GetPos().z), chrono.ChVectorD(x, y, z))
        ground.Set_SpringK(k)
        ground.Set_SpringR(b)
        ground.Set_SpringRestLength(diameter)
        my_system.AddLink(ground)
    if i==nb-1:
        ground=chrono.ChLinkSpring()
        ground.SetName("ground")
        ground.Initialize(bot, obj[0], False,chrono.ChVectorD(x, y, z) , chrono.ChVectorD(obj[0].GetPos().x,obj[0].GetPos().y ,obj[0].GetPos().z))
        ground.Set_SpringK(k)
        ground.Set_SpringR(b)
        ground.Set_SpringRestLength(diameter)
        my_system.AddLink(ground)        
    my_system.Add(bot)
    obj.append(bot)        

# In[Create interiors]
for i in range(nb,nb+ni):
    x=qx[i,count-1]
    y=qy[i,count-1]
    z=qz[i,count-1]
    gran = chrono.ChBody()
    gran = chrono.ChBodyEasyCylinder(diameter/2, height,density)
    gran.SetPos(chrono.ChVectorD(x,y,z))
    gran.SetRot(chrono.ChQuaternionD(rot0[i,count-1], rot1[i,count-1], rot2[i,count-1], rot3[i,count-1]))
    gran.SetMaterialSurface(material)
    gran.SetMass(.001)
    gran.SetId(i)
    gran.GetCollisionModel().ClearModel()
    gran.GetCollisionModel().AddCylinder(diameter/2,diameter/2,height/2) # hemi sizes
    gran.GetCollisionModel().BuildModel()
    gran.SetCollide(True)
    col_r = chrono.ChColorAsset()
    col_r.SetColor(chrono.ChColor(1, 0, 0))
    gran.AddAsset(col_r)
    my_system.Add(gran)
    obj.append(gran) 


        
# In[Irrlecht Simulation]

#myapplication = chronoirr.ChIrrApp(my_system, 'PyChrono example', chronoirr.dimension2du(1024,768))
#
#myapplication.AddTypicalSky()
#myapplication.AddTypicalLogo(chrono.GetChronoDataPath() + 'logo_pychrono_alpha.png')
#myapplication.AddTypicalCamera(chronoirr.vector3df(0.75,0.75,1.5))
#myapplication.AddLightWithShadow(chronoirr.vector3df(2,4,2),    # point
#                                 chronoirr.vector3df(0,0,0),    # aimpoint
#                                 9,                 # radius (power)
#                                 1,9,               # near, far
#                                 50)                # angle of FOV
#
#            # ==IMPORTANT!== Use this function for adding a ChIrrNodeAsset to all items
#                        # in the system. These ChIrrNodeAsset assets are 'proxies' to the Irrlicht meshes.
#                        # If you need a finer control on which item really needs a visualization proxy in
#                        # Irrlicht, just use application.AssetBind(myitem); on a per-item basis.
#
#myapplication.AssetBindAll();
#
#                        # ==IMPORTANT!== Use this function for 'converting' into Irrlicht meshes the assets
#                        # that you added to the bodies into 3D shapes, they can be visualized by Irrlicht!
#
#myapplication.AssetUpdateAll();
#
#            # If you want to show shadows because you used "AddLightWithShadow()'
#            # you must remember this:
#myapplication.AddShadowAll();
#
## ---------------------------------------------------------------------
##
##  Run the simulation
##
#
#myapplication.SetTimestep(0.01)
#myapplication.SetTryRealtime(True)
#
#while(myapplication.GetDevice().run()):
#    myapplication.BeginScene()
#    myapplication.DrawAll()
#    for substep in range(0,5):
#        myapplication.DoStep()
#    myapplication.EndScene()
        
        
Xpos=[]
Ypos=[]
Zpos=[]
Xforce=[]
Yforce=[]
Zforce=[]
Xcontact=[]
Ycontact=[]
Zcontact=[]
time=[]
rott0=[]
rott1=[]
rott2=[]
rott3=[]
count=0

nt=nb+ni
# In[Set up pov exporter]
pov_exporter = postprocess.ChPovRay(my_system)

# Sets some file names for in-out processes.
pov_exporter.SetTemplateFile("data/_template_POV.pov")
pov_exporter.SetOutputScriptFile("rendering2.pov")
pov_exporter.SetOutputDataFilebase("my_state")
pov_exporter.SetPictureFilebase("picture")

# create folders
if not os.path.exists("output2"):
    os.mkdir("output2")
if not os.path.exists("anim2"):
    os.mkdir("anim2")
pov_exporter.SetOutputDataFilebase("output2/my_state")
pov_exporter.SetPictureFilebase("anim2/picture")

# Export out objects
pov_exporter.AddAll() 
pov_exporter.SetCamera(chrono.ChVectorD(0,3,0), chrono.ChVectorD(0,0,0), 90)# specifiy camera location
pov_exporter.ExportScript()
 #In[Run the simulation]
count=0
while (my_system.GetChTime() < 3) :
    my_system.DoStepDynamics(0.01)
    print ('time=', my_system.GetChTime())
    pov_exporter.ExportData()
    
    for i in range(nt):
        temp=obj[i].GetContactForce()
        tempx=obj[i].Get_Xforce()
        Xforce.append(tempx.x)
        Yforce.append(tempx.y)
        Zforce.append(tempx.z)
        Xcontact.append(temp.x)
        Ycontact.append(temp.y)
        Zcontact.append(temp.z)
        Xpos.append(obj[i].GetPos().x)
        Ypos.append(obj[i].GetPos().y)
        Zpos.append(obj[i].GetPos().z)
        rott0.append(obj[i].GetRot().e0)
        rott1.append(obj[i].GetRot().e1)
        rott2.append(obj[i].GetRot().e2)
        rott3.append(obj[i].GetRot().e3)
    count=count+1



# positiond
qx=np.zeros((nt,count))
qy=np.zeros((nt,count))
qz=np.zeros((nt,count))
# contact forces
Fxc=np.zeros((nt,count))
Fyc=np.zeros((nt,count))
Fzc=np.zeros((nt,count))
# total forces
Fxt=np.zeros((nt,count))
Fyt=np.zeros((nt,count))
Fzt=np.zeros((nt,count))

rot0=np.zeros((nt,count))
rot1=np.zeros((nt,count))
rot2=np.zeros((nt,count))
rot3=np.zeros((nt,count))


for i in range(count):
    qx[:,i]=Xpos[nt*i:nt*i+nt]
    qy[:,i]=Ypos[nt*i:nt*i+nt]  
    qz[:,i]=Zpos[nt*i:nt*i+nt]  
    
    Fxt[:,i]=Xforce[nt*i:nt*i+nt] 
    Fyt[:,i]=Yforce[nt*i:nt*i+nt] 
    Fzt[:,i]=Zforce[nt*i:nt*i+nt] 
    
    Fxc[:,i]=Xcontact[nt*i:nt*i+nt] 
    Fyc[:,i]=Ycontact[nt*i:nt*i+nt] 
    Fzc[:,i]=Zcontact[nt*i:nt*i+nt]
    
    rot0[:,i]=rott0[nt*i:nt*i+nt]
    rot1[:,i]=rott1[nt*i:nt*i+nt]
    rot2[:,i]=rott2[nt*i:nt*i+nt]
    rot3[:,i]=rott3[nt*i:nt*i+nt]
    
np.savez('resumesqr2.npz',allow_pickle=True,Fxc=Fxc,Fyc=Fyc,Fzc=Fzc,Fxt=Fxt,Fyt=Fyt,Fzt=Fzt,qx=qx,qy=qy,qz=qz,nb=nb,ni=ni,mass=mass,height=height,diameter=diameter,Inertia=Inertia,volume=volume,density=density,time=time,count=count,rot0=rot0,rot1=rot1,rot2=rot2,rot3=rot3)

#obj = np.asarray(obj, dtype=np.float32)
#np.savez('resume1.npz',obj=obj,ni=ni,nb=nb,mass=mass,height=height,diameter=diameter, allow_pickle=True, fix_imports=True)        
        
