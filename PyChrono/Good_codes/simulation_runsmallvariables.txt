number of boundary(n/a)=8
 number of interior(n/a)=7
 diameter(m)=0.07
 height of each robot(m)=0.12
 volume(m^3)=0.00046181412007769964
 mass of each robot(kg)=0.18
 mass of each particle(kg)=0.04
 spring constant(N/m)=10
 damping coefficent(Ns/m)=0.2
sliding friction=0.4
material dampning=0.01
Rolling friction=0.01
Spinning friction=0.005
resting length=0.01