# -*- coding: utf-8 -*-
"""
Created on Tue Jan  7 16:30:22 2020

@author: dmulr
"""
import numpy as np


# name of simulation
sim="Experiment 6

# record ==1 means pov ray 
# records ==2 is irrlicht 

record=2

tstep=0.002 #Time Step
tset= 0.01 #Settling time, allow the simulation to reach and equilibrium 
tend=4 #Length of simulation in seconds


# In[Define frictional properties]
mu_f=.4     # friction
mu_b=.01    # dampning
mu_r=.1     # rolling friction
mu_s=.01     # SPinning fiction

# compliance properties dont change unless you need too 
Ct=.00001   
C=.00001
Cr=.0001
Cs=.0001


# In[Define frictional properties for floor material]
mu_f2=.1     # friction
mu_b2=.01    # dampning
mu_r2=0     # rolling friction
mu_s2=0     # SPinning fiction



# In[Create Floor] you will need since it needs to sit on something 

length=8    # Length of the body floor
tall=.1     # height of the body floor

# In[cylinder dimmensions/geometries]

nb=70           # number of robots 
diameter=.035        # diameter of cylinder and robots [meters]
R1=(diameter*nb/(np.pi*2))+.1   # radius of the the robots formation
#R1=(diameter*nb/(np.pi*2))+.1 
mr=.15              # mass of each robot
volume=np.pi*.25*height*(diameter)**2           # calculate volume

diameter2=.07      # diameter of interiors

n=np.array([35,32,27,25,15,12,5])-5     # number of granulars per ring
#n=np.array([0])
#n=np.arange(nb-5,5,-15)
volume2=np.pi*.25*height*(diameter2)**2         # calculate volume
ni=np.sum(n)        # sum them for total number of interior
nt=nb+ni            # total number of bots and particles
nr=nb/ni            # ratio of nb over ni

mp=.03              # mass of particles

height=0.06        # height of robots and particles
hhalf=height/2      # half height of cylinder

# In[Spring propertiies]
k=-75              # spring constant (bots)
spring_b=0         # damping constant
kj=-75      # Jamming Spring constant
rl=0                # resting length
rlj=0               # desired length jammed
rlmax=0.1           # max spring length in jammed configuration 
rowr=mr/volume # calculate density of robot
rowp=mp/volume2 # calculate density of particles

# In[External forces]

mag = 0 #[N]- magnitude of external force applied at each bot

magd=100     # Desired force
mag2= 700   # Force applied to blue bot
mag3=10
force=[] #empty array to store force objects
forceb=[] # empty array to store forces on ball

# Empty matrix 

obj=[]              # empty matrix of bots and particles
Springs=[]          # empty matrix of springs     
botcall=np.zeros((1,nb))    # botcall empty matrix

jamcall=np.zeros((1,nb))    # jamcall empty matrix

normcall=np.zeros((1,nb))   # normcall empty matrix


# ID number of robots to be active
bactive1=np.arange(0,int(nb/2),1)
bactive2=np.arange(int(nb/2),nb,1)
bactive=np.hstack((bactive1,bactive2))

# robots to jam tangently 
jactive1=np.arange(0,int(nb/2),1)
jactive2=np.arange(int(nb/2),nb,1)
jactive=np.hstack((jactive1,jactive2))

# robots to jam normally
nactive1=np.arange(0,int(nb/2),1)
nactive2=np.arange(int(nb/2),nb,1)
nactive=np.hstack((nactive1,nactive2))
# Active robots 
for i in (jactive):
    jamcall[:,i]=1

# For robots that are active fill botcall==1
for i in (bactive):
    botcall[:,i]=1
    
# call robots for normal force to be applied
for i in (nactive):
    normcall[:,i]=1
    


