# -*- coding: utf-8 -*-


'''

author: declan mulroy
project: JAMoEBA
email: dmulroy@hawk.iit.edu
date: 11/19/19
RL_model
'''

# In[import libraries
import pychrono.core as chrono
import pychrono.irrlicht as chronoirr
import pychrono.postprocess as postprocess
import os
import numpy as np
import timeit
from RL_objects import Material,BOTS,Floor,Interior,Controller


start = timeit.default_timer()
record=2
sim="trial"
tstep=0.005 #Time Step
tset= 0.01 #Settling time
tend=10 #Length of simulation
tp=5 # time to pull
tj=1.55   # time jamming starts
# In[Set Path]
path=chrono.SetChronoDataPath("C:/Users/dmulr/OneDrive/Documents/data/")
# In[Create sysem and other misselanous things]
my_system = chrono.ChSystemNSC()
my_system.SetSolverType(chrono.ChSolver.Type_SOR_MULTITHREAD)
#my_system.SetSolverType(chrono.ChSolver.Type_APGD)
my_system.Set_G_acc(chrono.ChVectorD(0, -9.81, 0))
my_system.SetMaxItersSolverSpeed(150)

# In[Define frictional properties]
mu_f=.4     # friction
mu_b=.01    # dampning
mu_r=.4     # rolling friction
mu_s=.1     # SPinning fiction

Ct=.00001   # Compliance terms 
C=.00001
Cr=.0001
Cs=.0001


length=8    # Length of the body floor
tall=.1     # height of the body floor


# Create The material
material=Material(mu_f,mu_b,mu_r,mu_s,C,Ct,Cr,Cs)


# In[Create Floor]
body_floor=Floor(material,length,tall)
my_system.Add(body_floor)


# In[Properties of the robot]
nb=100                                     # number of robots
diameter=.07                               # diameter of cylinder and robots
height=0.12                                # height of cylinder
mr=.18                                     # mass
volume=np.pi*.25*height*(diameter)**2      # calculate volume
rowr=mr/volume                             # calculate density of robot
R1=(diameter*nb/(np.pi*2))+.1              # Initial Radius of the robot





 # In[Properties of the particles]
n=np.array([104,97,91,85,78,72,66,60,53,47,41,34,28,22,16,9,3]) # Ring of interiors
ni=np.sum(n)                             # sum them for total number of interior
diameter2=.07                           # diameter
height2=.12                             # height 
mp=.03                                  # mass of particles
volume2=np.pi*.25*height*(diameter2)**2 # calculate volume
rowp=mp/volume2                         # calculate density of particles

obj=[]                                  # empty matrix of bots and particles



# In[External forces]

mag = 1.4 #[N]- magnitude of external force applied at each bot
magd=100     # Desired force
force=[] #empty array to store force objects
forceb=[] # empty array to store forces on ball

# Empty matrix      
botcall=np.zeros((1,nb))

jamcall=np.zeros((1,nb))

# ID number of robots to be active
active1=np.arange(0,int(nb/2),1)
active2=np.arange(int(nb/2),nb,1)
active=np.hstack((active1,active2))

sactive1=np.arange(int(11*nb/12),0,1)
sactive2=np.arange(0,int(1*nb/12),1)
sactive=np.hstack((active1,active2))

# Active robots 
for i in (sactive):
    jamcall[:,i]=1

# For robots that are active fill botcall==1
for i in (active):
    botcall[:,i]=1

# In[Membrane Properties]

k=-2            # resting tenion
kj=-5           # jammed tension
rl=0            # resting lenth 
rlmax=.02       # maximum allowed stretch       
rlj=.000        # desired length jammed
rljmax=.001     # desired length jammed max
Springs=[]      # empty spring matrix

# In[Create Robots]
for i in range(nb):
    print(i)
    theta=i*2*np.pi/nb
    x=R1*np.cos(theta)
    y=.5*height
    z=R1*np.sin(theta)
    BOTS(nb,i,x,y,z,diameter,height,theta,rowr,body_floor,obj,my_system,material,Springs,k,rl,botcall,force)


# In[Create Interior]
for i in range(n.size):
    print(i)
    for j in range(n[i]):
        print(j)
        R2=diameter2*n[i]/(np.pi*2)
        x=R2*np.cos(j*2*np.pi/n[i])
        y=.5*height
        z=R2*np.sin(j*2*np.pi/n[i])
        Interior(x,y,z,i,diameter2,height,rowp,R2,material,obj,my_system,body_floor)

# In[IRRLICH SIMULATION]
myapplication = chronoirr.ChIrrApp(my_system,sim, chronoirr.dimension2du(1200,800))
myapplication.AddTypicalSky()
myapplication.AddTypicalLogo(chrono.GetChronoDataPath() + 'logo_pychrono_alpha.png')
myapplication.AddTypicalCamera(chronoirr.vector3df(0.75,0.75,1.5))
myapplication.AddLightWithShadow(chronoirr.vector3df(2,5,2),chronoirr.vector3df(2,2,2),10,2,10,120)
myapplication.DrawAll               
myapplication.AssetBindAll();
myapplication.AssetUpdateAll();
myapplication.AddShadowAll();
    
count=0
Fm=[]
templ=[]
# Time step
myapplication.SetTimestep(tstep)
myapplication.SetTryRealtime(False)
while(myapplication.GetDevice().run()):
        myapplication.BeginScene()
        myapplication.DrawAll()
        print ('time=', my_system.GetChTime())
        t=tstep*count  
        count=count+1
        Controller(my_system,force,mag,botcall,i,tset,Fm,nb,k,templ,t,rl,rlmax,Springs)
        myapplication.DoStep()
        myapplication.EndScene()
# Close the simulation if time ends
        if t > tend:
            myapplication.GetDevice().closeDevice()