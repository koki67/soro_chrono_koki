# -*- coding: utf-8 -*-
"""
Created on Fri Jan  3 11:54:41 2020

@author: dmulr
"""
import pychrono.core as chrono
import pychrono.irrlicht as chronoirr
import pychrono.postprocess as postprocess
import os
import numpy as np
import timeit


# In[Create material]
def Material(mu_f,mu_b,mu_r,mu_s,C,Ct,Cr,Cs):
    material = chrono.ChMaterialSurfaceNSC()
    material.SetFriction(mu_f)
    material.SetDampingF(mu_b)
    material.SetCompliance (C)
    material.SetComplianceT(Ct)
    material.SetRollingFriction(mu_r)
    material.SetSpinningFriction(mu_s)
    material.SetComplianceRolling(Cr)
    material.SetComplianceSpinning(Cs)
    return material


# In[Create Floor]
def Floor(material,length,tall):
    body_floor = chrono.ChBody()
    body_floor.SetBodyFixed(True)
    body_floor.SetPos(chrono.ChVectorD(0, -tall, 0 ))
    body_floor.SetMaterialSurface(material)
    body_floor.GetCollisionModel().ClearModel()
    body_floor.GetCollisionModel().AddBox(length, tall, length) # hemi sizes
    body_floor.GetCollisionModel().BuildModel()
    body_floor.SetCollide(True)
    body_floor_shape = chrono.ChBoxShape()
    body_floor_shape.GetBoxGeometry().Size = chrono.ChVectorD(length, tall, length)
    body_floor.GetAssets().push_back(body_floor_shape)
    body_floor_texture = chrono.ChTexture()
    body_floor_texture.SetTextureFilename(chrono.GetChronoDataPath() + 'aluminum.jpg')
    body_floor.GetAssets().push_back(body_floor_texture)
    return(body_floor)
    
    


def BOTS(nb,i,x,y,z,diameter,height,theta,rowr,body_floor,obj,my_system,material,Springs,k,rl,botcall,force):

    # Create bots    
    bot = chrono.ChBody()
    bot = chrono.ChBodyEasyCylinder(diameter/2, height,rowr)
    bot.SetPos(chrono.ChVectorD(x,y,z))
    bot.SetMaterialSurface(material)
    # rotate them
    rotation1 = chrono.ChQuaternionD()
    rotation1.Q_from_AngAxis(-theta, chrono.ChVectorD(0, 1, 0));  
    bot.SetRot(rotation1)
    # give ID number
    bot.SetId(i)
    
    # Apply forces to active bots
    if botcall[:,i]==1:
        myforcex = chrono.ChForce()
        bot.AddForce(myforcex)
        myforcex.SetMode(chrono.ChForce.FORCE)
        myforcex.SetDir(chrono.VECT_X)
        myforcex.SetVrelpoint(chrono.ChVectorD(x,.03*y,z))
        #myforcex.SetMforce(mag)
        force.append(myforcex)
        col_y = chrono.ChColorAsset()
        col_y.SetColor(chrono.ChColor(0.44, .11, 52))
        bot.AddAsset(col_y)
    # passive robots
    else:
        col_g = chrono.ChColorAsset()
        col_g.SetColor(chrono.ChColor(0, 1, 0))
    
    
    
    
    # collision model
    bot.GetCollisionModel().ClearModel()
    bot.GetCollisionModel().AddCylinder(diameter/2,diameter/2,height/2) # hemi sizes
    bot.GetCollisionModel().BuildModel()
    bot.SetCollide(True)
    bot.SetBodyFixed(False)
    pt=chrono.ChLinkMatePlane()
    pt.Initialize(body_floor,bot,False,chrono.ChVectorD(0,0,0),chrono.ChVectorD(0,0,0),chrono.ChVectorD(0,1, 0),chrono.ChVectorD(0,-1, 0))
    my_system.AddLink(pt)
    
    if i>=1:
        ground=chrono.ChLinkSpring()
        ground1=chrono.ChLinkSpring()
      # Identify points to be attatched to the springs 
        ground.SetName("ground")
        p1=0
        p2=diameter/2
        p3=0
        p4=-diameter/2
        h=0
        
        # Attatches first springs
        ground.Initialize(obj[i-1], bot,True,chrono.ChVectorD(p1,h,p2), chrono.ChVectorD(p3,h,p4),False)
        ground.Set_SpringF(k)
        ground.Set_SpringRestLength(rl)
        col1=chrono.ChColorAsset()
        col1.SetColor(chrono.ChColor(0,0,1))
        ground.AddAsset(col1)
        ground.AddAsset(chrono.ChPointPointSpring(.01,80,15))
        my_system.AddLink(ground)
        Springs.append(ground)
        # Attatch second springs
        ground1.Initialize(obj[i-1], bot,True,chrono.ChVectorD(p1,-h,p2), chrono.ChVectorD(p3,-h,p4),False)
        ground1.Set_SpringK(k)
        ground1.Set_SpringRestLength(rl)
        col1=chrono.ChColorAsset()
        col1.SetColor(chrono.ChColor(0,0,1))
        ground1.AddAsset(col1)
        ground1.AddAsset(chrono.ChPointPointSpring(.01,80,15))
        my_system.AddLink(ground1)
        Springs.append(ground1)
    # Last spring
    if i==nb-1:        
        ground=chrono.ChLinkSpring()
        ground.SetName("ground")
        ground.Initialize(bot, obj[0], True, chrono.ChVectorD(p1,h,p2), chrono.ChVectorD(p3,h,p4),False)
        ground.Set_SpringF(k)
        ground.Set_SpringRestLength(rl)
        col1=chrono.ChColorAsset()
        col1.SetColor(chrono.ChColor(0,0,1))
        ground.AddAsset(col1)
        ground.AddAsset(chrono.ChPointPointSpring(.01,80,15))
        my_system.AddLink(ground)
        Springs.append(ground) 
            



    my_system.Add(bot)
    obj.append(bot)
    
    return(obj,bot,my_system,Springs)
    
# In[Create robots]    
def Interior(x,y,z,i,diameter,height,rowp,R2,material,obj,my_system,body_floor):
    gran = chrono.ChBody()
    gran = chrono.ChBodyEasyCylinder(diameter/2, height,rowp)
    gran.SetPos(chrono.ChVectorD(x,y,z))
    gran.SetMaterialSurface(material)
    gran.SetId(i)
    gran.GetCollisionModel().ClearModel()
    gran.GetCollisionModel().AddCylinder(diameter/2,diameter/2,height/2) # hemi sizes
    gran.GetCollisionModel().BuildModel()
    gran.SetCollide(True)
    col_r = chrono.ChColorAsset()
    col_r.SetColor(chrono.ChColor(1, 0, 0))
    gran.AddAsset(col_r)
    pt=chrono.ChLinkMatePlane()
    pt.Initialize(body_floor,gran,False,chrono.ChVectorD(0,0,0),chrono.ChVectorD(0,0,0),chrono.ChVectorD(0,1, 0),chrono.ChVectorD(0,-1, 0))
    my_system.AddLink(pt)
    
    my_system.Add(gran)
    obj.append(gran)
    return my_system,obj     



# In[Call springs to be jammed]
def Jamsprings(k,rl,rlmax,Springs,t,tj,Fm,kj,rlj,rljmax,i,jamcall):
    if t>tj:
        if jamcall==1:
     
            k=kj

            rl=rlj
            rlmax=rljmax
        else:
            k=k
            rl=rl
            rlmax=rlmax
    else:
        k=k
        rl=rl
        rlmax=rlmax
        
    return(k,rlmax,rl)
# In[Set springs]   
def setSpring(k,rl,rlmax,Springs,Fm,templ,i):
    var1=Springs[i].Get_SpringLength()
    if var1<rl:
        Springs[i].Set_SpringF(0)
        var2=Springs[i].Get_SpringF()    
    if var1>rlmax:
        Springs[i].Set_SpringF(2*k)
        var2=Springs[i].Get_SpringF()    
    else:
        Springs[i].Set_SpringF(k)
        var2=Springs[i].Get_SpringF()    
  
    Fm.append(var2)
    templ.append(var1)
    return(Springs,Fm,templ)


# In[Grab controller]
def Controller(my_system,force,mag,botcall,i,tset,Fm,nb,k,templ,t,rl,rlmax,Springs):
    
    for i in range(nb):
        (Springs,Fm,templ)=setSpring(k,rl,rlmax,Springs,Fm,templ,i)
        # if past settling time  
        if t > tset:
            for i in range(len(force)):
                #force[i].SetDir(chrono.VECT_X)
                force[i].SetDir(chrono.ChVectorD(1,0,0))
                force[i].SetMforce(mag)

    return force,Springs,templ,Fm
    
   # Apply forces to robots
    for i in range(len(force)):
        force[i].SetDir(chrono.VECT_X)
    
    for i in range(nb):
        (k1,rlmax1,rl1)=Jamsprings(k,rl,rlmax,Springs,t,tj,Fm,kj,rlj,rljmax,i,jamcall[0,i])
         
        
           
        (Springs,Fm,templ)=setSpring(k1,rl1,rlmax1,Springs,Fm,templ,i)   
        # if past settling time  
        if t > tset:
            for i in range(len(force)):
                force[i].SetMforce(mag)
            for j in range(len(forceb)):
                forceb[j].SetMforce(-mag2)
                forceb[j].SetDir(chrono.ChVectorD(1,0,0))
        # if past jamming time 
        if t > tj:
            for i in range(len(force)):
                force[i].SetMforce(mag)
        
            for j in range(len(forceb)):
                forceb[j].SetMforce(-mag2) 
                forceb[j].SetDir(chrono.ChVectorD(1,0,0))
        # if past pulling time
        if t>tp:
            for i in range(len(force)):
                force[i].SetMforce(mag)
            for j in range(len(forceb)):
                forceb[j].SetMforce(1*magf)
                forceb[j].SetDir(chrono.ChVectorD(1,0,0))   
    return force,Springs,templ,Fm
# In[Extract Data]
def ExtractData(obj,nb,nt,Xpos,Ypos,Zpos,Xforce,Yforce,Zforce,rott0,rott1,rott2,rott3,Xvel,Yvel,Zvel,ballp,Balls,Springs,Fm,templ):
    
    
    for i in range(nb):
        
        var1=(Springs[i].Get_SpringLength()-Springs[i].Get_SpringRestLength())
        var2=Springs[i].Get_SpringK()
        var3=Springs[i].Get_SpringVelocity()
        var4=Springs[i].Get_SpringR()              
        Fm.append(var1*var2+var3*var4)
        templ.append(var1)
    
    for i in range(nt):
            
        tempx=obj[i].Get_Xforce()
        tempxx=obj[i].GetPos_dt()
        
        # Total forces
        Xforce.append(tempx.x)
        Yforce.append((tempx.y))
        Zforce.append(tempx.z)
            
        #positions
        Xpos.append(obj[i].GetPos().x)
        Ypos.append((obj[i].GetPos().y))
        Zpos.append(obj[i].GetPos().z)
        
        # Rotation positions
        rott0.append(obj[i].GetRot().e0)
        rott1.append(obj[i].GetRot().e1)
        rott2.append(obj[i].GetRot().e2)
        rott3.append(obj[i].GetRot().e3)
            
   
        # fill in the temporary velocity matrices
        Xvel.append(tempxx.x)
        Yvel.append(tempxx.y)
        Zvel.append(tempxx.z)

    # pull data for ball                      
    for i in range(len(Balls)):
        ballp.append(Balls[i].GetPos().x)   

    return (obj,Springs,Fm,ballp,Balls,templ,Xforce,Yforce,Zforce,Xpos,Ypos,Zpos,rott0,rott1,rott2,rott3,Xvel,Yvel,Zvel)
