# PyChrono script generated from SolidWorks using Chrono::SolidWorks add-in 
# Assembly: C:\Users\Amin\Documents\SolidCAD\sim2\SimAssem2.SLDASM


import pychrono as chrono 
import builtins 

shapes_dir = 'Sim_shapes/' 

if hasattr(builtins, 'exported_system_relpath'): 
    shapes_dir = builtins.exported_system_relpath + shapes_dir 

exported_items = [] 

body_0= chrono.ChBodyAuxRef()
body_0.SetName('ground')
body_0.SetBodyFixed(True)
exported_items.append(body_0)

# Rigid body part
body_1= chrono.ChBodyAuxRef()
body_1.SetName('robo_leg_link-5/link1-1')
body_1.SetPos(chrono.ChVectorD(1.96339440615989,0.0780000000000083,-2.35008741480099))
body_1.SetRot(chrono.ChQuaternionD(-1.31036243926746e-14,0.977166871801662,3.16706058140986e-15,0.212473303389753))
body_1.SetMass(0.02)
body_1.SetInertiaXX(chrono.ChVectorD(2.1102890321418e-05,3.42254939964149e-05,1.66734961647356e-05))
body_1.SetInertiaXY(chrono.ChVectorD(1.44186233077584e-08,8.15219838791517e-11,1.07420085359569e-11))
body_1.SetFrame_COG_to_REF(chrono.ChFrameD(chrono.ChVectorD(-2.37534873691438e-05,0.0334739058996041,1.03286017725802e-08),chrono.ChQuaternionD(1,0,0,0)))

# Visualization shape 
body_1_1_shape = chrono.ChObjShapeFile() 
body_1_1_shape.SetFilename(shapes_dir +'body_1_1.obj') 
body_1_1_level = chrono.ChAssetLevel() 
body_1_1_level.GetFrame().SetPos(chrono.ChVectorD(0,0,0)) 
body_1_1_level.GetFrame().SetRot(chrono.ChQuaternionD(1,0,0,0)) 
body_1_1_level.GetAssets().push_back(body_1_1_shape) 
body_1.GetAssets().push_back(body_1_1_level) 

exported_items.append(body_1)



# Rigid body part
body_2= chrono.ChBodyAuxRef()
body_2.SetName('robo_leg_link-5/link2-2')
body_2.SetPos(chrono.ChVectorD(1.96339440615989,0.0780000000000092,-2.350087414801))
body_2.SetRot(chrono.ChQuaternionD(-8.87629086718009e-18,0.977166871801662,6.85249654946303e-16,0.212473303389753))
body_2.SetMass(0.02)
body_2.SetInertiaXX(chrono.ChVectorD(1.04983041023413e-05,2.08152025768899e-05,1.23483128699975e-05))
body_2.SetInertiaXY(chrono.ChVectorD(-2.4713143785015e-11,-1.47996614267629e-11,-7.2032729945964e-11))
body_2.SetFrame_COG_to_REF(chrono.ChFrameD(chrono.ChVectorD(1.363578295109e-07,0.0335000115691779,0.0764730062719303),chrono.ChQuaternionD(1,0,0,0)))

# Visualization shape 
body_2_1_shape = chrono.ChObjShapeFile() 
body_2_1_shape.SetFilename(shapes_dir +'body_2_1.obj') 
body_2_1_level = chrono.ChAssetLevel() 
body_2_1_level.GetFrame().SetPos(chrono.ChVectorD(0,0,0)) 
body_2_1_level.GetFrame().SetRot(chrono.ChQuaternionD(1,0,0,0)) 
body_2_1_level.GetAssets().push_back(body_2_1_shape) 
body_2.GetAssets().push_back(body_2_1_level) 

# Collision shapes 
body_2.GetCollisionModel().ClearModel()
mr = chrono.ChMatrix33D()
mr[0,0]=1; mr[1,0]=0; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=1; mr[2,1]=0 
mr[0,2]=0; mr[1,2]=0; mr[2,2]=1 
body_2.GetCollisionModel().AddBox(0.0113982379521279,0.0140083638039784,0.013,chrono.ChVectorD(-0.000134890389966315,0.0335,0.0511),mr)
body_2.GetCollisionModel().BuildModel()
body_2.SetCollide(True)

exported_items.append(body_2)



# Rigid body part
body_3= chrono.ChBodyAuxRef()
body_3.SetName('robo_leg_link-5/leg-1')
body_3.SetPos(chrono.ChVectorD(1.95861595884145,0.0170000000000082,-2.34443530747498))
body_3.SetRot(chrono.ChQuaternionD(0.0961724845299989,0.871336543311411,0.442298229294181,0.189461758338407))
body_3.SetMass(0.01)
body_3.SetInertiaXX(chrono.ChVectorD(1.89110723602526e-07,5.11889399343246e-07,5.94333456279105e-07))
body_3.SetInertiaXY(chrono.ChVectorD(-3.88054984756532e-11,-6.97451787573893e-24,-2.11470064202402e-24))
body_3.SetFrame_COG_to_REF(chrono.ChFrameD(chrono.ChVectorD(-5.02229192063373e-07,0.00570468434844058,0.004),chrono.ChQuaternionD(1,0,0,0)))

# Visualization shape 
body_3_1_shape = chrono.ChObjShapeFile() 
body_3_1_shape.SetFilename(shapes_dir +'body_3_1.obj') 
body_3_1_level = chrono.ChAssetLevel() 
body_3_1_level.GetFrame().SetPos(chrono.ChVectorD(0,0,0)) 
body_3_1_level.GetFrame().SetRot(chrono.ChQuaternionD(1,0,0,0)) 
body_3_1_level.GetAssets().push_back(body_3_1_shape) 
body_3.GetAssets().push_back(body_3_1_level) 

# Collision shapes 
body_3.GetCollisionModel().ClearModel()
mr = chrono.ChMatrix33D()
mr[0,0]=0; mr[1,0]=1; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=0; mr[2,1]=-1 
mr[0,2]=-1; mr[1,2]=0; mr[2,2]=0 
body_3.GetCollisionModel().AddCylinder(0.00178981093410736,0.00178981093410736,0.00400000000000001,chrono.ChVectorD(0.00836413257948577,0.0092893103184674,0.00399999999999999),mr)
mr = chrono.ChMatrix33D()
mr[0,0]=0; mr[1,0]=1; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=0; mr[2,1]=-1 
mr[0,2]=-1; mr[1,2]=0; mr[2,2]=0 
body_3.GetCollisionModel().AddCylinder(0.00178981093410736,0.00178981093410736,0.00400000000000001,chrono.ChVectorD(0.00195543081300292,0.0123461042574392,0.00399999999999999),mr)
mr = chrono.ChMatrix33D()
mr[0,0]=0; mr[1,0]=1; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=0; mr[2,1]=-1 
mr[0,2]=-1; mr[1,2]=0; mr[2,2]=0 
body_3.GetCollisionModel().AddCylinder(0.00178981093410736,0.00178981093410736,0.00400000000000001,chrono.ChVectorD(-0.00508420803844748,0.0114193182205325,0.00399999999999999),mr)
mr = chrono.ChMatrix33D()
mr[0,0]=0; mr[1,0]=1; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=0; mr[2,1]=-1 
mr[0,2]=-1; mr[1,2]=0; mr[2,2]=0 
body_3.GetCollisionModel().AddCylinder(0.00178981093410736,0.00178981093410736,0.00400000000000001,chrono.ChVectorD(-0.0104833820993178,0.00680798793768785,0.00399999999999999),mr)
mr = chrono.ChMatrix33D()
mr[0,0]=0; mr[1,0]=1; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=0; mr[2,1]=-1 
mr[0,2]=-1; mr[1,2]=0; mr[2,2]=0 
body_3.GetCollisionModel().AddCylinder(0.00178981093410736,0.00178981093410736,0.00400000000000001,chrono.ChVectorD(-0.0125,0,0.00399999999999999),mr)
mr = chrono.ChMatrix33D()
mr[0,0]=0; mr[1,0]=1; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=0; mr[2,1]=-1 
mr[0,2]=-1; mr[1,2]=0; mr[2,2]=0 
body_3.GetCollisionModel().AddCylinder(0.00178981093410736,0.00178981093410736,0.00400000000000001,chrono.ChVectorD(0.0120740728286134,0.00323523806378145,0.00399999999999999),mr)
body_3.GetCollisionModel().BuildModel()
body_3.SetCollide(True)

exported_items.append(body_3)



# Rigid body part
body_4= chrono.ChBodyAuxRef()
body_4.SetName('robo_leg_link-5/link2-1')
body_4.SetPos(chrono.ChVectorD(1.96339440615989,0.0110000000000069,-2.350087414801))
body_4.SetRot(chrono.ChQuaternionD(0.977166871801662,1.39357766614727e-16,-0.212473303389753,-1.06515490406161e-17))
body_4.SetMass(0.02)
body_4.SetInertiaXX(chrono.ChVectorD(1.04983041023413e-05,2.08152025768899e-05,1.23483128699975e-05))
body_4.SetInertiaXY(chrono.ChVectorD(-2.4713143785015e-11,-1.47996614267629e-11,-7.2032729945964e-11))
body_4.SetFrame_COG_to_REF(chrono.ChFrameD(chrono.ChVectorD(1.363578295109e-07,0.0335000115691779,0.0764730062719303),chrono.ChQuaternionD(1,0,0,0)))

# Visualization shape 
body_2_1_shape = chrono.ChObjShapeFile() 
body_2_1_shape.SetFilename(shapes_dir +'body_2_1.obj') 
body_2_1_level = chrono.ChAssetLevel() 
body_2_1_level.GetFrame().SetPos(chrono.ChVectorD(0,0,0)) 
body_2_1_level.GetFrame().SetRot(chrono.ChQuaternionD(1,0,0,0)) 
body_2_1_level.GetAssets().push_back(body_2_1_shape) 
body_4.GetAssets().push_back(body_2_1_level) 

# Collision shapes 
body_4.GetCollisionModel().ClearModel()
mr = chrono.ChMatrix33D()
mr[0,0]=1; mr[1,0]=0; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=1; mr[2,1]=0 
mr[0,2]=0; mr[1,2]=0; mr[2,2]=1 
body_4.GetCollisionModel().AddBox(0.0113982379521279,0.0140083638039784,0.013,chrono.ChVectorD(-0.000134890389966315,0.0335,0.0511),mr)
body_4.GetCollisionModel().BuildModel()
body_4.SetCollide(True)

exported_items.append(body_4)



# Rigid body part
body_5= chrono.ChBodyAuxRef()
body_5.SetName('robo_leg_link-5/single_bot1-1')
body_5.SetPos(chrono.ChVectorD(2.05513776937894,-0.11469261684464,-2.29032763036226))
body_5.SetRot(chrono.ChQuaternionD(0.533861627607585,-0.578496118283883,-0.455607264557997,-0.415639295760927))
body_5.SetMass(0.35)
body_5.SetInertiaXX(chrono.ChVectorD(0.000243287100418674,0.000250960901009867,0.000110709858882453))
body_5.SetInertiaXY(chrono.ChVectorD(4.8045401853377e-06,-2.33417013594839e-05,-1.75548754038152e-06))
body_5.SetFrame_COG_to_REF(chrono.ChFrameD(chrono.ChVectorD(-0.0658557785731179,-0.0789654441773446,0.173168884203731),chrono.ChQuaternionD(1,0,0,0)))

# Visualization shape 
body_5_1_shape = chrono.ChObjShapeFile() 
body_5_1_shape.SetFilename(shapes_dir +'body_5_1.obj') 
body_5_1_level = chrono.ChAssetLevel() 
body_5_1_level.GetFrame().SetPos(chrono.ChVectorD(0,0,0)) 
body_5_1_level.GetFrame().SetRot(chrono.ChQuaternionD(1,0,0,0)) 
body_5_1_level.GetAssets().push_back(body_5_1_shape) 
body_5.GetAssets().push_back(body_5_1_level) 

# Auxiliary marker (coordinate system feature)
marker_5_1 =chrono.ChMarker()
marker_5_1.SetName('My_marker')
body_5.AddMarker(marker_5_1)
marker_5_1.Impose_Abs_Coord(chrono.ChCoordsysD(chrono.ChVectorD(1.95833774553134,0.0170000000000083,-2.34382580164721),chrono.ChQuaternionD(0.976968826148917,-0.00427755548126929,-0.212430240720244,0.0196725209327631)))

# Collision shapes 
body_5.GetCollisionModel().ClearModel()
body_5.GetCollisionModel().AddSphere(0.00450000000000001, chrono.ChVectorD(-0.0490106583570424,-0.0813717222701078,0.130238444426058))
body_5.GetCollisionModel().AddSphere(0.00448981850569662, chrono.ChVectorD(-0.0783561237483722,-0.0580446528925914,0.133040222294353))
body_5.GetCollisionModel().AddSphere(0.00450000000000001, chrono.ChVectorD(-0.0817763692558815,-0.0926100044370168,0.13281194772533))
mr = chrono.ChMatrix33D()
mr[0,0]=0; mr[1,0]=0.996410323712302; mr[2,0]=0.0148275661003368 
mr[0,1]=-0.0833463261523019; mr[1,1]=0.0148275661003368; mr[2,1]=-0.996410323712302 
mr[0,2]=-0.993053389916914; mr[1,2]=-0.00123582316024348; mr[2,2]=0.0830471398216463 
body_5.GetCollisionModel().AddCylinder(0.038575,0.038575,0.0163750000000019,chrono.ChVectorD(-0.0652414174348167,-0.0773807616988142,0.179201155078968),mr)
body_5.GetCollisionModel().BuildModel()
body_5.SetCollide(True)

exported_items.append(body_5)



# Rigid body part
body_6= chrono.ChBodyAuxRef()
body_6.SetName('robo_leg_link-6/link2-1')
body_6.SetPos(chrono.ChVectorD(2.07118562550692,0.0110000004582814,-2.41663763936066))
body_6.SetRot(chrono.ChQuaternionD(0.694136983037166,3.74866240749758e-16,-0.719842933409824,5.34809170136321e-16))
body_6.SetMass(0.02)
body_6.SetInertiaXX(chrono.ChVectorD(1.04983041023413e-05,2.08152025768899e-05,1.23483128699975e-05))
body_6.SetInertiaXY(chrono.ChVectorD(-2.4713143785015e-11,-1.47996614267629e-11,-7.2032729945964e-11))
body_6.SetFrame_COG_to_REF(chrono.ChFrameD(chrono.ChVectorD(1.363578295109e-07,0.0335000115691779,0.0764730062719303),chrono.ChQuaternionD(1,0,0,0)))

# Visualization shape 
body_2_1_shape = chrono.ChObjShapeFile() 
body_2_1_shape.SetFilename(shapes_dir +'body_2_1.obj') 
body_2_1_level = chrono.ChAssetLevel() 
body_2_1_level.GetFrame().SetPos(chrono.ChVectorD(0,0,0)) 
body_2_1_level.GetFrame().SetRot(chrono.ChQuaternionD(1,0,0,0)) 
body_2_1_level.GetAssets().push_back(body_2_1_shape) 
body_6.GetAssets().push_back(body_2_1_level) 

# Collision shapes 
body_6.GetCollisionModel().ClearModel()
mr = chrono.ChMatrix33D()
mr[0,0]=1; mr[1,0]=0; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=1; mr[2,1]=0 
mr[0,2]=0; mr[1,2]=0; mr[2,2]=1 
body_6.GetCollisionModel().AddBox(0.0113982379521279,0.0140083638039784,0.013,chrono.ChVectorD(-0.000134890389966315,0.0335,0.0511),mr)
body_6.GetCollisionModel().BuildModel()
body_6.SetCollide(True)

exported_items.append(body_6)



# Rigid body part
body_7= chrono.ChBodyAuxRef()
body_7.SetName('robo_leg_link-6/link1-1')
body_7.SetPos(chrono.ChVectorD(2.07118562699141,0.0780000000000098,-2.41663763677774))
body_7.SetRot(chrono.ChQuaternionD(-6.69460182575684e-15,0.694136983037166,1.30060352964758e-14,0.719842933409824))
body_7.SetMass(0.02)
body_7.SetInertiaXX(chrono.ChVectorD(2.1102890321418e-05,3.42254939964149e-05,1.66734961647356e-05))
body_7.SetInertiaXY(chrono.ChVectorD(1.44186233077584e-08,8.15219838791517e-11,1.07420085359569e-11))
body_7.SetFrame_COG_to_REF(chrono.ChFrameD(chrono.ChVectorD(-2.37534873691438e-05,0.0334739058996041,1.03286017725802e-08),chrono.ChQuaternionD(1,0,0,0)))

# Visualization shape 
body_1_1_shape = chrono.ChObjShapeFile() 
body_1_1_shape.SetFilename(shapes_dir +'body_1_1.obj') 
body_1_1_level = chrono.ChAssetLevel() 
body_1_1_level.GetFrame().SetPos(chrono.ChVectorD(0,0,0)) 
body_1_1_level.GetFrame().SetRot(chrono.ChQuaternionD(1,0,0,0)) 
body_1_1_level.GetAssets().push_back(body_1_1_shape) 
body_7.GetAssets().push_back(body_1_1_level) 

exported_items.append(body_7)



# Rigid body part
body_8= chrono.ChBodyAuxRef()
body_8.SetName('robo_leg_link-6/single_bot1-1')
body_8.SetPos(chrono.ChVectorD(2.05099261208623,-0.114692616844637,-2.30902574765788))
body_8.SetRot(chrono.ChQuaternionD(-0.190482820594726,0.249803611354101,0.675501674319258,0.667092151658679))
body_8.SetMass(0.35)
body_8.SetInertiaXX(chrono.ChVectorD(0.000243287100418674,0.000250960901009867,0.000110709858882453))
body_8.SetInertiaXY(chrono.ChVectorD(4.8045401853377e-06,-2.33417013594839e-05,-1.75548754038152e-06))
body_8.SetFrame_COG_to_REF(chrono.ChFrameD(chrono.ChVectorD(-0.0658557785731179,-0.0789654441773446,0.173168884203731),chrono.ChQuaternionD(1,0,0,0)))

# Visualization shape 
body_5_1_shape = chrono.ChObjShapeFile() 
body_5_1_shape.SetFilename(shapes_dir +'body_5_1.obj') 
body_5_1_level = chrono.ChAssetLevel() 
body_5_1_level.GetFrame().SetPos(chrono.ChVectorD(0,0,0)) 
body_5_1_level.GetFrame().SetRot(chrono.ChQuaternionD(1,0,0,0)) 
body_5_1_level.GetAssets().push_back(body_5_1_shape) 
body_8.GetAssets().push_back(body_5_1_level) 

# Auxiliary marker (coordinate system feature)
marker_8_1 =chrono.ChMarker()
marker_8_1.SetName('My_marker')
body_8.AddMarker(marker_8_1)
marker_8_1.Impose_Abs_Coord(chrono.ChCoordsysD(chrono.ChVectorD(2.06346747395125,0.0170000000000099,-2.41891968183615),chrono.ChQuaternionD(0.69399629999124,-0.0144920234040482,-0.719697040453636,0.0139745060163863)))

# Collision shapes 
body_8.GetCollisionModel().ClearModel()
body_8.GetCollisionModel().AddSphere(0.00450000000000001, chrono.ChVectorD(-0.0490106583570424,-0.0813717222701078,0.130238444426058))
body_8.GetCollisionModel().AddSphere(0.00448981850569662, chrono.ChVectorD(-0.0783561237483722,-0.0580446528925914,0.133040222294353))
body_8.GetCollisionModel().AddSphere(0.00450000000000001, chrono.ChVectorD(-0.0817763692558815,-0.0926100044370168,0.13281194772533))
mr = chrono.ChMatrix33D()
mr[0,0]=0; mr[1,0]=0.996410323712302; mr[2,0]=0.0148275661003368 
mr[0,1]=-0.0833463261523019; mr[1,1]=0.0148275661003368; mr[2,1]=-0.996410323712302 
mr[0,2]=-0.993053389916914; mr[1,2]=-0.00123582316024348; mr[2,2]=0.0830471398216463 
body_8.GetCollisionModel().AddCylinder(0.038575,0.038575,0.0163750000000019,chrono.ChVectorD(-0.0652414174348167,-0.0773807616988142,0.179201155078968),mr)
body_8.GetCollisionModel().BuildModel()
body_8.SetCollide(True)

exported_items.append(body_8)



# Rigid body part
body_9= chrono.ChBodyAuxRef()
body_9.SetName('robo_leg_link-6/link2-2')
body_9.SetPos(chrono.ChVectorD(2.07118562548952,0.0780000003984775,-2.41663763936129))
body_9.SetRot(chrono.ChQuaternionD(-8.91649631220314e-17,0.694136983037166,7.85615621021141e-16,0.719842933409824))
body_9.SetMass(0.02)
body_9.SetInertiaXX(chrono.ChVectorD(1.04983041023413e-05,2.08152025768899e-05,1.23483128699975e-05))
body_9.SetInertiaXY(chrono.ChVectorD(-2.4713143785015e-11,-1.47996614267629e-11,-7.2032729945964e-11))
body_9.SetFrame_COG_to_REF(chrono.ChFrameD(chrono.ChVectorD(1.363578295109e-07,0.0335000115691779,0.0764730062719303),chrono.ChQuaternionD(1,0,0,0)))

# Visualization shape 
body_2_1_shape = chrono.ChObjShapeFile() 
body_2_1_shape.SetFilename(shapes_dir +'body_2_1.obj') 
body_2_1_level = chrono.ChAssetLevel() 
body_2_1_level.GetFrame().SetPos(chrono.ChVectorD(0,0,0)) 
body_2_1_level.GetFrame().SetRot(chrono.ChQuaternionD(1,0,0,0)) 
body_2_1_level.GetAssets().push_back(body_2_1_shape) 
body_9.GetAssets().push_back(body_2_1_level) 

# Collision shapes 
body_9.GetCollisionModel().ClearModel()
mr = chrono.ChMatrix33D()
mr[0,0]=1; mr[1,0]=0; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=1; mr[2,1]=0 
mr[0,2]=0; mr[1,2]=0; mr[2,2]=1 
body_9.GetCollisionModel().AddBox(0.0113982379521279,0.0140083638039784,0.013,chrono.ChVectorD(-0.000134890389966315,0.0335,0.0511),mr)
body_9.GetCollisionModel().BuildModel()
body_9.SetCollide(True)

exported_items.append(body_9)



# Rigid body part
body_10= chrono.ChBodyAuxRef()
body_10.SetName('robo_leg_link-6/leg-1')
body_10.SetPos(chrono.ChVectorD(2.06413703121801,0.0170000000000098,-2.41889532887879))
body_10.SetRot(chrono.ChQuaternionD(0.325824886709504,0.618959690423917,0.314189517409903,0.641881602774842))
body_10.SetMass(0.01)
body_10.SetInertiaXX(chrono.ChVectorD(1.89110723602526e-07,5.11889399343246e-07,5.94333456279105e-07))
body_10.SetInertiaXY(chrono.ChVectorD(-3.88054984756532e-11,-6.97451787573893e-24,-2.11470064202402e-24))
body_10.SetFrame_COG_to_REF(chrono.ChFrameD(chrono.ChVectorD(-5.02229192063373e-07,0.00570468434844058,0.004),chrono.ChQuaternionD(1,0,0,0)))

# Visualization shape 
body_3_1_shape = chrono.ChObjShapeFile() 
body_3_1_shape.SetFilename(shapes_dir +'body_3_1.obj') 
body_3_1_level = chrono.ChAssetLevel() 
body_3_1_level.GetFrame().SetPos(chrono.ChVectorD(0,0,0)) 
body_3_1_level.GetFrame().SetRot(chrono.ChQuaternionD(1,0,0,0)) 
body_3_1_level.GetAssets().push_back(body_3_1_shape) 
body_10.GetAssets().push_back(body_3_1_level) 

# Collision shapes 
body_10.GetCollisionModel().ClearModel()
mr = chrono.ChMatrix33D()
mr[0,0]=0; mr[1,0]=1; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=0; mr[2,1]=-1 
mr[0,2]=-1; mr[1,2]=0; mr[2,2]=0 
body_10.GetCollisionModel().AddCylinder(0.00178981093410736,0.00178981093410736,0.00400000000000001,chrono.ChVectorD(0.00836413257948577,0.0092893103184674,0.00399999999999999),mr)
mr = chrono.ChMatrix33D()
mr[0,0]=0; mr[1,0]=1; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=0; mr[2,1]=-1 
mr[0,2]=-1; mr[1,2]=0; mr[2,2]=0 
body_10.GetCollisionModel().AddCylinder(0.00178981093410736,0.00178981093410736,0.00400000000000001,chrono.ChVectorD(0.00195543081300292,0.0123461042574392,0.00399999999999999),mr)
mr = chrono.ChMatrix33D()
mr[0,0]=0; mr[1,0]=1; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=0; mr[2,1]=-1 
mr[0,2]=-1; mr[1,2]=0; mr[2,2]=0 
body_10.GetCollisionModel().AddCylinder(0.00178981093410736,0.00178981093410736,0.00400000000000001,chrono.ChVectorD(-0.00508420803844748,0.0114193182205325,0.00399999999999999),mr)
mr = chrono.ChMatrix33D()
mr[0,0]=0; mr[1,0]=1; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=0; mr[2,1]=-1 
mr[0,2]=-1; mr[1,2]=0; mr[2,2]=0 
body_10.GetCollisionModel().AddCylinder(0.00178981093410736,0.00178981093410736,0.00400000000000001,chrono.ChVectorD(-0.0104833820993178,0.00680798793768785,0.00399999999999999),mr)
mr = chrono.ChMatrix33D()
mr[0,0]=0; mr[1,0]=1; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=0; mr[2,1]=-1 
mr[0,2]=-1; mr[1,2]=0; mr[2,2]=0 
body_10.GetCollisionModel().AddCylinder(0.00178981093410736,0.00178981093410736,0.00400000000000001,chrono.ChVectorD(-0.0125,0,0.00399999999999999),mr)
mr = chrono.ChMatrix33D()
mr[0,0]=0; mr[1,0]=1; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=0; mr[2,1]=-1 
mr[0,2]=-1; mr[1,2]=0; mr[2,2]=0 
body_10.GetCollisionModel().AddCylinder(0.00178981093410736,0.00178981093410736,0.00400000000000001,chrono.ChVectorD(0.0120740728286134,0.00323523806378145,0.00399999999999999),mr)
body_10.GetCollisionModel().BuildModel()
body_10.SetCollide(True)

exported_items.append(body_10)



# Rigid body part
body_11= chrono.ChBodyAuxRef()
body_11.SetName('robo_leg_link-10/single_bot1-1')
body_11.SetPos(chrono.ChVectorD(2.38036127105806,-0.114692616844656,-2.06712020442964))
body_11.SetRot(chrono.ChQuaternionD(0.331642696816139,-0.283096286710058,0.618546149125804,0.653659143209314))
body_11.SetMass(0.35)
body_11.SetInertiaXX(chrono.ChVectorD(0.000243287100418674,0.000250960901009867,0.000110709858882453))
body_11.SetInertiaXY(chrono.ChVectorD(4.8045401853377e-06,-2.33417013594839e-05,-1.75548754038152e-06))
body_11.SetFrame_COG_to_REF(chrono.ChFrameD(chrono.ChVectorD(-0.0658557785731179,-0.0789654441773446,0.173168884203731),chrono.ChQuaternionD(1,0,0,0)))

# Visualization shape 
body_5_1_shape = chrono.ChObjShapeFile() 
body_5_1_shape.SetFilename(shapes_dir +'body_5_1.obj') 
body_5_1_level = chrono.ChAssetLevel() 
body_5_1_level.GetFrame().SetPos(chrono.ChVectorD(0,0,0)) 
body_5_1_level.GetFrame().SetRot(chrono.ChQuaternionD(1,0,0,0)) 
body_5_1_level.GetAssets().push_back(body_5_1_shape) 
body_11.GetAssets().push_back(body_5_1_level) 

# Auxiliary marker (coordinate system feature)
marker_11_1 =chrono.ChMarker()
marker_11_1.SetName('My_marker')
body_11.AddMarker(marker_11_1)
marker_11_1.Impose_Abs_Coord(chrono.ChCoordsysD(chrono.ChVectorD(2.49063957972862,0.0169999999999912,-2.05869443535556),chrono.ChQuaternionD(-0.000210600360624374,0.0201322016372249,0.999797304500639,-4.2407084953085E-06)))

# Collision shapes 
body_11.GetCollisionModel().ClearModel()
body_11.GetCollisionModel().AddSphere(0.00450000000000001, chrono.ChVectorD(-0.0490106583570424,-0.0813717222701078,0.130238444426058))
body_11.GetCollisionModel().AddSphere(0.00448981850569662, chrono.ChVectorD(-0.0783561237483722,-0.0580446528925914,0.133040222294353))
body_11.GetCollisionModel().AddSphere(0.00450000000000001, chrono.ChVectorD(-0.0817763692558815,-0.0926100044370168,0.13281194772533))
mr = chrono.ChMatrix33D()
mr[0,0]=0; mr[1,0]=0.996410323712302; mr[2,0]=0.0148275661003368 
mr[0,1]=-0.0833463261523019; mr[1,1]=0.0148275661003368; mr[2,1]=-0.996410323712302 
mr[0,2]=-0.993053389916914; mr[1,2]=-0.00123582316024348; mr[2,2]=0.0830471398216463 
body_11.GetCollisionModel().AddCylinder(0.038575,0.038575,0.0163750000000019,chrono.ChVectorD(-0.0652414174348167,-0.0773807616988142,0.179201155078968),mr)
body_11.GetCollisionModel().BuildModel()
body_11.SetCollide(True)

exported_items.append(body_11)



# Rigid body part
body_12= chrono.ChBodyAuxRef()
body_12.SetName('robo_leg_link-10/link1-1')
body_12.SetPos(chrono.ChVectorD(2.4886428642525,0.0779999999999912,-2.0508975934752))
body_12.SetRot(chrono.ChQuaternionD(4.00374187137848e-15,0.000210643052356596,1.29072103093551e-14,0.999999977814752))
body_12.SetMass(0.02)
body_12.SetInertiaXX(chrono.ChVectorD(2.1102890321418e-05,3.42254939964149e-05,1.66734961647356e-05))
body_12.SetInertiaXY(chrono.ChVectorD(1.44186233077584e-08,8.15219838791517e-11,1.07420085359569e-11))
body_12.SetFrame_COG_to_REF(chrono.ChFrameD(chrono.ChVectorD(-2.37534873691438e-05,0.0334739058996041,1.03286017725802e-08),chrono.ChQuaternionD(1,0,0,0)))

# Visualization shape 
body_1_1_shape = chrono.ChObjShapeFile() 
body_1_1_shape.SetFilename(shapes_dir +'body_1_1.obj') 
body_1_1_level = chrono.ChAssetLevel() 
body_1_1_level.GetFrame().SetPos(chrono.ChVectorD(0,0,0)) 
body_1_1_level.GetFrame().SetRot(chrono.ChQuaternionD(1,0,0,0)) 
body_1_1_level.GetAssets().push_back(body_1_1_shape) 
body_12.GetAssets().push_back(body_1_1_level) 

exported_items.append(body_12)



# Rigid body part
body_13= chrono.ChBodyAuxRef()
body_13.SetName('robo_leg_link-10/leg-1')
body_13.SetPos(chrono.ChVectorD(2.49063986199031,0.0169999999999911,-2.05802443541501))
body_13.SetRot(chrono.ChQuaternionD(0.452634715140653,0.00018782957301482,9.53443601104474e-05,0.891696007773414))
body_13.SetMass(0.01)
body_13.SetInertiaXX(chrono.ChVectorD(1.89110723602526e-07,5.11889399343246e-07,5.94333456279105e-07))
body_13.SetInertiaXY(chrono.ChVectorD(-3.88054984756532e-11,-6.97451787573893e-24,-2.11470064202402e-24))
body_13.SetFrame_COG_to_REF(chrono.ChFrameD(chrono.ChVectorD(-5.02229192063373e-07,0.00570468434844058,0.004),chrono.ChQuaternionD(1,0,0,0)))

# Visualization shape 
body_3_1_shape = chrono.ChObjShapeFile() 
body_3_1_shape.SetFilename(shapes_dir +'body_3_1.obj') 
body_3_1_level = chrono.ChAssetLevel() 
body_3_1_level.GetFrame().SetPos(chrono.ChVectorD(0,0,0)) 
body_3_1_level.GetFrame().SetRot(chrono.ChQuaternionD(1,0,0,0)) 
body_3_1_level.GetAssets().push_back(body_3_1_shape) 
body_13.GetAssets().push_back(body_3_1_level) 

# Collision shapes 
body_13.GetCollisionModel().ClearModel()
mr = chrono.ChMatrix33D()
mr[0,0]=0; mr[1,0]=1; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=0; mr[2,1]=-1 
mr[0,2]=-1; mr[1,2]=0; mr[2,2]=0 
body_13.GetCollisionModel().AddCylinder(0.00178981093410736,0.00178981093410736,0.00400000000000001,chrono.ChVectorD(0.00836413257948577,0.0092893103184674,0.00399999999999999),mr)
mr = chrono.ChMatrix33D()
mr[0,0]=0; mr[1,0]=1; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=0; mr[2,1]=-1 
mr[0,2]=-1; mr[1,2]=0; mr[2,2]=0 
body_13.GetCollisionModel().AddCylinder(0.00178981093410736,0.00178981093410736,0.00400000000000001,chrono.ChVectorD(0.00195543081300292,0.0123461042574392,0.00399999999999999),mr)
mr = chrono.ChMatrix33D()
mr[0,0]=0; mr[1,0]=1; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=0; mr[2,1]=-1 
mr[0,2]=-1; mr[1,2]=0; mr[2,2]=0 
body_13.GetCollisionModel().AddCylinder(0.00178981093410736,0.00178981093410736,0.00400000000000001,chrono.ChVectorD(-0.00508420803844748,0.0114193182205325,0.00399999999999999),mr)
mr = chrono.ChMatrix33D()
mr[0,0]=0; mr[1,0]=1; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=0; mr[2,1]=-1 
mr[0,2]=-1; mr[1,2]=0; mr[2,2]=0 
body_13.GetCollisionModel().AddCylinder(0.00178981093410736,0.00178981093410736,0.00400000000000001,chrono.ChVectorD(-0.0104833820993178,0.00680798793768785,0.00399999999999999),mr)
mr = chrono.ChMatrix33D()
mr[0,0]=0; mr[1,0]=1; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=0; mr[2,1]=-1 
mr[0,2]=-1; mr[1,2]=0; mr[2,2]=0 
body_13.GetCollisionModel().AddCylinder(0.00178981093410736,0.00178981093410736,0.00400000000000001,chrono.ChVectorD(-0.0125,0,0.00399999999999999),mr)
mr = chrono.ChMatrix33D()
mr[0,0]=0; mr[1,0]=1; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=0; mr[2,1]=-1 
mr[0,2]=-1; mr[1,2]=0; mr[2,2]=0 
body_13.GetCollisionModel().AddCylinder(0.00178981093410736,0.00178981093410736,0.00400000000000001,chrono.ChVectorD(0.0120740728286134,0.00323523806378145,0.00399999999999999),mr)
body_13.GetCollisionModel().BuildModel()
body_13.SetCollide(True)

exported_items.append(body_13)



# Rigid body part
body_14= chrono.ChBodyAuxRef()
body_14.SetName('robo_leg_link-10/link2-1')
body_14.SetPos(chrono.ChVectorD(2.48864286980112,0.0109999952145798,-2.05089759076276))
body_14.SetRot(chrono.ChQuaternionD(-0.000210643052356528,4.00027242434955e-15,0.999999977814752,4.10262111170282e-16))
body_14.SetMass(0.02)
body_14.SetInertiaXX(chrono.ChVectorD(1.04983041023413e-05,2.08152025768899e-05,1.23483128699975e-05))
body_14.SetInertiaXY(chrono.ChVectorD(-2.4713143785015e-11,-1.47996614267629e-11,-7.2032729945964e-11))
body_14.SetFrame_COG_to_REF(chrono.ChFrameD(chrono.ChVectorD(1.363578295109e-07,0.0335000115691779,0.0764730062719303),chrono.ChQuaternionD(1,0,0,0)))

# Visualization shape 
body_2_1_shape = chrono.ChObjShapeFile() 
body_2_1_shape.SetFilename(shapes_dir +'body_2_1.obj') 
body_2_1_level = chrono.ChAssetLevel() 
body_2_1_level.GetFrame().SetPos(chrono.ChVectorD(0,0,0)) 
body_2_1_level.GetFrame().SetRot(chrono.ChQuaternionD(1,0,0,0)) 
body_2_1_level.GetAssets().push_back(body_2_1_shape) 
body_14.GetAssets().push_back(body_2_1_level) 

# Collision shapes 
body_14.GetCollisionModel().ClearModel()
mr = chrono.ChMatrix33D()
mr[0,0]=1; mr[1,0]=0; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=1; mr[2,1]=0 
mr[0,2]=0; mr[1,2]=0; mr[2,2]=1 
body_14.GetCollisionModel().AddBox(0.0113982379521279,0.0140083638039784,0.013,chrono.ChVectorD(-0.000134890389966315,0.0335,0.0511),mr)
body_14.GetCollisionModel().BuildModel()
body_14.SetCollide(True)

exported_items.append(body_14)



# Rigid body part
body_15= chrono.ChBodyAuxRef()
body_15.SetName('robo_leg_link-10/link2-2')
body_15.SetPos(chrono.ChVectorD(2.48864286506617,0.0779999994243682,-2.05089759368565))
body_15.SetRot(chrono.ChQuaternionD(3.97945574217601e-15,0.000210643052356528,-4.11996834684744e-16,0.999999977814752))
body_15.SetMass(0.02)
body_15.SetInertiaXX(chrono.ChVectorD(1.04983041023413e-05,2.08152025768899e-05,1.23483128699975e-05))
body_15.SetInertiaXY(chrono.ChVectorD(-2.4713143785015e-11,-1.47996614267629e-11,-7.2032729945964e-11))
body_15.SetFrame_COG_to_REF(chrono.ChFrameD(chrono.ChVectorD(1.363578295109e-07,0.0335000115691779,0.0764730062719303),chrono.ChQuaternionD(1,0,0,0)))

# Visualization shape 
body_2_1_shape = chrono.ChObjShapeFile() 
body_2_1_shape.SetFilename(shapes_dir +'body_2_1.obj') 
body_2_1_level = chrono.ChAssetLevel() 
body_2_1_level.GetFrame().SetPos(chrono.ChVectorD(0,0,0)) 
body_2_1_level.GetFrame().SetRot(chrono.ChQuaternionD(1,0,0,0)) 
body_2_1_level.GetAssets().push_back(body_2_1_shape) 
body_15.GetAssets().push_back(body_2_1_level) 

# Collision shapes 
body_15.GetCollisionModel().ClearModel()
mr = chrono.ChMatrix33D()
mr[0,0]=1; mr[1,0]=0; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=1; mr[2,1]=0 
mr[0,2]=0; mr[1,2]=0; mr[2,2]=1 
body_15.GetCollisionModel().AddBox(0.0113982379521279,0.0140083638039784,0.013,chrono.ChVectorD(-0.000134890389966315,0.0335,0.0511),mr)
body_15.GetCollisionModel().BuildModel()
body_15.SetCollide(True)

exported_items.append(body_15)



# Rigid body part
body_16= chrono.ChBodyAuxRef()
body_16.SetName('robo_leg_link-3/single_bot1-1')
body_16.SetPos(chrono.ChVectorD(2.086917980491,-0.114692613350303,-2.16364834462723))
body_16.SetRot(chrono.ChQuaternionD(0.701741045872204,-0.710767085089222,-0.0120715155645652,0.047158602650317))
body_16.SetMass(0.35)
body_16.SetInertiaXX(chrono.ChVectorD(0.000243287100418674,0.000250960901009867,0.000110709858882453))
body_16.SetInertiaXY(chrono.ChVectorD(4.8045401853377e-06,-2.33417013594839e-05,-1.75548754038152e-06))
body_16.SetFrame_COG_to_REF(chrono.ChFrameD(chrono.ChVectorD(-0.0658557785731179,-0.0789654441773446,0.173168884203731),chrono.ChQuaternionD(1,0,0,0)))

# Visualization shape 
body_5_1_shape = chrono.ChObjShapeFile() 
body_5_1_shape.SetFilename(shapes_dir +'body_5_1.obj') 
body_5_1_level = chrono.ChAssetLevel() 
body_5_1_level.GetFrame().SetPos(chrono.ChVectorD(0,0,0)) 
body_5_1_level.GetFrame().SetRot(chrono.ChQuaternionD(1,0,0,0)) 
body_5_1_level.GetAssets().push_back(body_5_1_shape) 
body_16.GetAssets().push_back(body_5_1_level) 

# Auxiliary marker (coordinate system feature)
marker_16_1 =chrono.ChMarker()
marker_16_1.SetName('My_marker')
body_16.AddMarker(marker_16_1)
marker_16_1.Impose_Abs_Coord(chrono.ChCoordsysD(chrono.ChVectorD(2.01591028981691,0.0170000034943454,-2.07885325742955),chrono.ChQuaternionD(0.889035062785107,0.00921026089710719,0.457396274118991,0.0179018617733612)))

# Collision shapes 
body_16.GetCollisionModel().ClearModel()
body_16.GetCollisionModel().AddSphere(0.00450000000000001, chrono.ChVectorD(-0.0490106583570424,-0.0813717222701078,0.130238444426058))
body_16.GetCollisionModel().AddSphere(0.00448981850569662, chrono.ChVectorD(-0.0783561237483722,-0.0580446528925914,0.133040222294353))
body_16.GetCollisionModel().AddSphere(0.00450000000000001, chrono.ChVectorD(-0.0817763692558815,-0.0926100044370168,0.13281194772533))
mr = chrono.ChMatrix33D()
mr[0,0]=0; mr[1,0]=0.996410323712302; mr[2,0]=0.0148275661003368 
mr[0,1]=-0.0833463261523019; mr[1,1]=0.0148275661003368; mr[2,1]=-0.996410323712302 
mr[0,2]=-0.993053389916914; mr[1,2]=-0.00123582316024348; mr[2,2]=0.0830471398216463 
body_16.GetCollisionModel().AddCylinder(0.038575,0.038575,0.0163750000000019,chrono.ChVectorD(-0.0652414174348167,-0.0773807616988142,0.179201155078968),mr)
body_16.GetCollisionModel().BuildModel()
body_16.SetCollide(True)

exported_items.append(body_16)



# Rigid body part
body_17= chrono.ChBodyAuxRef()
body_17.SetName('robo_leg_link-3/leg-1')
body_17.SetPos(chrono.ChVectorD(2.01536517105067,0.0170000000000035,-2.07924279722952))
body_17.SetRot(chrono.ChQuaternionD(-0.207074745002325,0.792910387155882,0.402488431456828,-0.407941454703924))
body_17.SetMass(0.01)
body_17.SetInertiaXX(chrono.ChVectorD(1.89110723602526e-07,5.11889399343246e-07,5.94333456279105e-07))
body_17.SetInertiaXY(chrono.ChVectorD(-3.88054984756532e-11,-6.97451787573893e-24,-2.11470064202402e-24))
body_17.SetFrame_COG_to_REF(chrono.ChFrameD(chrono.ChVectorD(-5.02229192063373e-07,0.00570468434844058,0.004),chrono.ChQuaternionD(1,0,0,0)))

# Visualization shape 
body_3_1_shape = chrono.ChObjShapeFile() 
body_3_1_shape.SetFilename(shapes_dir +'body_3_1.obj') 
body_3_1_level = chrono.ChAssetLevel() 
body_3_1_level.GetFrame().SetPos(chrono.ChVectorD(0,0,0)) 
body_3_1_level.GetFrame().SetRot(chrono.ChQuaternionD(1,0,0,0)) 
body_3_1_level.GetAssets().push_back(body_3_1_shape) 
body_17.GetAssets().push_back(body_3_1_level) 

# Collision shapes 
body_17.GetCollisionModel().ClearModel()
mr = chrono.ChMatrix33D()
mr[0,0]=0; mr[1,0]=1; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=0; mr[2,1]=-1 
mr[0,2]=-1; mr[1,2]=0; mr[2,2]=0 
body_17.GetCollisionModel().AddCylinder(0.00178981093410736,0.00178981093410736,0.00400000000000001,chrono.ChVectorD(0.00836413257948577,0.0092893103184674,0.00399999999999999),mr)
mr = chrono.ChMatrix33D()
mr[0,0]=0; mr[1,0]=1; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=0; mr[2,1]=-1 
mr[0,2]=-1; mr[1,2]=0; mr[2,2]=0 
body_17.GetCollisionModel().AddCylinder(0.00178981093410736,0.00178981093410736,0.00400000000000001,chrono.ChVectorD(0.00195543081300292,0.0123461042574392,0.00399999999999999),mr)
mr = chrono.ChMatrix33D()
mr[0,0]=0; mr[1,0]=1; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=0; mr[2,1]=-1 
mr[0,2]=-1; mr[1,2]=0; mr[2,2]=0 
body_17.GetCollisionModel().AddCylinder(0.00178981093410736,0.00178981093410736,0.00400000000000001,chrono.ChVectorD(-0.00508420803844748,0.0114193182205325,0.00399999999999999),mr)
mr = chrono.ChMatrix33D()
mr[0,0]=0; mr[1,0]=1; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=0; mr[2,1]=-1 
mr[0,2]=-1; mr[1,2]=0; mr[2,2]=0 
body_17.GetCollisionModel().AddCylinder(0.00178981093410736,0.00178981093410736,0.00400000000000001,chrono.ChVectorD(-0.0104833820993178,0.00680798793768785,0.00399999999999999),mr)
mr = chrono.ChMatrix33D()
mr[0,0]=0; mr[1,0]=1; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=0; mr[2,1]=-1 
mr[0,2]=-1; mr[1,2]=0; mr[2,2]=0 
body_17.GetCollisionModel().AddCylinder(0.00178981093410736,0.00178981093410736,0.00400000000000001,chrono.ChVectorD(-0.0125,0,0.00399999999999999),mr)
mr = chrono.ChMatrix33D()
mr[0,0]=0; mr[1,0]=1; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=0; mr[2,1]=-1 
mr[0,2]=-1; mr[1,2]=0; mr[2,2]=0 
body_17.GetCollisionModel().AddCylinder(0.00178981093410736,0.00178981093410736,0.00400000000000001,chrono.ChVectorD(0.0120740728286134,0.00323523806378145,0.00399999999999999),mr)
body_17.GetCollisionModel().BuildModel()
body_17.SetCollide(True)

exported_items.append(body_17)



# Rigid body part
body_18= chrono.ChBodyAuxRef()
body_18.SetName('robo_leg_link-3/link1-1')
body_18.SetPos(chrono.ChVectorD(2.01073018272952,0.0780000034943576,-2.08501313620782))
body_18.SetRot(chrono.ChQuaternionD(-1.17347936796063e-14,0.889215282997468,-6.40414879117105e-15,-0.457488994931826))
body_18.SetMass(0.02)
body_18.SetInertiaXX(chrono.ChVectorD(2.1102890321418e-05,3.42254939964149e-05,1.66734961647356e-05))
body_18.SetInertiaXY(chrono.ChVectorD(1.44186233077584e-08,8.15219838791517e-11,1.07420085359569e-11))
body_18.SetFrame_COG_to_REF(chrono.ChFrameD(chrono.ChVectorD(-2.37534873691438e-05,0.0334739058996041,1.03286017725802e-08),chrono.ChQuaternionD(1,0,0,0)))

# Visualization shape 
body_1_1_shape = chrono.ChObjShapeFile() 
body_1_1_shape.SetFilename(shapes_dir +'body_1_1.obj') 
body_1_1_level = chrono.ChAssetLevel() 
body_1_1_level.GetFrame().SetPos(chrono.ChVectorD(0,0,0)) 
body_1_1_level.GetFrame().SetRot(chrono.ChQuaternionD(1,0,0,0)) 
body_1_1_level.GetAssets().push_back(body_1_1_shape) 
body_18.GetAssets().push_back(body_1_1_level) 

exported_items.append(body_18)



# Rigid body part
body_19= chrono.ChBodyAuxRef()
body_19.SetName('robo_leg_link-3/link2-2')
body_19.SetPos(chrono.ChVectorD(2.01073018306082,0.0780000000239364,-2.08501313597108))
body_19.SetRot(chrono.ChQuaternionD(-1.2648750025735e-16,0.889215282997468,5.97781708375034e-17,-0.457488994931826))
body_19.SetMass(0.02)
body_19.SetInertiaXX(chrono.ChVectorD(1.04983041023413e-05,2.08152025768899e-05,1.23483128699975e-05))
body_19.SetInertiaXY(chrono.ChVectorD(-2.4713143785015e-11,-1.47996614267629e-11,-7.2032729945964e-11))
body_19.SetFrame_COG_to_REF(chrono.ChFrameD(chrono.ChVectorD(1.363578295109e-07,0.0335000115691779,0.0764730062719303),chrono.ChQuaternionD(1,0,0,0)))

# Visualization shape 
body_2_1_shape = chrono.ChObjShapeFile() 
body_2_1_shape.SetFilename(shapes_dir +'body_2_1.obj') 
body_2_1_level = chrono.ChAssetLevel() 
body_2_1_level.GetFrame().SetPos(chrono.ChVectorD(0,0,0)) 
body_2_1_level.GetFrame().SetRot(chrono.ChQuaternionD(1,0,0,0)) 
body_2_1_level.GetAssets().push_back(body_2_1_shape) 
body_19.GetAssets().push_back(body_2_1_level) 

# Collision shapes 
body_19.GetCollisionModel().ClearModel()
mr = chrono.ChMatrix33D()
mr[0,0]=1; mr[1,0]=0; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=1; mr[2,1]=0 
mr[0,2]=0; mr[1,2]=0; mr[2,2]=1 
body_19.GetCollisionModel().AddBox(0.0113982379521279,0.0140083638039784,0.013,chrono.ChVectorD(-0.000134890389966315,0.0335,0.0511),mr)
body_19.GetCollisionModel().BuildModel()
body_19.SetCollide(True)

exported_items.append(body_19)



# Rigid body part
body_20= chrono.ChBodyAuxRef()
body_20.SetName('robo_leg_link-3/link2-1')
body_20.SetPos(chrono.ChVectorD(2.01073018298745,0.0110000001591642,-2.08501313602351))
body_20.SetRot(chrono.ChQuaternionD(0.889215282997468,-6.82796650261364e-18,0.457488994931826,-2.77020355248896e-16))
body_20.SetMass(0.02)
body_20.SetInertiaXX(chrono.ChVectorD(1.04983041023413e-05,2.08152025768899e-05,1.23483128699975e-05))
body_20.SetInertiaXY(chrono.ChVectorD(-2.4713143785015e-11,-1.47996614267629e-11,-7.2032729945964e-11))
body_20.SetFrame_COG_to_REF(chrono.ChFrameD(chrono.ChVectorD(1.363578295109e-07,0.0335000115691779,0.0764730062719303),chrono.ChQuaternionD(1,0,0,0)))

# Visualization shape 
body_2_1_shape = chrono.ChObjShapeFile() 
body_2_1_shape.SetFilename(shapes_dir +'body_2_1.obj') 
body_2_1_level = chrono.ChAssetLevel() 
body_2_1_level.GetFrame().SetPos(chrono.ChVectorD(0,0,0)) 
body_2_1_level.GetFrame().SetRot(chrono.ChQuaternionD(1,0,0,0)) 
body_2_1_level.GetAssets().push_back(body_2_1_shape) 
body_20.GetAssets().push_back(body_2_1_level) 

# Collision shapes 
body_20.GetCollisionModel().ClearModel()
mr = chrono.ChMatrix33D()
mr[0,0]=1; mr[1,0]=0; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=1; mr[2,1]=0 
mr[0,2]=0; mr[1,2]=0; mr[2,2]=1 
body_20.GetCollisionModel().AddBox(0.0113982379521279,0.0140083638039784,0.013,chrono.ChVectorD(-0.000134890389966315,0.0335,0.0511),mr)
body_20.GetCollisionModel().BuildModel()
body_20.SetCollide(True)

exported_items.append(body_20)



# Rigid body part
body_21= chrono.ChBodyAuxRef()
body_21.SetName('robo_leg_link-7/link2-2')
body_21.SetPos(chrono.ChVectorD(2.2052300366344,0.0780000003814677,-2.3643236618442))
body_21.SetRot(chrono.ChQuaternionD(2.90283399375905e-16,0.418217969145976,1.02267605503814e-15,0.908346701586688))
body_21.SetMass(0.02)
body_21.SetInertiaXX(chrono.ChVectorD(1.04983041023413e-05,2.08152025768899e-05,1.23483128699975e-05))
body_21.SetInertiaXY(chrono.ChVectorD(-2.4713143785015e-11,-1.47996614267629e-11,-7.2032729945964e-11))
body_21.SetFrame_COG_to_REF(chrono.ChFrameD(chrono.ChVectorD(1.363578295109e-07,0.0335000115691779,0.0764730062719303),chrono.ChQuaternionD(1,0,0,0)))

# Visualization shape 
body_2_1_shape = chrono.ChObjShapeFile() 
body_2_1_shape.SetFilename(shapes_dir +'body_2_1.obj') 
body_2_1_level = chrono.ChAssetLevel() 
body_2_1_level.GetFrame().SetPos(chrono.ChVectorD(0,0,0)) 
body_2_1_level.GetFrame().SetRot(chrono.ChQuaternionD(1,0,0,0)) 
body_2_1_level.GetAssets().push_back(body_2_1_shape) 
body_21.GetAssets().push_back(body_2_1_level) 

# Collision shapes 
body_21.GetCollisionModel().ClearModel()
mr = chrono.ChMatrix33D()
mr[0,0]=1; mr[1,0]=0; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=1; mr[2,1]=0 
mr[0,2]=0; mr[1,2]=0; mr[2,2]=1 
body_21.GetCollisionModel().AddBox(0.0113982379521279,0.0140083638039784,0.013,chrono.ChVectorD(-0.000134890389966315,0.0335,0.0511),mr)
body_21.GetCollisionModel().BuildModel()
body_21.SetCollide(True)

exported_items.append(body_21)



# Rigid body part
body_22= chrono.ChBodyAuxRef()
body_22.SetName('robo_leg_link-7/single_bot1-1')
body_22.SetPos(chrono.ChVectorD(2.12247097929269,-0.114692616453853,-2.29263673863715))
body_22.SetRot(chrono.ChQuaternionD(0.0427070011868376,0.0160724123487371,0.700544309090773,0.712148482137996))
body_22.SetMass(0.35)
body_22.SetInertiaXX(chrono.ChVectorD(0.000243287100418674,0.000250960901009867,0.000110709858882453))
body_22.SetInertiaXY(chrono.ChVectorD(4.8045401853377e-06,-2.33417013594839e-05,-1.75548754038152e-06))
body_22.SetFrame_COG_to_REF(chrono.ChFrameD(chrono.ChVectorD(-0.0658557785731179,-0.0789654441773446,0.173168884203731),chrono.ChQuaternionD(1,0,0,0)))

# Visualization shape 
body_5_1_shape = chrono.ChObjShapeFile() 
body_5_1_shape.SetFilename(shapes_dir +'body_5_1.obj') 
body_5_1_level = chrono.ChAssetLevel() 
body_5_1_level.GetFrame().SetPos(chrono.ChVectorD(0,0,0)) 
body_5_1_level.GetFrame().SetRot(chrono.ChQuaternionD(1,0,0,0)) 
body_5_1_level.GetAssets().push_back(body_5_1_shape) 
body_22.GetAssets().push_back(body_5_1_level) 

# Auxiliary marker (coordinate system feature)
marker_22_1 =chrono.ChMarker()
marker_22_1.SetName('My_marker')
body_22.AddMarker(marker_22_1)
marker_22_1.Impose_Abs_Coord(chrono.ChCoordsysD(chrono.ChVectorD(2.20060721480915,0.0170000003907949,-2.37091207093952),chrono.ChQuaternionD(-0.418133207522261,0.0182870193585505,0.908162603946232,-0.00841964866994696)))

# Collision shapes 
body_22.GetCollisionModel().ClearModel()
body_22.GetCollisionModel().AddSphere(0.00450000000000001, chrono.ChVectorD(-0.0490106583570424,-0.0813717222701078,0.130238444426058))
body_22.GetCollisionModel().AddSphere(0.00448981850569662, chrono.ChVectorD(-0.0783561237483722,-0.0580446528925914,0.133040222294353))
body_22.GetCollisionModel().AddSphere(0.00450000000000001, chrono.ChVectorD(-0.0817763692558815,-0.0926100044370168,0.13281194772533))
mr = chrono.ChMatrix33D()
mr[0,0]=0; mr[1,0]=0.996410323712302; mr[2,0]=0.0148275661003368 
mr[0,1]=-0.0833463261523019; mr[1,1]=0.0148275661003368; mr[2,1]=-0.996410323712302 
mr[0,2]=-0.993053389916914; mr[1,2]=-0.00123582316024348; mr[2,2]=0.0830471398216463 
body_22.GetCollisionModel().AddCylinder(0.038575,0.038575,0.0163750000000019,chrono.ChVectorD(-0.0652414174348167,-0.0773807616988142,0.179201155078968),mr)
body_22.GetCollisionModel().BuildModel()
body_22.SetCollide(True)

exported_items.append(body_22)



# Rigid body part
body_23= chrono.ChBodyAuxRef()
body_23.SetName('robo_leg_link-7/link1-1')
body_23.SetPos(chrono.ChVectorD(2.20523003663267,0.0780000003907721,-2.36432366184567))
body_23.SetRot(chrono.ChQuaternionD(-5.58748509450933e-15,0.418217969145976,1.2965292624873e-14,0.908346701586688))
body_23.SetMass(0.02)
body_23.SetInertiaXX(chrono.ChVectorD(2.1102890321418e-05,3.42254939964149e-05,1.66734961647356e-05))
body_23.SetInertiaXY(chrono.ChVectorD(1.44186233077584e-08,8.15219838791517e-11,1.07420085359569e-11))
body_23.SetFrame_COG_to_REF(chrono.ChFrameD(chrono.ChVectorD(-2.37534873691438e-05,0.0334739058996041,1.03286017725802e-08),chrono.ChQuaternionD(1,0,0,0)))

# Visualization shape 
body_1_1_shape = chrono.ChObjShapeFile() 
body_1_1_shape.SetFilename(shapes_dir +'body_1_1.obj') 
body_1_1_level = chrono.ChAssetLevel() 
body_1_1_level.GetFrame().SetPos(chrono.ChVectorD(0,0,0)) 
body_1_1_level.GetFrame().SetRot(chrono.ChQuaternionD(1,0,0,0)) 
body_1_1_level.GetAssets().push_back(body_1_1_shape) 
body_23.GetAssets().push_back(body_1_1_level) 

exported_items.append(body_23)



# Rigid body part
body_24= chrono.ChBodyAuxRef()
body_24.SetName('robo_leg_link-7/leg-1')
body_24.SetPos(chrono.ChVectorD(2.20111626649813,0.0169999999999841,-2.37047644122374))
body_24.SetRot(chrono.ChQuaternionD(0.411147991456849,0.372923598130466,0.189299391636665,0.80996978919242))
body_24.SetMass(0.01)
body_24.SetInertiaXX(chrono.ChVectorD(1.89110723602526e-07,5.11889399343246e-07,5.94333456279105e-07))
body_24.SetInertiaXY(chrono.ChVectorD(-3.88054984756532e-11,-6.97451787573893e-24,-2.11470064202402e-24))
body_24.SetFrame_COG_to_REF(chrono.ChFrameD(chrono.ChVectorD(-5.02229192063373e-07,0.00570468434844058,0.004),chrono.ChQuaternionD(1,0,0,0)))

# Visualization shape 
body_3_1_shape = chrono.ChObjShapeFile() 
body_3_1_shape.SetFilename(shapes_dir +'body_3_1.obj') 
body_3_1_level = chrono.ChAssetLevel() 
body_3_1_level.GetFrame().SetPos(chrono.ChVectorD(0,0,0)) 
body_3_1_level.GetFrame().SetRot(chrono.ChQuaternionD(1,0,0,0)) 
body_3_1_level.GetAssets().push_back(body_3_1_shape) 
body_24.GetAssets().push_back(body_3_1_level) 

# Collision shapes 
body_24.GetCollisionModel().ClearModel()
mr = chrono.ChMatrix33D()
mr[0,0]=0; mr[1,0]=1; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=0; mr[2,1]=-1 
mr[0,2]=-1; mr[1,2]=0; mr[2,2]=0 
body_24.GetCollisionModel().AddCylinder(0.00178981093410736,0.00178981093410736,0.00400000000000001,chrono.ChVectorD(0.00836413257948577,0.0092893103184674,0.00399999999999999),mr)
mr = chrono.ChMatrix33D()
mr[0,0]=0; mr[1,0]=1; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=0; mr[2,1]=-1 
mr[0,2]=-1; mr[1,2]=0; mr[2,2]=0 
body_24.GetCollisionModel().AddCylinder(0.00178981093410736,0.00178981093410736,0.00400000000000001,chrono.ChVectorD(0.00195543081300292,0.0123461042574392,0.00399999999999999),mr)
mr = chrono.ChMatrix33D()
mr[0,0]=0; mr[1,0]=1; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=0; mr[2,1]=-1 
mr[0,2]=-1; mr[1,2]=0; mr[2,2]=0 
body_24.GetCollisionModel().AddCylinder(0.00178981093410736,0.00178981093410736,0.00400000000000001,chrono.ChVectorD(-0.00508420803844748,0.0114193182205325,0.00399999999999999),mr)
mr = chrono.ChMatrix33D()
mr[0,0]=0; mr[1,0]=1; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=0; mr[2,1]=-1 
mr[0,2]=-1; mr[1,2]=0; mr[2,2]=0 
body_24.GetCollisionModel().AddCylinder(0.00178981093410736,0.00178981093410736,0.00400000000000001,chrono.ChVectorD(-0.0104833820993178,0.00680798793768785,0.00399999999999999),mr)
mr = chrono.ChMatrix33D()
mr[0,0]=0; mr[1,0]=1; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=0; mr[2,1]=-1 
mr[0,2]=-1; mr[1,2]=0; mr[2,2]=0 
body_24.GetCollisionModel().AddCylinder(0.00178981093410736,0.00178981093410736,0.00400000000000001,chrono.ChVectorD(-0.0125,0,0.00399999999999999),mr)
mr = chrono.ChMatrix33D()
mr[0,0]=0; mr[1,0]=1; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=0; mr[2,1]=-1 
mr[0,2]=-1; mr[1,2]=0; mr[2,2]=0 
body_24.GetCollisionModel().AddCylinder(0.00178981093410736,0.00178981093410736,0.00400000000000001,chrono.ChVectorD(0.0120740728286134,0.00323523806378145,0.00399999999999999),mr)
body_24.GetCollisionModel().BuildModel()
body_24.SetCollide(True)

exported_items.append(body_24)



# Rigid body part
body_25= chrono.ChBodyAuxRef()
body_25.SetName('robo_leg_link-7/link2-1')
body_25.SetPos(chrono.ChVectorD(2.2052300366299,0.011000000398478,-2.36432366184805))
body_25.SetRot(chrono.ChQuaternionD(-0.418217969145976,-3.62854249219881e-16,0.908346701586688,-7.41941451694337e-16))
body_25.SetMass(0.02)
body_25.SetInertiaXX(chrono.ChVectorD(1.04983041023413e-05,2.08152025768899e-05,1.23483128699975e-05))
body_25.SetInertiaXY(chrono.ChVectorD(-2.4713143785015e-11,-1.47996614267629e-11,-7.2032729945964e-11))
body_25.SetFrame_COG_to_REF(chrono.ChFrameD(chrono.ChVectorD(1.363578295109e-07,0.0335000115691779,0.0764730062719303),chrono.ChQuaternionD(1,0,0,0)))

# Visualization shape 
body_2_1_shape = chrono.ChObjShapeFile() 
body_2_1_shape.SetFilename(shapes_dir +'body_2_1.obj') 
body_2_1_level = chrono.ChAssetLevel() 
body_2_1_level.GetFrame().SetPos(chrono.ChVectorD(0,0,0)) 
body_2_1_level.GetFrame().SetRot(chrono.ChQuaternionD(1,0,0,0)) 
body_2_1_level.GetAssets().push_back(body_2_1_shape) 
body_25.GetAssets().push_back(body_2_1_level) 

# Collision shapes 
body_25.GetCollisionModel().ClearModel()
mr = chrono.ChMatrix33D()
mr[0,0]=1; mr[1,0]=0; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=1; mr[2,1]=0 
mr[0,2]=0; mr[1,2]=0; mr[2,2]=1 
body_25.GetCollisionModel().AddBox(0.0113982379521279,0.0140083638039784,0.013,chrono.ChVectorD(-0.000134890389966315,0.0335,0.0511),mr)
body_25.GetCollisionModel().BuildModel()
body_25.SetCollide(True)

exported_items.append(body_25)



# Rigid body part
body_26= chrono.ChBodyAuxRef()
body_26.SetName('robo_leg_link-8/link1-1')
body_26.SetPos(chrono.ChVectorD(2.3327183543703,0.077999999999958,-2.28374426781457))
body_26.SetRot(chrono.ChQuaternionD(-4.09754555590713e-15,0.54438733018393,1.30860530576383e-14,0.838833973283875))
body_26.SetMass(0.02)
body_26.SetInertiaXX(chrono.ChVectorD(2.1102890321418e-05,3.42254939964149e-05,1.66734961647356e-05))
body_26.SetInertiaXY(chrono.ChVectorD(1.44186233077584e-08,8.15219838791517e-11,1.07420085359569e-11))
body_26.SetFrame_COG_to_REF(chrono.ChFrameD(chrono.ChVectorD(-2.37534873691438e-05,0.0334739058996041,1.03286017725802e-08),chrono.ChQuaternionD(1,0,0,0)))

# Visualization shape 
body_1_1_shape = chrono.ChObjShapeFile() 
body_1_1_shape.SetFilename(shapes_dir +'body_1_1.obj') 
body_1_1_level = chrono.ChAssetLevel() 
body_1_1_level.GetFrame().SetPos(chrono.ChVectorD(0,0,0)) 
body_1_1_level.GetFrame().SetRot(chrono.ChQuaternionD(1,0,0,0)) 
body_1_1_level.GetAssets().push_back(body_1_1_shape) 
body_26.GetAssets().push_back(body_1_1_level) 

exported_items.append(body_26)



# Rigid body part
body_27= chrono.ChBodyAuxRef()
body_27.SetName('robo_leg_link-8/single_bot1-1')
body_27.SetPos(chrono.ChVectorD(2.2737618964007,-0.114692616844689,-2.19148261492138))
body_27.SetRot(chrono.ChQuaternionD(-0.0583882006707884,0.118225009135724,0.699411920810813,0.702450446839501))
body_27.SetMass(0.35)
body_27.SetInertiaXX(chrono.ChVectorD(0.000243287100418674,0.000250960901009867,0.000110709858882453))
body_27.SetInertiaXY(chrono.ChVectorD(4.8045401853377e-06,-2.33417013594839e-05,-1.75548754038152e-06))
body_27.SetFrame_COG_to_REF(chrono.ChFrameD(chrono.ChVectorD(-0.0658557785731179,-0.0789654441773446,0.173168884203731),chrono.ChQuaternionD(1,0,0,0)))

# Visualization shape 
body_5_1_shape = chrono.ChObjShapeFile() 
body_5_1_shape.SetFilename(shapes_dir +'body_5_1.obj') 
body_5_1_level = chrono.ChAssetLevel() 
body_5_1_level.GetFrame().SetPos(chrono.ChVectorD(0,0,0)) 
body_5_1_level.GetFrame().SetRot(chrono.ChQuaternionD(1,0,0,0)) 
body_5_1_level.GetAssets().push_back(body_5_1_shape) 
body_27.GetAssets().push_back(body_5_1_level) 

# Auxiliary marker (coordinate system feature)
marker_27_1 =chrono.ChMarker()
marker_27_1.SetName('My_marker')
body_27.AddMarker(marker_27_1)
marker_27_1.Impose_Abs_Coord(chrono.ChCoordsysD(chrono.ChVectorD(2.32641282815388,0.0169999999999579,-2.2887460630057),chrono.ChQuaternionD(0.544276997397108,-0.0168875750649613,-0.83866396401875,0.0109597157431556)))

# Collision shapes 
body_27.GetCollisionModel().ClearModel()
body_27.GetCollisionModel().AddSphere(0.00450000000000001, chrono.ChVectorD(-0.0490106583570424,-0.0813717222701078,0.130238444426058))
body_27.GetCollisionModel().AddSphere(0.00448981850569662, chrono.ChVectorD(-0.0783561237483722,-0.0580446528925914,0.133040222294353))
body_27.GetCollisionModel().AddSphere(0.00450000000000001, chrono.ChVectorD(-0.0817763692558815,-0.0926100044370168,0.13281194772533))
mr = chrono.ChMatrix33D()
mr[0,0]=0; mr[1,0]=0.996410323712302; mr[2,0]=0.0148275661003368 
mr[0,1]=-0.0833463261523019; mr[1,1]=0.0148275661003368; mr[2,1]=-0.996410323712302 
mr[0,2]=-0.993053389916914; mr[1,2]=-0.00123582316024348; mr[2,2]=0.0830471398216463 
body_27.GetCollisionModel().AddCylinder(0.038575,0.038575,0.0163750000000019,chrono.ChVectorD(-0.0652414174348167,-0.0773807616988142,0.179201155078968),mr)
body_27.GetCollisionModel().BuildModel()
body_27.SetCollide(True)

exported_items.append(body_27)



# Rigid body part
body_28= chrono.ChBodyAuxRef()
body_28.SetName('robo_leg_link-8/leg-1')
body_28.SetPos(chrono.ChVectorD(2.32702474042913,0.0169999999999859,-2.28847318050821))
body_28.SetRot(chrono.ChQuaternionD(0.379684211589981,0.485428406322858,0.246407848088564,0.747985517376177))
body_28.SetMass(0.01)
body_28.SetInertiaXX(chrono.ChVectorD(1.89110723602526e-07,5.11889399343246e-07,5.94333456279105e-07))
body_28.SetInertiaXY(chrono.ChVectorD(-3.88054984756532e-11,-6.97451787573893e-24,-2.11470064202402e-24))
body_28.SetFrame_COG_to_REF(chrono.ChFrameD(chrono.ChVectorD(-5.02229192063373e-07,0.00570468434844058,0.004),chrono.ChQuaternionD(1,0,0,0)))

# Visualization shape 
body_3_1_shape = chrono.ChObjShapeFile() 
body_3_1_shape.SetFilename(shapes_dir +'body_3_1.obj') 
body_3_1_level = chrono.ChAssetLevel() 
body_3_1_level.GetFrame().SetPos(chrono.ChVectorD(0,0,0)) 
body_3_1_level.GetFrame().SetRot(chrono.ChQuaternionD(1,0,0,0)) 
body_3_1_level.GetAssets().push_back(body_3_1_shape) 
body_28.GetAssets().push_back(body_3_1_level) 

# Collision shapes 
body_28.GetCollisionModel().ClearModel()
mr = chrono.ChMatrix33D()
mr[0,0]=0; mr[1,0]=1; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=0; mr[2,1]=-1 
mr[0,2]=-1; mr[1,2]=0; mr[2,2]=0 
body_28.GetCollisionModel().AddCylinder(0.00178981093410736,0.00178981093410736,0.00400000000000001,chrono.ChVectorD(0.00836413257948577,0.0092893103184674,0.00399999999999999),mr)
mr = chrono.ChMatrix33D()
mr[0,0]=0; mr[1,0]=1; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=0; mr[2,1]=-1 
mr[0,2]=-1; mr[1,2]=0; mr[2,2]=0 
body_28.GetCollisionModel().AddCylinder(0.00178981093410736,0.00178981093410736,0.00400000000000001,chrono.ChVectorD(0.00195543081300292,0.0123461042574392,0.00399999999999999),mr)
mr = chrono.ChMatrix33D()
mr[0,0]=0; mr[1,0]=1; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=0; mr[2,1]=-1 
mr[0,2]=-1; mr[1,2]=0; mr[2,2]=0 
body_28.GetCollisionModel().AddCylinder(0.00178981093410736,0.00178981093410736,0.00400000000000001,chrono.ChVectorD(-0.00508420803844748,0.0114193182205325,0.00399999999999999),mr)
mr = chrono.ChMatrix33D()
mr[0,0]=0; mr[1,0]=1; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=0; mr[2,1]=-1 
mr[0,2]=-1; mr[1,2]=0; mr[2,2]=0 
body_28.GetCollisionModel().AddCylinder(0.00178981093410736,0.00178981093410736,0.00400000000000001,chrono.ChVectorD(-0.0104833820993178,0.00680798793768785,0.00399999999999999),mr)
mr = chrono.ChMatrix33D()
mr[0,0]=0; mr[1,0]=1; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=0; mr[2,1]=-1 
mr[0,2]=-1; mr[1,2]=0; mr[2,2]=0 
body_28.GetCollisionModel().AddCylinder(0.00178981093410736,0.00178981093410736,0.00400000000000001,chrono.ChVectorD(-0.0125,0,0.00399999999999999),mr)
mr = chrono.ChMatrix33D()
mr[0,0]=0; mr[1,0]=1; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=0; mr[2,1]=-1 
mr[0,2]=-1; mr[1,2]=0; mr[2,2]=0 
body_28.GetCollisionModel().AddCylinder(0.00178981093410736,0.00178981093410736,0.00400000000000001,chrono.ChVectorD(0.0120740728286134,0.00323523806378145,0.00399999999999999),mr)
body_28.GetCollisionModel().BuildModel()
body_28.SetCollide(True)

exported_items.append(body_28)



# Rigid body part
body_29= chrono.ChBodyAuxRef()
body_29.SetName('robo_leg_link-8/link2-1')
body_29.SetPos(chrono.ChVectorD(2.33271835231286,0.0110000003814756,-2.28374426963228))
body_29.SetRot(chrono.ChQuaternionD(0.54438733018393,3.91947012200039e-16,-0.838833973283875,-3.38970434331537e-16))
body_29.SetMass(0.02)
body_29.SetInertiaXX(chrono.ChVectorD(1.04983041023413e-05,2.08152025768899e-05,1.23483128699975e-05))
body_29.SetInertiaXY(chrono.ChVectorD(-2.4713143785015e-11,-1.47996614267629e-11,-7.2032729945964e-11))
body_29.SetFrame_COG_to_REF(chrono.ChFrameD(chrono.ChVectorD(1.363578295109e-07,0.0335000115691779,0.0764730062719303),chrono.ChQuaternionD(1,0,0,0)))

# Visualization shape 
body_2_1_shape = chrono.ChObjShapeFile() 
body_2_1_shape.SetFilename(shapes_dir +'body_2_1.obj') 
body_2_1_level = chrono.ChAssetLevel() 
body_2_1_level.GetFrame().SetPos(chrono.ChVectorD(0,0,0)) 
body_2_1_level.GetFrame().SetRot(chrono.ChQuaternionD(1,0,0,0)) 
body_2_1_level.GetAssets().push_back(body_2_1_shape) 
body_29.GetAssets().push_back(body_2_1_level) 

# Collision shapes 
body_29.GetCollisionModel().ClearModel()
mr = chrono.ChMatrix33D()
mr[0,0]=1; mr[1,0]=0; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=1; mr[2,1]=0 
mr[0,2]=0; mr[1,2]=0; mr[2,2]=1 
body_29.GetCollisionModel().AddBox(0.0113982379521279,0.0140083638039784,0.013,chrono.ChVectorD(-0.000134890389966315,0.0335,0.0511),mr)
body_29.GetCollisionModel().BuildModel()
body_29.SetCollide(True)

exported_items.append(body_29)



# Rigid body part
body_30= chrono.ChBodyAuxRef()
body_30.SetName('robo_leg_link-8/link2-2')
body_30.SetPos(chrono.ChVectorD(2.33271835230855,0.078000000506861,-2.2837442696342))
body_30.SetRot(chrono.ChQuaternionD(-8.55642309497291e-17,0.54438733018393,-1.03400883322935e-16,0.838833973283875))
body_30.SetMass(0.02)
body_30.SetInertiaXX(chrono.ChVectorD(1.04983041023413e-05,2.08152025768899e-05,1.23483128699975e-05))
body_30.SetInertiaXY(chrono.ChVectorD(-2.4713143785015e-11,-1.47996614267629e-11,-7.2032729945964e-11))
body_30.SetFrame_COG_to_REF(chrono.ChFrameD(chrono.ChVectorD(1.363578295109e-07,0.0335000115691779,0.0764730062719303),chrono.ChQuaternionD(1,0,0,0)))

# Visualization shape 
body_2_1_shape = chrono.ChObjShapeFile() 
body_2_1_shape.SetFilename(shapes_dir +'body_2_1.obj') 
body_2_1_level = chrono.ChAssetLevel() 
body_2_1_level.GetFrame().SetPos(chrono.ChVectorD(0,0,0)) 
body_2_1_level.GetFrame().SetRot(chrono.ChQuaternionD(1,0,0,0)) 
body_2_1_level.GetAssets().push_back(body_2_1_shape) 
body_30.GetAssets().push_back(body_2_1_level) 

# Collision shapes 
body_30.GetCollisionModel().ClearModel()
mr = chrono.ChMatrix33D()
mr[0,0]=1; mr[1,0]=0; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=1; mr[2,1]=0 
mr[0,2]=0; mr[1,2]=0; mr[2,2]=1 
body_30.GetCollisionModel().AddBox(0.0113982379521279,0.0140083638039784,0.013,chrono.ChVectorD(-0.000134890389966315,0.0335,0.0511),mr)
body_30.GetCollisionModel().BuildModel()
body_30.SetCollide(True)

exported_items.append(body_30)



# Rigid body part
body_31= chrono.ChBodyAuxRef()
body_31.SetName('robo_leg_link-11/leg-1')
body_31.SetPos(chrono.ChVectorD(2.47867323555018,0.0169999946430885,-1.90632229272711))
body_31.SetRot(chrono.ChQuaternionD(-0.0979330109589207,-0.086565189406716,0.00855126634561134,0.991384117883369))
body_31.SetMass(0.01)
body_31.SetInertiaXX(chrono.ChVectorD(1.89110723602526e-07,5.11889399343246e-07,5.94333456279105e-07))
body_31.SetInertiaXY(chrono.ChVectorD(-3.88054984756532e-11,-6.97451787573893e-24,-2.11470064202402e-24))
body_31.SetFrame_COG_to_REF(chrono.ChFrameD(chrono.ChVectorD(-5.02229192063373e-07,0.00570468434844058,0.004),chrono.ChQuaternionD(1,0,0,0)))

# Visualization shape 
body_3_1_shape = chrono.ChObjShapeFile() 
body_3_1_shape.SetFilename(shapes_dir +'body_3_1.obj') 
body_3_1_level = chrono.ChAssetLevel() 
body_3_1_level.GetFrame().SetPos(chrono.ChVectorD(0,0,0)) 
body_3_1_level.GetFrame().SetRot(chrono.ChQuaternionD(1,0,0,0)) 
body_3_1_level.GetAssets().push_back(body_3_1_shape) 
body_31.GetAssets().push_back(body_3_1_level) 

# Collision shapes 
body_31.GetCollisionModel().ClearModel()
mr = chrono.ChMatrix33D()
mr[0,0]=0; mr[1,0]=1; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=0; mr[2,1]=-1 
mr[0,2]=-1; mr[1,2]=0; mr[2,2]=0 
body_31.GetCollisionModel().AddCylinder(0.00178981093410736,0.00178981093410736,0.00400000000000001,chrono.ChVectorD(0.00836413257948577,0.0092893103184674,0.00399999999999999),mr)
mr = chrono.ChMatrix33D()
mr[0,0]=0; mr[1,0]=1; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=0; mr[2,1]=-1 
mr[0,2]=-1; mr[1,2]=0; mr[2,2]=0 
body_31.GetCollisionModel().AddCylinder(0.00178981093410736,0.00178981093410736,0.00400000000000001,chrono.ChVectorD(0.00195543081300292,0.0123461042574392,0.00399999999999999),mr)
mr = chrono.ChMatrix33D()
mr[0,0]=0; mr[1,0]=1; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=0; mr[2,1]=-1 
mr[0,2]=-1; mr[1,2]=0; mr[2,2]=0 
body_31.GetCollisionModel().AddCylinder(0.00178981093410736,0.00178981093410736,0.00400000000000001,chrono.ChVectorD(-0.00508420803844748,0.0114193182205325,0.00399999999999999),mr)
mr = chrono.ChMatrix33D()
mr[0,0]=0; mr[1,0]=1; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=0; mr[2,1]=-1 
mr[0,2]=-1; mr[1,2]=0; mr[2,2]=0 
body_31.GetCollisionModel().AddCylinder(0.00178981093410736,0.00178981093410736,0.00400000000000001,chrono.ChVectorD(-0.0104833820993178,0.00680798793768785,0.00399999999999999),mr)
mr = chrono.ChMatrix33D()
mr[0,0]=0; mr[1,0]=1; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=0; mr[2,1]=-1 
mr[0,2]=-1; mr[1,2]=0; mr[2,2]=0 
body_31.GetCollisionModel().AddCylinder(0.00178981093410736,0.00178981093410736,0.00400000000000001,chrono.ChVectorD(-0.0125,0,0.00399999999999999),mr)
mr = chrono.ChMatrix33D()
mr[0,0]=0; mr[1,0]=1; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=0; mr[2,1]=-1 
mr[0,2]=-1; mr[1,2]=0; mr[2,2]=0 
body_31.GetCollisionModel().AddCylinder(0.00178981093410736,0.00178981093410736,0.00400000000000001,chrono.ChVectorD(0.0120740728286134,0.00323523806378145,0.00399999999999999),mr)
body_31.GetCollisionModel().BuildModel()
body_31.SetCollide(True)

exported_items.append(body_31)



# Rigid body part
body_32= chrono.ChBodyAuxRef()
body_32.SetName('robo_leg_link-11/link2-2')
body_32.SetPos(chrono.ChVectorD(2.47546847077953,0.0780000002246144,-1.8996507569739))
body_32.SetRot(chrono.ChQuaternionD(5.57223675383974e-16,-0.0869865286876893,6.04239422994497e-16,0.996209487922528))
body_32.SetMass(0.02)
body_32.SetInertiaXX(chrono.ChVectorD(1.04983041023413e-05,2.08152025768899e-05,1.23483128699975e-05))
body_32.SetInertiaXY(chrono.ChVectorD(-2.4713143785015e-11,-1.47996614267629e-11,-7.2032729945964e-11))
body_32.SetFrame_COG_to_REF(chrono.ChFrameD(chrono.ChVectorD(1.363578295109e-07,0.0335000115691779,0.0764730062719303),chrono.ChQuaternionD(1,0,0,0)))

# Visualization shape 
body_2_1_shape = chrono.ChObjShapeFile() 
body_2_1_shape.SetFilename(shapes_dir +'body_2_1.obj') 
body_2_1_level = chrono.ChAssetLevel() 
body_2_1_level.GetFrame().SetPos(chrono.ChVectorD(0,0,0)) 
body_2_1_level.GetFrame().SetRot(chrono.ChQuaternionD(1,0,0,0)) 
body_2_1_level.GetAssets().push_back(body_2_1_shape) 
body_32.GetAssets().push_back(body_2_1_level) 

# Collision shapes 
body_32.GetCollisionModel().ClearModel()
mr = chrono.ChMatrix33D()
mr[0,0]=1; mr[1,0]=0; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=1; mr[2,1]=0 
mr[0,2]=0; mr[1,2]=0; mr[2,2]=1 
body_32.GetCollisionModel().AddBox(0.0113982379521279,0.0140083638039784,0.013,chrono.ChVectorD(-0.000134890389966315,0.0335,0.0511),mr)
body_32.GetCollisionModel().BuildModel()
body_32.SetCollide(True)

exported_items.append(body_32)



# Rigid body part
body_33= chrono.ChBodyAuxRef()
body_33.SetName('robo_leg_link-11/single_bot1-1')
body_33.SetPos(chrono.ChVectorD(2.37165178085031,-0.114692612747855,-1.93443828253148))
body_33.SetRot(chrono.ChQuaternionD(0.384314496839523,-0.339014718753166,0.587272155366509,0.626484479870707))
body_33.SetMass(0.35)
body_33.SetInertiaXX(chrono.ChVectorD(0.000243287100418674,0.000250960901009867,0.000110709858882453))
body_33.SetInertiaXY(chrono.ChVectorD(4.8045401853377e-06,-2.33417013594839e-05,-1.75548754038152e-06))
body_33.SetFrame_COG_to_REF(chrono.ChFrameD(chrono.ChVectorD(-0.0658557785731179,-0.0789654441773446,0.173168884203731),chrono.ChQuaternionD(1,0,0,0)))

# Visualization shape 
body_5_1_shape = chrono.ChObjShapeFile() 
body_5_1_shape.SetFilename(shapes_dir +'body_5_1.obj') 
body_5_1_level = chrono.ChAssetLevel() 
body_5_1_level.GetFrame().SetPos(chrono.ChVectorD(0,0,0)) 
body_5_1_level.GetFrame().SetRot(chrono.ChQuaternionD(1,0,0,0)) 
body_5_1_level.GetAssets().push_back(body_5_1_shape) 
body_33.GetAssets().push_back(body_5_1_level) 

# Auxiliary marker (coordinate system feature)
marker_33_1 =chrono.ChMarker()
marker_33_1.SetName('My_marker')
body_33.AddMarker(marker_33_1)
marker_33_1.Impose_Abs_Coord(chrono.ChCoordsysD(chrono.ChVectorD(2.47878935559418,0.0170000040967925,-1.90698215342101),chrono.ChQuaternionD(0.0869688988392516,0.020055890728714,0.99600758283958,0.00175123037411333)))

# Collision shapes 
body_33.GetCollisionModel().ClearModel()
body_33.GetCollisionModel().AddSphere(0.00450000000000001, chrono.ChVectorD(-0.0490106583570424,-0.0813717222701078,0.130238444426058))
body_33.GetCollisionModel().AddSphere(0.00448981850569662, chrono.ChVectorD(-0.0783561237483722,-0.0580446528925914,0.133040222294353))
body_33.GetCollisionModel().AddSphere(0.00450000000000001, chrono.ChVectorD(-0.0817763692558815,-0.0926100044370168,0.13281194772533))
mr = chrono.ChMatrix33D()
mr[0,0]=0; mr[1,0]=0.996410323712302; mr[2,0]=0.0148275661003368 
mr[0,1]=-0.0833463261523019; mr[1,1]=0.0148275661003368; mr[2,1]=-0.996410323712302 
mr[0,2]=-0.993053389916914; mr[1,2]=-0.00123582316024348; mr[2,2]=0.0830471398216463 
body_33.GetCollisionModel().AddCylinder(0.038575,0.038575,0.0163750000000019,chrono.ChVectorD(-0.0652414174348167,-0.0773807616988142,0.179201155078968),mr)
body_33.GetCollisionModel().BuildModel()
body_33.SetCollide(True)

exported_items.append(body_33)



# Rigid body part
body_34= chrono.ChBodyAuxRef()
body_34.SetName('robo_leg_link-11/link1-1')
body_34.SetPos(chrono.ChVectorD(2.4754684693122,0.0780000040967138,-1.89965075994486))
body_34.SetRot(chrono.ChQuaternionD(1.48360803570983e-15,-0.0869865286876892,1.57607233934386e-14,0.996209487922528))
body_34.SetMass(0.02)
body_34.SetInertiaXX(chrono.ChVectorD(2.1102890321418e-05,3.42254939964149e-05,1.66734961647356e-05))
body_34.SetInertiaXY(chrono.ChVectorD(1.44186233077584e-08,8.15219838791517e-11,1.07420085359569e-11))
body_34.SetFrame_COG_to_REF(chrono.ChFrameD(chrono.ChVectorD(-2.37534873691438e-05,0.0334739058996041,1.03286017725802e-08),chrono.ChQuaternionD(1,0,0,0)))

# Visualization shape 
body_1_1_shape = chrono.ChObjShapeFile() 
body_1_1_shape.SetFilename(shapes_dir +'body_1_1.obj') 
body_1_1_level = chrono.ChAssetLevel() 
body_1_1_level.GetFrame().SetPos(chrono.ChVectorD(0,0,0)) 
body_1_1_level.GetFrame().SetRot(chrono.ChQuaternionD(1,0,0,0)) 
body_1_1_level.GetAssets().push_back(body_1_1_shape) 
body_34.GetAssets().push_back(body_1_1_level) 

exported_items.append(body_34)



# Rigid body part
body_35= chrono.ChBodyAuxRef()
body_35.SetName('robo_leg_link-11/link2-1')
body_35.SetPos(chrono.ChVectorD(2.47546847077865,0.0110000008644015,-1.89965075696889))
body_35.SetRot(chrono.ChQuaternionD(0.0869865286876893,1.67167102615192e-16,0.996209487922528,-6.39501233702389e-16))
body_35.SetMass(0.02)
body_35.SetInertiaXX(chrono.ChVectorD(1.04983041023413e-05,2.08152025768899e-05,1.23483128699975e-05))
body_35.SetInertiaXY(chrono.ChVectorD(-2.4713143785015e-11,-1.47996614267629e-11,-7.2032729945964e-11))
body_35.SetFrame_COG_to_REF(chrono.ChFrameD(chrono.ChVectorD(1.363578295109e-07,0.0335000115691779,0.0764730062719303),chrono.ChQuaternionD(1,0,0,0)))

# Visualization shape 
body_2_1_shape = chrono.ChObjShapeFile() 
body_2_1_shape.SetFilename(shapes_dir +'body_2_1.obj') 
body_2_1_level = chrono.ChAssetLevel() 
body_2_1_level.GetFrame().SetPos(chrono.ChVectorD(0,0,0)) 
body_2_1_level.GetFrame().SetRot(chrono.ChQuaternionD(1,0,0,0)) 
body_2_1_level.GetAssets().push_back(body_2_1_shape) 
body_35.GetAssets().push_back(body_2_1_level) 

# Collision shapes 
body_35.GetCollisionModel().ClearModel()
mr = chrono.ChMatrix33D()
mr[0,0]=1; mr[1,0]=0; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=1; mr[2,1]=0 
mr[0,2]=0; mr[1,2]=0; mr[2,2]=1 
body_35.GetCollisionModel().AddBox(0.0113982379521279,0.0140083638039784,0.013,chrono.ChVectorD(-0.000134890389966315,0.0335,0.0511),mr)
body_35.GetCollisionModel().BuildModel()
body_35.SetCollide(True)

exported_items.append(body_35)



# Rigid body part
body_36= chrono.ChBodyAuxRef()
body_36.SetName('robo_leg_link-4/single_bot1-1')
body_36.SetPos(chrono.ChVectorD(2.0496560390867,-0.114692616844642,-2.20093875500083))
body_36.SetRot(chrono.ChQuaternionD(0.636024372749718,-0.66838460093445,-0.296747728244331,-0.24632459942899))
body_36.SetMass(0.35)
body_36.SetInertiaXX(chrono.ChVectorD(0.000243287100418674,0.000250960901009867,0.000110709858882453))
body_36.SetInertiaXY(chrono.ChVectorD(4.8045401853377e-06,-2.33417013594839e-05,-1.75548754038152e-06))
body_36.SetFrame_COG_to_REF(chrono.ChFrameD(chrono.ChVectorD(-0.0658557785731179,-0.0789654441773446,0.173168884203731),chrono.ChQuaternionD(1,0,0,0)))

# Visualization shape 
body_5_1_shape = chrono.ChObjShapeFile() 
body_5_1_shape.SetFilename(shapes_dir +'body_5_1.obj') 
body_5_1_level = chrono.ChAssetLevel() 
body_5_1_level.GetFrame().SetPos(chrono.ChVectorD(0,0,0)) 
body_5_1_level.GetFrame().SetRot(chrono.ChQuaternionD(1,0,0,0)) 
body_5_1_level.GetAssets().push_back(body_5_1_shape) 
body_36.GetAssets().push_back(body_5_1_level) 

# Auxiliary marker (coordinate system feature)
marker_36_1 =chrono.ChMarker()
marker_36_1.SetName('My_marker')
body_36.AddMarker(marker_36_1)
marker_36_1.Impose_Abs_Coord(chrono.ChCoordsysD(chrono.ChVectorD(1.93912394212624,0.0170000000000052,-2.19707165152996),chrono.ChQuaternionD(0.998239839934742,0.00112329356533359,0.0557845534741953,0.020100840089697)))

# Collision shapes 
body_36.GetCollisionModel().ClearModel()
body_36.GetCollisionModel().AddSphere(0.00450000000000001, chrono.ChVectorD(-0.0490106583570424,-0.0813717222701078,0.130238444426058))
body_36.GetCollisionModel().AddSphere(0.00448981850569662, chrono.ChVectorD(-0.0783561237483722,-0.0580446528925914,0.133040222294353))
body_36.GetCollisionModel().AddSphere(0.00450000000000001, chrono.ChVectorD(-0.0817763692558815,-0.0926100044370168,0.13281194772533))
mr = chrono.ChMatrix33D()
mr[0,0]=0; mr[1,0]=0.996410323712302; mr[2,0]=0.0148275661003368 
mr[0,1]=-0.0833463261523019; mr[1,1]=0.0148275661003368; mr[2,1]=-0.996410323712302 
mr[0,2]=-0.993053389916914; mr[1,2]=-0.00123582316024348; mr[2,2]=0.0830471398216463 
body_36.GetCollisionModel().AddCylinder(0.038575,0.038575,0.0163750000000019,chrono.ChVectorD(-0.0652414174348167,-0.0773807616988142,0.179201155078968),mr)
body_36.GetCollisionModel().BuildModel()
body_36.SetCollide(True)

exported_items.append(body_36)



# Rigid body part
body_37= chrono.ChBodyAuxRef()
body_37.SetName('robo_leg_link-4/leg-1')
body_37.SetPos(chrono.ChVectorD(1.93904929214279,0.0170000000000053,-2.19773747987118))
body_37.SetRot(chrono.ChQuaternionD(-0.0252550621712674,0.890307689792528,0.451928135107277,-0.0497529901561234))
body_37.SetMass(0.01)
body_37.SetInertiaXX(chrono.ChVectorD(1.89110723602526e-07,5.11889399343246e-07,5.94333456279105e-07))
body_37.SetInertiaXY(chrono.ChVectorD(-3.88054984756532e-11,-6.97451787573893e-24,-2.11470064202402e-24))
body_37.SetFrame_COG_to_REF(chrono.ChFrameD(chrono.ChVectorD(-5.02229192063373e-07,0.00570468434844058,0.004),chrono.ChQuaternionD(1,0,0,0)))

# Visualization shape 
body_3_1_shape = chrono.ChObjShapeFile() 
body_3_1_shape.SetFilename(shapes_dir +'body_3_1.obj') 
body_3_1_level = chrono.ChAssetLevel() 
body_3_1_level.GetFrame().SetPos(chrono.ChVectorD(0,0,0)) 
body_3_1_level.GetFrame().SetRot(chrono.ChQuaternionD(1,0,0,0)) 
body_3_1_level.GetAssets().push_back(body_3_1_shape) 
body_37.GetAssets().push_back(body_3_1_level) 

# Collision shapes 
body_37.GetCollisionModel().ClearModel()
mr = chrono.ChMatrix33D()
mr[0,0]=0; mr[1,0]=1; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=0; mr[2,1]=-1 
mr[0,2]=-1; mr[1,2]=0; mr[2,2]=0 
body_37.GetCollisionModel().AddCylinder(0.00178981093410736,0.00178981093410736,0.00400000000000001,chrono.ChVectorD(0.00836413257948577,0.0092893103184674,0.00399999999999999),mr)
mr = chrono.ChMatrix33D()
mr[0,0]=0; mr[1,0]=1; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=0; mr[2,1]=-1 
mr[0,2]=-1; mr[1,2]=0; mr[2,2]=0 
body_37.GetCollisionModel().AddCylinder(0.00178981093410736,0.00178981093410736,0.00400000000000001,chrono.ChVectorD(0.00195543081300292,0.0123461042574392,0.00399999999999999),mr)
mr = chrono.ChMatrix33D()
mr[0,0]=0; mr[1,0]=1; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=0; mr[2,1]=-1 
mr[0,2]=-1; mr[1,2]=0; mr[2,2]=0 
body_37.GetCollisionModel().AddCylinder(0.00178981093410736,0.00178981093410736,0.00400000000000001,chrono.ChVectorD(-0.00508420803844748,0.0114193182205325,0.00399999999999999),mr)
mr = chrono.ChMatrix33D()
mr[0,0]=0; mr[1,0]=1; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=0; mr[2,1]=-1 
mr[0,2]=-1; mr[1,2]=0; mr[2,2]=0 
body_37.GetCollisionModel().AddCylinder(0.00178981093410736,0.00178981093410736,0.00400000000000001,chrono.ChVectorD(-0.0104833820993178,0.00680798793768785,0.00399999999999999),mr)
mr = chrono.ChMatrix33D()
mr[0,0]=0; mr[1,0]=1; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=0; mr[2,1]=-1 
mr[0,2]=-1; mr[1,2]=0; mr[2,2]=0 
body_37.GetCollisionModel().AddCylinder(0.00178981093410736,0.00178981093410736,0.00400000000000001,chrono.ChVectorD(-0.0125,0,0.00399999999999999),mr)
mr = chrono.ChMatrix33D()
mr[0,0]=0; mr[1,0]=1; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=0; mr[2,1]=-1 
mr[0,2]=-1; mr[1,2]=0; mr[2,2]=0 
body_37.GetCollisionModel().AddCylinder(0.00178981093410736,0.00178981093410736,0.00400000000000001,chrono.ChVectorD(0.0120740728286134,0.00323523806378145,0.00399999999999999),mr)
body_37.GetCollisionModel().BuildModel()
body_37.SetCollide(True)

exported_items.append(body_37)



# Rigid body part
body_38= chrono.ChBodyAuxRef()
body_38.SetName('robo_leg_link-4/link2-1')
body_38.SetPos(chrono.ChVectorD(1.94024287557615,0.0110000000000048,-2.20504194662705))
body_38.SetRot(chrono.ChQuaternionD(0.998442197528355,1.47030017879796e-16,0.0557958618066781,-2.08491605856137e-17))
body_38.SetMass(0.02)
body_38.SetInertiaXX(chrono.ChVectorD(1.04983041023413e-05,2.08152025768899e-05,1.23483128699975e-05))
body_38.SetInertiaXY(chrono.ChVectorD(-2.4713143785015e-11,-1.47996614267629e-11,-7.2032729945964e-11))
body_38.SetFrame_COG_to_REF(chrono.ChFrameD(chrono.ChVectorD(1.363578295109e-07,0.0335000115691779,0.0764730062719303),chrono.ChQuaternionD(1,0,0,0)))

# Visualization shape 
body_2_1_shape = chrono.ChObjShapeFile() 
body_2_1_shape.SetFilename(shapes_dir +'body_2_1.obj') 
body_2_1_level = chrono.ChAssetLevel() 
body_2_1_level.GetFrame().SetPos(chrono.ChVectorD(0,0,0)) 
body_2_1_level.GetFrame().SetRot(chrono.ChQuaternionD(1,0,0,0)) 
body_2_1_level.GetAssets().push_back(body_2_1_shape) 
body_38.GetAssets().push_back(body_2_1_level) 

# Collision shapes 
body_38.GetCollisionModel().ClearModel()
mr = chrono.ChMatrix33D()
mr[0,0]=1; mr[1,0]=0; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=1; mr[2,1]=0 
mr[0,2]=0; mr[1,2]=0; mr[2,2]=1 
body_38.GetCollisionModel().AddBox(0.0113982379521279,0.0140083638039784,0.013,chrono.ChVectorD(-0.000134890389966315,0.0335,0.0511),mr)
body_38.GetCollisionModel().BuildModel()
body_38.SetCollide(True)

exported_items.append(body_38)



# Rigid body part
body_39= chrono.ChBodyAuxRef()
body_39.SetName('robo_leg_link-4/link2-2')
body_39.SetPos(chrono.ChVectorD(1.94024287557615,0.0780000000000073,-2.20504194662705))
body_39.SetRot(chrono.ChQuaternionD(-1.4908951305571e-16,0.998442197528355,-4.30801577630596e-17,-0.055795861806678))
body_39.SetMass(0.02)
body_39.SetInertiaXX(chrono.ChVectorD(1.04983041023413e-05,2.08152025768899e-05,1.23483128699975e-05))
body_39.SetInertiaXY(chrono.ChVectorD(-2.4713143785015e-11,-1.47996614267629e-11,-7.2032729945964e-11))
body_39.SetFrame_COG_to_REF(chrono.ChFrameD(chrono.ChVectorD(1.363578295109e-07,0.0335000115691779,0.0764730062719303),chrono.ChQuaternionD(1,0,0,0)))

# Visualization shape 
body_2_1_shape = chrono.ChObjShapeFile() 
body_2_1_shape.SetFilename(shapes_dir +'body_2_1.obj') 
body_2_1_level = chrono.ChAssetLevel() 
body_2_1_level.GetFrame().SetPos(chrono.ChVectorD(0,0,0)) 
body_2_1_level.GetFrame().SetRot(chrono.ChQuaternionD(1,0,0,0)) 
body_2_1_level.GetAssets().push_back(body_2_1_shape) 
body_39.GetAssets().push_back(body_2_1_level) 

# Collision shapes 
body_39.GetCollisionModel().ClearModel()
mr = chrono.ChMatrix33D()
mr[0,0]=1; mr[1,0]=0; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=1; mr[2,1]=0 
mr[0,2]=0; mr[1,2]=0; mr[2,2]=1 
body_39.GetCollisionModel().AddBox(0.0113982379521279,0.0140083638039784,0.013,chrono.ChVectorD(-0.000134890389966315,0.0335,0.0511),mr)
body_39.GetCollisionModel().BuildModel()
body_39.SetCollide(True)

exported_items.append(body_39)



# Rigid body part
body_40= chrono.ChBodyAuxRef()
body_40.SetName('robo_leg_link-4/link1-1')
body_40.SetPos(chrono.ChVectorD(1.94024287557615,0.0780000000000054,-2.20504194662704))
body_40.SetRot(chrono.ChQuaternionD(-1.36937721083823e-14,0.998442197528355,3.31501653311258e-15,-0.0557958618066781))
body_40.SetMass(0.02)
body_40.SetInertiaXX(chrono.ChVectorD(2.1102890321418e-05,3.42254939964149e-05,1.66734961647356e-05))
body_40.SetInertiaXY(chrono.ChVectorD(1.44186233077584e-08,8.15219838791517e-11,1.07420085359569e-11))
body_40.SetFrame_COG_to_REF(chrono.ChFrameD(chrono.ChVectorD(-2.37534873691438e-05,0.0334739058996041,1.03286017725802e-08),chrono.ChQuaternionD(1,0,0,0)))

# Visualization shape 
body_1_1_shape = chrono.ChObjShapeFile() 
body_1_1_shape.SetFilename(shapes_dir +'body_1_1.obj') 
body_1_1_level = chrono.ChAssetLevel() 
body_1_1_level.GetFrame().SetPos(chrono.ChVectorD(0,0,0)) 
body_1_1_level.GetFrame().SetRot(chrono.ChQuaternionD(1,0,0,0)) 
body_1_1_level.GetAssets().push_back(body_1_1_shape) 
body_40.GetAssets().push_back(body_1_1_level) 

exported_items.append(body_40)



# Rigid body part
body_41= chrono.ChBodyAuxRef()
body_41.SetName('robo_leg_link-12/leg-1')
body_41.SetPos(chrono.ChVectorD(2.39418245194493,0.0169999999999956,-1.83645281661912))
body_41.SetRot(chrono.ChQuaternionD(-0.286281620628219,0.690682347019932,0.350588787701081,-0.563993112674087))
body_41.SetMass(0.01)
body_41.SetInertiaXX(chrono.ChVectorD(1.89110723602526e-07,5.11889399343246e-07,5.94333456279105e-07))
body_41.SetInertiaXY(chrono.ChVectorD(-3.88054984756532e-11,-6.97451787573893e-24,-2.11470064202402e-24))
body_41.SetFrame_COG_to_REF(chrono.ChFrameD(chrono.ChVectorD(-5.02229192063373e-07,0.00570468434844058,0.004),chrono.ChQuaternionD(1,0,0,0)))

# Visualization shape 
body_3_1_shape = chrono.ChObjShapeFile() 
body_3_1_shape.SetFilename(shapes_dir +'body_3_1.obj') 
body_3_1_level = chrono.ChAssetLevel() 
body_3_1_level.GetFrame().SetPos(chrono.ChVectorD(0,0,0)) 
body_3_1_level.GetFrame().SetRot(chrono.ChQuaternionD(1,0,0,0)) 
body_3_1_level.GetAssets().push_back(body_3_1_shape) 
body_41.GetAssets().push_back(body_3_1_level) 

# Collision shapes 
body_41.GetCollisionModel().ClearModel()
mr = chrono.ChMatrix33D()
mr[0,0]=0; mr[1,0]=1; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=0; mr[2,1]=-1 
mr[0,2]=-1; mr[1,2]=0; mr[2,2]=0 
body_41.GetCollisionModel().AddCylinder(0.00178981093410736,0.00178981093410736,0.00400000000000001,chrono.ChVectorD(0.00836413257948577,0.0092893103184674,0.00399999999999999),mr)
mr = chrono.ChMatrix33D()
mr[0,0]=0; mr[1,0]=1; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=0; mr[2,1]=-1 
mr[0,2]=-1; mr[1,2]=0; mr[2,2]=0 
body_41.GetCollisionModel().AddCylinder(0.00178981093410736,0.00178981093410736,0.00400000000000001,chrono.ChVectorD(0.00195543081300292,0.0123461042574392,0.00399999999999999),mr)
mr = chrono.ChMatrix33D()
mr[0,0]=0; mr[1,0]=1; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=0; mr[2,1]=-1 
mr[0,2]=-1; mr[1,2]=0; mr[2,2]=0 
body_41.GetCollisionModel().AddCylinder(0.00178981093410736,0.00178981093410736,0.00400000000000001,chrono.ChVectorD(-0.00508420803844748,0.0114193182205325,0.00399999999999999),mr)
mr = chrono.ChMatrix33D()
mr[0,0]=0; mr[1,0]=1; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=0; mr[2,1]=-1 
mr[0,2]=-1; mr[1,2]=0; mr[2,2]=0 
body_41.GetCollisionModel().AddCylinder(0.00178981093410736,0.00178981093410736,0.00400000000000001,chrono.ChVectorD(-0.0104833820993178,0.00680798793768785,0.00399999999999999),mr)
mr = chrono.ChMatrix33D()
mr[0,0]=0; mr[1,0]=1; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=0; mr[2,1]=-1 
mr[0,2]=-1; mr[1,2]=0; mr[2,2]=0 
body_41.GetCollisionModel().AddCylinder(0.00178981093410736,0.00178981093410736,0.00400000000000001,chrono.ChVectorD(-0.0125,0,0.00399999999999999),mr)
mr = chrono.ChMatrix33D()
mr[0,0]=0; mr[1,0]=1; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=0; mr[2,1]=-1 
mr[0,2]=-1; mr[1,2]=0; mr[2,2]=0 
body_41.GetCollisionModel().AddCylinder(0.00178981093410736,0.00178981093410736,0.00400000000000001,chrono.ChVectorD(0.0120740728286134,0.00323523806378145,0.00399999999999999),mr)
body_41.GetCollisionModel().BuildModel()
body_41.SetCollide(True)

exported_items.append(body_41)



# Rigid body part
body_42= chrono.ChBodyAuxRef()
body_42.SetName('robo_leg_link-12/single_bot1-1')
body_42.SetPos(chrono.ChVectorD(2.42518507534978,-0.114692616844651,-1.94267399141015))
body_42.SetRot(chrono.ChQuaternionD(0.688895104902287,-0.685399895170276,0.134200415019886,0.194012285051056))
body_42.SetMass(0.35)
body_42.SetInertiaXX(chrono.ChVectorD(0.000243287100418674,0.000250960901009867,0.000110709858882453))
body_42.SetInertiaXY(chrono.ChVectorD(4.8045401853377e-06,-2.33417013594839e-05,-1.75548754038152e-06))
body_42.SetFrame_COG_to_REF(chrono.ChFrameD(chrono.ChVectorD(-0.0658557785731179,-0.0789654441773446,0.173168884203731),chrono.ChQuaternionD(1,0,0,0)))

# Visualization shape 
body_5_1_shape = chrono.ChObjShapeFile() 
body_5_1_shape.SetFilename(shapes_dir +'body_5_1.obj') 
body_5_1_level = chrono.ChAssetLevel() 
body_5_1_level.GetFrame().SetPos(chrono.ChVectorD(0,0,0)) 
body_5_1_level.GetFrame().SetRot(chrono.ChQuaternionD(1,0,0,0)) 
body_5_1_level.GetAssets().push_back(body_5_1_shape) 
body_42.GetAssets().push_back(body_5_1_level) 

# Auxiliary marker (coordinate system feature)
marker_42_1 =chrono.ChMarker()
marker_42_1.SetName('My_marker')
body_42.AddMarker(marker_42_1)
marker_42_1.Impose_Abs_Coord(chrono.ChCoordsysD(chrono.ChVectorD(2.39483892761046,0.0169999999999957,-1.83631887745171),chrono.ChQuaternionD(0.774410380618748,0.0127334451009314,0.632363231718275,0.0155937467148543)))

# Collision shapes 
body_42.GetCollisionModel().ClearModel()
body_42.GetCollisionModel().AddSphere(0.00450000000000001, chrono.ChVectorD(-0.0490106583570424,-0.0813717222701078,0.130238444426058))
body_42.GetCollisionModel().AddSphere(0.00448981850569662, chrono.ChVectorD(-0.0783561237483722,-0.0580446528925914,0.133040222294353))
body_42.GetCollisionModel().AddSphere(0.00450000000000001, chrono.ChVectorD(-0.0817763692558815,-0.0926100044370168,0.13281194772533))
mr = chrono.ChMatrix33D()
mr[0,0]=0; mr[1,0]=0.996410323712302; mr[2,0]=0.0148275661003368 
mr[0,1]=-0.0833463261523019; mr[1,1]=0.0148275661003368; mr[2,1]=-0.996410323712302 
mr[0,2]=-0.993053389916914; mr[1,2]=-0.00123582316024348; mr[2,2]=0.0830471398216463 
body_42.GetCollisionModel().AddCylinder(0.038575,0.038575,0.0163750000000019,chrono.ChVectorD(-0.0652414174348167,-0.0773807616988142,0.179201155078968),mr)
body_42.GetCollisionModel().BuildModel()
body_42.SetCollide(True)

exported_items.append(body_42)



# Rigid body part
body_43= chrono.ChBodyAuxRef()
body_43.SetName('robo_leg_link-12/link2-2')
body_43.SetPos(chrono.ChVectorD(2.38760011275428,0.0779999999999973,-1.83983699846694))
body_43.SetRot(chrono.ChQuaternionD(6.01893334758558e-17,0.774567364757046,-8.19134836048159e-16,-0.632491420853536))
body_43.SetMass(0.02)
body_43.SetInertiaXX(chrono.ChVectorD(1.04983041023413e-05,2.08152025768899e-05,1.23483128699975e-05))
body_43.SetInertiaXY(chrono.ChVectorD(-2.4713143785015e-11,-1.47996614267629e-11,-7.2032729945964e-11))
body_43.SetFrame_COG_to_REF(chrono.ChFrameD(chrono.ChVectorD(1.363578295109e-07,0.0335000115691779,0.0764730062719303),chrono.ChQuaternionD(1,0,0,0)))

# Visualization shape 
body_2_1_shape = chrono.ChObjShapeFile() 
body_2_1_shape.SetFilename(shapes_dir +'body_2_1.obj') 
body_2_1_level = chrono.ChAssetLevel() 
body_2_1_level.GetFrame().SetPos(chrono.ChVectorD(0,0,0)) 
body_2_1_level.GetFrame().SetRot(chrono.ChQuaternionD(1,0,0,0)) 
body_2_1_level.GetAssets().push_back(body_2_1_shape) 
body_43.GetAssets().push_back(body_2_1_level) 

# Collision shapes 
body_43.GetCollisionModel().ClearModel()
mr = chrono.ChMatrix33D()
mr[0,0]=1; mr[1,0]=0; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=1; mr[2,1]=0 
mr[0,2]=0; mr[1,2]=0; mr[2,2]=1 
body_43.GetCollisionModel().AddBox(0.0113982379521279,0.0140083638039784,0.013,chrono.ChVectorD(-0.000134890389966315,0.0335,0.0511),mr)
body_43.GetCollisionModel().BuildModel()
body_43.SetCollide(True)

exported_items.append(body_43)



# Rigid body part
body_44= chrono.ChBodyAuxRef()
body_44.SetName('robo_leg_link-12/link2-1')
body_44.SetPos(chrono.ChVectorD(2.38760011275428,0.010999999999995,-1.83983699846694))
body_44.SetRot(chrono.ChQuaternionD(0.774567364757046,-4.8991317945464e-17,0.632491420853536,-8.02337812752571e-16))
body_44.SetMass(0.02)
body_44.SetInertiaXX(chrono.ChVectorD(1.04983041023413e-05,2.08152025768899e-05,1.23483128699975e-05))
body_44.SetInertiaXY(chrono.ChVectorD(-2.4713143785015e-11,-1.47996614267629e-11,-7.2032729945964e-11))
body_44.SetFrame_COG_to_REF(chrono.ChFrameD(chrono.ChVectorD(1.363578295109e-07,0.0335000115691779,0.0764730062719303),chrono.ChQuaternionD(1,0,0,0)))

# Visualization shape 
body_2_1_shape = chrono.ChObjShapeFile() 
body_2_1_shape.SetFilename(shapes_dir +'body_2_1.obj') 
body_2_1_level = chrono.ChAssetLevel() 
body_2_1_level.GetFrame().SetPos(chrono.ChVectorD(0,0,0)) 
body_2_1_level.GetFrame().SetRot(chrono.ChQuaternionD(1,0,0,0)) 
body_2_1_level.GetAssets().push_back(body_2_1_shape) 
body_44.GetAssets().push_back(body_2_1_level) 

# Collision shapes 
body_44.GetCollisionModel().ClearModel()
mr = chrono.ChMatrix33D()
mr[0,0]=1; mr[1,0]=0; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=1; mr[2,1]=0 
mr[0,2]=0; mr[1,2]=0; mr[2,2]=1 
body_44.GetCollisionModel().AddBox(0.0113982379521279,0.0140083638039784,0.013,chrono.ChVectorD(-0.000134890389966315,0.0335,0.0511),mr)
body_44.GetCollisionModel().BuildModel()
body_44.SetCollide(True)

exported_items.append(body_44)



# Rigid body part
body_45= chrono.ChBodyAuxRef()
body_45.SetName('robo_leg_link-12/link1-1')
body_45.SetPos(chrono.ChVectorD(2.38760011275428,0.0779999999999956,-1.83983699846694))
body_45.SetRot(chrono.ChQuaternionD(-1.2857457182009e-14,0.774567364757046,-6.00193970644394e-15,-0.632491420853536))
body_45.SetMass(0.02)
body_45.SetInertiaXX(chrono.ChVectorD(2.1102890321418e-05,3.42254939964149e-05,1.66734961647356e-05))
body_45.SetInertiaXY(chrono.ChVectorD(1.44186233077584e-08,8.15219838791517e-11,1.07420085359569e-11))
body_45.SetFrame_COG_to_REF(chrono.ChFrameD(chrono.ChVectorD(-2.37534873691438e-05,0.0334739058996041,1.03286017725802e-08),chrono.ChQuaternionD(1,0,0,0)))

# Visualization shape 
body_1_1_shape = chrono.ChObjShapeFile() 
body_1_1_shape.SetFilename(shapes_dir +'body_1_1.obj') 
body_1_1_level = chrono.ChAssetLevel() 
body_1_1_level.GetFrame().SetPos(chrono.ChVectorD(0,0,0)) 
body_1_1_level.GetFrame().SetRot(chrono.ChQuaternionD(1,0,0,0)) 
body_1_1_level.GetAssets().push_back(body_1_1_shape) 
body_45.GetAssets().push_back(body_1_1_level) 

exported_items.append(body_45)



# Rigid body part
body_46= chrono.ChBodyAuxRef()
body_46.SetName('object-1')
body_46.SetPos(chrono.ChVectorD(2.58738777521909,0.0100000000000007,-2.74788941954557))
body_46.SetRot(chrono.ChQuaternionD(0.707106781186548,-0.707106781186547,0,0))
body_46.SetMass(0.2296712567856)
body_46.SetInertiaXX(chrono.ChVectorD(0.000200653250454309,9.87831386851984e-05,0.000200653250454309))
body_46.SetInertiaXY(chrono.ChVectorD(-2.18658709118247e-21,7.46322558787853e-22,3.66176004107828e-20))
body_46.SetFrame_COG_to_REF(chrono.ChFrameD(chrono.ChVectorD(0.00635000000000003,0.000151190476190455,0.04445),chrono.ChQuaternionD(1,0,0,0)))
body_46.SetBodyFixed(True)

# Visualization shape 
body_46_1_shape = chrono.ChObjShapeFile() 
body_46_1_shape.SetFilename(shapes_dir +'body_46_1.obj') 
body_46_1_level = chrono.ChAssetLevel() 
body_46_1_level.GetFrame().SetPos(chrono.ChVectorD(0,0,0)) 
body_46_1_level.GetFrame().SetRot(chrono.ChQuaternionD(1,0,0,0)) 
body_46_1_level.GetAssets().push_back(body_46_1_shape) 
body_46.GetAssets().push_back(body_46_1_level) 

# Auxiliary marker (coordinate system feature)
marker_46_1 =chrono.ChMarker()
marker_46_1.SetName('object_marker')
body_46.AddMarker(marker_46_1)
marker_46_1.Impose_Abs_Coord(chrono.ChCoordsysD(chrono.ChVectorD(2.59373777521908,0.0100000000000007,-2.74804061002176),chrono.ChQuaternionD(-9.65854120745433E-17,9.65854120745432E-17,0.707106781186547,0.707106781186548)))

# Collision shapes 
body_46.GetCollisionModel().ClearModel()
mr = chrono.ChMatrix33D()
mr[0,0]=-1; mr[1,0]=0; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=-1; mr[2,1]=0 
mr[0,2]=0; mr[1,2]=0; mr[2,2]=1 
body_46.GetCollisionModel().AddBox(0.0254,0.0254,0.04445,chrono.ChVectorD(0.00635000000000003,0.000151190476190454,0.04445),mr)
body_46.GetCollisionModel().BuildModel()
body_46.SetCollide(True)

exported_items.append(body_46)



# Rigid body part
body_47= chrono.ChBodyAuxRef()
body_47.SetName('robo_leg_link-9/link2-1')
body_47.SetPos(chrono.ChVectorD(2.44546132777055,0.0110000005068609,-2.18990337954995))
body_47.SetRot(chrono.ChQuaternionD(-0.296460017460361,-3.17866199065608e-17,0.955045264920675,1.38044749308492e-16))
body_47.SetMass(0.02)
body_47.SetInertiaXX(chrono.ChVectorD(1.04983041023413e-05,2.08152025768899e-05,1.23483128699975e-05))
body_47.SetInertiaXY(chrono.ChVectorD(-2.4713143785015e-11,-1.47996614267629e-11,-7.2032729945964e-11))
body_47.SetFrame_COG_to_REF(chrono.ChFrameD(chrono.ChVectorD(1.363578295109e-07,0.0335000115691779,0.0764730062719303),chrono.ChQuaternionD(1,0,0,0)))

# Visualization shape 
body_2_1_shape = chrono.ChObjShapeFile() 
body_2_1_shape.SetFilename(shapes_dir +'body_2_1.obj') 
body_2_1_level = chrono.ChAssetLevel() 
body_2_1_level.GetFrame().SetPos(chrono.ChVectorD(0,0,0)) 
body_2_1_level.GetFrame().SetRot(chrono.ChQuaternionD(1,0,0,0)) 
body_2_1_level.GetAssets().push_back(body_2_1_shape) 
body_47.GetAssets().push_back(body_2_1_level) 

# Collision shapes 
body_47.GetCollisionModel().ClearModel()
mr = chrono.ChMatrix33D()
mr[0,0]=1; mr[1,0]=0; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=1; mr[2,1]=0 
mr[0,2]=0; mr[1,2]=0; mr[2,2]=1 
body_47.GetCollisionModel().AddBox(0.0113982379521279,0.0140083638039784,0.013,chrono.ChVectorD(-0.000134890389966315,0.0335,0.0511),mr)
body_47.GetCollisionModel().BuildModel()
body_47.SetCollide(True)

exported_items.append(body_47)



# Rigid body part
body_48= chrono.ChBodyAuxRef()
body_48.SetName('robo_leg_link-9/leg-1')
body_48.SetPos(chrono.ChVectorD(2.44307457137872,0.0169999999999888,-2.1969093163633))
body_48.SetRot(chrono.ChQuaternionD(0.432285321210353,0.264352429436765,0.134187685737098,0.851610743892692))
body_48.SetMass(0.01)
body_48.SetInertiaXX(chrono.ChVectorD(1.89110723602526e-07,5.11889399343246e-07,5.94333456279105e-07))
body_48.SetInertiaXY(chrono.ChVectorD(-3.88054984756532e-11,-6.97451787573893e-24,-2.11470064202402e-24))
body_48.SetFrame_COG_to_REF(chrono.ChFrameD(chrono.ChVectorD(-5.02229192063373e-07,0.00570468434844058,0.004),chrono.ChQuaternionD(1,0,0,0)))

# Visualization shape 
body_3_1_shape = chrono.ChObjShapeFile() 
body_3_1_shape.SetFilename(shapes_dir +'body_3_1.obj') 
body_3_1_level = chrono.ChAssetLevel() 
body_3_1_level.GetFrame().SetPos(chrono.ChVectorD(0,0,0)) 
body_3_1_level.GetFrame().SetRot(chrono.ChQuaternionD(1,0,0,0)) 
body_3_1_level.GetAssets().push_back(body_3_1_shape) 
body_48.GetAssets().push_back(body_3_1_level) 

# Collision shapes 
body_48.GetCollisionModel().ClearModel()
mr = chrono.ChMatrix33D()
mr[0,0]=0; mr[1,0]=1; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=0; mr[2,1]=-1 
mr[0,2]=-1; mr[1,2]=0; mr[2,2]=0 
body_48.GetCollisionModel().AddCylinder(0.00178981093410736,0.00178981093410736,0.00400000000000001,chrono.ChVectorD(0.00836413257948577,0.0092893103184674,0.00399999999999999),mr)
mr = chrono.ChMatrix33D()
mr[0,0]=0; mr[1,0]=1; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=0; mr[2,1]=-1 
mr[0,2]=-1; mr[1,2]=0; mr[2,2]=0 
body_48.GetCollisionModel().AddCylinder(0.00178981093410736,0.00178981093410736,0.00400000000000001,chrono.ChVectorD(0.00195543081300292,0.0123461042574392,0.00399999999999999),mr)
mr = chrono.ChMatrix33D()
mr[0,0]=0; mr[1,0]=1; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=0; mr[2,1]=-1 
mr[0,2]=-1; mr[1,2]=0; mr[2,2]=0 
body_48.GetCollisionModel().AddCylinder(0.00178981093410736,0.00178981093410736,0.00400000000000001,chrono.ChVectorD(-0.00508420803844748,0.0114193182205325,0.00399999999999999),mr)
mr = chrono.ChMatrix33D()
mr[0,0]=0; mr[1,0]=1; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=0; mr[2,1]=-1 
mr[0,2]=-1; mr[1,2]=0; mr[2,2]=0 
body_48.GetCollisionModel().AddCylinder(0.00178981093410736,0.00178981093410736,0.00400000000000001,chrono.ChVectorD(-0.0104833820993178,0.00680798793768785,0.00399999999999999),mr)
mr = chrono.ChMatrix33D()
mr[0,0]=0; mr[1,0]=1; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=0; mr[2,1]=-1 
mr[0,2]=-1; mr[1,2]=0; mr[2,2]=0 
body_48.GetCollisionModel().AddCylinder(0.00178981093410736,0.00178981093410736,0.00400000000000001,chrono.ChVectorD(-0.0125,0,0.00399999999999999),mr)
mr = chrono.ChMatrix33D()
mr[0,0]=0; mr[1,0]=1; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=0; mr[2,1]=-1 
mr[0,2]=-1; mr[1,2]=0; mr[2,2]=0 
body_48.GetCollisionModel().AddCylinder(0.00178981093410736,0.00178981093410736,0.00400000000000001,chrono.ChVectorD(0.0120740728286134,0.00323523806378145,0.00399999999999999),mr)
body_48.GetCollisionModel().BuildModel()
body_48.SetCollide(True)

exported_items.append(body_48)



# Rigid body part
body_49= chrono.ChBodyAuxRef()
body_49.SetName('robo_leg_link-9/link1-1')
body_49.SetPos(chrono.ChVectorD(2.44546133329877,0.0779999999999889,-2.18990337291956))
body_49.SetRot(chrono.ChQuaternionD(-3.59778842771719e-15,0.296460017460361,1.27254500918378e-14,0.955045264920675))
body_49.SetMass(0.02)
body_49.SetInertiaXX(chrono.ChVectorD(2.1102890321418e-05,3.42254939964149e-05,1.66734961647356e-05))
body_49.SetInertiaXY(chrono.ChVectorD(1.44186233077584e-08,8.15219838791517e-11,1.07420085359569e-11))
body_49.SetFrame_COG_to_REF(chrono.ChFrameD(chrono.ChVectorD(-2.37534873691438e-05,0.0334739058996041,1.03286017725802e-08),chrono.ChQuaternionD(1,0,0,0)))

# Visualization shape 
body_1_1_shape = chrono.ChObjShapeFile() 
body_1_1_shape.SetFilename(shapes_dir +'body_1_1.obj') 
body_1_1_level = chrono.ChAssetLevel() 
body_1_1_level.GetFrame().SetPos(chrono.ChVectorD(0,0,0)) 
body_1_1_level.GetFrame().SetRot(chrono.ChQuaternionD(1,0,0,0)) 
body_1_1_level.GetAssets().push_back(body_1_1_shape) 
body_49.GetAssets().push_back(body_1_1_level) 

exported_items.append(body_49)



# Rigid body part
body_50= chrono.ChBodyAuxRef()
body_50.SetName('robo_leg_link-9/link2-2')
body_50.SetPos(chrono.ChVectorD(2.44546132777171,0.0780000005011455,-2.18990337954826))
body_50.SetRot(chrono.ChQuaternionD(3.83482864444151e-16,0.296460017460361,-6.53896180934964e-17,0.955045264920675))
body_50.SetMass(0.02)
body_50.SetInertiaXX(chrono.ChVectorD(1.04983041023413e-05,2.08152025768899e-05,1.23483128699975e-05))
body_50.SetInertiaXY(chrono.ChVectorD(-2.4713143785015e-11,-1.47996614267629e-11,-7.2032729945964e-11))
body_50.SetFrame_COG_to_REF(chrono.ChFrameD(chrono.ChVectorD(1.363578295109e-07,0.0335000115691779,0.0764730062719303),chrono.ChQuaternionD(1,0,0,0)))

# Visualization shape 
body_2_1_shape = chrono.ChObjShapeFile() 
body_2_1_shape.SetFilename(shapes_dir +'body_2_1.obj') 
body_2_1_level = chrono.ChAssetLevel() 
body_2_1_level.GetFrame().SetPos(chrono.ChVectorD(0,0,0)) 
body_2_1_level.GetFrame().SetRot(chrono.ChQuaternionD(1,0,0,0)) 
body_2_1_level.GetAssets().push_back(body_2_1_shape) 
body_50.GetAssets().push_back(body_2_1_level) 

# Collision shapes 
body_50.GetCollisionModel().ClearModel()
mr = chrono.ChMatrix33D()
mr[0,0]=1; mr[1,0]=0; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=1; mr[2,1]=0 
mr[0,2]=0; mr[1,2]=0; mr[2,2]=1 
body_50.GetCollisionModel().AddBox(0.0113982379521279,0.0140083638039784,0.013,chrono.ChVectorD(-0.000134890389966315,0.0335,0.0511),mr)
body_50.GetCollisionModel().BuildModel()
body_50.SetCollide(True)

exported_items.append(body_50)



# Rigid body part
body_51= chrono.ChBodyAuxRef()
body_51.SetName('robo_leg_link-9/single_bot1-1')
body_51.SetPos(chrono.ChVectorD(2.34700666848999,-0.114692616844659,-2.1419997664969))
body_51.SetRot(chrono.ChQuaternionD(0.133504727469708,-0.0767351430091608,0.689030263989238,0.708184651681695))
body_51.SetMass(0.35)
body_51.SetInertiaXX(chrono.ChVectorD(0.000243287100418674,0.000250960901009867,0.000110709858882453))
body_51.SetInertiaXY(chrono.ChVectorD(4.8045401853377e-06,-2.33417013594839e-05,-1.75548754038152e-06))
body_51.SetFrame_COG_to_REF(chrono.ChFrameD(chrono.ChVectorD(-0.0658557785731179,-0.0789654441773446,0.173168884203731),chrono.ChQuaternionD(1,0,0,0)))

# Visualization shape 
body_5_1_shape = chrono.ChObjShapeFile() 
body_5_1_shape.SetFilename(shapes_dir +'body_5_1.obj') 
body_5_1_level = chrono.ChAssetLevel() 
body_5_1_level.GetFrame().SetPos(chrono.ChVectorD(0,0,0)) 
body_5_1_level.GetFrame().SetRot(chrono.ChQuaternionD(1,0,0,0)) 
body_5_1_level.GetAssets().push_back(body_5_1_shape) 
body_51.GetAssets().push_back(body_5_1_level) 

# Auxiliary marker (coordinate system feature)
marker_51_1 =chrono.ChMarker()
marker_51_1.SetName('My_marker')
body_51.AddMarker(marker_51_1)
marker_51_1.Impose_Abs_Coord(chrono.ChCoordsysD(chrono.ChVectorD(2.44269517351259,0.0169999999999889,-2.19746154571708),chrono.ChQuaternionD(-0.296399932924782,0.0192271642726164,0.954851702727413,-0.00596839298129452)))

# Collision shapes 
body_51.GetCollisionModel().ClearModel()
body_51.GetCollisionModel().AddSphere(0.00450000000000001, chrono.ChVectorD(-0.0490106583570424,-0.0813717222701078,0.130238444426058))
body_51.GetCollisionModel().AddSphere(0.00448981850569662, chrono.ChVectorD(-0.0783561237483722,-0.0580446528925914,0.133040222294353))
body_51.GetCollisionModel().AddSphere(0.00450000000000001, chrono.ChVectorD(-0.0817763692558815,-0.0926100044370168,0.13281194772533))
mr = chrono.ChMatrix33D()
mr[0,0]=0; mr[1,0]=0.996410323712302; mr[2,0]=0.0148275661003368 
mr[0,1]=-0.0833463261523019; mr[1,1]=0.0148275661003368; mr[2,1]=-0.996410323712302 
mr[0,2]=-0.993053389916914; mr[1,2]=-0.00123582316024348; mr[2,2]=0.0830471398216463 
body_51.GetCollisionModel().AddCylinder(0.038575,0.038575,0.0163750000000019,chrono.ChVectorD(-0.0652414174348167,-0.0773807616988142,0.179201155078968),mr)
body_51.GetCollisionModel().BuildModel()
body_51.SetCollide(True)

exported_items.append(body_51)



# Rigid body part
body_52= chrono.ChBodyAuxRef()
body_52.SetName('graound-1')
body_52.SetPos(chrono.ChVectorD(0.25,-1.10171934447595e-17,-0.25))
body_52.SetRot(chrono.ChQuaternionD(0.707106781186548,-0.707106781186547,0,0))
body_52.SetMass(160.310974449907)
body_52.SetInertiaXX(chrono.ChVectorD(213.929220761236,427.825165923698,213.899655627765))
body_52.SetInertiaXY(chrono.ChVectorD(0.000941966655763467,0.00121521831061318,0.00037066909524368))
body_52.SetFrame_COG_to_REF(chrono.ChFrameD(chrono.ChVectorD(1.75010692404897,1.75004207520538,0.00504630746441585),chrono.ChQuaternionD(1,0,0,0)))
body_52.SetBodyFixed(True)

# Visualization shape 
body_52_1_shape = chrono.ChObjShapeFile() 
body_52_1_shape.SetFilename(shapes_dir +'body_52_1.obj') 
body_52_1_level = chrono.ChAssetLevel() 
body_52_1_level.GetFrame().SetPos(chrono.ChVectorD(0,0,0)) 
body_52_1_level.GetFrame().SetRot(chrono.ChQuaternionD(1,0,0,0)) 
body_52_1_level.GetAssets().push_back(body_52_1_shape) 
body_52.GetAssets().push_back(body_52_1_level) 

# Auxiliary marker (coordinate system feature)
marker_52_1 =chrono.ChMarker()
marker_52_1.SetName('ground_marker')
body_52.AddMarker(marker_52_1)
marker_52_1.Impose_Abs_Coord(chrono.ChCoordsysD(chrono.ChVectorD(4,0.00999999999999983,-4),chrono.ChQuaternionD(0.707106781186548,1.02176270012416E-16,-0.707106781186547,1.55806643888669E-17)))

# Collision shapes 
body_52.GetCollisionModel().ClearModel()
mr = chrono.ChMatrix33D()
mr[0,0]=1; mr[1,0]=0; mr[2,0]=0 
mr[0,1]=5.89786600208644E-17; mr[1,1]=-1; mr[2,1]=0 
mr[0,2]=0; mr[1,2]=0; mr[2,2]=-1 
body_52.GetCollisionModel().AddBox(0.0305042045565396,1.88241479923824,0.05,chrono.ChVectorD(-0.219495795443461,1.75342339406911,0.06),mr)
mr = chrono.ChMatrix33D()
mr[0,0]=-1; mr[1,0]=0; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=1; mr[2,1]=0 
mr[0,2]=0; mr[1,2]=0; mr[2,2]=-1 
body_52.GetCollisionModel().AddBox(0.0413523486386163,1.87726618574267,0.05,chrono.ChVectorD(3.70864765136138,1.73439272589085,0.06),mr)
mr = chrono.ChMatrix33D()
mr[0,0]=1; mr[1,0]=0; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=-1; mr[2,1]=0 
mr[0,2]=0; mr[1,2]=0; mr[2,2]=-1 
body_52.GetCollisionModel().AddBox(2,0.0450620362723377,0.05,chrono.ChVectorD(1.75,3.70493796372766,0.06),mr)
mr = chrono.ChMatrix33D()
mr[0,0]=-1; mr[1,0]=6.93889390390723E-18; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=1; mr[2,1]=0 
mr[0,2]=0; mr[1,2]=0; mr[2,2]=-1 
body_52.GetCollisionModel().AddBox(2,0.040792554490671,0.05,chrono.ChVectorD(1.75,-0.209207445509329,0.06),mr)
mr = chrono.ChMatrix33D()
mr[0,0]=1; mr[1,0]=0; mr[2,0]=0 
mr[0,1]=9.71445146547012E-17; mr[1,1]=-1; mr[2,1]=0 
mr[0,2]=0; mr[1,2]=0; mr[2,2]=-1 
body_52.GetCollisionModel().AddBox(2,2,0.00500000000005684,chrono.ChVectorD(1.75,1.75,0.00499999999994316),mr)
body_52.GetCollisionModel().BuildModel()
body_52.SetCollide(True)

exported_items.append(body_52)



# Rigid body part
body_53= chrono.ChBodyAuxRef()
body_53.SetName('robo_leg_link-1/single_bot1-1')
body_53.SetPos(chrono.ChVectorD(2.32073712733235,-0.114692616844649,-1.97857885001272))
body_53.SetRot(chrono.ChQuaternionD(0.701818129151777,-0.709305505786214,0.00612621773520716,0.0655704393116576))
body_53.SetMass(0.35)
body_53.SetInertiaXX(chrono.ChVectorD(0.000243287100418674,0.000250960901009867,0.000110709858882453))
body_53.SetInertiaXY(chrono.ChVectorD(4.8045401853377e-06,-2.33417013594839e-05,-1.75548754038152e-06))
body_53.SetFrame_COG_to_REF(chrono.ChFrameD(chrono.ChVectorD(-0.0658557785731179,-0.0789654441773446,0.173168884203731),chrono.ChQuaternionD(1,0,0,0)))

# Visualization shape 
body_5_1_shape = chrono.ChObjShapeFile() 
body_5_1_shape.SetFilename(shapes_dir +'body_5_1.obj') 
body_5_1_level = chrono.ChAssetLevel() 
body_5_1_level.GetFrame().SetPos(chrono.ChVectorD(0,0,0)) 
body_5_1_level.GetFrame().SetRot(chrono.ChQuaternionD(1,0,0,0)) 
body_5_1_level.GetAssets().push_back(body_5_1_shape) 
body_53.GetAssets().push_back(body_5_1_level) 

# Auxiliary marker (coordinate system feature)
marker_53_1 =chrono.ChMarker()
marker_53_1.SetName('My_marker')
body_53.AddMarker(marker_53_1)
marker_53_1.Impose_Abs_Coord(chrono.ChCoordsysD(chrono.ChVectorD(2.25422029523284,0.0169999999999987,-1.89021703702105),chrono.ChQuaternionD(0.876877540154952,0.00967129715797022,0.480292071567973,0.0176570544550201)))

# Collision shapes 
body_53.GetCollisionModel().ClearModel()
body_53.GetCollisionModel().AddSphere(0.00450000000000001, chrono.ChVectorD(-0.0490106583570424,-0.0813717222701078,0.130238444426058))
body_53.GetCollisionModel().AddSphere(0.00448981850569662, chrono.ChVectorD(-0.0783561237483722,-0.0580446528925914,0.133040222294353))
body_53.GetCollisionModel().AddSphere(0.00450000000000001, chrono.ChVectorD(-0.0817763692558815,-0.0926100044370168,0.13281194772533))
mr = chrono.ChMatrix33D()
mr[0,0]=0; mr[1,0]=0.996410323712302; mr[2,0]=0.0148275661003368 
mr[0,1]=-0.0833463261523019; mr[1,1]=0.0148275661003368; mr[2,1]=-0.996410323712302 
mr[0,2]=-0.993053389916914; mr[1,2]=-0.00123582316024348; mr[2,2]=0.0830471398216463 
body_53.GetCollisionModel().AddCylinder(0.038575,0.038575,0.0163750000000019,chrono.ChVectorD(-0.0652414174348167,-0.0773807616988142,0.179201155078968),mr)
body_53.GetCollisionModel().BuildModel()
body_53.SetCollide(True)

exported_items.append(body_53)



# Rigid body part
body_54= chrono.ChBodyAuxRef()
body_54.SetName('robo_leg_link-1/link1-1')
body_54.SetPos(chrono.ChVectorD(2.24872785151409,0.0779999999999987,-1.89610012107589))
body_54.SetRot(chrono.ChQuaternionD(-1.15963996656011e-14,0.877055295862366,-6.90087641707015e-15,-0.480389433688729))
body_54.SetMass(0.02)
body_54.SetInertiaXX(chrono.ChVectorD(2.1102890321418e-05,3.42254939964149e-05,1.66734961647356e-05))
body_54.SetInertiaXY(chrono.ChVectorD(1.44186233077584e-08,8.15219838791517e-11,1.07420085359569e-11))
body_54.SetFrame_COG_to_REF(chrono.ChFrameD(chrono.ChVectorD(-2.37534873691438e-05,0.0334739058996041,1.03286017725802e-08),chrono.ChQuaternionD(1,0,0,0)))

# Visualization shape 
body_1_1_shape = chrono.ChObjShapeFile() 
body_1_1_shape.SetFilename(shapes_dir +'body_1_1.obj') 
body_1_1_level = chrono.ChAssetLevel() 
body_1_1_level.GetFrame().SetPos(chrono.ChVectorD(0,0,0)) 
body_1_1_level.GetFrame().SetRot(chrono.ChQuaternionD(1,0,0,0)) 
body_1_1_level.GetAssets().push_back(body_1_1_shape) 
body_54.GetAssets().push_back(body_1_1_level) 

exported_items.append(body_54)



# Rigid body part
body_55= chrono.ChBodyAuxRef()
body_55.SetName('robo_leg_link-1/link2-1')
body_55.SetPos(chrono.ChVectorD(2.24872785151409,0.0109999999999974,-1.89610012107589))
body_55.SetRot(chrono.ChQuaternionD(0.877055295862366,-2.16579526417077e-16,0.480389433688729,-7.75334925620951e-16))
body_55.SetMass(0.02)
body_55.SetInertiaXX(chrono.ChVectorD(1.04983041023413e-05,2.08152025768899e-05,1.23483128699975e-05))
body_55.SetInertiaXY(chrono.ChVectorD(-2.4713143785015e-11,-1.47996614267629e-11,-7.2032729945964e-11))
body_55.SetFrame_COG_to_REF(chrono.ChFrameD(chrono.ChVectorD(1.363578295109e-07,0.0335000115691779,0.0764730062719303),chrono.ChQuaternionD(1,0,0,0)))

# Visualization shape 
body_2_1_shape = chrono.ChObjShapeFile() 
body_2_1_shape.SetFilename(shapes_dir +'body_2_1.obj') 
body_2_1_level = chrono.ChAssetLevel() 
body_2_1_level.GetFrame().SetPos(chrono.ChVectorD(0,0,0)) 
body_2_1_level.GetFrame().SetRot(chrono.ChQuaternionD(1,0,0,0)) 
body_2_1_level.GetAssets().push_back(body_2_1_shape) 
body_55.GetAssets().push_back(body_2_1_level) 

# Collision shapes 
body_55.GetCollisionModel().ClearModel()
mr = chrono.ChMatrix33D()
mr[0,0]=1; mr[1,0]=0; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=1; mr[2,1]=0 
mr[0,2]=0; mr[1,2]=0; mr[2,2]=1 
body_55.GetCollisionModel().AddBox(0.0113982379521279,0.0140083638039784,0.013,chrono.ChVectorD(-0.000134890389966315,0.0335,0.0511),mr)
body_55.GetCollisionModel().BuildModel()
body_55.SetCollide(True)

exported_items.append(body_55)



# Rigid body part
body_56= chrono.ChBodyAuxRef()
body_56.SetName('robo_leg_link-1/leg-1')
body_56.SetPos(chrono.ChVectorD(2.25365571558301,0.0169999999999987,-1.89057779985034))
body_56.SetRot(chrono.ChQuaternionD(-0.217440251383971,0.782067360793538,0.396984426875478,-0.428361698892249))
body_56.SetMass(0.01)
body_56.SetInertiaXX(chrono.ChVectorD(1.89110723602526e-07,5.11889399343246e-07,5.94333456279105e-07))
body_56.SetInertiaXY(chrono.ChVectorD(-3.88054984756532e-11,-6.97451787573893e-24,-2.11470064202402e-24))
body_56.SetFrame_COG_to_REF(chrono.ChFrameD(chrono.ChVectorD(-5.02229192063373e-07,0.00570468434844058,0.004),chrono.ChQuaternionD(1,0,0,0)))

# Visualization shape 
body_3_1_shape = chrono.ChObjShapeFile() 
body_3_1_shape.SetFilename(shapes_dir +'body_3_1.obj') 
body_3_1_level = chrono.ChAssetLevel() 
body_3_1_level.GetFrame().SetPos(chrono.ChVectorD(0,0,0)) 
body_3_1_level.GetFrame().SetRot(chrono.ChQuaternionD(1,0,0,0)) 
body_3_1_level.GetAssets().push_back(body_3_1_shape) 
body_56.GetAssets().push_back(body_3_1_level) 

# Collision shapes 
body_56.GetCollisionModel().ClearModel()
mr = chrono.ChMatrix33D()
mr[0,0]=0; mr[1,0]=1; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=0; mr[2,1]=-1 
mr[0,2]=-1; mr[1,2]=0; mr[2,2]=0 
body_56.GetCollisionModel().AddCylinder(0.00178981093410736,0.00178981093410736,0.00400000000000001,chrono.ChVectorD(0.00836413257948577,0.0092893103184674,0.00399999999999999),mr)
mr = chrono.ChMatrix33D()
mr[0,0]=0; mr[1,0]=1; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=0; mr[2,1]=-1 
mr[0,2]=-1; mr[1,2]=0; mr[2,2]=0 
body_56.GetCollisionModel().AddCylinder(0.00178981093410736,0.00178981093410736,0.00400000000000001,chrono.ChVectorD(0.00195543081300292,0.0123461042574392,0.00399999999999999),mr)
mr = chrono.ChMatrix33D()
mr[0,0]=0; mr[1,0]=1; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=0; mr[2,1]=-1 
mr[0,2]=-1; mr[1,2]=0; mr[2,2]=0 
body_56.GetCollisionModel().AddCylinder(0.00178981093410736,0.00178981093410736,0.00400000000000001,chrono.ChVectorD(-0.00508420803844748,0.0114193182205325,0.00399999999999999),mr)
mr = chrono.ChMatrix33D()
mr[0,0]=0; mr[1,0]=1; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=0; mr[2,1]=-1 
mr[0,2]=-1; mr[1,2]=0; mr[2,2]=0 
body_56.GetCollisionModel().AddCylinder(0.00178981093410736,0.00178981093410736,0.00400000000000001,chrono.ChVectorD(-0.0104833820993178,0.00680798793768785,0.00399999999999999),mr)
mr = chrono.ChMatrix33D()
mr[0,0]=0; mr[1,0]=1; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=0; mr[2,1]=-1 
mr[0,2]=-1; mr[1,2]=0; mr[2,2]=0 
body_56.GetCollisionModel().AddCylinder(0.00178981093410736,0.00178981093410736,0.00400000000000001,chrono.ChVectorD(-0.0125,0,0.00399999999999999),mr)
mr = chrono.ChMatrix33D()
mr[0,0]=0; mr[1,0]=1; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=0; mr[2,1]=-1 
mr[0,2]=-1; mr[1,2]=0; mr[2,2]=0 
body_56.GetCollisionModel().AddCylinder(0.00178981093410736,0.00178981093410736,0.00400000000000001,chrono.ChVectorD(0.0120740728286134,0.00323523806378145,0.00399999999999999),mr)
body_56.GetCollisionModel().BuildModel()
body_56.SetCollide(True)

exported_items.append(body_56)



# Rigid body part
body_57= chrono.ChBodyAuxRef()
body_57.SetName('robo_leg_link-1/link2-2')
body_57.SetPos(chrono.ChVectorD(2.24872785151409,0.0779999999999997,-1.89610012107589))
body_57.SetRot(chrono.ChQuaternionD(1.58973328180571e-16,0.877055295862365,-5.45899080284139e-16,-0.48038943368873))
body_57.SetMass(0.02)
body_57.SetInertiaXX(chrono.ChVectorD(1.04983041023413e-05,2.08152025768899e-05,1.23483128699975e-05))
body_57.SetInertiaXY(chrono.ChVectorD(-2.4713143785015e-11,-1.47996614267629e-11,-7.2032729945964e-11))
body_57.SetFrame_COG_to_REF(chrono.ChFrameD(chrono.ChVectorD(1.363578295109e-07,0.0335000115691779,0.0764730062719303),chrono.ChQuaternionD(1,0,0,0)))

# Visualization shape 
body_2_1_shape = chrono.ChObjShapeFile() 
body_2_1_shape.SetFilename(shapes_dir +'body_2_1.obj') 
body_2_1_level = chrono.ChAssetLevel() 
body_2_1_level.GetFrame().SetPos(chrono.ChVectorD(0,0,0)) 
body_2_1_level.GetFrame().SetRot(chrono.ChQuaternionD(1,0,0,0)) 
body_2_1_level.GetAssets().push_back(body_2_1_shape) 
body_57.GetAssets().push_back(body_2_1_level) 

# Collision shapes 
body_57.GetCollisionModel().ClearModel()
mr = chrono.ChMatrix33D()
mr[0,0]=1; mr[1,0]=0; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=1; mr[2,1]=0 
mr[0,2]=0; mr[1,2]=0; mr[2,2]=1 
body_57.GetCollisionModel().AddBox(0.0113982379521279,0.0140083638039784,0.013,chrono.ChVectorD(-0.000134890389966315,0.0335,0.0511),mr)
body_57.GetCollisionModel().BuildModel()
body_57.SetCollide(True)

exported_items.append(body_57)



# Rigid body part
body_58= chrono.ChBodyAuxRef()
body_58.SetName('robo_leg_link-2/single_bot1-1')
body_58.SetPos(chrono.ChVectorD(2.21414536359312,-0.114692616844647,-2.05728600039473))
body_58.SetRot(chrono.ChQuaternionD(0.699565305136257,-0.71232696836389,-0.0565207997064706,0.00201821497058782))
body_58.SetMass(0.35)
body_58.SetInertiaXX(chrono.ChVectorD(0.000243287100418674,0.000250960901009867,0.000110709858882453))
body_58.SetInertiaXY(chrono.ChVectorD(4.8045401853377e-06,-2.33417013594839e-05,-1.75548754038152e-06))
body_58.SetFrame_COG_to_REF(chrono.ChFrameD(chrono.ChVectorD(-0.0658557785731179,-0.0789654441773446,0.173168884203731),chrono.ChQuaternionD(1,0,0,0)))

# Visualization shape 
body_5_1_shape = chrono.ChObjShapeFile() 
body_5_1_shape.SetFilename(shapes_dir +'body_5_1.obj') 
body_5_1_level = chrono.ChAssetLevel() 
body_5_1_level.GetFrame().SetPos(chrono.ChVectorD(0,0,0)) 
body_5_1_level.GetFrame().SetRot(chrono.ChQuaternionD(1,0,0,0)) 
body_5_1_level.GetAssets().push_back(body_5_1_shape) 
body_58.GetAssets().push_back(body_5_1_level) 

# Auxiliary marker (coordinate system feature)
marker_58_1 =chrono.ChMarker()
marker_58_1.SetName('My_marker')
body_58.AddMarker(marker_58_1)
marker_58_1.Impose_Abs_Coord(chrono.ChCoordsysD(chrono.ChVectorD(2.13298173734239,0.0170000000000006,-1.98215435112785),chrono.ChQuaternionD(0.916235806411522,0.00805719702538196,0.40013327965652,0.0184495836494932)))

# Collision shapes 
body_58.GetCollisionModel().ClearModel()
body_58.GetCollisionModel().AddSphere(0.00450000000000001, chrono.ChVectorD(-0.0490106583570424,-0.0813717222701078,0.130238444426058))
body_58.GetCollisionModel().AddSphere(0.00448981850569662, chrono.ChVectorD(-0.0783561237483722,-0.0580446528925914,0.133040222294353))
body_58.GetCollisionModel().AddSphere(0.00450000000000001, chrono.ChVectorD(-0.0817763692558815,-0.0926100044370168,0.13281194772533))
mr = chrono.ChMatrix33D()
mr[0,0]=0; mr[1,0]=0.996410323712302; mr[2,0]=0.0148275661003368 
mr[0,1]=-0.0833463261523019; mr[1,1]=0.0148275661003368; mr[2,1]=-0.996410323712302 
mr[0,2]=-0.993053389916914; mr[1,2]=-0.00123582316024348; mr[2,2]=0.0830471398216463 
body_58.GetCollisionModel().AddCylinder(0.038575,0.038575,0.0163750000000019,chrono.ChVectorD(-0.0652414174348167,-0.0773807616988142,0.179201155078968),mr)
body_58.GetCollisionModel().BuildModel()
body_58.SetCollide(True)

exported_items.append(body_58)



# Rigid body part
body_59= chrono.ChBodyAuxRef()
body_59.SetName('robo_leg_link-2/link1-1')
body_59.SetPos(chrono.ChVectorD(2.12862244981808,0.0780000000000006,-1.98892001652605))
body_59.SetRot(chrono.ChQuaternionD(-1.31682959342233e-14,0.916421540606399,-2.11635409788436e-15,-0.400214392435596))
body_59.SetMass(0.02)
body_59.SetInertiaXX(chrono.ChVectorD(2.1102890321418e-05,3.42254939964149e-05,1.66734961647356e-05))
body_59.SetInertiaXY(chrono.ChVectorD(1.44186233077584e-08,8.15219838791517e-11,1.07420085359569e-11))
body_59.SetFrame_COG_to_REF(chrono.ChFrameD(chrono.ChVectorD(-2.37534873691438e-05,0.0334739058996041,1.03286017725802e-08),chrono.ChQuaternionD(1,0,0,0)))

# Visualization shape 
body_1_1_shape = chrono.ChObjShapeFile() 
body_1_1_shape.SetFilename(shapes_dir +'body_1_1.obj') 
body_1_1_level = chrono.ChAssetLevel() 
body_1_1_level.GetFrame().SetPos(chrono.ChVectorD(0,0,0)) 
body_1_1_level.GetFrame().SetRot(chrono.ChQuaternionD(1,0,0,0)) 
body_1_1_level.GetAssets().push_back(body_1_1_shape) 
body_59.GetAssets().push_back(body_1_1_level) 

exported_items.append(body_59)



# Rigid body part
body_60= chrono.ChBodyAuxRef()
body_60.SetName('robo_leg_link-2/link2-2')
body_60.SetPos(chrono.ChVectorD(2.12862244981808,0.0780000000000024,-1.98892001652605))
body_60.SetRot(chrono.ChQuaternionD(8.76605655732361e-18,0.916421540606399,-2.6875391686946e-16,-0.400214392435596))
body_60.SetMass(0.02)
body_60.SetInertiaXX(chrono.ChVectorD(1.04983041023413e-05,2.08152025768899e-05,1.23483128699975e-05))
body_60.SetInertiaXY(chrono.ChVectorD(-2.4713143785015e-11,-1.47996614267629e-11,-7.2032729945964e-11))
body_60.SetFrame_COG_to_REF(chrono.ChFrameD(chrono.ChVectorD(1.363578295109e-07,0.0335000115691779,0.0764730062719303),chrono.ChQuaternionD(1,0,0,0)))

# Visualization shape 
body_2_1_shape = chrono.ChObjShapeFile() 
body_2_1_shape.SetFilename(shapes_dir +'body_2_1.obj') 
body_2_1_level = chrono.ChAssetLevel() 
body_2_1_level.GetFrame().SetPos(chrono.ChVectorD(0,0,0)) 
body_2_1_level.GetFrame().SetRot(chrono.ChQuaternionD(1,0,0,0)) 
body_2_1_level.GetAssets().push_back(body_2_1_shape) 
body_60.GetAssets().push_back(body_2_1_level) 

# Collision shapes 
body_60.GetCollisionModel().ClearModel()
mr = chrono.ChMatrix33D()
mr[0,0]=1; mr[1,0]=0; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=1; mr[2,1]=0 
mr[0,2]=0; mr[1,2]=0; mr[2,2]=1 
body_60.GetCollisionModel().AddBox(0.0113982379521279,0.0140083638039784,0.013,chrono.ChVectorD(-0.000134890389966315,0.0335,0.0511),mr)
body_60.GetCollisionModel().BuildModel()
body_60.SetCollide(True)

exported_items.append(body_60)



# Rigid body part
body_61= chrono.ChBodyAuxRef()
body_61.SetName('robo_leg_link-2/leg-1')
body_61.SetPos(chrono.ChVectorD(2.13249027212167,0.0170000000000006,-1.98260972123757))
body_61.SetRot(chrono.ChQuaternionD(-0.181150341308432,0.81717013336765,0.414802860669143,-0.356869883509987))
body_61.SetMass(0.01)
body_61.SetInertiaXX(chrono.ChVectorD(1.89110723602526e-07,5.11889399343246e-07,5.94333456279105e-07))
body_61.SetInertiaXY(chrono.ChVectorD(-3.88054984756532e-11,-6.97451787573893e-24,-2.11470064202402e-24))
body_61.SetFrame_COG_to_REF(chrono.ChFrameD(chrono.ChVectorD(-5.02229192063373e-07,0.00570468434844058,0.004),chrono.ChQuaternionD(1,0,0,0)))

# Visualization shape 
body_3_1_shape = chrono.ChObjShapeFile() 
body_3_1_shape.SetFilename(shapes_dir +'body_3_1.obj') 
body_3_1_level = chrono.ChAssetLevel() 
body_3_1_level.GetFrame().SetPos(chrono.ChVectorD(0,0,0)) 
body_3_1_level.GetFrame().SetRot(chrono.ChQuaternionD(1,0,0,0)) 
body_3_1_level.GetAssets().push_back(body_3_1_shape) 
body_61.GetAssets().push_back(body_3_1_level) 

# Collision shapes 
body_61.GetCollisionModel().ClearModel()
mr = chrono.ChMatrix33D()
mr[0,0]=0; mr[1,0]=1; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=0; mr[2,1]=-1 
mr[0,2]=-1; mr[1,2]=0; mr[2,2]=0 
body_61.GetCollisionModel().AddCylinder(0.00178981093410736,0.00178981093410736,0.00400000000000001,chrono.ChVectorD(0.00836413257948577,0.0092893103184674,0.00399999999999999),mr)
mr = chrono.ChMatrix33D()
mr[0,0]=0; mr[1,0]=1; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=0; mr[2,1]=-1 
mr[0,2]=-1; mr[1,2]=0; mr[2,2]=0 
body_61.GetCollisionModel().AddCylinder(0.00178981093410736,0.00178981093410736,0.00400000000000001,chrono.ChVectorD(0.00195543081300292,0.0123461042574392,0.00399999999999999),mr)
mr = chrono.ChMatrix33D()
mr[0,0]=0; mr[1,0]=1; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=0; mr[2,1]=-1 
mr[0,2]=-1; mr[1,2]=0; mr[2,2]=0 
body_61.GetCollisionModel().AddCylinder(0.00178981093410736,0.00178981093410736,0.00400000000000001,chrono.ChVectorD(-0.00508420803844748,0.0114193182205325,0.00399999999999999),mr)
mr = chrono.ChMatrix33D()
mr[0,0]=0; mr[1,0]=1; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=0; mr[2,1]=-1 
mr[0,2]=-1; mr[1,2]=0; mr[2,2]=0 
body_61.GetCollisionModel().AddCylinder(0.00178981093410736,0.00178981093410736,0.00400000000000001,chrono.ChVectorD(-0.0104833820993178,0.00680798793768785,0.00399999999999999),mr)
mr = chrono.ChMatrix33D()
mr[0,0]=0; mr[1,0]=1; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=0; mr[2,1]=-1 
mr[0,2]=-1; mr[1,2]=0; mr[2,2]=0 
body_61.GetCollisionModel().AddCylinder(0.00178981093410736,0.00178981093410736,0.00400000000000001,chrono.ChVectorD(-0.0125,0,0.00399999999999999),mr)
mr = chrono.ChMatrix33D()
mr[0,0]=0; mr[1,0]=1; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=0; mr[2,1]=-1 
mr[0,2]=-1; mr[1,2]=0; mr[2,2]=0 
body_61.GetCollisionModel().AddCylinder(0.00178981093410736,0.00178981093410736,0.00400000000000001,chrono.ChVectorD(0.0120740728286134,0.00323523806378145,0.00399999999999999),mr)
body_61.GetCollisionModel().BuildModel()
body_61.SetCollide(True)

exported_items.append(body_61)



# Rigid body part
body_62= chrono.ChBodyAuxRef()
body_62.SetName('robo_leg_link-2/link2-1')
body_62.SetPos(chrono.ChVectorD(2.12862244981808,0.0110000000000002,-1.98892001652605))
body_62.SetRot(chrono.ChQuaternionD(0.916421540606399,-1.90239645851524e-16,0.400214392435596,-5.792371306524e-16))
body_62.SetMass(0.02)
body_62.SetInertiaXX(chrono.ChVectorD(1.04983041023413e-05,2.08152025768899e-05,1.23483128699975e-05))
body_62.SetInertiaXY(chrono.ChVectorD(-2.4713143785015e-11,-1.47996614267629e-11,-7.2032729945964e-11))
body_62.SetFrame_COG_to_REF(chrono.ChFrameD(chrono.ChVectorD(1.363578295109e-07,0.0335000115691779,0.0764730062719303),chrono.ChQuaternionD(1,0,0,0)))

# Visualization shape 
body_2_1_shape = chrono.ChObjShapeFile() 
body_2_1_shape.SetFilename(shapes_dir +'body_2_1.obj') 
body_2_1_level = chrono.ChAssetLevel() 
body_2_1_level.GetFrame().SetPos(chrono.ChVectorD(0,0,0)) 
body_2_1_level.GetFrame().SetRot(chrono.ChQuaternionD(1,0,0,0)) 
body_2_1_level.GetAssets().push_back(body_2_1_shape) 
body_62.GetAssets().push_back(body_2_1_level) 

# Collision shapes 
body_62.GetCollisionModel().ClearModel()
mr = chrono.ChMatrix33D()
mr[0,0]=1; mr[1,0]=0; mr[2,0]=0 
mr[0,1]=0; mr[1,1]=1; mr[2,1]=0 
mr[0,2]=0; mr[1,2]=0; mr[2,2]=1 
body_62.GetCollisionModel().AddBox(0.0113982379521279,0.0140083638039784,0.013,chrono.ChVectorD(-0.000134890389966315,0.0335,0.0511),mr)
body_62.GetCollisionModel().BuildModel()
body_62.SetCollide(True)

exported_items.append(body_62)




# Mate constraint: Concentric9 [MateConcentric] type:1 align:1 flip:False
#   Entity 0: C::E name: body_30 , SW name: robo_leg_link-8/link2-2 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_47 , SW name: robo_leg_link-9/link2-1 ,  SW ref.type:2 (2)

link_1 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.40231190179533,0.0910000005068609,-2.25270916258056)
dA = chrono.ChVectorD(2.77555756156289e-17,-1,-2.63677968348475e-16)
cB = chrono.ChVectorD(2.40231189881728,-0.00199999949313909,-2.25270916575638)
dB = chrono.ChVectorD(1.04083408558608e-17,1,2.91433543964104e-16)
link_1.SetFlipped(True)
link_1.Initialize(body_30,body_47,False,cA,cB,dA,dB)
link_1.SetName("Concentric9")
exported_items.append(link_1)

link_2 = chrono.ChLinkMateGeneric()
link_2.SetConstrainedCoords(False, True, True, False, False, False)
cA = chrono.ChVectorD(2.40231190179533,0.0910000005068609,-2.25270916258056)
cB = chrono.ChVectorD(2.40231189881728,-0.00199999949313909,-2.25270916575638)
dA = chrono.ChVectorD(2.77555756156289e-17,-1,-2.63677968348475e-16)
dB = chrono.ChVectorD(1.04083408558608e-17,1,2.91433543964104e-16)
link_2.Initialize(body_30,body_47,False,cA,cB,dA,dB)
link_2.SetName("Concentric9")
exported_items.append(link_2)


# Mate constraint: Coincident22 [MateCoincident] type:0 align:0 flip:False
#   Entity 0: C::E name: body_30 , SW name: robo_leg_link-8/link2-2 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_47 , SW name: robo_leg_link-9/link2-1 ,  SW ref.type:2 (2)

link_3 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(2.44057922100131,0.0585083643108394,-2.23564392654975)
cB = chrono.ChVectorD(2.3785853755477,0.0585083643108393,-2.28724410594075)
dA = chrono.ChVectorD(-2.67995526953467e-16,1,8.02395328441059e-16)
dB = chrono.ChVectorD(1.04083408558608e-17,1,2.91433543964104e-16)
link_3.Initialize(body_30,body_47,False,cA,cB,dB)
link_3.SetDistance(0)
link_3.SetName("Coincident22")
exported_items.append(link_3)

link_4 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.44057922100131,0.0585083643108394,-2.23564392654975)
dA = chrono.ChVectorD(-2.67995526953467e-16,1,8.02395328441059e-16)
cB = chrono.ChVectorD(2.3785853755477,0.0585083643108393,-2.28724410594075)
dB = chrono.ChVectorD(1.04083408558608e-17,1,2.91433543964104e-16)
link_4.Initialize(body_30,body_47,False,cA,cB,dA,dB)
link_4.SetName("Coincident22")
exported_items.append(link_4)


# Mate constraint: Concentric11 [MateConcentric] type:1 align:1 flip:False
#   Entity 0: C::E name: body_21 , SW name: robo_leg_link-7/link2-2 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_29 , SW name: robo_leg_link-8/link2-1 ,  SW ref.type:2 (2)

link_5 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.26312480214786,0.0910000003814678,-2.31477937734901)
dA = chrono.ChVectorD(3.05311331771918e-16,-1,2.10248485288389e-15)
cB = chrono.ChVectorD(2.26312480282608,-0.00199999961852441,-2.31477937668593)
dB = chrono.ChVectorD(-2.77555756156289e-16,1,9.78384040450919e-16)
link_5.SetFlipped(True)
link_5.Initialize(body_21,body_29,False,cA,cB,dA,dB)
link_5.SetName("Concentric11")
exported_items.append(link_5)

link_6 = chrono.ChLinkMateGeneric()
link_6.SetConstrainedCoords(False, True, True, False, False, False)
cA = chrono.ChVectorD(2.26312480214786,0.0910000003814678,-2.31477937734901)
cB = chrono.ChVectorD(2.26312480282608,-0.00199999961852441,-2.31477937668593)
dA = chrono.ChVectorD(3.05311331771918e-16,-1,2.10248485288389e-15)
dB = chrono.ChVectorD(-2.77555756156289e-16,1,9.78384040450919e-16)
link_6.Initialize(body_21,body_29,False,cA,cB,dA,dB)
link_6.SetName("Concentric11")
exported_items.append(link_6)


# Mate constraint: Coincident24 [MateCoincident] type:0 align:0 flip:False
#   Entity 0: C::E name: body_21 , SW name: robo_leg_link-7/link2-2 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_29 , SW name: robo_leg_link-8/link2-1 ,  SW ref.type:2 (2)

link_7 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(2.29495932544201,0.0585083641854463,-2.28753652275126)
cB = chrono.ChVectorD(2.22485748362009,0.058508364185454,-2.33184461271674)
dA = chrono.ChVectorD(-6.88829148405149e-16,1,-1.65432671939883e-15)
dB = chrono.ChVectorD(-2.77555756156289e-16,1,9.78384040450919e-16)
link_7.Initialize(body_21,body_29,False,cA,cB,dB)
link_7.SetDistance(0)
link_7.SetName("Coincident24")
exported_items.append(link_7)

link_8 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.29495932544201,0.0585083641854463,-2.28753652275126)
dA = chrono.ChVectorD(-6.88829148405149e-16,1,-1.65432671939883e-15)
cB = chrono.ChVectorD(2.22485748362009,0.058508364185454,-2.33184461271674)
dB = chrono.ChVectorD(-2.77555756156289e-16,1,9.78384040450919e-16)
link_8.Initialize(body_21,body_29,False,cA,cB,dA,dB)
link_8.SetName("Coincident24")
exported_items.append(link_8)


# Mate constraint: Concentric13 [MateConcentric] type:1 align:1 flip:False
#   Entity 0: C::E name: body_9 , SW name: robo_leg_link-6/link2-2 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_25 , SW name: robo_leg_link-7/link2-1 ,  SW ref.type:2 (2)

link_9 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.14733527284312,0.0910000003984776,-2.41386794480721)
dA = chrono.ChVectorD(1.2490009027033e-15,-1,1.01307850997046e-15)
cB = chrono.ChVectorD(2.14733527111643,-0.00199999960152214,-2.41386794634324)
dB = chrono.ChVectorD(-1.26287869051112e-15,1,-1.04083408558608e-15)
link_9.SetFlipped(True)
link_9.Initialize(body_9,body_25,False,cA,cB,dA,dB)
link_9.SetName("Concentric13")
exported_items.append(link_9)

link_10 = chrono.ChLinkMateGeneric()
link_10.SetConstrainedCoords(False, True, True, False, False, False)
cA = chrono.ChVectorD(2.14733527284312,0.0910000003984776,-2.41386794480721)
cB = chrono.ChVectorD(2.14733527111643,-0.00199999960152214,-2.41386794634324)
dA = chrono.ChVectorD(1.2490009027033e-15,-1,1.01307850997046e-15)
dB = chrono.ChVectorD(-1.26287869051112e-15,1,-1.04083408558608e-15)
link_10.Initialize(body_9,body_25,False,cA,cB,dA,dB)
link_10.SetName("Concentric13")
exported_items.append(link_10)


# Mate constraint: Coincident26 [MateCoincident] type:0 align:0 flip:False
#   Entity 0: C::E name: body_9 , SW name: robo_leg_link-6/link2-2 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_25 , SW name: robo_leg_link-7/link2-1 ,  SW ref.type:2 (2)

link_11 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(2.18920758549555,0.0585083642024561,-2.41234497627944)
cB = chrono.ChVectorD(2.11550074782228,0.0585083642024562,-2.44111080094099)
dA = chrono.ChVectorD(-1.27044085726002e-15,1,-4.23610990396615e-16)
dB = chrono.ChVectorD(-1.26287869051112e-15,1,-1.04083408558608e-15)
link_11.Initialize(body_9,body_25,False,cA,cB,dB)
link_11.SetDistance(0)
link_11.SetName("Coincident26")
exported_items.append(link_11)

link_12 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.18920758549555,0.0585083642024561,-2.41234497627944)
dA = chrono.ChVectorD(-1.27044085726002e-15,1,-4.23610990396615e-16)
cB = chrono.ChVectorD(2.11550074782228,0.0585083642024562,-2.44111080094099)
dB = chrono.ChVectorD(-1.26287869051112e-15,1,-1.04083408558608e-15)
link_12.Initialize(body_9,body_25,False,cA,cB,dA,dB)
link_12.SetName("Coincident26")
exported_items.append(link_12)


# Mate constraint: Concentric14 [MateConcentric] type:1 align:1 flip:False
#   Entity 0: C::E name: body_2 , SW name: robo_leg_link-5/link2-2 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_6 , SW name: robo_leg_link-6/link2-1 ,  SW ref.type:2 (2)

link_13 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(1.99503597963782,0.0910000000000092,-2.41940733133182)
dA = chrono.ChVectorD(1.33226762955019e-15,-1,2.63677968348475e-16)
cB = chrono.ChVectorD(1.99503597815332,-0.00199999954171871,-2.41940733391474)
dB = chrono.ChVectorD(-1.29063426612674e-15,1,-2.4980018054066e-16)
link_13.SetFlipped(True)
link_13.Initialize(body_2,body_6,False,cA,cB,dA,dB)
link_13.SetName("Concentric14")
exported_items.append(link_13)

link_14 = chrono.ChLinkMateGeneric()
link_14.SetConstrainedCoords(False, True, True, False, False, False)
cA = chrono.ChVectorD(1.99503597963782,0.0910000000000092,-2.41940733133182)
cB = chrono.ChVectorD(1.99503597815332,-0.00199999954171871,-2.41940733391474)
dA = chrono.ChVectorD(1.33226762955019e-15,-1,2.63677968348475e-16)
dB = chrono.ChVectorD(-1.29063426612674e-15,1,-2.4980018054066e-16)
link_14.Initialize(body_2,body_6,False,cA,cB,dA,dB)
link_14.SetName("Concentric14")
exported_items.append(link_14)


# Mate constraint: Coincident27 [MateCoincident] type:0 align:0 flip:False
#   Entity 0: C::E name: body_2 , SW name: robo_leg_link-5/link2-2 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_6 , SW name: robo_leg_link-6/link2-1 ,  SW ref.type:2 (2)

link_15 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(2.01243469261321,0.0585083638039877,-2.45752418832187)
cB = chrono.ChVectorD(1.95316366550089,0.0585083642622597,-2.42093030244251)
dA = chrono.ChVectorD(-7.95668437373318e-16,1,-1.87434153775619e-17)
dB = chrono.ChVectorD(-1.29063426612674e-15,1,-2.4980018054066e-16)
link_15.Initialize(body_2,body_6,False,cA,cB,dB)
link_15.SetDistance(0)
link_15.SetName("Coincident27")
exported_items.append(link_15)

link_16 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.01243469261321,0.0585083638039877,-2.45752418832187)
dA = chrono.ChVectorD(-7.95668437373318e-16,1,-1.87434153775619e-17)
cB = chrono.ChVectorD(1.95316366550089,0.0585083642622597,-2.42093030244251)
dB = chrono.ChVectorD(-1.29063426612674e-15,1,-2.4980018054066e-16)
link_16.Initialize(body_2,body_6,False,cA,cB,dA,dB)
link_16.SetName("Coincident27")
exported_items.append(link_16)


# Mate constraint: Concentric15 [MateConcentric] type:1 align:1 flip:False
#   Entity 0: C::E name: body_39 , SW name: robo_leg_link-4/link2-2 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_4 , SW name: robo_leg_link-5/link2-1 ,  SW ref.type:2 (2)

link_17 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(1.93175283268197,0.0910000000000073,-2.28076749827017)
dA = chrono.ChVectorD(-1.02663250508198e-16,-1,-2.92907133029236e-16)
cB = chrono.ChVectorD(1.93175283268197,-0.00199999999999308,-2.28076749827017)
dB = chrono.ChVectorD(-2.77555756156289e-17,1,2.77555756156289e-16)
link_17.SetFlipped(True)
link_17.Initialize(body_39,body_4,False,cA,cB,dA,dB)
link_17.SetName("Concentric15")
exported_items.append(link_17)

link_18 = chrono.ChLinkMateGeneric()
link_18.SetConstrainedCoords(False, True, True, False, False, False)
cA = chrono.ChVectorD(1.93175283268197,0.0910000000000073,-2.28076749827017)
cB = chrono.ChVectorD(1.93175283268197,-0.00199999999999308,-2.28076749827017)
dA = chrono.ChVectorD(-1.02663250508198e-16,-1,-2.92907133029236e-16)
dB = chrono.ChVectorD(-2.77555756156289e-17,1,2.77555756156289e-16)
link_18.Initialize(body_39,body_4,False,cA,cB,dA,dB)
link_18.SetName("Concentric15")
exported_items.append(link_18)


# Mate constraint: Coincident29 [MateCoincident] type:0 align:0 flip:False
#   Entity 0: C::E name: body_39 , SW name: robo_leg_link-4/link2-2 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_4 , SW name: robo_leg_link-5/link2-1 ,  SW ref.type:2 (2)

link_19 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(1.92708442326902,0.0585083638039857,-2.32240661393745)
cB = chrono.ChVectorD(1.91435411970657,0.0585083638039853,-2.24265064128012)
dA = chrono.ChVectorD(6.88847883618289e-16,1,2.27186480342903e-16)
dB = chrono.ChVectorD(-2.77555756156289e-17,1,2.77555756156289e-16)
link_19.Initialize(body_39,body_4,False,cA,cB,dB)
link_19.SetDistance(0)
link_19.SetName("Coincident29")
exported_items.append(link_19)

link_20 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(1.92708442326902,0.0585083638039857,-2.32240661393745)
dA = chrono.ChVectorD(6.88847883618289e-16,1,2.27186480342903e-16)
cB = chrono.ChVectorD(1.91435411970657,0.0585083638039853,-2.24265064128012)
dB = chrono.ChVectorD(-2.77555756156289e-17,1,2.77555756156289e-16)
link_20.Initialize(body_39,body_4,False,cA,cB,dA,dB)
link_20.SetName("Coincident29")
exported_items.append(link_20)


# Mate constraint: Concentric16 [MateConcentric] type:1 align:1 flip:False
#   Entity 0: C::E name: body_19 , SW name: robo_leg_link-3/link2-2 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_38 , SW name: robo_leg_link-4/link2-1 ,  SW ref.type:2 (2)

link_21 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(1.94873291725171,0.0910000000239365,-2.12931639806536)
dA = chrono.ChVectorD(-9.42195253166489e-18,-1,-2.79644947264588e-16)
cB = chrono.ChVectorD(1.94873291847034,-0.0019999999999952,-2.12931639498393)
dB = chrono.ChVectorD(1.38777878078145e-17,1,2.98372437868011e-16)
link_21.SetFlipped(True)
link_21.Initialize(body_19,body_38,False,cA,cB,dA,dB)
link_21.SetName("Concentric16")
exported_items.append(link_21)

link_22 = chrono.ChLinkMateGeneric()
link_22.SetConstrainedCoords(False, True, True, False, False, False)
cA = chrono.ChVectorD(1.94873291725171,0.0910000000239365,-2.12931639806536)
cB = chrono.ChVectorD(1.94873291847034,-0.0019999999999952,-2.12931639498393)
dA = chrono.ChVectorD(-9.42195253166489e-18,-1,-2.79644947264588e-16)
dB = chrono.ChVectorD(1.38777878078145e-17,1,2.98372437868011e-16)
link_22.Initialize(body_19,body_38,False,cA,cB,dA,dB)
link_22.SetName("Concentric16")
exported_items.append(link_22)


# Mate constraint: Coincident30 [MateCoincident] type:0 align:0 flip:False
#   Entity 0: C::E name: body_19 , SW name: robo_leg_link-3/link2-2 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_38 , SW name: robo_leg_link-4/link2-1 ,  SW ref.type:2 (2)

link_23 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(1.91464255718082,0.0585083638279149,-2.15367737814082)
cB = chrono.ChVectorD(1.95340132788329,0.0585083638039832,-2.08767727931665)
dA = chrono.ChVectorD(3.52369489694634e-16,1,-2.00270269245353e-16)
dB = chrono.ChVectorD(1.38777878078145e-17,1,2.98372437868011e-16)
link_23.Initialize(body_19,body_38,False,cA,cB,dB)
link_23.SetDistance(0)
link_23.SetName("Coincident30")
exported_items.append(link_23)

link_24 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(1.91464255718082,0.0585083638279149,-2.15367737814082)
dA = chrono.ChVectorD(3.52369489694634e-16,1,-2.00270269245353e-16)
cB = chrono.ChVectorD(1.95340132788329,0.0585083638039832,-2.08767727931665)
dB = chrono.ChVectorD(1.38777878078145e-17,1,2.98372437868011e-16)
link_24.Initialize(body_19,body_38,False,cA,cB,dA,dB)
link_24.SetName("Coincident30")
exported_items.append(link_24)


# Mate constraint: Concentric17 [MateConcentric] type:1 align:1 flip:False
#   Entity 0: C::E name: body_50 , SW name: robo_leg_link-9/link2-2 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_14 , SW name: robo_leg_link-10/link2-1 ,  SW ref.type:2 (2)

link_25 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.48861075672498,0.0910000005011455,-2.12709759334183)
dA = chrono.ChVectorD(-7.71084585071691e-16,-1,9.71445146547012e-17)
cB = chrono.ChVectorD(2.48861076780065,-0.0020000047854201,-2.1270975840007)
dB = chrono.ChVectorD(7.99360577730113e-15,1,8.15320033709099e-16)
link_25.SetFlipped(True)
link_25.Initialize(body_50,body_14,False,cA,cB,dA,dB)
link_25.SetName("Concentric17")
exported_items.append(link_25)

link_26 = chrono.ChLinkMateGeneric()
link_26.SetConstrainedCoords(False, True, True, False, False, False)
cA = chrono.ChVectorD(2.48861075672498,0.0910000005011455,-2.12709759334183)
cB = chrono.ChVectorD(2.48861076780065,-0.0020000047854201,-2.1270975840007)
dA = chrono.ChVectorD(-7.71084585071691e-16,-1,9.71445146547012e-17)
dB = chrono.ChVectorD(7.99360577730113e-15,1,8.15320033709099e-16)
link_26.Initialize(body_50,body_14,False,cA,cB,dA,dB)
link_26.SetName("Concentric17")
exported_items.append(link_26)


# Mate constraint: Coincident36 [MateCoincident] type:0 align:0 flip:False
#   Entity 0: C::E name: body_50 , SW name: robo_leg_link-9/link2-2 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_14 , SW name: robo_leg_link-10/link2-1 ,  SW ref.type:2 (2)

link_27 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(2.51233727999456,0.0585083643051239,-2.09256265315746)
cB = chrono.ChVectorD(2.48859311591326,0.0585083590185584,-2.16899758028245)
dA = chrono.ChVectorD(2.84910685433307e-16,1,2.36871304722669e-16)
dB = chrono.ChVectorD(7.99360577730113e-15,1,8.15320033709099e-16)
link_27.Initialize(body_50,body_14,False,cA,cB,dB)
link_27.SetDistance(0)
link_27.SetName("Coincident36")
exported_items.append(link_27)

link_28 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.51233727999456,0.0585083643051239,-2.09256265315746)
dA = chrono.ChVectorD(2.84910685433307e-16,1,2.36871304722669e-16)
cB = chrono.ChVectorD(2.48859311591326,0.0585083590185584,-2.16899758028245)
dB = chrono.ChVectorD(7.99360577730113e-15,1,8.15320033709099e-16)
link_28.Initialize(body_50,body_14,False,cA,cB,dA,dB)
link_28.SetName("Coincident36")
exported_items.append(link_28)


# Mate constraint: Concentric22 [MateConcentric] type:1 align:1 flip:False
#   Entity 0: C::E name: body_60 , SW name: robo_leg_link-2/link2-2 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_20 , SW name: robo_leg_link-3/link2-1 ,  SW ref.type:2 (2)

link_29 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.07272745008857,0.0910000000000024,-2.04070987079537)
dA = chrono.ChVectorD(-4.85567153084739e-16,-1,2.31185177219806e-16)
cB = chrono.ChVectorD(2.07272744879656,-0.00199999984083585,-2.04070987392923)
dB = chrono.ChVectorD(4.57966997657877e-16,1,-2.4980018054066e-16)
link_29.SetFlipped(True)
link_29.Initialize(body_60,body_20,False,cA,cB,dA,dB)
link_29.SetName("Concentric22")
exported_items.append(link_29)

link_30 = chrono.ChLinkMateGeneric()
link_30.SetConstrainedCoords(False, True, True, False, False, False)
cA = chrono.ChVectorD(2.07272745008857,0.0910000000000024,-2.04070987079537)
cB = chrono.ChVectorD(2.07272744879656,-0.00199999984083585,-2.04070987392923)
dA = chrono.ChVectorD(-4.85567153084739e-16,-1,2.31185177219806e-16)
dB = chrono.ChVectorD(4.57966997657877e-16,1,-2.4980018054066e-16)
link_30.Initialize(body_60,body_20,False,cA,cB,dA,dB)
link_30.SetName("Concentric22")
exported_items.append(link_30)


# Mate constraint: Coincident55 [MateCoincident] type:0 align:0 flip:False
#   Entity 0: C::E name: body_60 , SW name: robo_leg_link-2/link2-2 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_20 , SW name: robo_leg_link-3/link2-1 ,  SW ref.type:2 (2)

link_31 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(2.04199253553913,0.0585083638039809,-2.06918749407469)
cB = chrono.ChVectorD(2.10681780886745,0.0585083639631426,-2.01634889385377)
dA = chrono.ChVectorD(8.86467721848639e-16,1,-6.63863304987679e-16)
dB = chrono.ChVectorD(4.57966997657877e-16,1,-2.4980018054066e-16)
link_31.Initialize(body_60,body_20,False,cA,cB,dB)
link_31.SetDistance(0)
link_31.SetName("Coincident55")
exported_items.append(link_31)

link_32 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.04199253553913,0.0585083638039809,-2.06918749407469)
dA = chrono.ChVectorD(8.86467721848639e-16,1,-6.63863304987679e-16)
cB = chrono.ChVectorD(2.10681780886745,0.0585083639631426,-2.01634889385377)
dB = chrono.ChVectorD(4.57966997657877e-16,1,-2.4980018054066e-16)
link_32.Initialize(body_60,body_20,False,cA,cB,dA,dB)
link_32.SetName("Coincident55")
exported_items.append(link_32)


# Mate constraint: Concentric27 [MateConcentric] type:1 align:1 flip:False
#   Entity 0: C::E name: body_32 , SW name: robo_leg_link-11/link2-2 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_44 , SW name: robo_leg_link-12/link2-1 ,  SW ref.type:2 (2)

link_33 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.46226197366704,0.0910000002246144,-1.82460391537468)
dA = chrono.ChVectorD(-1.22124532708767e-15,-1,1.11369247157711e-15)
cB = chrono.ChVectorD(2.46226197202797,-0.0020000000000051,-1.82460391703883)
dB = chrono.ChVectorD(1.18134668714021e-15,1,-1.09634523681734e-15)
link_33.SetFlipped(True)
link_33.Initialize(body_32,body_44,False,cA,cB,dA,dB)
link_33.SetName("Concentric27")
exported_items.append(link_33)

link_34 = chrono.ChLinkMateGeneric()
link_34.SetConstrainedCoords(False, True, True, False, False, False)
cA = chrono.ChVectorD(2.46226197366704,0.0910000002246144,-1.82460391537468)
cB = chrono.ChVectorD(2.46226197202797,-0.0020000000000051,-1.82460391703883)
dA = chrono.ChVectorD(-1.22124532708767e-15,-1,1.11369247157711e-15)
dB = chrono.ChVectorD(1.18134668714021e-15,1,-1.09634523681734e-15)
link_34.Initialize(body_32,body_44,False,cA,cB,dA,dB)
link_34.SetName("Concentric27")
exported_items.append(link_34)


# Mate constraint: Coincident58 [MateCoincident] type:0 align:0 flip:False
#   Entity 0: C::E name: body_32 , SW name: robo_leg_link-11/link2-2 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_44 , SW name: robo_leg_link-12/link2-1 ,  SW ref.type:2 (2)

link_35 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(2.45500013339127,0.0585083640285929,-1.78333800116199)
cB = chrono.ChVectorD(2.50331619648424,0.0585083638039733,-1.81622772134542)
dA = chrono.ChVectorD(6.4031452692356e-16,1,-1.21592276896792e-15)
dB = chrono.ChVectorD(1.18134668714021e-15,1,-1.09634523681734e-15)
link_35.Initialize(body_32,body_44,False,cA,cB,dB)
link_35.SetDistance(0)
link_35.SetName("Coincident58")
exported_items.append(link_35)

link_36 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.45500013339127,0.0585083640285929,-1.78333800116199)
dA = chrono.ChVectorD(6.4031452692356e-16,1,-1.21592276896792e-15)
cB = chrono.ChVectorD(2.50331619648424,0.0585083638039733,-1.81622772134542)
dB = chrono.ChVectorD(1.18134668714021e-15,1,-1.09634523681734e-15)
link_36.Initialize(body_32,body_44,False,cA,cB,dA,dB)
link_36.SetName("Coincident58")
exported_items.append(link_36)


# Mate constraint: Concentric28 [MateConcentric] type:1 align:1 flip:False
#   Entity 0: C::E name: body_15 , SW name: robo_leg_link-10/link2-2 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_35 , SW name: robo_leg_link-11/link2-1 ,  SW ref.type:2 (2)

link_37 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.48867496706664,0.0909999994243681,-1.97469760044771)
dA = chrono.ChVectorD(-7.95197241387768e-15,-1,-8.15320033709099e-16)
cB = chrono.ChVectorD(2.48867496789115,-0.0019999991355986,-1.97469759856811)
dB = chrono.ChVectorD(4.57966997657877e-16,1,-1.2490009027033e-15)
link_37.SetFlipped(True)
link_37.Initialize(body_15,body_35,False,cA,cB,dA,dB)
link_37.SetName("Concentric28")
exported_items.append(link_37)

link_38 = chrono.ChLinkMateGeneric()
link_38.SetConstrainedCoords(False, True, True, False, False, False)
cA = chrono.ChVectorD(2.48867496706664,0.0909999994243681,-1.97469760044771)
cB = chrono.ChVectorD(2.48867496789115,-0.0019999991355986,-1.97469759856811)
dA = chrono.ChVectorD(-7.95197241387768e-15,-1,-8.15320033709099e-16)
dB = chrono.ChVectorD(4.57966997657877e-16,1,-1.2490009027033e-15)
link_38.Initialize(body_15,body_35,False,cA,cB,dA,dB)
link_38.SetName("Concentric28")
exported_items.append(link_38)


# Mate constraint: Coincident59 [MateCoincident] type:0 align:0 flip:False
#   Entity 0: C::E name: body_15 , SW name: robo_leg_link-10/link2-2 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_35 , SW name: robo_leg_link-11/link2-1 ,  SW ref.type:2 (2)

link_39 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(2.48869261895403,0.0585083632283466,-1.93279760416596)
cB = chrono.ChVectorD(2.49593680816692,0.0585083646683798,-2.0159635127808)
dA = chrono.ChVectorD(7.36211517137545e-15,1,8.15568532385669e-16)
dB = chrono.ChVectorD(4.57966997657877e-16,1,-1.2490009027033e-15)
link_39.Initialize(body_15,body_35,False,cA,cB,dB)
link_39.SetDistance(0)
link_39.SetName("Coincident59")
exported_items.append(link_39)

link_40 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.48869261895403,0.0585083632283466,-1.93279760416596)
dA = chrono.ChVectorD(7.36211517137545e-15,1,8.15568532385669e-16)
cB = chrono.ChVectorD(2.49593680816692,0.0585083646683798,-2.0159635127808)
dB = chrono.ChVectorD(4.57966997657877e-16,1,-1.2490009027033e-15)
link_40.Initialize(body_15,body_35,False,cA,cB,dA,dB)
link_40.SetName("Coincident59")
exported_items.append(link_40)


# Mate constraint: Parallel2 [MateParallel] type:3 align:0 flip:False
#   Entity 0: C::E name: body_27 , SW name: robo_leg_link-8/single_bot1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_22 , SW name: robo_leg_link-7/single_bot1-1 ,  SW ref.type:2 (2)

link_41 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.31825387829483,0.0099999999999579,-2.32149321987767)
dA = chrono.ChVectorD(2.22044604925031e-16,-1,-8.16187395447088e-16)
cB = chrono.ChVectorD(2.20209751038717,0.0100000003907951,-2.40462740521178)
dB = chrono.ChVectorD(6.10622663543836e-15,-1,-4.65599780952175e-15)
link_41.Initialize(body_27,body_22,False,cA,cB,dA,dB)
link_41.SetName("Parallel2")
exported_items.append(link_41)


# Mate constraint: Parallel3 [MateParallel] type:3 align:0 flip:False
#   Entity 0: C::E name: body_27 , SW name: robo_leg_link-8/single_bot1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_8 , SW name: robo_leg_link-6/single_bot1-1 ,  SW ref.type:2 (2)

link_42 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.31825387829483,0.0099999999999579,-2.32149321987767)
dA = chrono.ChVectorD(2.22044604925031e-16,-1,-8.16187395447088e-16)
cB = chrono.ChVectorD(2.04365851477653,0.0100000000000099,-2.44624274686214)
dB = chrono.ChVectorD(1.31838984174237e-15,-1,2.70616862252382e-16)
link_42.Initialize(body_27,body_8,False,cA,cB,dA,dB)
link_42.SetName("Parallel3")
exported_items.append(link_42)


# Mate constraint: Parallel4 [MateParallel] type:3 align:0 flip:False
#   Entity 0: C::E name: body_27 , SW name: robo_leg_link-8/single_bot1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_5 , SW name: robo_leg_link-5/single_bot1-1 ,  SW ref.type:2 (2)

link_43 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.31825387829483,0.0099999999999579,-2.32149321987767)
dA = chrono.ChVectorD(2.22044604925031e-16,-1,-8.16187395447088e-16)
cB = chrono.ChVectorD(1.92552059786724,0.0100000000000085,-2.33595309149172)
dB = chrono.ChVectorD(-6.78537087628328e-15,-1,-3.41393580072236e-15)
link_43.Initialize(body_27,body_5,False,cA,cB,dA,dB)
link_43.SetName("Parallel4")
exported_items.append(link_43)


# Mate constraint: Parallel5 [MateParallel] type:3 align:0 flip:False
#   Entity 0: C::E name: body_27 , SW name: robo_leg_link-8/single_bot1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_36 , SW name: robo_leg_link-4/single_bot1-1 ,  SW ref.type:2 (2)

link_44 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.31825387829483,0.0099999999999579,-2.32149321987767)
dA = chrono.ChVectorD(2.22044604925031e-16,-1,-8.16187395447088e-16)
cB = chrono.ChVectorD(1.91502073623772,0.0100000000000053,-2.17345004676027)
dB = chrono.ChVectorD(-5.55111512312578e-17,-1,-2.91433543964104e-16)
link_44.Initialize(body_27,body_36,False,cA,cB,dA,dB)
link_44.SetName("Parallel5")
exported_items.append(link_44)


# Mate constraint: Parallel6 [MateParallel] type:3 align:0 flip:False
#   Entity 0: C::E name: body_27 , SW name: robo_leg_link-8/single_bot1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_16 , SW name: robo_leg_link-3/single_bot1-1 ,  SW ref.type:2 (2)

link_45 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.31825387829483,0.0099999999999579,-2.32149321987767)
dA = chrono.ChVectorD(2.22044604925031e-16,-1,-8.16187395447088e-16)
cB = chrono.ChVectorD(2.01736775617031,0.0100000034943457,-2.04513648804354)
dB = chrono.ChVectorD(-5.23192600354605e-15,-1,7.11063152802893e-15)
link_45.Initialize(body_27,body_16,False,cA,cB,dA,dB)
link_45.SetName("Parallel6")
exported_items.append(link_45)


# Mate constraint: Parallel7 [MateParallel] type:3 align:0 flip:False
#   Entity 0: C::E name: body_27 , SW name: robo_leg_link-8/single_bot1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_58 , SW name: robo_leg_link-2/single_bot1-1 ,  SW ref.type:2 (2)

link_46 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.31825387829483,0.0099999999999579,-2.32149321987767)
dA = chrono.ChVectorD(2.22044604925031e-16,-1,-8.16187395447088e-16)
cB = chrono.ChVectorD(2.13016241809865,0.0100000000000006,-1.94852406440588)
dB = chrono.ChVectorD(-9.29811783123569e-16,-1,6.80011602582908e-16)
link_46.Initialize(body_27,body_58,False,cA,cB,dA,dB)
link_46.SetName("Parallel7")
exported_items.append(link_46)


# Mate constraint: Parallel8 [MateParallel] type:3 align:0 flip:False
#   Entity 0: C::E name: body_27 , SW name: robo_leg_link-8/single_bot1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_53 , SW name: robo_leg_link-1/single_bot1-1 ,  SW ref.type:2 (2)

link_47 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.31825387829483,0.0099999999999579,-2.32149321987767)
dA = chrono.ChVectorD(2.22044604925031e-16,-1,-8.16187395447088e-16)
cB = chrono.ChVectorD(2.25742352885081,0.009999999999999,-1.85662114381386)
dB = chrono.ChVectorD(-5.23192600354605e-15,-1,7.55819018483095e-15)
link_47.Initialize(body_27,body_53,False,cA,cB,dA,dB)
link_47.SetName("Parallel8")
exported_items.append(link_47)


# Mate constraint: Parallel9 [MateParallel] type:3 align:0 flip:False
#   Entity 0: C::E name: body_27 , SW name: robo_leg_link-8/single_bot1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_42 , SW name: robo_leg_link-12/single_bot1-1 ,  SW ref.type:2 (2)

link_48 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.31825387829483,0.0099999999999579,-2.32149321987767)
dA = chrono.ChVectorD(2.22044604925031e-16,-1,-8.16187395447088e-16)
cB = chrono.ChVectorD(2.40989370349858,0.00999999999999572,-1.80611459846745)
dB = chrono.ChVectorD(-1.19348975147204e-15,-1,1.15532583500055e-15)
link_48.Initialize(body_27,body_42,False,cA,cB,dA,dB)
link_48.SetName("Parallel9")
exported_items.append(link_48)


# Mate constraint: Parallel10 [MateParallel] type:3 align:0 flip:False
#   Entity 0: C::E name: body_27 , SW name: robo_leg_link-8/single_bot1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_33 , SW name: robo_leg_link-11/single_bot1-1 ,  SW ref.type:2 (2)

link_49 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.31825387829483,0.0099999999999579,-2.32149321987767)
dA = chrono.ChVectorD(2.22044604925031e-16,-1,-8.16187395447088e-16)
cB = chrono.ChVectorD(2.50857505313207,0.0100000040967927,-1.92284900465815)
dB = chrono.ChVectorD(7.00481339599435e-15,-1,6.34214902817121e-15)
link_49.Initialize(body_27,body_33,False,cA,cB,dA,dB)
link_49.SetName("Parallel10")
exported_items.append(link_49)


# Mate constraint: Parallel11 [MateParallel] type:3 align:0 flip:False
#   Entity 0: C::E name: body_27 , SW name: robo_leg_link-8/single_bot1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_11 , SW name: robo_leg_link-10/single_bot1-1 ,  SW ref.type:2 (2)

link_50 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.31825387829483,0.0099999999999579,-2.32149321987767)
dA = chrono.ChVectorD(2.22044604925031e-16,-1,-8.16187395447088e-16)
cB = chrono.ChVectorD(2.51721581925281,0.00999999999999122,-2.07949463340157)
dB = chrono.ChVectorD(2.28983498828938e-16,-1,-8.18789480661053e-16)
link_50.Initialize(body_27,body_11,False,cA,cB,dA,dB)
link_50.SetName("Parallel11")
exported_items.append(link_50)


# Mate constraint: Parallel12 [MateParallel] type:3 align:0 flip:False
#   Entity 0: C::E name: body_27 , SW name: robo_leg_link-8/single_bot1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_51 , SW name: robo_leg_link-9/single_bot1-1 ,  SW ref.type:2 (2)

link_51 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.31825387829483,0.0099999999999579,-2.32149321987767)
dA = chrono.ChVectorD(2.22044604925031e-16,-1,-8.16187395447088e-16)
cB = chrono.ChVectorD(2.45283504684315,0.00999999999998913,-2.22965048348832)
dB = chrono.ChVectorD(6.12010442324618e-15,-1,-4.38538094726937e-15)
link_51.Initialize(body_27,body_51,False,cA,cB,dA,dB)
link_51.SetName("Parallel12")
exported_items.append(link_51)


# Mate constraint: Concentric29 [MateConcentric] type:1 align:1 flip:False
#   Entity 0: C::E name: body_57 , SW name: robo_leg_link-1/link2-2 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_62 , SW name: robo_leg_link-2/link2-1 ,  SW ref.type:2 (2)

link_52 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.1845174495476,0.0909999999999997,-1.93713016225673)
dA = chrono.ChVectorD(-8.04911692853238e-16,-1,8.07513778067204e-16)
cB = chrono.ChVectorD(2.1845174495476,-0.00199999999999983,-1.93713016225673)
dB = chrono.ChVectorD(8.88178419700125e-16,1,-8.08381139805192e-16)
link_52.SetFlipped(True)
link_52.Initialize(body_57,body_62,False,cA,cB,dA,dB)
link_52.SetName("Concentric29")
exported_items.append(link_52)

link_53 = chrono.ChLinkMateGeneric()
link_53.SetConstrainedCoords(False, True, True, False, False, False)
cA = chrono.ChVectorD(2.1845174495476,0.0909999999999997,-1.93713016225673)
cB = chrono.ChVectorD(2.1845174495476,-0.00199999999999983,-1.93713016225673)
dA = chrono.ChVectorD(-8.04911692853238e-16,-1,8.07513778067204e-16)
dB = chrono.ChVectorD(8.88178419700125e-16,1,-8.08381139805192e-16)
link_53.Initialize(body_57,body_62,False,cA,cB,dA,dB)
link_53.SetName("Concentric29")
exported_items.append(link_53)


# Mate constraint: Coincident71 [MateCoincident] type:0 align:0 flip:False
#   Entity 0: C::E name: body_57 , SW name: robo_leg_link-1/link2-2 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_62 , SW name: robo_leg_link-2/link2-1 ,  SW ref.type:2 (2)

link_54 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(2.14921015502796,0.0585083638039782,-1.95969130038634)
cB = chrono.ChVectorD(2.21525236409703,0.0585083638039786,-1.9086525389774)
dA = chrono.ChVectorD(1.12252152354061e-15,1,-1.3045606810197e-15)
dB = chrono.ChVectorD(8.88178419700125e-16,1,-8.08381139805192e-16)
link_54.Initialize(body_57,body_62,False,cA,cB,dB)
link_54.SetDistance(0)
link_54.SetName("Coincident71")
exported_items.append(link_54)

link_55 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.14921015502796,0.0585083638039782,-1.95969130038634)
dA = chrono.ChVectorD(1.12252152354061e-15,1,-1.3045606810197e-15)
cB = chrono.ChVectorD(2.21525236409703,0.0585083638039786,-1.9086525389774)
dB = chrono.ChVectorD(8.88178419700125e-16,1,-8.08381139805192e-16)
link_55.Initialize(body_57,body_62,False,cA,cB,dA,dB)
link_55.SetName("Coincident71")
exported_items.append(link_55)


# Mate constraint: Concentric30 [MateConcentric] type:1 align:1 flip:False
#   Entity 0: C::E name: body_55 , SW name: robo_leg_link-1/link2-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_43 , SW name: robo_leg_link-12/link2-2 ,  SW ref.type:2 (2)

link_56 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.31293825348059,-0.00200000000000257,-1.85507007989506)
dA = chrono.ChVectorD(1.1518563880486e-15,1,-1.12063136548102e-15)
cB = chrono.ChVectorD(2.31293825348059,0.0909999999999974,-1.85507007989506)
dB = chrono.ChVectorD(-1.19175502799607e-15,-1,1.13797860024079e-15)
link_56.SetFlipped(True)
link_56.Initialize(body_55,body_43,False,cA,cB,dA,dB)
link_56.SetName("Concentric30")
exported_items.append(link_56)

link_57 = chrono.ChLinkMateGeneric()
link_57.SetConstrainedCoords(False, True, True, False, False, False)
cA = chrono.ChVectorD(2.31293825348059,-0.00200000000000257,-1.85507007989506)
cB = chrono.ChVectorD(2.31293825348059,0.0909999999999974,-1.85507007989506)
dA = chrono.ChVectorD(1.1518563880486e-15,1,-1.12063136548102e-15)
dB = chrono.ChVectorD(-1.19175502799607e-15,-1,1.13797860024079e-15)
link_57.Initialize(body_55,body_43,False,cA,cB,dA,dB)
link_57.SetName("Concentric30")
exported_items.append(link_57)


# Mate constraint: Coincident72 [MateCoincident] type:0 align:0 flip:False
#   Entity 0: C::E name: body_55 , SW name: robo_leg_link-1/link2-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_43 , SW name: robo_leg_link-12/link2-2 ,  SW ref.type:2 (2)

link_58 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(2.34824554800022,0.0585083638039758,-1.83250894176544)
cB = chrono.ChVectorD(2.27188402902432,0.0585083638039759,-1.86344627558847)
dA = chrono.ChVectorD(1.1518563880486e-15,1,-1.12063136548102e-15)
dB = chrono.ChVectorD(1.30967293092739e-15,1,-1.71592928710966e-15)
link_58.Initialize(body_55,body_43,False,cA,cB,dB)
link_58.SetDistance(0)
link_58.SetName("Coincident72")
exported_items.append(link_58)

link_59 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.34824554800022,0.0585083638039758,-1.83250894176544)
dA = chrono.ChVectorD(1.1518563880486e-15,1,-1.12063136548102e-15)
cB = chrono.ChVectorD(2.27188402902432,0.0585083638039759,-1.86344627558847)
dB = chrono.ChVectorD(1.30967293092739e-15,1,-1.71592928710966e-15)
link_59.Initialize(body_55,body_43,False,cA,cB,dA,dB)
link_59.SetName("Coincident72")
exported_items.append(link_59)


# Mate constraint: Coincident73 [MateCoincident] type:0 align:1 flip:False
#   Entity 0: C::E name: body_52 , SW name: graound-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_46 , SW name: object-1 ,  SW ref.type:2 (2)

link_60 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(0.25,0.00999999999999999,-0.25)
cB = chrono.ChVectorD(2.58738777521909,0.0100000000000007,-2.74788941954557)
dA = chrono.ChVectorD(0,1,2.88998133608509e-16)
dB = chrono.ChVectorD(0,-1,-2.88998133608509e-16)
link_60.Initialize(body_52,body_46,False,cA,cB,dB)
link_60.SetDistance(0)
link_60.SetName("Coincident73")
exported_items.append(link_60)

link_61 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(0.25,0.00999999999999999,-0.25)
dA = chrono.ChVectorD(0,1,2.88998133608509e-16)
cB = chrono.ChVectorD(2.58738777521909,0.0100000000000007,-2.74788941954557)
dB = chrono.ChVectorD(0,-1,-2.88998133608509e-16)
link_61.SetFlipped(True)
link_61.Initialize(body_52,body_46,False,cA,cB,dA,dB)
link_61.SetName("Coincident73")
exported_items.append(link_61)


# Mate constraint: Parallel15 [MateParallel] type:3 align:1 flip:False
#   Entity 0: C::E name: body_52 , SW name: graound-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_58 , SW name: robo_leg_link-2/single_bot1-1 ,  SW ref.type:2 (2)

link_62 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(0.25,0.00999999999999999,-0.25)
dA = chrono.ChVectorD(0,1,2.88998133608509e-16)
cB = chrono.ChVectorD(2.13016241809865,0.0100000000000006,-1.94852406440588)
dB = chrono.ChVectorD(-9.29811783123569e-16,-1,6.80011602582908e-16)
link_62.SetFlipped(True)
link_62.Initialize(body_52,body_58,False,cA,cB,dA,dB)
link_62.SetName("Parallel15")
exported_items.append(link_62)


# Mate constraint: Coincident79 [MateCoincident] type:0 align:1 flip:False
#   Entity 0: C::E name: body_52 , SW name: graound-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_42 , SW name: robo_leg_link-12/single_bot1-1 ,  SW ref.type:2 (2)

link_63 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(0.25,0.00999999999999999,-0.25)
cB = chrono.ChVectorD(2.40989370349858,0.00999999999999572,-1.80611459846745)
dA = chrono.ChVectorD(0,1,2.88998133608509e-16)
dB = chrono.ChVectorD(-1.19348975147204e-15,-1,1.15532583500055e-15)
link_63.Initialize(body_52,body_42,False,cA,cB,dB)
link_63.SetDistance(0)
link_63.SetName("Coincident79")
exported_items.append(link_63)

link_64 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(0.25,0.00999999999999999,-0.25)
dA = chrono.ChVectorD(0,1,2.88998133608509e-16)
cB = chrono.ChVectorD(2.40989370349858,0.00999999999999572,-1.80611459846745)
dB = chrono.ChVectorD(-1.19348975147204e-15,-1,1.15532583500055e-15)
link_64.SetFlipped(True)
link_64.Initialize(body_52,body_42,False,cA,cB,dA,dB)
link_64.SetName("Coincident79")
exported_items.append(link_64)


# Mate constraint: Concentric2 [MateConcentric] type:1 align:1 flip:False
#   Entity 0: C::E name: body_3 , SW name: robo_leg_link-5/leg-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_5 , SW name: robo_leg_link-5/single_bot1-1 ,  SW ref.type:2 (2)

link_1 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(1.95861595884145,0.0170000000000082,-2.34443530747498)
dA = chrono.ChVectorD(0.415243746429461,3.00107161343988e-16,-0.909710190693292)
cB = chrono.ChVectorD(1.96193790881289,0.0170000000000082,-2.35171298900052)
dB = chrono.ChVectorD(-0.41524374642947,3.41913997115029e-15,0.909710190693288)
link_1.SetFlipped(True)
link_1.Initialize(body_3,body_5,False,cA,cB,dA,dB)
link_1.SetName("Concentric2")
exported_items.append(link_1)

link_2 = chrono.ChLinkMateGeneric()
link_2.SetConstrainedCoords(False, True, True, False, False, False)
cA = chrono.ChVectorD(1.95861595884145,0.0170000000000082,-2.34443530747498)
cB = chrono.ChVectorD(1.96193790881289,0.0170000000000082,-2.35171298900052)
dA = chrono.ChVectorD(0.415243746429461,3.00107161343988e-16,-0.909710190693292)
dB = chrono.ChVectorD(-0.41524374642947,3.41913997115029e-15,0.909710190693288)
link_2.Initialize(body_3,body_5,False,cA,cB,dA,dB)
link_2.SetName("Concentric2")
exported_items.append(link_2)


# Mate constraint: Coincident4 [MateCoincident] type:0 align:1 flip:False
#   Entity 0: C::E name: body_5 , SW name: robo_leg_link-5/single_bot1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_1 , SW name: robo_leg_link-5/link1-1 ,  SW ref.type:2 (2)

link_3 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(1.94926008285061,0.0110000000000085,-2.38796122309365)
cB = chrono.ChVectorD(1.96339440615989,0.0110000000000083,-2.350087414801)
dA = chrono.ChVectorD(6.84174938925253e-15,1,3.42087469462626e-15)
dB = chrono.ChVectorD(1.17267306976032e-14,-1,-2.42791897697714e-14)
link_3.Initialize(body_5,body_1,False,cA,cB,dB)
link_3.SetDistance(0)
link_3.SetName("Coincident4")
exported_items.append(link_3)

link_4 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(1.94926008285061,0.0110000000000085,-2.38796122309365)
dA = chrono.ChVectorD(6.84174938925253e-15,1,3.42087469462626e-15)
cB = chrono.ChVectorD(1.96339440615989,0.0110000000000083,-2.350087414801)
dB = chrono.ChVectorD(1.17267306976032e-14,-1,-2.42791897697714e-14)
link_4.SetFlipped(True)
link_4.Initialize(body_5,body_1,False,cA,cB,dA,dB)
link_4.SetName("Coincident4")
exported_items.append(link_4)


# Mate constraint: Parallel3 [MateParallel] type:3 align:1 flip:False
#   Entity 0: C::E name: body_5 , SW name: robo_leg_link-5/single_bot1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_1 , SW name: robo_leg_link-5/link1-1 ,  SW ref.type:2 (2)

link_5 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(1.94899181600753,0.0633100000000283,-2.37362495642581)
dA = chrono.ChVectorD(-0.909710190693292,7.25808302348696e-15,-0.41524374642946)
cB = chrono.ChVectorD(1.99560006611557,0.0212000000000088,-2.35509068634053)
dB = chrono.ChVectorD(0.909710190693292,6.52256026967279e-16,0.41524374642946)
link_5.SetFlipped(True)
link_5.Initialize(body_5,body_1,False,cA,cB,dA,dB)
link_5.SetName("Parallel3")
exported_items.append(link_5)


# Mate constraint: Coincident7 [MateCoincident] type:0 align:1 flip:False
#   Entity 0: C::E name: body_3 , SW name: robo_leg_link-5/leg-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_5 , SW name: robo_leg_link-5/single_bot1-1 ,  SW ref.type:2 (2)

link_6 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(1.96193790881289,0.0170000000000082,-2.35171298900052)
cB = chrono.ChVectorD(1.96011757872131,0.0149990000000083,-2.35254389173713)
dA = chrono.ChVectorD(0.415243746429461,3.00107161343988e-16,-0.909710190693292)
dB = chrono.ChVectorD(-0.41524374642947,3.41913997115029e-15,0.909710190693288)
link_6.Initialize(body_3,body_5,False,cA,cB,dB)
link_6.SetDistance(0)
link_6.SetName("Coincident7")
exported_items.append(link_6)

link_7 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(1.96193790881289,0.0170000000000082,-2.35171298900052)
dA = chrono.ChVectorD(0.415243746429461,3.00107161343988e-16,-0.909710190693292)
cB = chrono.ChVectorD(1.96011757872131,0.0149990000000083,-2.35254389173713)
dB = chrono.ChVectorD(-0.41524374642947,3.41913997115029e-15,0.909710190693288)
link_7.SetFlipped(True)
link_7.Initialize(body_3,body_5,False,cA,cB,dA,dB)
link_7.SetName("Coincident7")
exported_items.append(link_7)


# Mate constraint: Concentric4 [MateConcentric] type:1 align:1 flip:False
#   Entity 0: C::E name: body_5 , SW name: robo_leg_link-5/single_bot1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_1 , SW name: robo_leg_link-5/link1-1 ,  SW ref.type:2 (2)

link_8 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(1.96339440615989,0.0789500000000388,-2.35008741480099)
dA = chrono.ChVectorD(1.16989751219876e-14,-1,-2.42653119819636e-14)
cB = chrono.ChVectorD(1.96339440615989,0.0110000000000083,-2.350087414801)
dB = chrono.ChVectorD(-1.17267306976032e-14,1,2.42791897697714e-14)
link_8.SetFlipped(True)
link_8.Initialize(body_5,body_1,False,cA,cB,dA,dB)
link_8.SetName("Concentric4")
exported_items.append(link_8)

link_9 = chrono.ChLinkMateGeneric()
link_9.SetConstrainedCoords(False, True, True, False, False, False)
cA = chrono.ChVectorD(1.96339440615989,0.0789500000000388,-2.35008741480099)
cB = chrono.ChVectorD(1.96339440615989,0.0110000000000083,-2.350087414801)
dA = chrono.ChVectorD(1.16989751219876e-14,-1,-2.42653119819636e-14)
dB = chrono.ChVectorD(-1.17267306976032e-14,1,2.42791897697714e-14)
link_9.Initialize(body_5,body_1,False,cA,cB,dA,dB)
link_9.SetName("Concentric4")
exported_items.append(link_9)


# Mate constraint: Parallel4 [MateParallel] type:3 align:1 flip:False
#   Entity 0: C::E name: body_5 , SW name: robo_leg_link-5/single_bot1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_1 , SW name: robo_leg_link-5/link1-1 ,  SW ref.type:2 (2)

link_10 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(1.9539679764709,0.0789500000000397,-2.3909556435786)
dA = chrono.ChVectorD(1.16989751219876e-14,-1,-2.42653119819636e-14)
cB = chrono.ChVectorD(1.96339440615989,0.0780000000000083,-2.35008741480099)
dB = chrono.ChVectorD(-1.17267306976032e-14,1,2.42791897697714e-14)
link_10.SetFlipped(True)
link_10.Initialize(body_5,body_1,False,cA,cB,dA,dB)
link_10.SetName("Parallel4")
exported_items.append(link_10)


# Mate constraint: Concentric5 [MateConcentric] type:1 align:0 flip:False
#   Entity 0: C::E name: body_5 , SW name: robo_leg_link-5/single_bot1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_1 , SW name: robo_leg_link-5/link1-1 ,  SW ref.type:2 (2)

link_11 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(1.96339440615989,0.0110000000000083,-2.350087414801)
dA = chrono.ChVectorD(6.84174938925253e-15,1,3.42087469462626e-15)
cB = chrono.ChVectorD(1.96339440615989,0.0110000000000083,-2.350087414801)
dB = chrono.ChVectorD(-1.17267306976032e-14,1,2.42791897697714e-14)
link_11.Initialize(body_5,body_1,False,cA,cB,dA,dB)
link_11.SetName("Concentric5")
exported_items.append(link_11)

link_12 = chrono.ChLinkMateGeneric()
link_12.SetConstrainedCoords(False, True, True, False, False, False)
cA = chrono.ChVectorD(1.96339440615989,0.0110000000000083,-2.350087414801)
cB = chrono.ChVectorD(1.96339440615989,0.0110000000000083,-2.350087414801)
dA = chrono.ChVectorD(6.84174938925253e-15,1,3.42087469462626e-15)
dB = chrono.ChVectorD(-1.17267306976032e-14,1,2.42791897697714e-14)
link_12.Initialize(body_5,body_1,False,cA,cB,dA,dB)
link_12.SetName("Concentric5")
exported_items.append(link_12)


# Mate constraint: Concentric6 [MateConcentric] type:1 align:0 flip:False
#   Entity 0: C::E name: body_1 , SW name: robo_leg_link-5/link1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_2 , SW name: robo_leg_link-5/link2-2 ,  SW ref.type:2 (2)

link_13 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.04098315308375,0.0445000000000095,-2.3635331892594)
dA = chrono.ChVectorD(-0.909710190693292,-6.52256026967279e-16,-0.41524374642946)
cB = chrono.ChVectorD(2.05826764670692,0.0445000000000093,-2.35564355807724)
dB = chrono.ChVectorD(-0.909710190693292,-1.346145417358e-15,-0.41524374642946)
link_13.Initialize(body_1,body_2,False,cA,cB,dA,dB)
link_13.SetName("Concentric6")
exported_items.append(link_13)

link_14 = chrono.ChLinkMateGeneric()
link_14.SetConstrainedCoords(False, True, True, False, False, False)
cA = chrono.ChVectorD(2.04098315308375,0.0445000000000095,-2.3635331892594)
cB = chrono.ChVectorD(2.05826764670692,0.0445000000000093,-2.35564355807724)
dA = chrono.ChVectorD(-0.909710190693292,-6.52256026967279e-16,-0.41524374642946)
dB = chrono.ChVectorD(-0.909710190693292,-1.346145417358e-15,-0.41524374642946)
link_14.Initialize(body_1,body_2,False,cA,cB,dA,dB)
link_14.SetName("Concentric6")
exported_items.append(link_14)


# Mate constraint: Concentric7 [MateConcentric] type:1 align:0 flip:False
#   Entity 0: C::E name: body_1 , SW name: robo_leg_link-5/link1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_4 , SW name: robo_leg_link-5/link2-1 ,  SW ref.type:2 (2)

link_15 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.00406798402617,0.0445000000000071,-2.28265995330676)
dA = chrono.ChVectorD(-0.909710190693292,-6.52256026967279e-16,-0.41524374642946)
cB = chrono.ChVectorD(2.02135247764934,0.0445000000000069,-2.2747703221246)
dB = chrono.ChVectorD(-0.909710190693292,6.93889390390723e-17,-0.41524374642946)
link_15.Initialize(body_1,body_4,False,cA,cB,dA,dB)
link_15.SetName("Concentric7")
exported_items.append(link_15)

link_16 = chrono.ChLinkMateGeneric()
link_16.SetConstrainedCoords(False, True, True, False, False, False)
cA = chrono.ChVectorD(2.00406798402617,0.0445000000000071,-2.28265995330676)
cB = chrono.ChVectorD(2.02135247764934,0.0445000000000069,-2.2747703221246)
dA = chrono.ChVectorD(-0.909710190693292,-6.52256026967279e-16,-0.41524374642946)
dB = chrono.ChVectorD(-0.909710190693292,6.93889390390723e-17,-0.41524374642946)
link_16.Initialize(body_1,body_4,False,cA,cB,dA,dB)
link_16.SetName("Concentric7")
exported_items.append(link_16)


# Mate constraint: Coincident9 [MateCoincident] type:0 align:0 flip:False
#   Entity 0: C::E name: body_1 , SW name: robo_leg_link-5/link1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_2 , SW name: robo_leg_link-5/link2-2 ,  SW ref.type:2 (2)

link_17 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(1.8942361770273,0.0780000000000067,-2.316579515087)
cB = chrono.ChVectorD(1.96785889326924,0.0780000000000092,-2.47787113189692)
dA = chrono.ChVectorD(-0.909710190693292,6.93889390390723e-17,-0.415243746429461)
dB = chrono.ChVectorD(-0.909710190693292,-1.346145417358e-15,-0.41524374642946)
link_17.Initialize(body_1,body_2,False,cA,cB,dB)
link_17.SetDistance(0)
link_17.SetName("Coincident9")
exported_items.append(link_17)

link_18 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(1.8942361770273,0.0780000000000067,-2.316579515087)
dA = chrono.ChVectorD(-0.909710190693292,6.93889390390723e-17,-0.415243746429461)
cB = chrono.ChVectorD(1.96785889326924,0.0780000000000092,-2.47787113189692)
dB = chrono.ChVectorD(-0.909710190693292,-1.346145417358e-15,-0.41524374642946)
link_18.Initialize(body_1,body_2,False,cA,cB,dA,dB)
link_18.SetName("Coincident9")
exported_items.append(link_18)


# Mate constraint: Coincident10 [MateCoincident] type:0 align:0 flip:False
#   Entity 0: C::E name: body_1 , SW name: robo_leg_link-5/link1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_4 , SW name: robo_leg_link-5/link2-1 ,  SW ref.type:2 (2)

link_19 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(1.8942361770273,0.0780000000000067,-2.316579515087)
cB = chrono.ChVectorD(1.8697783203626,0.0110000000000069,-2.26299758485516)
dA = chrono.ChVectorD(-0.909710190693292,6.93889390390723e-17,-0.415243746429461)
dB = chrono.ChVectorD(-0.909710190693292,6.93889390390723e-17,-0.41524374642946)
link_19.Initialize(body_1,body_4,False,cA,cB,dB)
link_19.SetDistance(0)
link_19.SetName("Coincident10")
exported_items.append(link_19)

link_20 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(1.8942361770273,0.0780000000000067,-2.316579515087)
dA = chrono.ChVectorD(-0.909710190693292,6.93889390390723e-17,-0.415243746429461)
cB = chrono.ChVectorD(1.8697783203626,0.0110000000000069,-2.26299758485516)
dB = chrono.ChVectorD(-0.909710190693292,6.93889390390723e-17,-0.41524374642946)
link_20.Initialize(body_1,body_4,False,cA,cB,dA,dB)
link_20.SetName("Coincident10")
exported_items.append(link_20)


# Mate constraint: Coincident11 [MateCoincident] type:0 align:0 flip:False
#   Entity 0: C::E name: body_1 , SW name: robo_leg_link-5/link1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_4 , SW name: robo_leg_link-5/link2-1 ,  SW ref.type:2 (2)

link_21 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(1.98338777571524,0.0780000000000067,-2.27588562793691)
cB = chrono.ChVectorD(1.95892991905054,0.0110000000000069,-2.22230369770507)
dA = chrono.ChVectorD(0.909710190693292,6.52256026967279e-16,0.41524374642946)
dB = chrono.ChVectorD(0.909710190693292,6.80011602582908e-16,0.415243746429461)
link_21.Initialize(body_1,body_4,False,cA,cB,dB)
link_21.SetDistance(0)
link_21.SetName("Coincident11")
exported_items.append(link_21)

link_22 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(1.98338777571524,0.0780000000000067,-2.27588562793691)
dA = chrono.ChVectorD(0.909710190693292,6.52256026967279e-16,0.41524374642946)
cB = chrono.ChVectorD(1.95892991905054,0.0110000000000069,-2.22230369770507)
dB = chrono.ChVectorD(0.909710190693292,6.80011602582908e-16,0.415243746429461)
link_22.Initialize(body_1,body_4,False,cA,cB,dA,dB)
link_22.SetName("Coincident11")
exported_items.append(link_22)


# Mate constraint: Coincident12 [MateCoincident] type:0 align:0 flip:False
#   Entity 0: C::E name: body_1 , SW name: robo_leg_link-5/link1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_2 , SW name: robo_leg_link-5/link2-2 ,  SW ref.type:2 (2)

link_23 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(1.98338777571524,0.0780000000000067,-2.27588562793691)
cB = chrono.ChVectorD(2.05701049195718,0.0780000000000093,-2.43717724474683)
dA = chrono.ChVectorD(0.909710190693292,6.52256026967279e-16,0.41524374642946)
dB = chrono.ChVectorD(0.909710190693292,5.82867087928207e-16,0.41524374642946)
link_23.Initialize(body_1,body_2,False,cA,cB,dB)
link_23.SetDistance(0)
link_23.SetName("Coincident12")
exported_items.append(link_23)

link_24 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(1.98338777571524,0.0780000000000067,-2.27588562793691)
dA = chrono.ChVectorD(0.909710190693292,6.52256026967279e-16,0.41524374642946)
cB = chrono.ChVectorD(2.05701049195718,0.0780000000000093,-2.43717724474683)
dB = chrono.ChVectorD(0.909710190693292,5.82867087928207e-16,0.41524374642946)
link_24.Initialize(body_1,body_2,False,cA,cB,dA,dB)
link_24.SetName("Coincident12")
exported_items.append(link_24)


# Mate constraint: Concentric2 [MateConcentric] type:1 align:1 flip:False
#   Entity 0: C::E name: body_10 , SW name: robo_leg_link-6/leg-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_8 , SW name: robo_leg_link-6/single_bot1-1 ,  SW ref.type:2 (2)

link_1 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.06413703121801,0.0170000000000098,-2.41889532887879)
dA = chrono.ChVectorD(0.999339204115438,1.30451205393456e-15,0.0363476975601214)
cB = chrono.ChVectorD(2.07213174485093,0.0170000000000098,-2.4186045472983)
dB = chrono.ChVectorD(-0.999339204115438,2.34534613952064e-15,-0.0363476975601325)
link_1.SetFlipped(True)
link_1.Initialize(body_10,body_8,False,cA,cB,dA,dB)
link_1.SetName("Concentric2")
exported_items.append(link_1)

link_2 = chrono.ChLinkMateGeneric()
link_2.SetConstrainedCoords(False, True, True, False, False, False)
cA = chrono.ChVectorD(2.06413703121801,0.0170000000000098,-2.41889532887879)
cB = chrono.ChVectorD(2.07213174485093,0.0170000000000098,-2.4186045472983)
dA = chrono.ChVectorD(0.999339204115438,1.30451205393456e-15,0.0363476975601214)
dB = chrono.ChVectorD(-0.999339204115438,2.34534613952064e-15,-0.0363476975601325)
link_2.Initialize(body_10,body_8,False,cA,cB,dA,dB)
link_2.SetName("Concentric2")
exported_items.append(link_2)


# Mate constraint: Coincident4 [MateCoincident] type:0 align:1 flip:False
#   Entity 0: C::E name: body_8 , SW name: robo_leg_link-6/single_bot1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_7 , SW name: robo_leg_link-6/link1-1 ,  SW ref.type:2 (2)

link_3 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(2.10079073707581,0.0110000000000099,-2.44416474899263)
cB = chrono.ChVectorD(2.07118562699141,0.0110000000000098,-2.41663763677774)
dA = chrono.ChVectorD(-1.30451205393456e-15,1,-2.91433543964104e-16)
dB = chrono.ChVectorD(2.77000644643977e-14,-1,9.42995681540992e-15)
link_3.Initialize(body_8,body_7,False,cA,cB,dB)
link_3.SetDistance(0)
link_3.SetName("Coincident4")
exported_items.append(link_3)

link_4 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.10079073707581,0.0110000000000099,-2.44416474899263)
dA = chrono.ChVectorD(-1.30451205393456e-15,1,-2.91433543964104e-16)
cB = chrono.ChVectorD(2.07118562699141,0.0110000000000098,-2.41663763677774)
dB = chrono.ChVectorD(2.77000644643977e-14,-1,9.42995681540992e-15)
link_4.SetFlipped(True)
link_4.Initialize(body_8,body_7,False,cA,cB,dA,dB)
link_4.SetName("Coincident4")
exported_items.append(link_4)


# Mate constraint: Parallel3 [MateParallel] type:3 align:1 flip:False
#   Entity 0: C::E name: body_8 , SW name: robo_leg_link-6/single_bot1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_7 , SW name: robo_leg_link-6/link1-1 ,  SW ref.type:2 (2)

link_5 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.08743867566676,0.0633100000000297,-2.43893761161166)
dA = chrono.ChVectorD(0.0363476975601216,-5.62050406216485e-16,-0.999339204115439)
cB = chrono.ChVectorD(2.08810911536353,0.0212000000000105,-2.38878386143942)
dB = chrono.ChVectorD(-0.0363476975601215,8.42381719934338e-15,0.999339204115439)
link_5.SetFlipped(True)
link_5.Initialize(body_8,body_7,False,cA,cB,dA,dB)
link_5.SetName("Parallel3")
exported_items.append(link_5)


# Mate constraint: Coincident7 [MateCoincident] type:0 align:1 flip:False
#   Entity 0: C::E name: body_10 , SW name: robo_leg_link-6/leg-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_8 , SW name: robo_leg_link-6/single_bot1-1 ,  SW ref.type:2 (2)

link_6 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(2.07213174485093,0.0170000000000098,-2.41860454729831)
cB = chrono.ChVectorD(2.07220447659375,0.0149990000000099,-2.42060422504574)
dA = chrono.ChVectorD(0.999339204115438,1.30451205393456e-15,0.0363476975601214)
dB = chrono.ChVectorD(-0.999339204115438,2.34534613952064e-15,-0.0363476975601325)
link_6.Initialize(body_10,body_8,False,cA,cB,dB)
link_6.SetDistance(0)
link_6.SetName("Coincident7")
exported_items.append(link_6)

link_7 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.07213174485093,0.0170000000000098,-2.41860454729831)
dA = chrono.ChVectorD(0.999339204115438,1.30451205393456e-15,0.0363476975601214)
cB = chrono.ChVectorD(2.07220447659375,0.0149990000000099,-2.42060422504574)
dB = chrono.ChVectorD(-0.999339204115438,2.34534613952064e-15,-0.0363476975601325)
link_7.SetFlipped(True)
link_7.Initialize(body_10,body_8,False,cA,cB,dA,dB)
link_7.SetName("Coincident7")
exported_items.append(link_7)


# Mate constraint: Concentric4 [MateConcentric] type:1 align:1 flip:False
#   Entity 0: C::E name: body_8 , SW name: robo_leg_link-6/single_bot1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_7 , SW name: robo_leg_link-6/link1-1 ,  SW ref.type:2 (2)

link_8 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.07118562699141,0.0789500000000405,-2.41663763677774)
dA = chrono.ChVectorD(2.76445533131664e-14,-1,9.46465128492946e-15)
cB = chrono.ChVectorD(2.07118562699141,0.0110000000000098,-2.41663763677774)
dB = chrono.ChVectorD(-2.77000644643977e-14,1,-9.42995681540992e-15)
link_8.SetFlipped(True)
link_8.Initialize(body_8,body_7,False,cA,cB,dA,dB)
link_8.SetName("Concentric4")
exported_items.append(link_8)

link_9 = chrono.ChLinkMateGeneric()
link_9.SetConstrainedCoords(False, True, True, False, False, False)
cA = chrono.ChVectorD(2.07118562699141,0.0789500000000405,-2.41663763677774)
cB = chrono.ChVectorD(2.07118562699141,0.0110000000000098,-2.41663763677774)
dA = chrono.ChVectorD(2.76445533131664e-14,-1,9.46465128492946e-15)
dB = chrono.ChVectorD(-2.77000644643977e-14,1,-9.42995681540992e-15)
link_9.Initialize(body_8,body_7,False,cA,cB,dA,dB)
link_9.SetName("Concentric4")
exported_items.append(link_9)


# Mate constraint: Parallel4 [MateParallel] type:3 align:1 flip:False
#   Entity 0: C::E name: body_8 , SW name: robo_leg_link-6/single_bot1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_7 , SW name: robo_leg_link-6/link1-1 ,  SW ref.type:2 (2)

link_10 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.1053561480886,0.0789500000000412,-2.44095728291099)
dA = chrono.ChVectorD(2.76445533131664e-14,-1,9.46465128492946e-15)
cB = chrono.ChVectorD(2.07118562699141,0.0780000000000098,-2.41663763677774)
dB = chrono.ChVectorD(-2.77000644643977e-14,1,-9.42995681540992e-15)
link_10.SetFlipped(True)
link_10.Initialize(body_8,body_7,False,cA,cB,dA,dB)
link_10.SetName("Parallel4")
exported_items.append(link_10)


# Mate constraint: Concentric5 [MateConcentric] type:1 align:0 flip:False
#   Entity 0: C::E name: body_8 , SW name: robo_leg_link-6/single_bot1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_7 , SW name: robo_leg_link-6/link1-1 ,  SW ref.type:2 (2)

link_11 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.07118562699142,0.0110000000000099,-2.41663763677774)
dA = chrono.ChVectorD(-1.30451205393456e-15,1,-2.91433543964104e-16)
cB = chrono.ChVectorD(2.07118562699141,0.0110000000000098,-2.41663763677774)
dB = chrono.ChVectorD(-2.77000644643977e-14,1,-9.42995681540992e-15)
link_11.Initialize(body_8,body_7,False,cA,cB,dA,dB)
link_11.SetName("Concentric5")
exported_items.append(link_11)

link_12 = chrono.ChLinkMateGeneric()
link_12.SetConstrainedCoords(False, True, True, False, False, False)
cA = chrono.ChVectorD(2.07118562699142,0.0110000000000099,-2.41663763677774)
cB = chrono.ChVectorD(2.07118562699141,0.0110000000000098,-2.41663763677774)
dA = chrono.ChVectorD(-1.30451205393456e-15,1,-2.91433543964104e-16)
dB = chrono.ChVectorD(-2.77000644643977e-14,1,-9.42995681540992e-15)
link_12.Initialize(body_8,body_7,False,cA,cB,dA,dB)
link_12.SetName("Concentric5")
exported_items.append(link_12)


# Mate constraint: Concentric6 [MateConcentric] type:1 align:0 flip:False
#   Entity 0: C::E name: body_7 , SW name: robo_leg_link-6/link1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_9 , SW name: robo_leg_link-6/link2-2 ,  SW ref.type:2 (2)

link_13 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.11324365427294,0.0445000000000116,-2.35006493335369)
dA = chrono.ChVectorD(0.0363476975601215,-8.42381719934338e-15,-0.999339204115439)
cB = chrono.ChVectorD(2.1125530465174,0.0445000003984777,-2.33107749105905)
dB = chrono.ChVectorD(0.0363476975601216,-9.92261828258734e-16,-0.999339204115439)
link_13.Initialize(body_7,body_9,False,cA,cB,dA,dB)
link_13.SetName("Concentric6")
exported_items.append(link_13)

link_14 = chrono.ChLinkMateGeneric()
link_14.SetConstrainedCoords(False, True, True, False, False, False)
cA = chrono.ChVectorD(2.11324365427294,0.0445000000000116,-2.35006493335369)
cB = chrono.ChVectorD(2.1125530465174,0.0445000003984777,-2.33107749105905)
dA = chrono.ChVectorD(0.0363476975601215,-8.42381719934338e-15,-0.999339204115439)
dB = chrono.ChVectorD(0.0363476975601216,-9.92261828258734e-16,-0.999339204115439)
link_14.Initialize(body_7,body_9,False,cA,cB,dA,dB)
link_14.SetName("Concentric6")
exported_items.append(link_14)


# Mate constraint: Concentric7 [MateConcentric] type:1 align:0 flip:False
#   Entity 0: C::E name: body_7 , SW name: robo_leg_link-6/link1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_6 , SW name: robo_leg_link-6/link2-1 ,  SW ref.type:2 (2)

link_15 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.02440239902707,0.0445000000000091,-2.35329624366679)
dA = chrono.ChVectorD(0.0363476975601215,-8.42381719934338e-15,-0.999339204115439)
cB = chrono.ChVectorD(2.02371179128894,0.0445000004582813,-2.33430880137151)
dB = chrono.ChVectorD(0.0363476975601213,-1.94289029309402e-16,-0.999339204115438)
link_15.Initialize(body_7,body_6,False,cA,cB,dA,dB)
link_15.SetName("Concentric7")
exported_items.append(link_15)

link_16 = chrono.ChLinkMateGeneric()
link_16.SetConstrainedCoords(False, True, True, False, False, False)
cA = chrono.ChVectorD(2.02440239902707,0.0445000000000091,-2.35329624366679)
cB = chrono.ChVectorD(2.02371179128894,0.0445000004582813,-2.33430880137151)
dA = chrono.ChVectorD(0.0363476975601215,-8.42381719934338e-15,-0.999339204115439)
dB = chrono.ChVectorD(0.0363476975601213,-1.94289029309402e-16,-0.999339204115438)
link_16.Initialize(body_7,body_6,False,cA,cB,dA,dB)
link_16.SetName("Concentric7")
exported_items.append(link_16)


# Mate constraint: Coincident9 [MateCoincident] type:0 align:0 flip:False
#   Entity 0: C::E name: body_7 , SW name: robo_leg_link-6/link1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_9 , SW name: robo_leg_link-6/link2-2 ,  SW ref.type:2 (2)

link_17 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(2.01380578328822,0.0780000000000078,-2.46775704147496)
cB = chrono.ChVectorD(2.190988622676,0.0780000003984776,-2.4613125972811)
dA = chrono.ChVectorD(0.0363476975601216,-7.71605002114484e-15,-0.999339204115439)
dB = chrono.ChVectorD(0.0363476975601216,-9.92261828258734e-16,-0.999339204115439)
link_17.Initialize(body_7,body_9,False,cA,cB,dB)
link_17.SetDistance(0)
link_17.SetName("Coincident9")
exported_items.append(link_17)

link_18 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.01380578328822,0.0780000000000078,-2.46775704147496)
dA = chrono.ChVectorD(0.0363476975601216,-7.71605002114484e-15,-0.999339204115439)
cB = chrono.ChVectorD(2.190988622676,0.0780000003984776,-2.4613125972811)
dB = chrono.ChVectorD(0.0363476975601216,-9.92261828258734e-16,-0.999339204115439)
link_18.Initialize(body_7,body_9,False,cA,cB,dA,dB)
link_18.SetName("Coincident9")
exported_items.append(link_18)


# Mate constraint: Coincident10 [MateCoincident] type:0 align:0 flip:False
#   Entity 0: C::E name: body_7 , SW name: robo_leg_link-6/link1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_6 , SW name: robo_leg_link-6/link2-1 ,  SW ref.type:2 (2)

link_19 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(2.01380578328822,0.0780000000000078,-2.46775704147496)
cB = chrono.ChVectorD(1.95494470268133,0.0110000004582812,-2.46989792344417)
dA = chrono.ChVectorD(0.0363476975601216,-7.71605002114484e-15,-0.999339204115439)
dB = chrono.ChVectorD(0.0363476975601213,-1.94289029309402e-16,-0.999339204115438)
link_19.Initialize(body_7,body_6,False,cA,cB,dB)
link_19.SetDistance(0)
link_19.SetName("Coincident10")
exported_items.append(link_19)

link_20 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.01380578328822,0.0780000000000078,-2.46775704147496)
dA = chrono.ChVectorD(0.0363476975601216,-7.71605002114484e-15,-0.999339204115439)
cB = chrono.ChVectorD(1.95494470268133,0.0110000004582812,-2.46989792344417)
dB = chrono.ChVectorD(0.0363476975601213,-1.94289029309402e-16,-0.999339204115438)
link_20.Initialize(body_7,body_6,False,cA,cB,dA,dB)
link_20.SetName("Coincident10")
exported_items.append(link_20)


# Mate constraint: Coincident11 [MateCoincident] type:0 align:0 flip:False
#   Entity 0: C::E name: body_7 , SW name: robo_leg_link-6/link1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_6 , SW name: robo_leg_link-6/link2-1 ,  SW ref.type:2 (2)

link_21 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(2.01024370892733,0.0780000000000086,-2.36982179947165)
cB = chrono.ChVectorD(1.95138262832044,0.0110000004582813,-2.37196268144086)
dA = chrono.ChVectorD(-0.0363476975601215,8.42381719934338e-15,0.999339204115439)
dB = chrono.ChVectorD(-0.0363476975601214,9.57567358739198e-16,0.999339204115438)
link_21.Initialize(body_7,body_6,False,cA,cB,dB)
link_21.SetDistance(0)
link_21.SetName("Coincident11")
exported_items.append(link_21)

link_22 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.01024370892733,0.0780000000000086,-2.36982179947165)
dA = chrono.ChVectorD(-0.0363476975601215,8.42381719934338e-15,0.999339204115439)
cB = chrono.ChVectorD(1.95138262832044,0.0110000004582813,-2.37196268144086)
dB = chrono.ChVectorD(-0.0363476975601214,9.57567358739198e-16,0.999339204115438)
link_22.Initialize(body_7,body_6,False,cA,cB,dA,dB)
link_22.SetName("Coincident11")
exported_items.append(link_22)


# Mate constraint: Coincident12 [MateCoincident] type:0 align:0 flip:False
#   Entity 0: C::E name: body_7 , SW name: robo_leg_link-6/link1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_9 , SW name: robo_leg_link-6/link2-2 ,  SW ref.type:2 (2)

link_23 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(2.01024370892733,0.0780000000000086,-2.36982179947165)
cB = chrono.ChVectorD(2.18742654831511,0.0780000003984777,-2.36337735527779)
dA = chrono.ChVectorD(-0.0363476975601215,8.42381719934338e-15,0.999339204115439)
dB = chrono.ChVectorD(-0.0363476975601216,2.42861286636753e-16,0.999339204115439)
link_23.Initialize(body_7,body_9,False,cA,cB,dB)
link_23.SetDistance(0)
link_23.SetName("Coincident12")
exported_items.append(link_23)

link_24 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.01024370892733,0.0780000000000086,-2.36982179947165)
dA = chrono.ChVectorD(-0.0363476975601215,8.42381719934338e-15,0.999339204115439)
cB = chrono.ChVectorD(2.18742654831511,0.0780000003984777,-2.36337735527779)
dB = chrono.ChVectorD(-0.0363476975601216,2.42861286636753e-16,0.999339204115439)
link_24.Initialize(body_7,body_9,False,cA,cB,dA,dB)
link_24.SetName("Coincident12")
exported_items.append(link_24)


# Mate constraint: Concentric2 [MateConcentric] type:1 align:1 flip:False
#   Entity 0: C::E name: body_13 , SW name: robo_leg_link-10/leg-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_11 , SW name: robo_leg_link-10/single_bot1-1 ,  SW ref.type:2 (2)

link_1 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.49063986199031,0.0169999999999911,-2.05802443541501)
dA = chrono.ChVectorD(0.000421286095355889,-4.68375338513738e-15,0.999999911259009)
cB = chrono.ChVectorD(2.49064323227907,0.0169999999999911,-2.05002443612494)
dB = chrono.ChVectorD(-0.000421286095355846,4.52415882534751e-15,-0.999999911259009)
link_1.SetFlipped(True)
link_1.Initialize(body_13,body_11,False,cA,cB,dA,dB)
link_1.SetName("Concentric2")
exported_items.append(link_1)

link_2 = chrono.ChLinkMateGeneric()
link_2.SetConstrainedCoords(False, True, True, False, False, False)
cA = chrono.ChVectorD(2.49063986199031,0.0169999999999911,-2.05802443541501)
cB = chrono.ChVectorD(2.49064323227907,0.0169999999999911,-2.05002443612494)
dA = chrono.ChVectorD(0.000421286095355889,-4.68375338513738e-15,0.999999911259009)
dB = chrono.ChVectorD(-0.000421286095355846,4.52415882534751e-15,-0.999999911259009)
link_2.Initialize(body_13,body_11,False,cA,cB,dA,dB)
link_2.SetName("Concentric2")
exported_items.append(link_2)


# Mate constraint: Coincident4 [MateCoincident] type:0 align:1 flip:False
#   Entity 0: C::E name: body_11 , SW name: robo_leg_link-10/single_bot1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_12 , SW name: robo_leg_link-10/link1-1 ,  SW ref.type:2 (2)

link_3 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(2.51723990417888,0.0109999999999911,-2.02232463847489)
cB = chrono.ChVectorD(2.4886428642525,0.0109999999999912,-2.0508975934752)
dA = chrono.ChVectorD(-2.35922392732846e-16,1,8.15320033709099e-16)
dB = chrono.ChVectorD(-7.99360577730113e-15,-1,2.58335020042466e-14)
link_3.Initialize(body_11,body_12,False,cA,cB,dB)
link_3.SetDistance(0)
link_3.SetName("Coincident4")
exported_items.append(link_3)

link_4 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.51723990417888,0.0109999999999911,-2.02232463847489)
dA = chrono.ChVectorD(-2.35922392732846e-16,1,8.15320033709099e-16)
cB = chrono.ChVectorD(2.4886428642525,0.0109999999999912,-2.0508975934752)
dB = chrono.ChVectorD(-7.99360577730113e-15,-1,2.58335020042466e-14)
link_4.SetFlipped(True)
link_4.Initialize(body_11,body_12,False,cA,cB,dA,dB)
link_4.SetName("Coincident4")
exported_items.append(link_4)


# Mate constraint: Parallel3 [MateParallel] type:3 align:1 flip:False
#   Entity 0: C::E name: body_11 , SW name: robo_leg_link-10/single_bot1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_12 , SW name: robo_leg_link-10/link1-1 ,  SW ref.type:2 (2)

link_5 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.5115253634067,0.063310000000011,-2.03547547619853)
dA = chrono.ChVectorD(0.999999911259009,-1.11022302462516e-16,-0.000421286095366747)
cB = chrono.ChVectorD(2.46143017826468,0.0211999999999918,-2.03296140155475)
dB = chrono.ChVectorD(-0.999999911259009,8.04911692853238e-15,0.000421286095366785)
link_5.SetFlipped(True)
link_5.Initialize(body_11,body_12,False,cA,cB,dA,dB)
link_5.SetName("Parallel3")
exported_items.append(link_5)


# Mate constraint: Coincident7 [MateCoincident] type:0 align:1 flip:False
#   Entity 0: C::E name: body_13 , SW name: robo_leg_link-10/leg-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_11 , SW name: robo_leg_link-10/single_bot1-1 ,  SW ref.type:2 (2)

link_6 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(2.49064323227907,0.0169999999999911,-2.05002443612494)
cB = chrono.ChVectorD(2.4926442321015,0.0149989999999911,-2.05002527911842)
dA = chrono.ChVectorD(0.000421286095355889,-4.68375338513738e-15,0.999999911259009)
dB = chrono.ChVectorD(-0.000421286095355846,4.52415882534751e-15,-0.999999911259009)
link_6.Initialize(body_13,body_11,False,cA,cB,dB)
link_6.SetDistance(0)
link_6.SetName("Coincident7")
exported_items.append(link_6)

link_7 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.49064323227907,0.0169999999999911,-2.05002443612494)
dA = chrono.ChVectorD(0.000421286095355889,-4.68375338513738e-15,0.999999911259009)
cB = chrono.ChVectorD(2.4926442321015,0.0149989999999911,-2.05002527911842)
dB = chrono.ChVectorD(-0.000421286095355846,4.52415882534751e-15,-0.999999911259009)
link_7.SetFlipped(True)
link_7.Initialize(body_13,body_11,False,cA,cB,dA,dB)
link_7.SetName("Coincident7")
exported_items.append(link_7)


# Mate constraint: Concentric4 [MateConcentric] type:1 align:1 flip:False
#   Entity 0: C::E name: body_11 , SW name: robo_leg_link-10/single_bot1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_12 , SW name: robo_leg_link-10/link1-1 ,  SW ref.type:2 (2)

link_8 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.4886428642525,0.0789500000000217,-2.0508975934752)
dA = chrono.ChVectorD(-7.97972798949331e-15,-1,2.58439103451025e-14)
cB = chrono.ChVectorD(2.4886428642525,0.0109999999999912,-2.0508975934752)
dB = chrono.ChVectorD(7.99360577730113e-15,1,-2.58335020042466e-14)
link_8.SetFlipped(True)
link_8.Initialize(body_11,body_12,False,cA,cB,dA,dB)
link_8.SetName("Concentric4")
exported_items.append(link_8)

link_9 = chrono.ChLinkMateGeneric()
link_9.SetConstrainedCoords(False, True, True, False, False, False)
cA = chrono.ChVectorD(2.4886428642525,0.0789500000000217,-2.0508975934752)
cB = chrono.ChVectorD(2.4886428642525,0.0109999999999912,-2.0508975934752)
dA = chrono.ChVectorD(-7.97972798949331e-15,-1,2.58439103451025e-14)
dB = chrono.ChVectorD(7.99360577730113e-15,1,-2.58335020042466e-14)
link_9.Initialize(body_11,body_12,False,cA,cB,dA,dB)
link_9.SetName("Concentric4")
exported_items.append(link_9)


# Mate constraint: Parallel4 [MateParallel] type:3 align:1 flip:False
#   Entity 0: C::E name: body_11 , SW name: robo_leg_link-10/single_bot1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_12 , SW name: robo_leg_link-10/link1-1 ,  SW ref.type:2 (2)

link_10 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.51420247121454,0.0789500000000224,-2.01764438021623)
dA = chrono.ChVectorD(-7.97972798949331e-15,-1,2.58439103451025e-14)
cB = chrono.ChVectorD(2.4886428642525,0.0779999999999912,-2.0508975934752)
dB = chrono.ChVectorD(7.99360577730113e-15,1,-2.58335020042466e-14)
link_10.SetFlipped(True)
link_10.Initialize(body_11,body_12,False,cA,cB,dA,dB)
link_10.SetName("Parallel4")
exported_items.append(link_10)


# Mate constraint: Concentric5 [MateConcentric] type:1 align:0 flip:False
#   Entity 0: C::E name: body_11 , SW name: robo_leg_link-10/single_bot1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_12 , SW name: robo_leg_link-10/link1-1 ,  SW ref.type:2 (2)

link_11 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.48864286425251,0.0109999999999912,-2.05089759347519)
dA = chrono.ChVectorD(-2.35922392732846e-16,1,8.15320033709099e-16)
cB = chrono.ChVectorD(2.4886428642525,0.0109999999999912,-2.0508975934752)
dB = chrono.ChVectorD(7.99360577730113e-15,1,-2.58335020042466e-14)
link_11.Initialize(body_11,body_12,False,cA,cB,dA,dB)
link_11.SetName("Concentric5")
exported_items.append(link_11)

link_12 = chrono.ChLinkMateGeneric()
link_12.SetConstrainedCoords(False, True, True, False, False, False)
cA = chrono.ChVectorD(2.48864286425251,0.0109999999999912,-2.05089759347519)
cB = chrono.ChVectorD(2.4886428642525,0.0109999999999912,-2.0508975934752)
dA = chrono.ChVectorD(-2.35922392732846e-16,1,8.15320033709099e-16)
dB = chrono.ChVectorD(7.99360577730113e-15,1,-2.58335020042466e-14)
link_12.Initialize(body_11,body_12,False,cA,cB,dA,dB)
link_12.SetName("Concentric5")
exported_items.append(link_12)


# Mate constraint: Concentric6 [MateConcentric] type:1 align:0 flip:False
#   Entity 0: C::E name: body_12 , SW name: robo_leg_link-10/link1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_15 , SW name: robo_leg_link-10/link2-2 ,  SW ref.type:2 (2)

link_13 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.42366159618761,0.0444999999999929,-2.00642021382353)
dA = chrono.ChVectorD(0.999999911259009,-8.04911692853238e-15,-0.000421286095366785)
cB = chrono.ChVectorD(2.40466159868735,0.0444999994243689,-2.00641220959817)
dB = chrono.ChVectorD(0.999999911259009,-7.97972798949331e-15,-0.000421286095366676)
link_13.Initialize(body_12,body_15,False,cA,cB,dA,dB)
link_13.SetName("Concentric6")
exported_items.append(link_13)

link_14 = chrono.ChLinkMateGeneric()
link_14.SetConstrainedCoords(False, True, True, False, False, False)
cA = chrono.ChVectorD(2.42366159618761,0.0444999999999929,-2.00642021382353)
cB = chrono.ChVectorD(2.40466159868735,0.0444999994243689,-2.00641220959817)
dA = chrono.ChVectorD(0.999999911259009,-8.04911692853238e-15,-0.000421286095366785)
dB = chrono.ChVectorD(0.999999911259009,-7.97972798949331e-15,-0.000421286095366676)
link_14.Initialize(body_12,body_15,False,cA,cB,dA,dB)
link_14.SetName("Concentric6")
exported_items.append(link_14)


# Mate constraint: Concentric7 [MateConcentric] type:1 align:0 flip:False
#   Entity 0: C::E name: body_12 , SW name: robo_leg_link-10/link1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_14 , SW name: robo_leg_link-10/link2-1 ,  SW ref.type:2 (2)

link_15 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.42362414385373,0.0444999999999905,-2.09532020593446)
dA = chrono.ChVectorD(0.999999911259009,-8.04911692853238e-15,-0.000421286095366785)
cB = chrono.ChVectorD(2.40462415108842,0.0444999952145806,-2.09531219878621)
dB = chrono.ChVectorD(0.999999911259009,-8.00748356510894e-15,-0.000421286095366675)
link_15.Initialize(body_12,body_14,False,cA,cB,dA,dB)
link_15.SetName("Concentric7")
exported_items.append(link_15)

link_16 = chrono.ChLinkMateGeneric()
link_16.SetConstrainedCoords(False, True, True, False, False, False)
cA = chrono.ChVectorD(2.42362414385373,0.0444999999999905,-2.09532020593446)
cB = chrono.ChVectorD(2.40462415108842,0.0444999952145806,-2.09531219878621)
dA = chrono.ChVectorD(0.999999911259009,-8.04911692853238e-15,-0.000421286095366785)
dB = chrono.ChVectorD(0.999999911259009,-8.00748356510894e-15,-0.000421286095366675)
link_16.Initialize(body_12,body_14,False,cA,cB,dA,dB)
link_16.SetName("Concentric7")
exported_items.append(link_16)


# Mate constraint: Coincident9 [MateCoincident] type:0 align:0 flip:False
#   Entity 0: C::E name: body_12 , SW name: robo_leg_link-10/link1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_15 , SW name: robo_leg_link-10/link2-2 ,  SW ref.type:2 (2)

link_17 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(2.53761791976735,0.0779999999999893,-2.1101182312404)
cB = chrono.ChVectorD(2.53769261460573,0.0779999994243677,-1.93281824718463)
dA = chrono.ChVectorD(0.999999911259009,-7.32747196252603e-15,-0.000421286095366746)
dB = chrono.ChVectorD(0.999999911259009,-7.97972798949331e-15,-0.000421286095366676)
link_17.Initialize(body_12,body_15,False,cA,cB,dB)
link_17.SetDistance(0)
link_17.SetName("Coincident9")
exported_items.append(link_17)

link_18 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.53761791976735,0.0779999999999893,-2.1101182312404)
dA = chrono.ChVectorD(0.999999911259009,-7.32747196252603e-15,-0.000421286095366746)
cB = chrono.ChVectorD(2.53769261460573,0.0779999994243677,-1.93281824718463)
dB = chrono.ChVectorD(0.999999911259009,-7.97972798949331e-15,-0.000421286095366676)
link_18.Initialize(body_12,body_15,False,cA,cB,dA,dB)
link_18.SetName("Coincident9")
exported_items.append(link_18)


# Mate constraint: Coincident10 [MateCoincident] type:0 align:0 flip:False
#   Entity 0: C::E name: body_12 , SW name: robo_leg_link-10/link1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_14 , SW name: robo_leg_link-10/link2-1 ,  SW ref.type:2 (2)

link_19 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(2.53761791976735,0.0779999999999893,-2.1101182312404)
cB = chrono.ChVectorD(2.53759311156495,0.0109999952145795,-2.16901822330112)
dA = chrono.ChVectorD(0.999999911259009,-7.32747196252603e-15,-0.000421286095366746)
dB = chrono.ChVectorD(0.999999911259009,-8.00748356510894e-15,-0.000421286095366675)
link_19.Initialize(body_12,body_14,False,cA,cB,dB)
link_19.SetDistance(0)
link_19.SetName("Coincident10")
exported_items.append(link_19)

link_20 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.53761791976735,0.0779999999999893,-2.1101182312404)
dA = chrono.ChVectorD(0.999999911259009,-7.32747196252603e-15,-0.000421286095366746)
cB = chrono.ChVectorD(2.53759311156495,0.0109999952145795,-2.16901822330112)
dB = chrono.ChVectorD(0.999999911259009,-8.00748356510894e-15,-0.000421286095366675)
link_20.Initialize(body_12,body_14,False,cA,cB,dA,dB)
link_20.SetName("Coincident10")
exported_items.append(link_20)


# Mate constraint: Coincident11 [MateCoincident] type:0 align:0 flip:False
#   Entity 0: C::E name: body_12 , SW name: robo_leg_link-10/link1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_14 , SW name: robo_leg_link-10/link2-1 ,  SW ref.type:2 (2)

link_21 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(2.43961792846397,0.07799999999999,-2.11007694520306)
cB = chrono.ChVectorD(2.43959312026157,0.0109999952145804,-2.16897693726378)
dA = chrono.ChVectorD(-0.999999911259009,8.04911692853238e-15,0.000421286095366785)
dB = chrono.ChVectorD(-0.999999911259009,8.77076189453874e-15,0.000421286095366713)
link_21.Initialize(body_12,body_14,False,cA,cB,dB)
link_21.SetDistance(0)
link_21.SetName("Coincident11")
exported_items.append(link_21)

link_22 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.43961792846397,0.07799999999999,-2.11007694520306)
dA = chrono.ChVectorD(-0.999999911259009,8.04911692853238e-15,0.000421286095366785)
cB = chrono.ChVectorD(2.43959312026157,0.0109999952145804,-2.16897693726378)
dB = chrono.ChVectorD(-0.999999911259009,8.77076189453874e-15,0.000421286095366713)
link_22.Initialize(body_12,body_14,False,cA,cB,dA,dB)
link_22.SetName("Coincident11")
exported_items.append(link_22)


# Mate constraint: Coincident12 [MateCoincident] type:0 align:0 flip:False
#   Entity 0: C::E name: body_12 , SW name: robo_leg_link-10/link1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_15 , SW name: robo_leg_link-10/link2-2 ,  SW ref.type:2 (2)

link_23 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(2.43961792846397,0.07799999999999,-2.11007694520306)
cB = chrono.ChVectorD(2.43969262330234,0.0779999994243685,-1.93277696114728)
dA = chrono.ChVectorD(-0.999999911259009,8.04911692853238e-15,0.000421286095366785)
dB = chrono.ChVectorD(-0.999999911259009,7.21644966006352e-15,0.000421286095366637)
link_23.Initialize(body_12,body_15,False,cA,cB,dB)
link_23.SetDistance(0)
link_23.SetName("Coincident12")
exported_items.append(link_23)

link_24 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.43961792846397,0.07799999999999,-2.11007694520306)
dA = chrono.ChVectorD(-0.999999911259009,8.04911692853238e-15,0.000421286095366785)
cB = chrono.ChVectorD(2.43969262330234,0.0779999994243685,-1.93277696114728)
dB = chrono.ChVectorD(-0.999999911259009,7.21644966006352e-15,0.000421286095366637)
link_24.Initialize(body_12,body_15,False,cA,cB,dA,dB)
link_24.SetName("Coincident12")
exported_items.append(link_24)


# Mate constraint: Concentric2 [MateConcentric] type:1 align:1 flip:False
#   Entity 0: C::E name: body_17 , SW name: robo_leg_link-3/leg-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_16 , SW name: robo_leg_link-3/single_bot1-1 ,  SW ref.type:2 (2)

link_1 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.01536517105067,0.0170000000000035,-2.07924279722952)
dA = chrono.ChVectorD(-0.813612412193062,2.08166817117217e-16,-0.581407639032536)
cB = chrono.ChVectorD(2.0088562702032,0.0170000034943454,-2.08389406165996)
dB = chrono.ChVectorD(0.813612412193056,3.59434704222394e-15,0.581407639032544)
link_1.SetFlipped(True)
link_1.Initialize(body_17,body_16,False,cA,cB,dA,dB)
link_1.SetName("Concentric2")
exported_items.append(link_1)

link_2 = chrono.ChLinkMateGeneric()
link_2.SetConstrainedCoords(False, True, True, False, False, False)
cA = chrono.ChVectorD(2.01536517105067,0.0170000000000035,-2.07924279722952)
cB = chrono.ChVectorD(2.0088562702032,0.0170000034943454,-2.08389406165996)
dA = chrono.ChVectorD(-0.813612412193062,2.08166817117217e-16,-0.581407639032536)
dB = chrono.ChVectorD(0.813612412193056,3.59434704222394e-15,0.581407639032544)
link_2.Initialize(body_17,body_16,False,cA,cB,dA,dB)
link_2.SetName("Concentric2")
exported_items.append(link_2)


# Mate constraint: Coincident4 [MateCoincident] type:0 align:1 flip:False
#   Entity 0: C::E name: body_16 , SW name: robo_leg_link-3/single_bot1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_18 , SW name: robo_leg_link-3/link1-1 ,  SW ref.type:2 (2)

link_3 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(1.97085353456524,0.0110000034943457,-2.07837556276703)
cB = chrono.ChVectorD(2.01073018272952,0.0110000034943576,-2.08501313620783)
dA = chrono.ChVectorD(5.24580379135386e-15,1,-7.13318293321663e-15)
dB = chrono.ChVectorD(-2.21073159778484e-14,-1,-1.50435219836709e-14)
link_3.Initialize(body_16,body_18,False,cA,cB,dB)
link_3.SetDistance(0)
link_3.SetName("Coincident4")
exported_items.append(link_3)

link_4 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(1.97085353456524,0.0110000034943457,-2.07837556276703)
dA = chrono.ChVectorD(5.24580379135386e-15,1,-7.13318293321663e-15)
cB = chrono.ChVectorD(2.01073018272952,0.0110000034943576,-2.08501313620783)
dB = chrono.ChVectorD(-2.21073159778484e-14,-1,-1.50435219836709e-14)
link_4.SetFlipped(True)
link_4.Initialize(body_16,body_18,False,cA,cB,dA,dB)
link_4.SetName("Coincident4")
exported_items.append(link_4)


# Mate constraint: Parallel3 [MateParallel] type:3 align:1 flip:False
#   Entity 0: C::E name: body_16 , SW name: robo_leg_link-3/single_bot1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_18 , SW name: robo_leg_link-3/link1-1 ,  SW ref.type:2 (2)

link_5 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(1.98487443336621,0.0633100034943654,-2.07537307948091)
dA = chrono.ChVectorD(-0.581407639032535,8.50708392619026e-15,0.813612412193062)
cB = chrono.ChVectorD(2.01197245854866,0.021200003494358,-2.11758143383094)
dB = chrono.ChVectorD(0.581407639032535,-6.52256026967279e-16,-0.813612412193062)
link_5.SetFlipped(True)
link_5.Initialize(body_16,body_18,False,cA,cB,dA,dB)
link_5.SetName("Parallel3")
exported_items.append(link_5)


# Mate constraint: Coincident7 [MateCoincident] type:0 align:1 flip:False
#   Entity 0: C::E name: body_17 , SW name: robo_leg_link-3/leg-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_16 , SW name: robo_leg_link-3/single_bot1-1 ,  SW ref.type:2 (2)

link_6 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(2.00885627175313,0.0170000000000035,-2.08389405834178)
cB = chrono.ChVectorD(2.00769287351749,0.0149990034943454,-2.08226602322316)
dA = chrono.ChVectorD(-0.813612412193062,2.08166817117217e-16,-0.581407639032536)
dB = chrono.ChVectorD(0.813612412193056,3.59434704222394e-15,0.581407639032544)
link_6.Initialize(body_17,body_16,False,cA,cB,dB)
link_6.SetDistance(0)
link_6.SetName("Coincident7")
exported_items.append(link_6)

link_7 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.00885627175313,0.0170000000000035,-2.08389405834178)
dA = chrono.ChVectorD(-0.813612412193062,2.08166817117217e-16,-0.581407639032536)
cB = chrono.ChVectorD(2.00769287351749,0.0149990034943454,-2.08226602322316)
dB = chrono.ChVectorD(0.813612412193056,3.59434704222394e-15,0.581407639032544)
link_7.SetFlipped(True)
link_7.Initialize(body_17,body_16,False,cA,cB,dA,dB)
link_7.SetName("Coincident7")
exported_items.append(link_7)


# Mate constraint: Concentric4 [MateConcentric] type:1 align:1 flip:False
#   Entity 0: C::E name: body_16 , SW name: robo_leg_link-3/single_bot1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_18 , SW name: robo_leg_link-3/link1-1 ,  SW ref.type:2 (2)

link_8 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.01073018272952,0.078950003494376,-2.08501313620783)
dA = chrono.ChVectorD(-2.21489493412719e-14,-1,-1.50504608775748e-14)
cB = chrono.ChVectorD(2.01073018272952,0.0110000034943576,-2.08501313620783)
dB = chrono.ChVectorD(2.21073159778484e-14,1,1.50435219836709e-14)
link_8.SetFlipped(True)
link_8.Initialize(body_16,body_18,False,cA,cB,dA,dB)
link_8.SetName("Concentric4")
exported_items.append(link_8)

link_9 = chrono.ChLinkMateGeneric()
link_9.SetConstrainedCoords(False, True, True, False, False, False)
cA = chrono.ChVectorD(2.01073018272952,0.078950003494376,-2.08501313620783)
cB = chrono.ChVectorD(2.01073018272952,0.0110000034943576,-2.08501313620783)
dA = chrono.ChVectorD(-2.21489493412719e-14,-1,-1.50504608775748e-14)
dB = chrono.ChVectorD(2.21073159778484e-14,1,1.50435219836709e-14)
link_9.Initialize(body_16,body_18,False,cA,cB,dA,dB)
link_9.SetName("Concentric4")
exported_items.append(link_9)


# Mate constraint: Parallel4 [MateParallel] type:3 align:1 flip:False
#   Entity 0: C::E name: body_16 , SW name: robo_leg_link-3/single_bot1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_18 , SW name: robo_leg_link-3/link1-1 ,  SW ref.type:2 (2)

link_10 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(1.96881379276245,0.0789500034943769,-2.08356885360797)
dA = chrono.ChVectorD(-2.21489493412719e-14,-1,-1.50504608775748e-14)
cB = chrono.ChVectorD(2.01073018272952,0.0780000034943576,-2.08501313620782)
dB = chrono.ChVectorD(2.21073159778484e-14,1,1.50435219836709e-14)
link_10.SetFlipped(True)
link_10.Initialize(body_16,body_18,False,cA,cB,dA,dB)
link_10.SetName("Parallel4")
exported_items.append(link_10)


# Mate constraint: Concentric5 [MateConcentric] type:1 align:0 flip:False
#   Entity 0: C::E name: body_16 , SW name: robo_leg_link-3/single_bot1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_18 , SW name: robo_leg_link-3/link1-1 ,  SW ref.type:2 (2)

link_11 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.01073018272952,0.0110000034943454,-2.08501313620783)
dA = chrono.ChVectorD(5.24580379135386e-15,1,-7.13318293321663e-15)
cB = chrono.ChVectorD(2.01073018272952,0.0110000034943576,-2.08501313620783)
dB = chrono.ChVectorD(2.21073159778484e-14,1,1.50435219836709e-14)
link_11.Initialize(body_16,body_18,False,cA,cB,dA,dB)
link_11.SetName("Concentric5")
exported_items.append(link_11)

link_12 = chrono.ChLinkMateGeneric()
link_12.SetConstrainedCoords(False, True, True, False, False, False)
cA = chrono.ChVectorD(2.01073018272952,0.0110000034943454,-2.08501313620783)
cB = chrono.ChVectorD(2.01073018272952,0.0110000034943576,-2.08501313620783)
dA = chrono.ChVectorD(5.24580379135386e-15,1,-7.13318293321663e-15)
dB = chrono.ChVectorD(2.21073159778484e-14,1,1.50435219836709e-14)
link_12.Initialize(body_16,body_18,False,cA,cB,dA,dB)
link_12.SetName("Concentric5")
exported_items.append(link_12)


# Mate constraint: Concentric6 [MateConcentric] type:1 align:0 flip:False
#   Entity 0: C::E name: body_18 , SW name: robo_leg_link-3/link1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_19 , SW name: robo_leg_link-3/link2-2 ,  SW ref.type:2 (2)

link_13 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.01235660754465,0.0445000034943587,-2.16374151255537)
dA = chrono.ChVectorD(-0.581407639032535,6.52256026967279e-16,0.813612412193062)
cB = chrono.ChVectorD(2.02340335301757,0.0445000000239365,-2.17920014815029)
dB = chrono.ChVectorD(-0.581407639032536,-2.22044604925031e-16,0.813612412193062)
link_13.Initialize(body_18,body_19,False,cA,cB,dA,dB)
link_13.SetName("Concentric6")
exported_items.append(link_13)

link_14 = chrono.ChLinkMateGeneric()
link_14.SetConstrainedCoords(False, True, True, False, False, False)
cA = chrono.ChVectorD(2.01235660754465,0.0445000034943587,-2.16374151255537)
cB = chrono.ChVectorD(2.02340335301757,0.0445000000239365,-2.17920014815029)
dA = chrono.ChVectorD(-0.581407639032535,6.52256026967279e-16,0.813612412193062)
dB = chrono.ChVectorD(-0.581407639032536,-2.22044604925031e-16,0.813612412193062)
link_14.Initialize(body_18,body_19,False,cA,cB,dA,dB)
link_14.SetName("Concentric6")
exported_items.append(link_14)


# Mate constraint: Concentric7 [MateConcentric] type:1 align:0 flip:False
#   Entity 0: C::E name: body_18 , SW name: robo_leg_link-3/link1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_20 , SW name: robo_leg_link-3/link2-1 ,  SW ref.type:2 (2)

link_15 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.08468675098862,0.0445000034943563,-2.11205437344538)
dA = chrono.ChVectorD(-0.581407639032535,6.52256026967279e-16,0.813612412193062)
cB = chrono.ChVectorD(2.09573349638817,0.0445000001591641,-2.12751300909273)
dB = chrono.ChVectorD(-0.581407639032535,5.27355936696949e-16,0.813612412193062)
link_15.Initialize(body_18,body_20,False,cA,cB,dA,dB)
link_15.SetName("Concentric7")
exported_items.append(link_15)

link_16 = chrono.ChLinkMateGeneric()
link_16.SetConstrainedCoords(False, True, True, False, False, False)
cA = chrono.ChVectorD(2.08468675098862,0.0445000034943563,-2.11205437344538)
cB = chrono.ChVectorD(2.09573349638817,0.0445000001591641,-2.12751300909273)
dA = chrono.ChVectorD(-0.581407639032535,6.52256026967279e-16,0.813612412193062)
dB = chrono.ChVectorD(-0.581407639032535,5.27355936696949e-16,0.813612412193062)
link_16.Initialize(body_18,body_20,False,cA,cB,dA,dB)
link_16.SetName("Concentric7")
exported_items.append(link_16)


# Mate constraint: Coincident9 [MateCoincident] type:0 align:0 flip:False
#   Entity 0: C::E name: body_18 , SW name: robo_leg_link-3/link1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_19 , SW name: robo_leg_link-3/link2-2 ,  SW ref.type:2 (2)

link_17 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(2.03040706321876,0.0780000034943561,-2.01072679577964)
cB = chrono.ChVectorD(1.88615358286823,0.0780000000239365,-2.11381036994336)
dA = chrono.ChVectorD(-0.581407639032535,1.36002320516582e-15,0.813612412193062)
dB = chrono.ChVectorD(-0.581407639032536,-2.22044604925031e-16,0.813612412193062)
link_17.Initialize(body_18,body_19,False,cA,cB,dB)
link_17.SetDistance(0)
link_17.SetName("Coincident9")
exported_items.append(link_17)

link_18 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.03040706321876,0.0780000034943561,-2.01072679577964)
dA = chrono.ChVectorD(-0.581407639032535,1.36002320516582e-15,0.813612412193062)
cB = chrono.ChVectorD(1.88615358286823,0.0780000000239365,-2.11381036994336)
dB = chrono.ChVectorD(-0.581407639032536,-2.22044604925031e-16,0.813612412193062)
link_18.Initialize(body_18,body_19,False,cA,cB,dA,dB)
link_18.SetName("Coincident9")
exported_items.append(link_18)


# Mate constraint: Coincident10 [MateCoincident] type:0 align:0 flip:False
#   Entity 0: C::E name: body_18 , SW name: robo_leg_link-3/link1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_20 , SW name: robo_leg_link-3/link2-1 ,  SW ref.type:2 (2)

link_19 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(2.03040706321876,0.0780000034943561,-2.01072679577964)
cB = chrono.ChVectorD(2.07832883455486,0.0110000001591642,-1.97648188565631)
dA = chrono.ChVectorD(-0.581407639032535,1.36002320516582e-15,0.813612412193062)
dB = chrono.ChVectorD(-0.581407639032535,5.27355936696949e-16,0.813612412193062)
link_19.Initialize(body_18,body_20,False,cA,cB,dB)
link_19.SetDistance(0)
link_19.SetName("Coincident10")
exported_items.append(link_19)

link_20 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.03040706321876,0.0780000034943561,-2.01072679577964)
dA = chrono.ChVectorD(-0.581407639032535,1.36002320516582e-15,0.813612412193062)
cB = chrono.ChVectorD(2.07832883455486,0.0110000001591642,-1.97648188565631)
dB = chrono.ChVectorD(-0.581407639032535,5.27355936696949e-16,0.813612412193062)
link_20.Initialize(body_18,body_20,False,cA,cB,dA,dB)
link_20.SetName("Coincident10")
exported_items.append(link_20)


# Mate constraint: Coincident11 [MateCoincident] type:0 align:0 flip:False
#   Entity 0: C::E name: body_18 , SW name: robo_leg_link-3/link1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_20 , SW name: robo_leg_link-3/link2-1 ,  SW ref.type:2 (2)

link_21 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(2.08738501184395,0.078000003494356,-2.09046081217456)
cB = chrono.ChVectorD(2.13530678318005,0.0110000001591641,-2.05621590205123)
dA = chrono.ChVectorD(0.581407639032535,-6.52256026967279e-16,-0.813612412193062)
dB = chrono.ChVectorD(0.581407639032536,2.4980018054066e-16,-0.813612412193062)
link_21.Initialize(body_18,body_20,False,cA,cB,dB)
link_21.SetDistance(0)
link_21.SetName("Coincident11")
exported_items.append(link_21)

link_22 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.08738501184395,0.078000003494356,-2.09046081217456)
dA = chrono.ChVectorD(0.581407639032535,-6.52256026967279e-16,-0.813612412193062)
cB = chrono.ChVectorD(2.13530678318005,0.0110000001591641,-2.05621590205123)
dB = chrono.ChVectorD(0.581407639032536,2.4980018054066e-16,-0.813612412193062)
link_22.Initialize(body_18,body_20,False,cA,cB,dA,dB)
link_22.SetName("Coincident11")
exported_items.append(link_22)


# Mate constraint: Coincident12 [MateCoincident] type:0 align:0 flip:False
#   Entity 0: C::E name: body_18 , SW name: robo_leg_link-3/link1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_19 , SW name: robo_leg_link-3/link2-2 ,  SW ref.type:2 (2)

link_23 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(2.08738501184395,0.078000003494356,-2.09046081217456)
cB = chrono.ChVectorD(1.94313153149341,0.0780000000239364,-2.19354438633828)
dA = chrono.ChVectorD(0.581407639032535,-6.52256026967279e-16,-0.813612412193062)
dB = chrono.ChVectorD(0.581407639032536,-5.41233724504764e-16,-0.813612412193062)
link_23.Initialize(body_18,body_19,False,cA,cB,dB)
link_23.SetDistance(0)
link_23.SetName("Coincident12")
exported_items.append(link_23)

link_24 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.08738501184395,0.078000003494356,-2.09046081217456)
dA = chrono.ChVectorD(0.581407639032535,-6.52256026967279e-16,-0.813612412193062)
cB = chrono.ChVectorD(1.94313153149341,0.0780000000239364,-2.19354438633828)
dB = chrono.ChVectorD(0.581407639032536,-5.41233724504764e-16,-0.813612412193062)
link_24.Initialize(body_18,body_19,False,cA,cB,dA,dB)
link_24.SetName("Coincident12")
exported_items.append(link_24)


# Mate constraint: Concentric2 [MateConcentric] type:1 align:1 flip:False
#   Entity 0: C::E name: body_24 , SW name: robo_leg_link-7/leg-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_22 , SW name: robo_leg_link-7/single_bot1-1 ,  SW ref.type:2 (2)

link_1 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.20111626649813,0.0169999999999841,-2.37047644122374)
dA = chrono.ChVectorD(0.759773825636061,1.65145674912992e-15,0.650187460566831)
cB = chrono.ChVectorD(2.20719445387741,0.0170000003907949,-2.3652749456564)
dB = chrono.ChVectorD(-0.759773825636054,2.05738204250849e-15,-0.65018746056684)
link_1.SetFlipped(True)
link_1.Initialize(body_24,body_22,False,cA,cB,dA,dB)
link_1.SetName("Concentric2")
exported_items.append(link_1)

link_2 = chrono.ChLinkMateGeneric()
link_2.SetConstrainedCoords(False, True, True, False, False, False)
cA = chrono.ChVectorD(2.20111626649813,0.0169999999999841,-2.37047644122374)
cB = chrono.ChVectorD(2.20719445387741,0.0170000003907949,-2.3652749456564)
dA = chrono.ChVectorD(0.759773825636061,1.65145674912992e-15,0.650187460566831)
dB = chrono.ChVectorD(-0.759773825636054,2.05738204250849e-15,-0.65018746056684)
link_2.Initialize(body_24,body_22,False,cA,cB,dA,dB)
link_2.SetName("Concentric2")
exported_items.append(link_2)


# Mate constraint: Coincident4 [MateCoincident] type:0 align:1 flip:False
#   Entity 0: C::E name: body_22 , SW name: robo_leg_link-7/single_bot1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_23 , SW name: robo_leg_link-7/link1-1 ,  SW ref.type:2 (2)

link_3 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(2.24553377999879,0.0110000003907952,-2.36745618809117)
cB = chrono.ChVectorD(2.20523003663267,0.0110000003907721,-2.36432366184567)
dA = chrono.ChVectorD(-6.09234884763055e-15,1,4.64905891561784e-15)
dB = chrono.ChVectorD(2.09832151654155e-14,-1,1.88946081003394e-14)
link_3.Initialize(body_22,body_23,False,cA,cB,dB)
link_3.SetDistance(0)
link_3.SetName("Coincident4")
exported_items.append(link_3)

link_4 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.24553377999879,0.0110000003907952,-2.36745618809117)
dA = chrono.ChVectorD(-6.09234884763055e-15,1,4.64905891561784e-15)
cB = chrono.ChVectorD(2.20523003663267,0.0110000003907721,-2.36432366184567)
dB = chrono.ChVectorD(2.09832151654155e-14,-1,1.88946081003394e-14)
link_4.SetFlipped(True)
link_4.Initialize(body_22,body_23,False,cA,cB,dA,dB)
link_4.SetName("Coincident4")
exported_items.append(link_4)


# Mate constraint: Parallel3 [MateParallel] type:3 align:1 flip:False
#   Entity 0: C::E name: body_22 , SW name: robo_leg_link-7/single_bot1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_23 , SW name: robo_leg_link-7/link1-1 ,  SW ref.type:2 (2)

link_5 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.23182836926298,0.063310000390815,-2.37167071364378)
dA = chrono.ChVectorD(0.650187460566831,7.17481629664007e-15,-0.759773825636061)
cB = chrono.ChVectorD(2.20115051566274,0.0212000003907726,-2.33198800400584)
dB = chrono.ChVectorD(-0.650187460566831,6.93889390390723e-16,0.759773825636061)
link_5.SetFlipped(True)
link_5.Initialize(body_22,body_23,False,cA,cB,dA,dB)
link_5.SetName("Parallel3")
exported_items.append(link_5)


# Mate constraint: Coincident7 [MateCoincident] type:0 align:1 flip:False
#   Entity 0: C::E name: body_24 , SW name: robo_leg_link-7/leg-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_22 , SW name: robo_leg_link-7/single_bot1-1 ,  SW ref.type:2 (2)

link_6 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(2.20719445710322,0.0169999999999842,-2.3652749415392)
cB = chrono.ChVectorD(2.20849547898601,0.0149990003907949,-2.3667952530815)
dA = chrono.ChVectorD(0.759773825636061,1.65145674912992e-15,0.650187460566831)
dB = chrono.ChVectorD(-0.759773825636054,2.05738204250849e-15,-0.65018746056684)
link_6.Initialize(body_24,body_22,False,cA,cB,dB)
link_6.SetDistance(0)
link_6.SetName("Coincident7")
exported_items.append(link_6)

link_7 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.20719445710322,0.0169999999999842,-2.3652749415392)
dA = chrono.ChVectorD(0.759773825636061,1.65145674912992e-15,0.650187460566831)
cB = chrono.ChVectorD(2.20849547898601,0.0149990003907949,-2.3667952530815)
dB = chrono.ChVectorD(-0.759773825636054,2.05738204250849e-15,-0.65018746056684)
link_7.SetFlipped(True)
link_7.Initialize(body_24,body_22,False,cA,cB,dA,dB)
link_7.SetName("Coincident7")
exported_items.append(link_7)


# Mate constraint: Concentric4 [MateConcentric] type:1 align:1 flip:False
#   Entity 0: C::E name: body_22 , SW name: robo_leg_link-7/single_bot1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_23 , SW name: robo_leg_link-7/link1-1 ,  SW ref.type:2 (2)

link_8 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.20523003663267,0.0789500003908255,-2.36432366184567)
dA = chrono.ChVectorD(2.09970929532233e-14,-1,1.89154247820511e-14)
cB = chrono.ChVectorD(2.20523003663267,0.0110000003907721,-2.36432366184567)
dB = chrono.ChVectorD(-2.09832151654155e-14,1,-1.88946081003394e-14)
link_8.SetFlipped(True)
link_8.Initialize(body_22,body_23,False,cA,cB,dA,dB)
link_8.SetName("Concentric4")
exported_items.append(link_8)

link_9 = chrono.ChLinkMateGeneric()
link_9.SetConstrainedCoords(False, True, True, False, False, False)
cA = chrono.ChVectorD(2.20523003663267,0.0789500003908255,-2.36432366184567)
cB = chrono.ChVectorD(2.20523003663267,0.0110000003907721,-2.36432366184567)
dA = chrono.ChVectorD(2.09970929532233e-14,-1,1.89154247820511e-14)
dB = chrono.ChVectorD(-2.09832151654155e-14,1,-1.88946081003394e-14)
link_9.Initialize(body_22,body_23,False,cA,cB,dA,dB)
link_9.SetName("Concentric4")
exported_items.append(link_9)


# Mate constraint: Parallel4 [MateParallel] type:3 align:1 flip:False
#   Entity 0: C::E name: body_22 , SW name: robo_leg_link-7/single_bot1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_23 , SW name: robo_leg_link-7/link1-1 ,  SW ref.type:2 (2)

link_10 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.24711256253419,0.0789500003908264,-2.36210471520173)
dA = chrono.ChVectorD(2.09970929532233e-14,-1,1.89154247820511e-14)
cB = chrono.ChVectorD(2.20523003663267,0.0780000003907721,-2.36432366184567)
dB = chrono.ChVectorD(-2.09832151654155e-14,1,-1.88946081003394e-14)
link_10.SetFlipped(True)
link_10.Initialize(body_22,body_23,False,cA,cB,dA,dB)
link_10.SetName("Parallel4")
exported_items.append(link_10)


# Mate constraint: Concentric5 [MateConcentric] type:1 align:0 flip:False
#   Entity 0: C::E name: body_22 , SW name: robo_leg_link-7/single_bot1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_23 , SW name: robo_leg_link-7/link1-1 ,  SW ref.type:2 (2)

link_11 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.20523003663268,0.011000000390795,-2.36432366184567)
dA = chrono.ChVectorD(-6.09234884763055e-15,1,4.64905891561784e-15)
cB = chrono.ChVectorD(2.20523003663267,0.0110000003907721,-2.36432366184567)
dB = chrono.ChVectorD(-2.09832151654155e-14,1,-1.88946081003394e-14)
link_11.Initialize(body_22,body_23,False,cA,cB,dA,dB)
link_11.SetName("Concentric5")
exported_items.append(link_11)

link_12 = chrono.ChLinkMateGeneric()
link_12.SetConstrainedCoords(False, True, True, False, False, False)
cA = chrono.ChVectorD(2.20523003663268,0.011000000390795,-2.36432366184567)
cB = chrono.ChVectorD(2.20523003663267,0.0110000003907721,-2.36432366184567)
dA = chrono.ChVectorD(-6.09234884763055e-15,1,4.64905891561784e-15)
dB = chrono.ChVectorD(-2.09832151654155e-14,1,-1.88946081003394e-14)
link_12.Initialize(body_22,body_23,False,cA,cB,dA,dB)
link_12.SetName("Concentric5")
exported_items.append(link_12)


# Mate constraint: Concentric6 [MateConcentric] type:1 align:0 flip:False
#   Entity 0: C::E name: body_23 , SW name: robo_leg_link-7/link1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_21 , SW name: robo_leg_link-7/link2-2 ,  SW ref.type:2 (2)

link_13 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.19673979824535,0.0445000003907734,-2.28603753055713)
dA = chrono.ChVectorD(0.650187460566831,-6.93889390390723e-16,-0.759773825636061)
cB = chrono.ChVectorD(2.1843862364963,0.0445000003814679,-2.27160182786857)
dB = chrono.ChVectorD(0.650187460566831,-1.37390099297363e-15,-0.759773825636061)
link_13.Initialize(body_23,body_21,False,cA,cB,dA,dB)
link_13.SetName("Concentric6")
exported_items.append(link_13)

link_14 = chrono.ChLinkMateGeneric()
link_14.SetConstrainedCoords(False, True, True, False, False, False)
cA = chrono.ChVectorD(2.19673979824535,0.0445000003907734,-2.28603753055713)
cB = chrono.ChVectorD(2.1843862364963,0.0445000003814679,-2.27160182786857)
dA = chrono.ChVectorD(0.650187460566831,-6.93889390390723e-16,-0.759773825636061)
dB = chrono.ChVectorD(0.650187460566831,-1.37390099297363e-15,-0.759773825636061)
link_14.Initialize(body_23,body_21,False,cA,cB,dA,dB)
link_14.SetName("Concentric6")
exported_items.append(link_14)


# Mate constraint: Concentric7 [MateConcentric] type:1 align:0 flip:False
#   Entity 0: C::E name: body_23 , SW name: robo_leg_link-7/link1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_25 , SW name: robo_leg_link-7/link2-1 ,  SW ref.type:2 (2)

link_15 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.12919590514631,0.0445000003907709,-2.34383919580152)
dA = chrono.ChVectorD(0.650187460566831,-6.93889390390723e-16,-0.759773825636061)
cB = chrono.ChVectorD(2.11684234339276,0.0445000003984779,-2.32940349311682)
dB = chrono.ChVectorD(0.650187460566831,5.55111512312578e-17,-0.759773825636061)
link_15.Initialize(body_23,body_25,False,cA,cB,dA,dB)
link_15.SetName("Concentric7")
exported_items.append(link_15)

link_16 = chrono.ChLinkMateGeneric()
link_16.SetConstrainedCoords(False, True, True, False, False, False)
cA = chrono.ChVectorD(2.12919590514631,0.0445000003907709,-2.34383919580152)
cB = chrono.ChVectorD(2.11684234339276,0.0445000003984779,-2.32940349311682)
dA = chrono.ChVectorD(0.650187460566831,-6.93889390390723e-16,-0.759773825636061)
dB = chrono.ChVectorD(0.650187460566831,5.55111512312578e-17,-0.759773825636061)
link_16.Initialize(body_23,body_25,False,cA,cB,dA,dB)
link_16.SetName("Concentric7")
exported_items.append(link_16)


# Mate constraint: Coincident9 [MateCoincident] type:0 align:0 flip:False
#   Entity 0: C::E name: body_23 , SW name: robo_leg_link-7/link1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_21 , SW name: robo_leg_link-7/link2-2 ,  SW ref.type:2 (2)

link_17 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(2.19211061172279,0.0780000003907704,-2.4400436769674)
cB = chrono.ChVectorD(2.32681851100979,0.0780000003814678,-2.32476544020742)
dA = chrono.ChVectorD(0.650187460566831,2.77555756156289e-17,-0.759773825636061)
dB = chrono.ChVectorD(0.650187460566831,-1.37390099297363e-15,-0.759773825636061)
link_17.Initialize(body_23,body_21,False,cA,cB,dB)
link_17.SetDistance(0)
link_17.SetName("Coincident9")
exported_items.append(link_17)

link_18 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.19211061172279,0.0780000003907704,-2.4400436769674)
dA = chrono.ChVectorD(0.650187460566831,2.77555756156289e-17,-0.759773825636061)
cB = chrono.ChVectorD(2.32681851100979,0.0780000003814678,-2.32476544020742)
dB = chrono.ChVectorD(0.650187460566831,-1.37390099297363e-15,-0.759773825636061)
link_18.Initialize(body_23,body_21,False,cA,cB,dA,dB)
link_18.SetName("Coincident9")
exported_items.append(link_18)


# Mate constraint: Coincident10 [MateCoincident] type:0 align:0 flip:False
#   Entity 0: C::E name: body_23 , SW name: robo_leg_link-7/link1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_25 , SW name: robo_leg_link-7/link2-1 ,  SW ref.type:2 (2)

link_19 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(2.19211061172279,0.0780000003907704,-2.4400436769674)
cB = chrono.ChVectorD(2.14735993339005,0.0110000003984778,-2.47833971839716)
dA = chrono.ChVectorD(0.650187460566831,2.77555756156289e-17,-0.759773825636061)
dB = chrono.ChVectorD(0.650187460566831,5.55111512312578e-17,-0.759773825636061)
link_19.Initialize(body_23,body_25,False,cA,cB,dB)
link_19.SetDistance(0)
link_19.SetName("Coincident10")
exported_items.append(link_19)

link_20 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.19211061172279,0.0780000003907704,-2.4400436769674)
dA = chrono.ChVectorD(0.650187460566831,2.77555756156289e-17,-0.759773825636061)
cB = chrono.ChVectorD(2.14735993339005,0.0110000003984778,-2.47833971839716)
dB = chrono.ChVectorD(0.650187460566831,5.55111512312578e-17,-0.759773825636061)
link_20.Initialize(body_23,body_25,False,cA,cB,dA,dB)
link_20.SetName("Coincident10")
exported_items.append(link_20)


# Mate constraint: Coincident11 [MateCoincident] type:0 align:0 flip:False
#   Entity 0: C::E name: body_23 , SW name: robo_leg_link-7/link1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_25 , SW name: robo_leg_link-7/link2-1 ,  SW ref.type:2 (2)

link_21 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(2.12839224058724,0.0780000003907705,-2.36558584205506)
cB = chrono.ChVectorD(2.0836415622545,0.0110000003984778,-2.40388188348483)
dA = chrono.ChVectorD(-0.650187460566831,6.93889390390723e-16,0.759773825636061)
dB = chrono.ChVectorD(-0.650187460566831,7.07767178198537e-16,0.759773825636061)
link_21.Initialize(body_23,body_25,False,cA,cB,dB)
link_21.SetDistance(0)
link_21.SetName("Coincident11")
exported_items.append(link_21)

link_22 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.12839224058724,0.0780000003907705,-2.36558584205506)
dA = chrono.ChVectorD(-0.650187460566831,6.93889390390723e-16,0.759773825636061)
cB = chrono.ChVectorD(2.0836415622545,0.0110000003984778,-2.40388188348483)
dB = chrono.ChVectorD(-0.650187460566831,7.07767178198537e-16,0.759773825636061)
link_22.Initialize(body_23,body_25,False,cA,cB,dA,dB)
link_22.SetName("Coincident11")
exported_items.append(link_22)


# Mate constraint: Coincident12 [MateCoincident] type:0 align:0 flip:False
#   Entity 0: C::E name: body_23 , SW name: robo_leg_link-7/link1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_21 , SW name: robo_leg_link-7/link2-2 ,  SW ref.type:2 (2)

link_23 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(2.12839224058724,0.0780000003907705,-2.36558584205506)
cB = chrono.ChVectorD(2.26310013987424,0.0780000003814679,-2.25030760529509)
dA = chrono.ChVectorD(-0.650187460566831,6.93889390390723e-16,0.759773825636061)
dB = chrono.ChVectorD(-0.650187460566831,6.24500451351651e-16,0.759773825636061)
link_23.Initialize(body_23,body_21,False,cA,cB,dB)
link_23.SetDistance(0)
link_23.SetName("Coincident12")
exported_items.append(link_23)

link_24 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.12839224058724,0.0780000003907705,-2.36558584205506)
dA = chrono.ChVectorD(-0.650187460566831,6.93889390390723e-16,0.759773825636061)
cB = chrono.ChVectorD(2.26310013987424,0.0780000003814679,-2.25030760529509)
dB = chrono.ChVectorD(-0.650187460566831,6.24500451351651e-16,0.759773825636061)
link_24.Initialize(body_23,body_21,False,cA,cB,dA,dB)
link_24.SetName("Coincident12")
exported_items.append(link_24)


# Mate constraint: Concentric2 [MateConcentric] type:1 align:1 flip:False
#   Entity 0: C::E name: body_28 , SW name: robo_leg_link-8/leg-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_27 , SW name: robo_leg_link-8/single_bot1-1 ,  SW ref.type:2 (2)

link_1 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.32702474042913,0.0169999999999859,-2.28847318050821)
dA = chrono.ChVectorD(0.913301174367174,-1.11022302462516e-16,0.407284869470426)
cB = chrono.ChVectorD(2.33433114933564,0.0169999999999579,-2.28521490318739)
dB = chrono.ChVectorD(-0.913301174367169,3.83026943495679e-15,-0.407284869470436)
link_1.SetFlipped(True)
link_1.Initialize(body_28,body_27,False,cA,cB,dA,dB)
link_1.SetName("Concentric2")
exported_items.append(link_1)

link_2 = chrono.ChLinkMateGeneric()
link_2.SetConstrainedCoords(False, True, True, False, False, False)
cA = chrono.ChVectorD(2.32702474042913,0.0169999999999859,-2.28847318050821)
cB = chrono.ChVectorD(2.33433114933564,0.0169999999999579,-2.28521490318739)
dA = chrono.ChVectorD(0.913301174367174,-1.11022302462516e-16,0.407284869470426)
dB = chrono.ChVectorD(-0.913301174367169,3.83026943495679e-15,-0.407284869470436)
link_2.Initialize(body_28,body_27,False,cA,cB,dA,dB)
link_2.SetName("Concentric2")
exported_items.append(link_2)


# Mate constraint: Coincident4 [MateCoincident] type:0 align:1 flip:False
#   Entity 0: C::E name: body_27 , SW name: robo_leg_link-8/single_bot1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_26 , SW name: robo_leg_link-8/link1-1 ,  SW ref.type:2 (2)

link_3 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(2.3704673064334,0.0109999999999579,-2.29820874389004)
cB = chrono.ChVectorD(2.3327183543703,0.010999999999958,-2.28374426781457)
dA = chrono.ChVectorD(-1.52655665885959e-16,1,8.53483950180589e-16)
dB = chrono.ChVectorD(2.11219930434936e-14,-1,1.74929515317501e-14)
link_3.Initialize(body_27,body_26,False,cA,cB,dB)
link_3.SetDistance(0)
link_3.SetName("Coincident4")
exported_items.append(link_3)

link_4 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.3704673064334,0.0109999999999579,-2.29820874389004)
dA = chrono.ChVectorD(-1.52655665885959e-16,1,8.53483950180589e-16)
cB = chrono.ChVectorD(2.3327183543703,0.010999999999958,-2.28374426781457)
dB = chrono.ChVectorD(2.11219930434936e-14,-1,1.74929515317501e-14)
link_4.SetFlipped(True)
link_4.Initialize(body_27,body_26,False,cA,cB,dA,dB)
link_4.SetName("Coincident4")
exported_items.append(link_4)


# Mate constraint: Parallel3 [MateParallel] type:3 align:1 flip:False
#   Entity 0: C::E name: body_27 , SW name: robo_leg_link-8/single_bot1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_26 , SW name: robo_leg_link-8/link1-1 ,  SW ref.type:2 (2)

link_5 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.35612924390961,0.0633099999999778,-2.29835182475661)
dA = chrono.ChVectorD(0.407284869470426,5.19549681055054e-16,-0.913301174367174)
cB = chrono.ChVectorD(2.33800263584796,0.0211999999999586,-2.25158352111081)
dB = chrono.ChVectorD(-0.407284869470426,7.37344213463942e-15,0.913301174367174)
link_5.SetFlipped(True)
link_5.Initialize(body_27,body_26,False,cA,cB,dA,dB)
link_5.SetName("Parallel3")
exported_items.append(link_5)


# Mate constraint: Coincident7 [MateCoincident] type:0 align:1 flip:False
#   Entity 0: C::E name: body_28 , SW name: robo_leg_link-8/leg-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_27 , SW name: robo_leg_link-8/single_bot1-1 ,  SW ref.type:2 (2)

link_6 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(2.33433114982407,0.0169999999999859,-2.28521490155244)
cB = chrono.ChVectorD(2.33514612635945,0.0149989999999579,-2.2870424188373)
dA = chrono.ChVectorD(0.913301174367174,-1.11022302462516e-16,0.407284869470426)
dB = chrono.ChVectorD(-0.913301174367169,3.83026943495679e-15,-0.407284869470436)
link_6.Initialize(body_28,body_27,False,cA,cB,dB)
link_6.SetDistance(0)
link_6.SetName("Coincident7")
exported_items.append(link_6)

link_7 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.33433114982407,0.0169999999999859,-2.28521490155244)
dA = chrono.ChVectorD(0.913301174367174,-1.11022302462516e-16,0.407284869470426)
cB = chrono.ChVectorD(2.33514612635945,0.0149989999999579,-2.2870424188373)
dB = chrono.ChVectorD(-0.913301174367169,3.83026943495679e-15,-0.407284869470436)
link_7.SetFlipped(True)
link_7.Initialize(body_28,body_27,False,cA,cB,dA,dB)
link_7.SetName("Coincident7")
exported_items.append(link_7)


# Mate constraint: Concentric4 [MateConcentric] type:1 align:1 flip:False
#   Entity 0: C::E name: body_27 , SW name: robo_leg_link-8/single_bot1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_26 , SW name: robo_leg_link-8/link1-1 ,  SW ref.type:2 (2)

link_8 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.3327183543703,0.0789499999999885,-2.28374426781457)
dA = chrono.ChVectorD(2.11358708313014e-14,-1,1.75137682134618e-14)
cB = chrono.ChVectorD(2.3327183543703,0.010999999999958,-2.28374426781457)
dB = chrono.ChVectorD(-2.11219930434936e-14,1,-1.74929515317501e-14)
link_8.SetFlipped(True)
link_8.Initialize(body_27,body_26,False,cA,cB,dA,dB)
link_8.SetName("Concentric4")
exported_items.append(link_8)

link_9 = chrono.ChLinkMateGeneric()
link_9.SetConstrainedCoords(False, True, True, False, False, False)
cA = chrono.ChVectorD(2.3327183543703,0.0789499999999885,-2.28374426781457)
cB = chrono.ChVectorD(2.3327183543703,0.010999999999958,-2.28374426781457)
dA = chrono.ChVectorD(2.11358708313014e-14,-1,1.75137682134618e-14)
dB = chrono.ChVectorD(-2.11219930434936e-14,1,-1.74929515317501e-14)
link_9.Initialize(body_27,body_26,False,cA,cB,dA,dB)
link_9.SetName("Concentric4")
exported_items.append(link_9)


# Mate constraint: Parallel4 [MateParallel] type:3 align:1 flip:False
#   Entity 0: C::E name: body_27 , SW name: robo_leg_link-8/single_bot1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_26 , SW name: robo_leg_link-8/link1-1 ,  SW ref.type:2 (2)

link_10 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.37350271929856,0.0789499999999892,-2.29352717523001)
dA = chrono.ChVectorD(2.11358708313014e-14,-1,1.75137682134618e-14)
cB = chrono.ChVectorD(2.3327183543703,0.077999999999958,-2.28374426781457)
dB = chrono.ChVectorD(-2.11219930434936e-14,1,-1.74929515317501e-14)
link_10.SetFlipped(True)
link_10.Initialize(body_27,body_26,False,cA,cB,dA,dB)
link_10.SetName("Parallel4")
exported_items.append(link_10)


# Mate constraint: Concentric5 [MateConcentric] type:1 align:0 flip:False
#   Entity 0: C::E name: body_27 , SW name: robo_leg_link-8/single_bot1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_26 , SW name: robo_leg_link-8/link1-1 ,  SW ref.type:2 (2)

link_11 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.3327183543703,0.0109999999999579,-2.28374426781457)
dA = chrono.ChVectorD(-1.52655665885959e-16,1,8.53483950180589e-16)
cB = chrono.ChVectorD(2.3327183543703,0.010999999999958,-2.28374426781457)
dB = chrono.ChVectorD(-2.11219930434936e-14,1,-1.74929515317501e-14)
link_11.Initialize(body_27,body_26,False,cA,cB,dA,dB)
link_11.SetName("Concentric5")
exported_items.append(link_11)

link_12 = chrono.ChLinkMateGeneric()
link_12.SetConstrainedCoords(False, True, True, False, False, False)
cA = chrono.ChVectorD(2.3327183543703,0.0109999999999579,-2.28374426781457)
cB = chrono.ChVectorD(2.3327183543703,0.010999999999958,-2.28374426781457)
dA = chrono.ChVectorD(-1.52655665885959e-16,1,8.53483950180589e-16)
dB = chrono.ChVectorD(-2.11219930434936e-14,1,-1.74929515317501e-14)
link_12.Initialize(body_27,body_26,False,cA,cB,dA,dB)
link_12.SetName("Concentric5")
exported_items.append(link_12)


# Mate constraint: Concentric6 [MateConcentric] type:1 align:0 flip:False
#   Entity 0: C::E name: body_26 , SW name: robo_leg_link-8/link1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_30 , SW name: robo_leg_link-8/link2-2 ,  SW ref.type:2 (2)

link_13 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.34684107505534,0.0444999999999596,-2.20627587903275)
dA = chrono.ChVectorD(0.407284869470426,-7.37344213463942e-15,-0.913301174367174)
cB = chrono.ChVectorD(2.33910266047365,0.0445000005068609,-2.1889231585394)
dB = chrono.ChVectorD(0.407284869470426,2.58473797920544e-16,-0.913301174367174)
link_13.Initialize(body_26,body_30,False,cA,cB,dA,dB)
link_13.SetName("Concentric6")
exported_items.append(link_13)

link_14 = chrono.ChLinkMateGeneric()
link_14.SetConstrainedCoords(False, True, True, False, False, False)
cA = chrono.ChVectorD(2.34684107505534,0.0444999999999596,-2.20627587903275)
cB = chrono.ChVectorD(2.33910266047365,0.0445000005068609,-2.1889231585394)
dA = chrono.ChVectorD(0.407284869470426,-7.37344213463942e-15,-0.913301174367174)
dB = chrono.ChVectorD(0.407284869470426,2.58473797920544e-16,-0.913301174367174)
link_14.Initialize(body_26,body_30,False,cA,cB,dA,dB)
link_14.SetName("Concentric6")
exported_items.append(link_14)


# Mate constraint: Concentric7 [MateConcentric] type:1 align:0 flip:False
#   Entity 0: C::E name: body_26 , SW name: robo_leg_link-8/link1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_29 , SW name: robo_leg_link-8/link2-1 ,  SW ref.type:2 (2)

link_15 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.2656486006541,0.0444999999999573,-2.24248350392867)
dA = chrono.ChVectorD(0.407284869470426,-7.37344213463942e-15,-0.913301174367174)
cB = chrono.ChVectorD(2.25791018607672,0.0445000003814755,-2.2251307834334)
dB = chrono.ChVectorD(0.407284869470426,1.01481323344643e-15,-0.913301174367174)
link_15.Initialize(body_26,body_29,False,cA,cB,dA,dB)
link_15.SetName("Concentric7")
exported_items.append(link_15)

link_16 = chrono.ChLinkMateGeneric()
link_16.SetConstrainedCoords(False, True, True, False, False, False)
cA = chrono.ChVectorD(2.2656486006541,0.0444999999999573,-2.24248350392867)
cB = chrono.ChVectorD(2.25791018607672,0.0445000003814755,-2.2251307834334)
dA = chrono.ChVectorD(0.407284869470426,-7.37344213463942e-15,-0.913301174367174)
dB = chrono.ChVectorD(0.407284869470426,1.01481323344643e-15,-0.913301174367174)
link_16.Initialize(body_26,body_29,False,cA,cB,dA,dB)
link_16.SetName("Concentric7")
exported_items.append(link_16)


# Mate constraint: Coincident9 [MateCoincident] type:0 align:0 flip:False
#   Entity 0: C::E name: body_26 , SW name: robo_leg_link-8/link1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_30 , SW name: robo_leg_link-8/link2-2 ,  SW ref.type:2 (2)

link_17 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(2.29860788345181,0.0779999999999561,-2.35260728963121)
cB = chrono.ChVectorD(2.46053617960536,0.078000000506861,-2.28039568409374)
dA = chrono.ChVectorD(0.407284869470426,-6.66654231817887e-15,-0.913301174367174)
dB = chrono.ChVectorD(0.407284869470426,2.58473797920544e-16,-0.913301174367174)
link_17.Initialize(body_26,body_30,False,cA,cB,dB)
link_17.SetDistance(0)
link_17.SetName("Coincident9")
exported_items.append(link_17)

link_18 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.29860788345181,0.0779999999999561,-2.35260728963121)
dA = chrono.ChVectorD(0.407284869470426,-6.66654231817887e-15,-0.913301174367174)
cB = chrono.ChVectorD(2.46053617960536,0.078000000506861,-2.28039568409374)
dB = chrono.ChVectorD(0.407284869470426,2.58473797920544e-16,-0.913301174367174)
link_18.Initialize(body_26,body_30,False,cA,cB,dA,dB)
link_18.SetName("Coincident9")
exported_items.append(link_18)


# Mate constraint: Coincident10 [MateCoincident] type:0 align:0 flip:False
#   Entity 0: C::E name: body_26 , SW name: robo_leg_link-8/link1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_29 , SW name: robo_leg_link-8/link2-1 ,  SW ref.type:2 (2)

link_19 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(2.29860788345181,0.0779999999999561,-2.35260728963121)
cB = chrono.ChVectorD(2.24481444222414,0.0110000003814756,-2.37659637026073)
dA = chrono.ChVectorD(0.407284869470426,-6.66654231817887e-15,-0.913301174367174)
dB = chrono.ChVectorD(0.407284869470426,1.01481323344643e-15,-0.913301174367174)
link_19.Initialize(body_26,body_29,False,cA,cB,dB)
link_19.SetDistance(0)
link_19.SetName("Coincident10")
exported_items.append(link_19)

link_20 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.29860788345181,0.0779999999999561,-2.35260728963121)
dA = chrono.ChVectorD(0.407284869470426,-6.66654231817887e-15,-0.913301174367174)
cB = chrono.ChVectorD(2.24481444222414,0.0110000003814756,-2.37659637026073)
dB = chrono.ChVectorD(0.407284869470426,1.01481323344643e-15,-0.913301174367174)
link_20.Initialize(body_26,body_29,False,cA,cB,dA,dB)
link_20.SetName("Coincident10")
exported_items.append(link_20)


# Mate constraint: Coincident11 [MateCoincident] type:0 align:0 flip:False
#   Entity 0: C::E name: body_26 , SW name: robo_leg_link-8/link1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_29 , SW name: robo_leg_link-8/link2-1 ,  SW ref.type:2 (2)

link_21 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(2.25869396624371,0.0779999999999568,-2.26310377454323)
cB = chrono.ChVectorD(2.20490052501604,0.0110000003814756,-2.28709285517275)
dA = chrono.ChVectorD(-0.407284869470426,7.37344213463942e-15,0.913301174367174)
dB = chrono.ChVectorD(-0.407284869470426,-2.62810606610486e-16,0.913301174367174)
link_21.Initialize(body_26,body_29,False,cA,cB,dB)
link_21.SetDistance(0)
link_21.SetName("Coincident11")
exported_items.append(link_21)

link_22 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.25869396624371,0.0779999999999568,-2.26310377454323)
dA = chrono.ChVectorD(-0.407284869470426,7.37344213463942e-15,0.913301174367174)
cB = chrono.ChVectorD(2.20490052501604,0.0110000003814756,-2.28709285517275)
dB = chrono.ChVectorD(-0.407284869470426,-2.62810606610486e-16,0.913301174367174)
link_22.Initialize(body_26,body_29,False,cA,cB,dA,dB)
link_22.SetName("Coincident11")
exported_items.append(link_22)


# Mate constraint: Coincident12 [MateCoincident] type:0 align:0 flip:False
#   Entity 0: C::E name: body_26 , SW name: robo_leg_link-8/link1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_30 , SW name: robo_leg_link-8/link2-2 ,  SW ref.type:2 (2)

link_23 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(2.25869396624371,0.0779999999999568,-2.26310377454323)
cB = chrono.ChVectorD(2.42062226239726,0.0780000005068609,-2.19089216900575)
dA = chrono.ChVectorD(-0.407284869470426,7.37344213463942e-15,0.913301174367174)
dB = chrono.ChVectorD(-0.407284869470426,-1.01221114823247e-15,0.913301174367174)
link_23.Initialize(body_26,body_30,False,cA,cB,dB)
link_23.SetDistance(0)
link_23.SetName("Coincident12")
exported_items.append(link_23)

link_24 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.25869396624371,0.0779999999999568,-2.26310377454323)
dA = chrono.ChVectorD(-0.407284869470426,7.37344213463942e-15,0.913301174367174)
cB = chrono.ChVectorD(2.42062226239726,0.0780000005068609,-2.19089216900575)
dB = chrono.ChVectorD(-0.407284869470426,-1.01221114823247e-15,0.913301174367174)
link_24.Initialize(body_26,body_30,False,cA,cB,dA,dB)
link_24.SetName("Coincident12")
exported_items.append(link_24)


# Mate constraint: Concentric2 [MateConcentric] type:1 align:1 flip:False
#   Entity 0: C::E name: body_31 , SW name: robo_leg_link-11/leg-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_33 , SW name: robo_leg_link-11/single_bot1-1 ,  SW ref.type:2 (2)

link_1 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.47867323555018,0.0169999946430885,-1.90632229272711)
dA = chrono.ChVectorD(-0.173313610400243,1.33747179997812e-15,0.984866687653732)
cB = chrono.ChVectorD(2.47728672659201,0.0170000040967925,-1.89844335923905)
dB = chrono.ChVectorD(0.173313610400253,-1.30451205393456e-15,-0.98486668765373)
link_1.SetFlipped(True)
link_1.Initialize(body_31,body_33,False,cA,cB,dA,dB)
link_1.SetName("Concentric2")
exported_items.append(link_1)

link_2 = chrono.ChLinkMateGeneric()
link_2.SetConstrainedCoords(False, True, True, False, False, False)
cA = chrono.ChVectorD(2.47867323555018,0.0169999946430885,-1.90632229272711)
cB = chrono.ChVectorD(2.47728672659201,0.0170000040967925,-1.89844335923905)
dA = chrono.ChVectorD(-0.173313610400243,1.33747179997812e-15,0.984866687653732)
dB = chrono.ChVectorD(0.173313610400253,-1.30451205393456e-15,-0.98486668765373)
link_2.Initialize(body_31,body_33,False,cA,cB,dA,dB)
link_2.SetName("Concentric2")
exported_items.append(link_2)


# Mate constraint: Coincident4 [MateCoincident] type:0 align:1 flip:False
#   Entity 0: C::E name: body_33 , SW name: robo_leg_link-11/single_bot1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_34 , SW name: robo_leg_link-11/link1-1 ,  SW ref.type:2 (2)

link_3 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(2.49866671402549,0.0110000040967929,-1.86654417612499)
cB = chrono.ChVectorD(2.4754684693122,0.0110000040967138,-1.89965075994486)
dA = chrono.ChVectorD(-7.02216063075412e-15,1,-6.32133234645949e-15)
dB = chrono.ChVectorD(-5.70377078901174e-15,-1,3.11486947346395e-14)
link_3.Initialize(body_33,body_34,False,cA,cB,dB)
link_3.SetDistance(0)
link_3.SetName("Coincident4")
exported_items.append(link_3)

link_4 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.49866671402549,0.0110000040967929,-1.86654417612499)
dA = chrono.ChVectorD(-7.02216063075412e-15,1,-6.32133234645949e-15)
cB = chrono.ChVectorD(2.4754684693122,0.0110000040967138,-1.89965075994486)
dB = chrono.ChVectorD(-5.70377078901174e-15,-1,3.11486947346395e-14)
link_4.SetFlipped(True)
link_4.Initialize(body_33,body_34,False,cA,cB,dA,dB)
link_4.SetName("Coincident4")
exported_items.append(link_4)


# Mate constraint: Parallel3 [MateParallel] type:3 align:1 flip:False
#   Entity 0: C::E name: body_33 , SW name: robo_leg_link-11/single_bot1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_34 , SW name: robo_leg_link-11/link1-1 ,  SW ref.type:2 (2)

link_5 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.49532374631408,0.0633100040968127,-1.88048781538994)
dA = chrono.ChVectorD(0.984866687653731,7.66053886991358e-15,0.173313610400243)
cB = chrono.ChVectorD(2.44555356288372,0.0212000040967144,-1.88671493246247)
dB = chrono.ChVectorD(-0.984866687653732,2.08166817117217e-16,-0.173313610400242)
link_5.SetFlipped(True)
link_5.Initialize(body_33,body_34,False,cA,cB,dA,dB)
link_5.SetName("Parallel3")
exported_items.append(link_5)


# Mate constraint: Coincident7 [MateCoincident] type:0 align:1 flip:False
#   Entity 0: C::E name: body_31 , SW name: robo_leg_link-11/leg-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_33 , SW name: robo_leg_link-11/single_bot1-1 ,  SW ref.type:2 (2)

link_6 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(2.47728672666698,0.0169999946430885,-1.89844335922588)
cB = chrono.ChVectorD(2.47925744483401,0.0149990040967925,-1.89809655870464)
dA = chrono.ChVectorD(-0.173313610400243,1.33747179997812e-15,0.984866687653732)
dB = chrono.ChVectorD(0.173313610400253,-1.30451205393456e-15,-0.98486668765373)
link_6.Initialize(body_31,body_33,False,cA,cB,dB)
link_6.SetDistance(0)
link_6.SetName("Coincident7")
exported_items.append(link_6)

link_7 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.47728672666698,0.0169999946430885,-1.89844335922588)
dA = chrono.ChVectorD(-0.173313610400243,1.33747179997812e-15,0.984866687653732)
cB = chrono.ChVectorD(2.47925744483401,0.0149990040967925,-1.89809655870464)
dB = chrono.ChVectorD(0.173313610400253,-1.30451205393456e-15,-0.98486668765373)
link_7.SetFlipped(True)
link_7.Initialize(body_31,body_33,False,cA,cB,dA,dB)
link_7.SetName("Coincident7")
exported_items.append(link_7)


# Mate constraint: Concentric4 [MateConcentric] type:1 align:1 flip:False
#   Entity 0: C::E name: body_33 , SW name: robo_leg_link-11/single_bot1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_34 , SW name: robo_leg_link-11/link1-1 ,  SW ref.type:2 (2)

link_8 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.4754684693122,0.0789500040968231,-1.89965075994487)
dA = chrono.ChVectorD(-5.70377078901174e-15,-1,3.11556336285435e-14)
cB = chrono.ChVectorD(2.4754684693122,0.0110000040967138,-1.89965075994486)
dB = chrono.ChVectorD(5.70377078901174e-15,1,-3.11486947346395e-14)
link_8.SetFlipped(True)
link_8.Initialize(body_33,body_34,False,cA,cB,dA,dB)
link_8.SetName("Concentric4")
exported_items.append(link_8)

link_9 = chrono.ChLinkMateGeneric()
link_9.SetConstrainedCoords(False, True, True, False, False, False)
cA = chrono.ChVectorD(2.4754684693122,0.0789500040968231,-1.89965075994487)
cB = chrono.ChVectorD(2.4754684693122,0.0110000040967138,-1.89965075994486)
dA = chrono.ChVectorD(-5.70377078901174e-15,-1,3.11556336285435e-14)
dB = chrono.ChVectorD(5.70377078901174e-15,1,-3.11486947346395e-14)
link_9.Initialize(body_33,body_34,False,cA,cB,dA,dB)
link_9.SetName("Concentric4")
exported_items.append(link_9)


# Mate constraint: Parallel4 [MateParallel] type:3 align:1 flip:False
#   Entity 0: C::E name: body_33 , SW name: robo_leg_link-11/single_bot1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_34 , SW name: robo_leg_link-11/link1-1 ,  SW ref.type:2 (2)

link_10 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.494862375252,0.0789500040968242,-1.86246277650266)
dA = chrono.ChVectorD(-5.70377078901174e-15,-1,3.11556336285435e-14)
cB = chrono.ChVectorD(2.4754684693122,0.0780000040967138,-1.89965075994486)
dB = chrono.ChVectorD(5.70377078901174e-15,1,-3.11486947346395e-14)
link_10.SetFlipped(True)
link_10.Initialize(body_33,body_34,False,cA,cB,dA,dB)
link_10.SetName("Parallel4")
exported_items.append(link_10)


# Mate constraint: Concentric5 [MateConcentric] type:1 align:0 flip:False
#   Entity 0: C::E name: body_33 , SW name: robo_leg_link-11/single_bot1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_34 , SW name: robo_leg_link-11/link1-1 ,  SW ref.type:2 (2)

link_11 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.4754684693122,0.0110000040967926,-1.89965075994486)
dA = chrono.ChVectorD(-7.02216063075412e-15,1,-6.32133234645949e-15)
cB = chrono.ChVectorD(2.4754684693122,0.0110000040967138,-1.89965075994486)
dB = chrono.ChVectorD(5.70377078901174e-15,1,-3.11486947346395e-14)
link_11.Initialize(body_33,body_34,False,cA,cB,dA,dB)
link_11.SetName("Concentric5")
exported_items.append(link_11)

link_12 = chrono.ChLinkMateGeneric()
link_12.SetConstrainedCoords(False, True, True, False, False, False)
cA = chrono.ChVectorD(2.4754684693122,0.0110000040967926,-1.89965075994486)
cB = chrono.ChVectorD(2.4754684693122,0.0110000040967138,-1.89965075994486)
dA = chrono.ChVectorD(-7.02216063075412e-15,1,-6.32133234645949e-15)
dB = chrono.ChVectorD(5.70377078901174e-15,1,-3.11486947346395e-14)
link_12.Initialize(body_33,body_34,False,cA,cB,dA,dB)
link_12.SetName("Concentric5")
exported_items.append(link_12)


# Mate constraint: Concentric6 [MateConcentric] type:1 align:0 flip:False
#   Entity 0: C::E name: body_34 , SW name: robo_leg_link-11/link1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_32 , SW name: robo_leg_link-11/link2-2 ,  SW ref.type:2 (2)

link_13 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.40374834463241,0.0445000040967153,-1.86713882035467)
dA = chrono.ChVectorD(0.984866687653732,-2.08166817117217e-16,0.173313610400242)
cB = chrono.ChVectorD(2.38503587903433,0.0445000002246145,-1.87043177598131)
dB = chrono.ChVectorD(0.984866687653732,-9.99200722162641e-16,0.173313610400243)
link_13.Initialize(body_34,body_32,False,cA,cB,dA,dB)
link_13.SetName("Concentric6")
exported_items.append(link_13)

link_14 = chrono.ChLinkMateGeneric()
link_14.SetConstrainedCoords(False, True, True, False, False, False)
cA = chrono.ChVectorD(2.40374834463241,0.0445000040967153,-1.86713882035467)
cB = chrono.ChVectorD(2.38503587903433,0.0445000002246145,-1.87043177598131)
dA = chrono.ChVectorD(0.984866687653732,-2.08166817117217e-16,0.173313610400242)
dB = chrono.ChVectorD(0.984866687653732,-9.99200722162641e-16,0.173313610400243)
link_14.Initialize(body_34,body_32,False,cA,cB,dA,dB)
link_14.SetName("Concentric6")
exported_items.append(link_14)


# Mate constraint: Concentric7 [MateConcentric] type:1 align:0 flip:False
#   Entity 0: C::E name: body_34 , SW name: robo_leg_link-11/link1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_35 , SW name: robo_leg_link-11/link2-1 ,  SW ref.type:2 (2)

link_15 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.419155924597,0.0445000040967125,-1.95469346888709)
dA = chrono.ChVectorD(0.984866687653732,-2.08166817117217e-16,0.173313610400242)
cB = chrono.ChVectorD(2.40044345899803,0.0445000008644015,-1.95798642450872)
dB = chrono.ChVectorD(0.984866687653732,-2.08166817117217e-16,0.173313610400242)
link_15.Initialize(body_34,body_35,False,cA,cB,dA,dB)
link_15.SetName("Concentric7")
exported_items.append(link_15)

link_16 = chrono.ChLinkMateGeneric()
link_16.SetConstrainedCoords(False, True, True, False, False, False)
cA = chrono.ChVectorD(2.419155924597,0.0445000040967125,-1.95469346888709)
cB = chrono.ChVectorD(2.40044345899803,0.0445000008644015,-1.95798642450872)
dA = chrono.ChVectorD(0.984866687653732,-2.08166817117217e-16,0.173313610400242)
dB = chrono.ChVectorD(0.984866687653732,-2.08166817117217e-16,0.173313610400242)
link_16.Initialize(body_34,body_35,False,cA,cB,dA,dB)
link_16.SetName("Concentric7")
exported_items.append(link_16)


# Mate constraint: Coincident9 [MateCoincident] type:0 align:0 flip:False
#   Entity 0: C::E name: body_34 , SW name: robo_leg_link-11/link1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_32 , SW name: robo_leg_link-11/link2-2 ,  SW ref.type:2 (2)

link_17 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(2.53398710274292,0.078000004096712,-1.94946250094435)
cB = chrono.ChVectorD(2.5032586010863,0.0780000002246145,-1.77484563425238)
dA = chrono.ChVectorD(0.984866687653732,4.9960036108132e-16,0.173313610400242)
dB = chrono.ChVectorD(0.984866687653732,-9.99200722162641e-16,0.173313610400243)
link_17.Initialize(body_34,body_32,False,cA,cB,dB)
link_17.SetDistance(0)
link_17.SetName("Coincident9")
exported_items.append(link_17)

link_18 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.53398710274292,0.078000004096712,-1.94946250094435)
dA = chrono.ChVectorD(0.984866687653732,4.9960036108132e-16,0.173313610400242)
cB = chrono.ChVectorD(2.5032586010863,0.0780000002246145,-1.77484563425238)
dB = chrono.ChVectorD(0.984866687653732,-9.99200722162641e-16,0.173313610400243)
link_18.Initialize(body_34,body_32,False,cA,cB,dA,dB)
link_18.SetName("Coincident9")
exported_items.append(link_18)


# Mate constraint: Coincident10 [MateCoincident] type:0 align:0 flip:False
#   Entity 0: C::E name: body_34 , SW name: robo_leg_link-11/link1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_35 , SW name: robo_leg_link-11/link2-1 ,  SW ref.type:2 (2)

link_19 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(2.53398710274292,0.078000004096712,-1.94946250094435)
cB = chrono.ChVectorD(2.54419527586195,0.0110000008644013,-2.00747114587119)
dA = chrono.ChVectorD(0.984866687653732,4.9960036108132e-16,0.173313610400242)
dB = chrono.ChVectorD(0.984866687653732,-2.08166817117217e-16,0.173313610400242)
link_19.Initialize(body_34,body_35,False,cA,cB,dB)
link_19.SetDistance(0)
link_19.SetName("Coincident10")
exported_items.append(link_19)

link_20 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.53398710274292,0.078000004096712,-1.94946250094435)
dA = chrono.ChVectorD(0.984866687653732,4.9960036108132e-16,0.173313610400242)
cB = chrono.ChVectorD(2.54419527586195,0.0110000008644013,-2.00747114587119)
dB = chrono.ChVectorD(0.984866687653732,-2.08166817117217e-16,0.173313610400242)
link_20.Initialize(body_34,body_35,False,cA,cB,dA,dB)
link_20.SetName("Coincident10")
exported_items.append(link_20)


# Mate constraint: Coincident11 [MateCoincident] type:0 align:0 flip:False
#   Entity 0: C::E name: body_34 , SW name: robo_leg_link-11/link1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_35 , SW name: robo_leg_link-11/link2-1 ,  SW ref.type:2 (2)

link_21 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(2.43747016735286,0.078000004096712,-1.96644723476358)
cB = chrono.ChVectorD(2.44767834047189,0.0110000008644014,-2.02445587969041)
dA = chrono.ChVectorD(-0.984866687653732,2.08166817117217e-16,-0.173313610400242)
dB = chrono.ChVectorD(-0.984866687653732,9.71445146547012e-16,-0.173313610400242)
link_21.Initialize(body_34,body_35,False,cA,cB,dB)
link_21.SetDistance(0)
link_21.SetName("Coincident11")
exported_items.append(link_21)

link_22 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.43747016735286,0.078000004096712,-1.96644723476358)
dA = chrono.ChVectorD(-0.984866687653732,2.08166817117217e-16,-0.173313610400242)
cB = chrono.ChVectorD(2.44767834047189,0.0110000008644014,-2.02445587969041)
dB = chrono.ChVectorD(-0.984866687653732,9.71445146547012e-16,-0.173313610400242)
link_22.Initialize(body_34,body_35,False,cA,cB,dA,dB)
link_22.SetName("Coincident11")
exported_items.append(link_22)


# Mate constraint: Coincident12 [MateCoincident] type:0 align:0 flip:False
#   Entity 0: C::E name: body_34 , SW name: robo_leg_link-11/link1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_32 , SW name: robo_leg_link-11/link2-2 ,  SW ref.type:2 (2)

link_23 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(2.43747016735286,0.078000004096712,-1.96644723476358)
cB = chrono.ChVectorD(2.40674166569623,0.0780000002246145,-1.7918303680716)
dA = chrono.ChVectorD(-0.984866687653732,2.08166817117217e-16,-0.173313610400242)
dB = chrono.ChVectorD(-0.984866687653732,2.35922392732846e-16,-0.173313610400243)
link_23.Initialize(body_34,body_32,False,cA,cB,dB)
link_23.SetDistance(0)
link_23.SetName("Coincident12")
exported_items.append(link_23)

link_24 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.43747016735286,0.078000004096712,-1.96644723476358)
dA = chrono.ChVectorD(-0.984866687653732,2.08166817117217e-16,-0.173313610400242)
cB = chrono.ChVectorD(2.40674166569623,0.0780000002246145,-1.7918303680716)
dB = chrono.ChVectorD(-0.984866687653732,2.35922392732846e-16,-0.173313610400243)
link_24.Initialize(body_34,body_32,False,cA,cB,dA,dB)
link_24.SetName("Coincident12")
exported_items.append(link_24)


# Mate constraint: Concentric2 [MateConcentric] type:1 align:1 flip:False
#   Entity 0: C::E name: body_37 , SW name: robo_leg_link-4/leg-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_36 , SW name: robo_leg_link-4/single_bot1-1 ,  SW ref.type:2 (2)

link_1 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(1.93904929214279,0.0170000000000053,-2.19773747987118)
dA = chrono.ChVectorD(-0.111417885750496,3.72965547335014e-16,-0.9937736436105)
cB = chrono.ChVectorD(1.93815794905679,0.0170000000000052,-2.20568766902007)
dB = chrono.ChVectorD(0.111417885750485,3.40612954508046e-15,0.993773643610501)
link_1.SetFlipped(True)
link_1.Initialize(body_37,body_36,False,cA,cB,dA,dB)
link_1.SetName("Concentric2")
exported_items.append(link_1)

link_2 = chrono.ChLinkMateGeneric()
link_2.SetConstrainedCoords(False, True, True, False, False, False)
cA = chrono.ChVectorD(1.93904929214279,0.0170000000000053,-2.19773747987118)
cB = chrono.ChVectorD(1.93815794905679,0.0170000000000052,-2.20568766902007)
dA = chrono.ChVectorD(-0.111417885750496,3.72965547335014e-16,-0.9937736436105)
dB = chrono.ChVectorD(0.111417885750485,3.40612954508046e-15,0.993773643610501)
link_2.Initialize(body_37,body_36,False,cA,cB,dA,dB)
link_2.SetName("Concentric2")
exported_items.append(link_2)


# Mate constraint: Coincident4 [MateCoincident] type:0 align:1 flip:False
#   Entity 0: C::E name: body_36 , SW name: robo_leg_link-4/single_bot1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_40 , SW name: robo_leg_link-4/link1-1 ,  SW ref.type:2 (2)

link_3 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(1.90865097570937,0.0110000000000053,-2.23026408596548)
cB = chrono.ChVectorD(1.94024287557615,0.0110000000000054,-2.20504194662705)
dA = chrono.ChVectorD(5.55111512312578e-17,1,2.98372437868011e-16)
dB = chrono.ChVectorD(5.10702591327572e-15,-1,-2.77139422522055e-14)
link_3.Initialize(body_36,body_40,False,cA,cB,dB)
link_3.SetDistance(0)
link_3.SetName("Coincident4")
exported_items.append(link_3)

link_4 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(1.90865097570937,0.0110000000000053,-2.23026408596548)
dA = chrono.ChVectorD(5.55111512312578e-17,1,2.98372437868011e-16)
cB = chrono.ChVectorD(1.94024287557615,0.0110000000000054,-2.20504194662705)
dB = chrono.ChVectorD(5.10702591327572e-15,-1,-2.77139422522055e-14)
link_4.SetFlipped(True)
link_4.Initialize(body_36,body_40,False,cA,cB,dA,dB)
link_4.SetName("Coincident4")
exported_items.append(link_4)


# Mate constraint: Parallel3 [MateParallel] type:3 align:1 flip:False
#   Entity 0: C::E name: body_36 , SW name: robo_leg_link-4/single_bot1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_40 , SW name: robo_leg_link-4/link1-1 ,  SW ref.type:2 (2)

link_5 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(1.91578993607945,0.0633100000000251,-2.21782882344694)
dA = chrono.ChVectorD(-0.9937736436105,-2.77555756156289e-16,0.111417885750496)
cB = chrono.ChVectorD(1.96529649742923,0.0212000000000061,-2.22588788847005)
dB = chrono.ChVectorD(0.9937736436105,8.13238365537927e-15,-0.111417885750496)
link_5.SetFlipped(True)
link_5.Initialize(body_36,body_40,False,cA,cB,dA,dB)
link_5.SetName("Parallel3")
exported_items.append(link_5)


# Mate constraint: Coincident7 [MateCoincident] type:0 align:1 flip:False
#   Entity 0: C::E name: body_37 , SW name: robo_leg_link-4/leg-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_36 , SW name: robo_leg_link-4/single_bot1-1 ,  SW ref.type:2 (2)

link_6 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(1.93815794905679,0.0170000000000053,-2.20568766902007)
cB = chrono.ChVectorD(1.93616940799592,0.0149990000000052,-2.20546472183068)
dA = chrono.ChVectorD(-0.111417885750496,3.72965547335014e-16,-0.9937736436105)
dB = chrono.ChVectorD(0.111417885750485,3.40612954508046e-15,0.993773643610501)
link_6.Initialize(body_37,body_36,False,cA,cB,dB)
link_6.SetDistance(0)
link_6.SetName("Coincident7")
exported_items.append(link_6)

link_7 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(1.93815794905679,0.0170000000000053,-2.20568766902007)
dA = chrono.ChVectorD(-0.111417885750496,3.72965547335014e-16,-0.9937736436105)
cB = chrono.ChVectorD(1.93616940799592,0.0149990000000052,-2.20546472183068)
dB = chrono.ChVectorD(0.111417885750485,3.40612954508046e-15,0.993773643610501)
link_7.SetFlipped(True)
link_7.Initialize(body_37,body_36,False,cA,cB,dA,dB)
link_7.SetName("Coincident7")
exported_items.append(link_7)


# Mate constraint: Concentric4 [MateConcentric] type:1 align:1 flip:False
#   Entity 0: C::E name: body_36 , SW name: robo_leg_link-4/single_bot1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_40 , SW name: robo_leg_link-4/link1-1 ,  SW ref.type:2 (2)

link_8 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(1.94024287557615,0.0789500000000358,-2.20504194662705)
dA = chrono.ChVectorD(5.16253706450698e-15,-1,-2.77017991878736e-14)
cB = chrono.ChVectorD(1.94024287557615,0.0110000000000054,-2.20504194662705)
dB = chrono.ChVectorD(-5.10702591327572e-15,1,2.77139422522055e-14)
link_8.SetFlipped(True)
link_8.Initialize(body_36,body_40,False,cA,cB,dA,dB)
link_8.SetName("Concentric4")
exported_items.append(link_8)

link_9 = chrono.ChLinkMateGeneric()
link_9.SetConstrainedCoords(False, True, True, False, False, False)
cA = chrono.ChVectorD(1.94024287557615,0.0789500000000358,-2.20504194662705)
cB = chrono.ChVectorD(1.94024287557615,0.0110000000000054,-2.20504194662705)
dA = chrono.ChVectorD(5.16253706450698e-15,-1,-2.77017991878736e-14)
dB = chrono.ChVectorD(-5.10702591327572e-15,1,2.77139422522055e-14)
link_9.Initialize(body_36,body_40,False,cA,cB,dA,dB)
link_9.SetName("Concentric4")
exported_items.append(link_9)


# Mate constraint: Parallel4 [MateParallel] type:3 align:1 flip:False
#   Entity 0: C::E name: body_36 , SW name: robo_leg_link-4/single_bot1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_40 , SW name: robo_leg_link-4/link1-1 ,  SW ref.type:2 (2)

link_10 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(1.91115013385655,0.0789500000000365,-2.23525257520932)
dA = chrono.ChVectorD(5.16253706450698e-15,-1,-2.77017991878736e-14)
cB = chrono.ChVectorD(1.94024287557615,0.0780000000000054,-2.20504194662704)
dB = chrono.ChVectorD(-5.10702591327572e-15,1,2.77139422522055e-14)
link_10.SetFlipped(True)
link_10.Initialize(body_36,body_40,False,cA,cB,dA,dB)
link_10.SetName("Parallel4")
exported_items.append(link_10)


# Mate constraint: Concentric5 [MateConcentric] type:1 align:0 flip:False
#   Entity 0: C::E name: body_36 , SW name: robo_leg_link-4/single_bot1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_40 , SW name: robo_leg_link-4/link1-1 ,  SW ref.type:2 (2)

link_11 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(1.94024287557615,0.0110000000000053,-2.20504194662705)
dA = chrono.ChVectorD(5.55111512312578e-17,1,2.98372437868011e-16)
cB = chrono.ChVectorD(1.94024287557615,0.0110000000000054,-2.20504194662705)
dB = chrono.ChVectorD(-5.10702591327572e-15,1,2.77139422522055e-14)
link_11.Initialize(body_36,body_40,False,cA,cB,dA,dB)
link_11.SetName("Concentric5")
exported_items.append(link_11)

link_12 = chrono.ChLinkMateGeneric()
link_12.SetConstrainedCoords(False, True, True, False, False, False)
cA = chrono.ChVectorD(1.94024287557615,0.0110000000000053,-2.20504194662705)
cB = chrono.ChVectorD(1.94024287557615,0.0110000000000054,-2.20504194662705)
dA = chrono.ChVectorD(5.55111512312578e-17,1,2.98372437868011e-16)
dB = chrono.ChVectorD(-5.10702591327572e-15,1,2.77139422522055e-14)
link_12.Initialize(body_36,body_40,False,cA,cB,dA,dB)
link_12.SetName("Concentric5")
exported_items.append(link_12)


# Mate constraint: Concentric6 [MateConcentric] type:1 align:0 flip:False
#   Entity 0: C::E name: body_40 , SW name: robo_leg_link-4/link1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_39 , SW name: robo_leg_link-4/link2-2 ,  SW ref.type:2 (2)

link_13 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(1.99988563738923,0.0445000000000071,-2.25645734765931)
dA = chrono.ChVectorD(-0.9937736436105,-8.13238365537927e-15,0.111417885750496)
cB = chrono.ChVectorD(2.01876733661783,0.0445000000000073,-2.25857428748857)
dB = chrono.ChVectorD(-0.9937736436105,6.93889390390723e-17,0.111417885750496)
link_13.Initialize(body_40,body_39,False,cA,cB,dA,dB)
link_13.SetName("Concentric6")
exported_items.append(link_13)

link_14 = chrono.ChLinkMateGeneric()
link_14.SetConstrainedCoords(False, True, True, False, False, False)
cA = chrono.ChVectorD(1.99988563738923,0.0445000000000071,-2.25645734765931)
cB = chrono.ChVectorD(2.01876733661783,0.0445000000000073,-2.25857428748857)
dA = chrono.ChVectorD(-0.9937736436105,-8.13238365537927e-15,0.111417885750496)
dB = chrono.ChVectorD(-0.9937736436105,6.93889390390723e-17,0.111417885750496)
link_14.Initialize(body_40,body_39,False,cA,cB,dA,dB)
link_14.SetName("Concentric6")
exported_items.append(link_14)


# Mate constraint: Concentric7 [MateConcentric] type:1 align:0 flip:False
#   Entity 0: C::E name: body_40 , SW name: robo_leg_link-4/link1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_38 , SW name: robo_leg_link-4/link2-1 ,  SW ref.type:2 (2)

link_15 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.00979068743245,0.0445000000000047,-2.16811087074234)
dA = chrono.ChVectorD(-0.9937736436105,-8.13238365537927e-15,0.111417885750496)
cB = chrono.ChVectorD(2.02867238666105,0.0445000000000048,-2.1702278105716)
dB = chrono.ChVectorD(-0.9937736436105,6.93889390390723e-17,0.111417885750496)
link_15.Initialize(body_40,body_38,False,cA,cB,dA,dB)
link_15.SetName("Concentric7")
exported_items.append(link_15)

link_16 = chrono.ChLinkMateGeneric()
link_16.SetConstrainedCoords(False, True, True, False, False, False)
cA = chrono.ChVectorD(2.00979068743245,0.0445000000000047,-2.16811087074234)
cB = chrono.ChVectorD(2.02867238666105,0.0445000000000048,-2.1702278105716)
dA = chrono.ChVectorD(-0.9937736436105,-8.13238365537927e-15,0.111417885750496)
dB = chrono.ChVectorD(-0.9937736436105,6.93889390390723e-17,0.111417885750496)
link_16.Initialize(body_40,body_38,False,cA,cB,dA,dB)
link_16.SetName("Concentric7")
exported_items.append(link_16)


# Mate constraint: Coincident9 [MateCoincident] type:0 align:0 flip:False
#   Entity 0: C::E name: body_40 , SW name: robo_leg_link-4/link1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_39 , SW name: robo_leg_link-4/link2-2 ,  SW ref.type:2 (2)

link_17 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(1.89814390587567,0.0780000000000034,-2.14075107052353)
cB = chrono.ChVectorD(1.87838951473211,0.0780000000000073,-2.31694713753567)
dA = chrono.ChVectorD(-0.9937736436105,-7.42461647718073e-15,0.111417885750496)
dB = chrono.ChVectorD(-0.9937736436105,6.93889390390723e-17,0.111417885750496)
link_17.Initialize(body_40,body_39,False,cA,cB,dB)
link_17.SetDistance(0)
link_17.SetName("Coincident9")
exported_items.append(link_17)

link_18 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(1.89814390587567,0.0780000000000034,-2.14075107052353)
dA = chrono.ChVectorD(-0.9937736436105,-7.42461647718073e-15,0.111417885750496)
cB = chrono.ChVectorD(1.87838951473211,0.0780000000000073,-2.31694713753567)
dB = chrono.ChVectorD(-0.9937736436105,6.93889390390723e-17,0.111417885750496)
link_18.Initialize(body_40,body_39,False,cA,cB,dA,dB)
link_18.SetName("Coincident9")
exported_items.append(link_18)


# Mate constraint: Coincident10 [MateCoincident] type:0 align:0 flip:False
#   Entity 0: C::E name: body_40 , SW name: robo_leg_link-4/link1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_38 , SW name: robo_leg_link-4/link2-1 ,  SW ref.type:2 (2)

link_19 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(1.89814390587567,0.0780000000000034,-2.14075107052353)
cB = chrono.ChVectorD(1.90470641934637,0.0110000000000048,-2.08221780291487)
dA = chrono.ChVectorD(-0.9937736436105,-7.42461647718073e-15,0.111417885750496)
dB = chrono.ChVectorD(-0.9937736436105,6.93889390390723e-17,0.111417885750496)
link_19.Initialize(body_40,body_38,False,cA,cB,dB)
link_19.SetDistance(0)
link_19.SetName("Coincident10")
exported_items.append(link_19)

link_20 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(1.89814390587567,0.0780000000000034,-2.14075107052353)
dA = chrono.ChVectorD(-0.9937736436105,-7.42461647718073e-15,0.111417885750496)
cB = chrono.ChVectorD(1.90470641934637,0.0110000000000048,-2.08221780291487)
dB = chrono.ChVectorD(-0.9937736436105,6.93889390390723e-17,0.111417885750496)
link_20.Initialize(body_40,body_38,False,cA,cB,dA,dB)
link_20.SetName("Coincident10")
exported_items.append(link_20)


# Mate constraint: Coincident11 [MateCoincident] type:0 align:0 flip:False
#   Entity 0: C::E name: body_40 , SW name: robo_leg_link-4/link1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_38 , SW name: robo_leg_link-4/link2-1 ,  SW ref.type:2 (2)

link_21 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(1.9955337229495,0.0780000000000042,-2.15167002332708)
cB = chrono.ChVectorD(2.0020962364202,0.0110000000000048,-2.09313675571842)
dA = chrono.ChVectorD(0.9937736436105,8.13238365537927e-15,-0.111417885750496)
dB = chrono.ChVectorD(0.9937736436105,6.93889390390723e-16,-0.111417885750496)
link_21.Initialize(body_40,body_38,False,cA,cB,dB)
link_21.SetDistance(0)
link_21.SetName("Coincident11")
exported_items.append(link_21)

link_22 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(1.9955337229495,0.0780000000000042,-2.15167002332708)
dA = chrono.ChVectorD(0.9937736436105,8.13238365537927e-15,-0.111417885750496)
cB = chrono.ChVectorD(2.0020962364202,0.0110000000000048,-2.09313675571842)
dB = chrono.ChVectorD(0.9937736436105,6.93889390390723e-16,-0.111417885750496)
link_22.Initialize(body_40,body_38,False,cA,cB,dA,dB)
link_22.SetName("Coincident11")
exported_items.append(link_22)


# Mate constraint: Coincident12 [MateCoincident] type:0 align:0 flip:False
#   Entity 0: C::E name: body_40 , SW name: robo_leg_link-4/link1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_39 , SW name: robo_leg_link-4/link2-2 ,  SW ref.type:2 (2)

link_23 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(1.9955337229495,0.0780000000000042,-2.15167002332708)
cB = chrono.ChVectorD(1.97577933180594,0.0780000000000073,-2.32786609033922)
dA = chrono.ChVectorD(0.9937736436105,8.13238365537927e-15,-0.111417885750496)
dB = chrono.ChVectorD(0.9937736436105,-8.18789480661053e-16,-0.111417885750496)
link_23.Initialize(body_40,body_39,False,cA,cB,dB)
link_23.SetDistance(0)
link_23.SetName("Coincident12")
exported_items.append(link_23)

link_24 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(1.9955337229495,0.0780000000000042,-2.15167002332708)
dA = chrono.ChVectorD(0.9937736436105,8.13238365537927e-15,-0.111417885750496)
cB = chrono.ChVectorD(1.97577933180594,0.0780000000000073,-2.32786609033922)
dB = chrono.ChVectorD(0.9937736436105,-8.18789480661053e-16,-0.111417885750496)
link_24.Initialize(body_40,body_39,False,cA,cB,dA,dB)
link_24.SetName("Coincident12")
exported_items.append(link_24)


# Mate constraint: Concentric2 [MateConcentric] type:1 align:1 flip:False
#   Entity 0: C::E name: body_41 , SW name: robo_leg_link-12/leg-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_42 , SW name: robo_leg_link-12/single_bot1-1 ,  SW ref.type:2 (2)

link_1 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.39418245194493,0.0169999999999956,-1.83645281661912)
dA = chrono.ChVectorD(-0.979814426163927,9.4542429440736e-16,-0.19990920509335)
cB = chrono.ChVectorD(2.38634393653562,0.0169999999999956,-1.83805209025987)
dB = chrono.ChVectorD(0.979814426163925,2.75647560332715e-15,0.199909205093361)
link_1.SetFlipped(True)
link_1.Initialize(body_41,body_42,False,cA,cB,dA,dB)
link_1.SetName("Concentric2")
exported_items.append(link_1)

link_2 = chrono.ChLinkMateGeneric()
link_2.SetConstrainedCoords(False, True, True, False, False, False)
cA = chrono.ChVectorD(2.39418245194493,0.0169999999999956,-1.83645281661912)
cB = chrono.ChVectorD(2.38634393653562,0.0169999999999956,-1.83805209025987)
dA = chrono.ChVectorD(-0.979814426163927,9.4542429440736e-16,-0.19990920509335)
dB = chrono.ChVectorD(0.979814426163925,2.75647560332715e-15,0.199909205093361)
link_2.Initialize(body_41,body_42,False,cA,cB,dA,dB)
link_2.SetName("Concentric2")
exported_items.append(link_2)


# Mate constraint: Coincident4 [MateCoincident] type:0 align:1 flip:False
#   Entity 0: C::E name: body_42 , SW name: robo_leg_link-12/single_bot1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_45 , SW name: robo_leg_link-12/link1-1 ,  SW ref.type:2 (2)

link_3 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(2.35387771275479,0.0109999999999957,-1.81754340772264)
cB = chrono.ChVectorD(2.38760011275428,0.0109999999999956,-1.83983699846695)
dA = chrono.ChVectorD(1.20910226275583e-15,1,-1.1518563880486e-15)
dB = chrono.ChVectorD(-2.55542115246143e-14,-1,-1.23651089367627e-14)
link_3.Initialize(body_42,body_45,False,cA,cB,dB)
link_3.SetDistance(0)
link_3.SetName("Coincident4")
exported_items.append(link_3)

link_4 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.35387771275479,0.0109999999999957,-1.81754340772264)
dA = chrono.ChVectorD(1.20910226275583e-15,1,-1.1518563880486e-15)
cB = chrono.ChVectorD(2.38760011275428,0.0109999999999956,-1.83983699846695)
dB = chrono.ChVectorD(-2.55542115246143e-14,-1,-1.23651089367627e-14)
link_4.SetFlipped(True)
link_4.Initialize(body_42,body_45,False,cA,cB,dA,dB)
link_4.SetName("Coincident4")
exported_items.append(link_4)


# Mate constraint: Parallel3 [MateParallel] type:3 align:1 flip:False
#   Entity 0: C::E name: body_42 , SW name: robo_leg_link-12/single_bot1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_45 , SW name: robo_leg_link-12/link1-1 ,  SW ref.type:2 (2)

link_5 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.36790673264165,0.0633100000000156,-1.82050771372653)
dA = chrono.ChVectorD(-0.19990920509335,9.99200722162641e-16,0.979814426163927)
cB = chrono.ChVectorD(2.37547878414105,0.0211999999999963,-1.87009109987368)
dB = chrono.ChVectorD(0.19990920509335,6.96664947952286e-15,-0.979814426163927)
link_5.SetFlipped(True)
link_5.Initialize(body_42,body_45,False,cA,cB,dA,dB)
link_5.SetName("Parallel3")
exported_items.append(link_5)


# Mate constraint: Coincident7 [MateCoincident] type:0 align:1 flip:False
#   Entity 0: C::E name: body_41 , SW name: robo_leg_link-12/leg-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_42 , SW name: robo_leg_link-12/single_bot1-1 ,  SW ref.type:2 (2)

link_6 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(2.38634393653562,0.0169999999999956,-1.83805209025987)
cB = chrono.ChVectorD(2.38594391821623,0.0149989999999956,-1.83609148159312)
dA = chrono.ChVectorD(-0.979814426163927,9.4542429440736e-16,-0.19990920509335)
dB = chrono.ChVectorD(0.979814426163925,2.75647560332715e-15,0.199909205093361)
link_6.Initialize(body_41,body_42,False,cA,cB,dB)
link_6.SetDistance(0)
link_6.SetName("Coincident7")
exported_items.append(link_6)

link_7 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.38634393653562,0.0169999999999956,-1.83805209025987)
dA = chrono.ChVectorD(-0.979814426163927,9.4542429440736e-16,-0.19990920509335)
cB = chrono.ChVectorD(2.38594391821623,0.0149989999999956,-1.83609148159312)
dB = chrono.ChVectorD(0.979814426163925,2.75647560332715e-15,0.199909205093361)
link_7.SetFlipped(True)
link_7.Initialize(body_41,body_42,False,cA,cB,dA,dB)
link_7.SetName("Coincident7")
exported_items.append(link_7)


# Mate constraint: Concentric4 [MateConcentric] type:1 align:1 flip:False
#   Entity 0: C::E name: body_42 , SW name: robo_leg_link-12/single_bot1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_45 , SW name: robo_leg_link-12/link1-1 ,  SW ref.type:2 (2)

link_8 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.38760011275428,0.0789500000000262,-1.83983699846694)
dA = chrono.ChVectorD(-2.56634991036009e-14,-1,-1.22124532708767e-14)
cB = chrono.ChVectorD(2.38760011275428,0.0109999999999956,-1.83983699846695)
dB = chrono.ChVectorD(2.55542115246143e-14,1,1.23651089367627e-14)
link_8.SetFlipped(True)
link_8.Initialize(body_42,body_45,False,cA,cB,dA,dB)
link_8.SetName("Concentric4")
exported_items.append(link_8)

link_9 = chrono.ChLinkMateGeneric()
link_9.SetConstrainedCoords(False, True, True, False, False, False)
cA = chrono.ChVectorD(2.38760011275428,0.0789500000000262,-1.83983699846694)
cB = chrono.ChVectorD(2.38760011275428,0.0109999999999956,-1.83983699846695)
dA = chrono.ChVectorD(-2.56634991036009e-14,-1,-1.22124532708767e-14)
dB = chrono.ChVectorD(2.55542115246143e-14,1,1.23651089367627e-14)
link_9.Initialize(body_42,body_45,False,cA,cB,dA,dB)
link_9.SetName("Concentric4")
exported_items.append(link_9)


# Mate constraint: Parallel4 [MateParallel] type:3 align:1 flip:False
#   Entity 0: C::E name: body_42 , SW name: robo_leg_link-12/single_bot1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_45 , SW name: robo_leg_link-12/link1-1 ,  SW ref.type:2 (2)

link_10 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.34990078732203,0.078950000000027,-1.82145683083839)
dA = chrono.ChVectorD(-2.56634991036009e-14,-1,-1.22124532708767e-14)
cB = chrono.ChVectorD(2.38760011275428,0.0779999999999956,-1.83983699846694)
dB = chrono.ChVectorD(2.55542115246143e-14,1,1.23651089367627e-14)
link_10.SetFlipped(True)
link_10.Initialize(body_42,body_45,False,cA,cB,dA,dB)
link_10.SetName("Parallel4")
exported_items.append(link_10)


# Mate constraint: Concentric5 [MateConcentric] type:1 align:0 flip:False
#   Entity 0: C::E name: body_42 , SW name: robo_leg_link-12/single_bot1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_45 , SW name: robo_leg_link-12/link1-1 ,  SW ref.type:2 (2)

link_11 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.38760011275428,0.0109999999999957,-1.83983699846694)
dA = chrono.ChVectorD(1.20910226275583e-15,1,-1.1518563880486e-15)
cB = chrono.ChVectorD(2.38760011275428,0.0109999999999956,-1.83983699846695)
dB = chrono.ChVectorD(2.55542115246143e-14,1,1.23651089367627e-14)
link_11.Initialize(body_42,body_45,False,cA,cB,dA,dB)
link_11.SetName("Concentric5")
exported_items.append(link_11)

link_12 = chrono.ChLinkMateGeneric()
link_12.SetConstrainedCoords(False, True, True, False, False, False)
cA = chrono.ChVectorD(2.38760011275428,0.0109999999999957,-1.83983699846694)
cB = chrono.ChVectorD(2.38760011275428,0.0109999999999956,-1.83983699846695)
dA = chrono.ChVectorD(1.20910226275583e-15,1,-1.1518563880486e-15)
dB = chrono.ChVectorD(2.55542115246143e-14,1,1.23651089367627e-14)
link_12.Initialize(body_42,body_45,False,cA,cB,dA,dB)
link_12.SetName("Concentric5")
exported_items.append(link_12)


# Mate constraint: Concentric6 [MateConcentric] type:1 align:0 flip:False
#   Entity 0: C::E name: body_45 , SW name: robo_leg_link-12/link1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_43 , SW name: robo_leg_link-12/link2-2 ,  SW ref.type:2 (2)

link_13 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.35704145984236,0.0444999999999973,-1.912410900334)
dA = chrono.ChVectorD(-0.19990920509335,-6.96664947952286e-15,0.979814426163927)
cB = chrono.ChVectorD(2.36083973473913,0.0444999999999973,-1.93102737443111)
dB = chrono.ChVectorD(-0.19990920509335,1.36002320516582e-15,0.979814426163927)
link_13.Initialize(body_45,body_43,False,cA,cB,dA,dB)
link_13.SetName("Concentric6")
exported_items.append(link_13)

link_14 = chrono.ChLinkMateGeneric()
link_14.SetConstrainedCoords(False, True, True, False, False, False)
cA = chrono.ChVectorD(2.35704145984236,0.0444999999999973,-1.912410900334)
cB = chrono.ChVectorD(2.36083973473913,0.0444999999999973,-1.93102737443111)
dA = chrono.ChVectorD(-0.19990920509335,-6.96664947952286e-15,0.979814426163927)
dB = chrono.ChVectorD(-0.19990920509335,1.36002320516582e-15,0.979814426163927)
link_14.Initialize(body_45,body_43,False,cA,cB,dA,dB)
link_14.SetName("Concentric6")
exported_items.append(link_14)


# Mate constraint: Concentric7 [MateConcentric] type:1 align:0 flip:False
#   Entity 0: C::E name: body_45 , SW name: robo_leg_link-12/link1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_44 , SW name: robo_leg_link-12/link2-1 ,  SW ref.type:2 (2)

link_15 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.44414696232833,0.0444999999999948,-1.8946389720012)
dA = chrono.ChVectorD(-0.19990920509335,-6.96664947952286e-15,0.979814426163927)
cB = chrono.ChVectorD(2.44794523722511,0.0444999999999948,-1.91325544609831)
dB = chrono.ChVectorD(-0.19990920509335,1.30451205393456e-15,0.979814426163927)
link_15.Initialize(body_45,body_44,False,cA,cB,dA,dB)
link_15.SetName("Concentric7")
exported_items.append(link_15)

link_16 = chrono.ChLinkMateGeneric()
link_16.SetConstrainedCoords(False, True, True, False, False, False)
cA = chrono.ChVectorD(2.44414696232833,0.0444999999999948,-1.8946389720012)
cB = chrono.ChVectorD(2.44794523722511,0.0444999999999948,-1.91325544609831)
dA = chrono.ChVectorD(-0.19990920509335,-6.96664947952286e-15,0.979814426163927)
dB = chrono.ChVectorD(-0.19990920509335,1.30451205393456e-15,0.979814426163927)
link_16.Initialize(body_45,body_44,False,cA,cB,dA,dB)
link_16.SetName("Concentric7")
exported_items.append(link_16)


# Mate constraint: Coincident9 [MateCoincident] type:0 align:0 flip:False
#   Entity 0: C::E name: body_45 , SW name: robo_leg_link-12/link1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_43 , SW name: robo_leg_link-12/link2-2 ,  SW ref.type:2 (2)

link_17 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(2.43580957573361,0.0779999999999937,-1.77999146664339)
cB = chrono.ChVectorD(2.26208847797475,0.0779999999999975,-1.81543536870644)
dA = chrono.ChVectorD(-0.19990920509335,-6.24500451351651e-15,0.979814426163927)
dB = chrono.ChVectorD(-0.19990920509335,1.36002320516582e-15,0.979814426163927)
link_17.Initialize(body_45,body_43,False,cA,cB,dB)
link_17.SetDistance(0)
link_17.SetName("Coincident9")
exported_items.append(link_17)

link_18 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.43580957573361,0.0779999999999937,-1.77999146664339)
dA = chrono.ChVectorD(-0.19990920509335,-6.24500451351651e-15,0.979814426163927)
cB = chrono.ChVectorD(2.26208847797475,0.0779999999999975,-1.81543536870644)
dB = chrono.ChVectorD(-0.19990920509335,1.36002320516582e-15,0.979814426163927)
link_18.Initialize(body_45,body_43,False,cA,cB,dA,dB)
link_18.SetName("Coincident9")
exported_items.append(link_18)


# Mate constraint: Coincident10 [MateCoincident] type:0 align:0 flip:False
#   Entity 0: C::E name: body_45 , SW name: robo_leg_link-12/link1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_44 , SW name: robo_leg_link-12/link2-1 ,  SW ref.type:2 (2)

link_19 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(2.43580957573361,0.0779999999999937,-1.77999146664339)
cB = chrono.ChVectorD(2.49352064543467,0.0109999999999949,-1.76821681446339)
dA = chrono.ChVectorD(-0.19990920509335,-6.24500451351651e-15,0.979814426163927)
dB = chrono.ChVectorD(-0.19990920509335,1.30451205393456e-15,0.979814426163927)
link_19.Initialize(body_45,body_44,False,cA,cB,dB)
link_19.SetDistance(0)
link_19.SetName("Coincident10")
exported_items.append(link_19)

link_20 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.43580957573361,0.0779999999999937,-1.77999146664339)
dA = chrono.ChVectorD(-0.19990920509335,-6.24500451351651e-15,0.979814426163927)
cB = chrono.ChVectorD(2.49352064543467,0.0109999999999949,-1.76821681446339)
dB = chrono.ChVectorD(-0.19990920509335,1.30451205393456e-15,0.979814426163927)
link_20.Initialize(body_45,body_44,False,cA,cB,dA,dB)
link_20.SetName("Coincident10")
exported_items.append(link_20)


# Mate constraint: Coincident11 [MateCoincident] type:0 align:0 flip:False
#   Entity 0: C::E name: body_45 , SW name: robo_leg_link-12/link1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_44 , SW name: robo_leg_link-12/link2-1 ,  SW ref.type:2 (2)

link_21 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(2.45540067783276,0.0779999999999943,-1.87601328040745)
cB = chrono.ChVectorD(2.51311174753381,0.0109999999999948,-1.86423862822745)
dA = chrono.ChVectorD(0.19990920509335,6.96664947952286e-15,-0.979814426163927)
dB = chrono.ChVectorD(0.19990920509335,-5.41233724504764e-16,-0.979814426163927)
link_21.Initialize(body_45,body_44,False,cA,cB,dB)
link_21.SetDistance(0)
link_21.SetName("Coincident11")
exported_items.append(link_21)

link_22 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.45540067783276,0.0779999999999943,-1.87601328040745)
dA = chrono.ChVectorD(0.19990920509335,6.96664947952286e-15,-0.979814426163927)
cB = chrono.ChVectorD(2.51311174753381,0.0109999999999948,-1.86423862822745)
dB = chrono.ChVectorD(0.19990920509335,-5.41233724504764e-16,-0.979814426163927)
link_22.Initialize(body_45,body_44,False,cA,cB,dA,dB)
link_22.SetName("Coincident11")
exported_items.append(link_22)


# Mate constraint: Coincident12 [MateCoincident] type:0 align:0 flip:False
#   Entity 0: C::E name: body_45 , SW name: robo_leg_link-12/link1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_43 , SW name: robo_leg_link-12/link2-2 ,  SW ref.type:2 (2)

link_23 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(2.45540067783276,0.0779999999999943,-1.87601328040745)
cB = chrono.ChVectorD(2.28167958007389,0.0779999999999973,-1.9114571824705)
dA = chrono.ChVectorD(0.19990920509335,6.96664947952286e-15,-0.979814426163927)
dB = chrono.ChVectorD(0.19990920509335,-2.1094237467878e-15,-0.979814426163927)
link_23.Initialize(body_45,body_43,False,cA,cB,dB)
link_23.SetDistance(0)
link_23.SetName("Coincident12")
exported_items.append(link_23)

link_24 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.45540067783276,0.0779999999999943,-1.87601328040745)
dA = chrono.ChVectorD(0.19990920509335,6.96664947952286e-15,-0.979814426163927)
cB = chrono.ChVectorD(2.28167958007389,0.0779999999999973,-1.9114571824705)
dB = chrono.ChVectorD(0.19990920509335,-2.1094237467878e-15,-0.979814426163927)
link_24.Initialize(body_45,body_43,False,cA,cB,dA,dB)
link_24.SetName("Coincident12")
exported_items.append(link_24)


# Mate constraint: Concentric2 [MateConcentric] type:1 align:1 flip:False
#   Entity 0: C::E name: body_48 , SW name: robo_leg_link-9/leg-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_51 , SW name: robo_leg_link-9/single_bot1-1 ,  SW ref.type:2 (2)

link_1 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.44307457137872,0.0169999999999888,-2.1969093163633)
dA = chrono.ChVectorD(0.566265471827638,-3.19189119579733e-16,0.824222916094804)
cB = chrono.ChVectorD(2.44760469515334,0.0169999999999888,-2.19031553303454)
dB = chrono.ChVectorD(-0.566265471827628,3.83026943495679e-15,-0.824222916094811)
link_1.SetFlipped(True)
link_1.Initialize(body_48,body_51,False,cA,cB,dA,dB)
link_1.SetName("Concentric2")
exported_items.append(link_1)

link_2 = chrono.ChLinkMateGeneric()
link_2.SetConstrainedCoords(False, True, True, False, False, False)
cA = chrono.ChVectorD(2.44307457137872,0.0169999999999888,-2.1969093163633)
cB = chrono.ChVectorD(2.44760469515334,0.0169999999999888,-2.19031553303454)
dA = chrono.ChVectorD(0.566265471827638,-3.19189119579733e-16,0.824222916094804)
dB = chrono.ChVectorD(-0.566265471827628,3.83026943495679e-15,-0.824222916094811)
link_2.Initialize(body_48,body_51,False,cA,cB,dA,dB)
link_2.SetName("Concentric2")
exported_items.append(link_2)


# Mate constraint: Coincident4 [MateCoincident] type:0 align:1 flip:False
#   Entity 0: C::E name: body_51 , SW name: robo_leg_link-9/single_bot1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_49 , SW name: robo_leg_link-9/link1-1 ,  SW ref.type:2 (2)

link_3 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(2.48520844386753,0.0109999999999891,-2.18252965937518)
cB = chrono.ChVectorD(2.44546133329877,0.0109999999999889,-2.18990337291956)
dA = chrono.ChVectorD(-6.11576761455623e-15,1,4.37150315946155e-15)
dB = chrono.ChVectorD(1.4432899320127e-14,-1,2.21628271290797e-14)
link_3.Initialize(body_51,body_49,False,cA,cB,dB)
link_3.SetDistance(0)
link_3.SetName("Coincident4")
exported_items.append(link_3)

link_4 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.48520844386753,0.0109999999999891,-2.18252965937518)
dA = chrono.ChVectorD(-6.11576761455623e-15,1,4.37150315946155e-15)
cB = chrono.ChVectorD(2.44546133329877,0.0109999999999889,-2.18990337291956)
dB = chrono.ChVectorD(1.4432899320127e-14,-1,2.21628271290797e-14)
link_4.SetFlipped(True)
link_4.Initialize(body_51,body_49,False,cA,cB,dA,dB)
link_4.SetName("Coincident4")
exported_items.append(link_4)


# Mate constraint: Parallel3 [MateParallel] type:3 align:1 flip:False
#   Entity 0: C::E name: body_51 , SW name: robo_leg_link-9/single_bot1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_49 , SW name: robo_leg_link-9/link1-1 ,  SW ref.type:2 (2)

link_5 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.47305472731236,0.0633100000000089,-2.19013805493318)
dA = chrono.ChVectorD(0.824222916094805,7.17481629664007e-15,-0.566265471827637)
cB = chrono.ChVectorD(2.43317594126727,0.0211999999999893,-2.1597155210266)
dB = chrono.ChVectorD(-0.824222916094805,6.73072708679001e-16,0.566265471827638)
link_5.SetFlipped(True)
link_5.Initialize(body_51,body_49,False,cA,cB,dA,dB)
link_5.SetName("Parallel3")
exported_items.append(link_5)


# Mate constraint: Coincident7 [MateCoincident] type:0 align:1 flip:False
#   Entity 0: C::E name: body_48 , SW name: robo_leg_link-9/leg-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_51 , SW name: robo_leg_link-9/single_bot1-1 ,  SW ref.type:2 (2)

link_6 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(2.44760469515334,0.0169999999999888,-2.19031553303454)
cB = chrono.ChVectorD(2.44925396520844,0.0149989999999889,-2.19144863024367)
dA = chrono.ChVectorD(0.566265471827638,-3.19189119579733e-16,0.824222916094804)
dB = chrono.ChVectorD(-0.566265471827628,3.83026943495679e-15,-0.824222916094811)
link_6.Initialize(body_48,body_51,False,cA,cB,dB)
link_6.SetDistance(0)
link_6.SetName("Coincident7")
exported_items.append(link_6)

link_7 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.44760469515334,0.0169999999999888,-2.19031553303454)
dA = chrono.ChVectorD(0.566265471827638,-3.19189119579733e-16,0.824222916094804)
cB = chrono.ChVectorD(2.44925396520844,0.0149989999999889,-2.19144863024367)
dB = chrono.ChVectorD(-0.566265471827628,3.83026943495679e-15,-0.824222916094811)
link_7.SetFlipped(True)
link_7.Initialize(body_48,body_51,False,cA,cB,dA,dB)
link_7.SetName("Coincident7")
exported_items.append(link_7)


# Mate constraint: Concentric4 [MateConcentric] type:1 align:1 flip:False
#   Entity 0: C::E name: body_51 , SW name: robo_leg_link-9/single_bot1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_49 , SW name: robo_leg_link-9/link1-1 ,  SW ref.type:2 (2)

link_8 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.44546133329877,0.0789500000000194,-2.18990337291956)
dA = chrono.ChVectorD(1.44294298731751e-14,-1,2.22460938559266e-14)
cB = chrono.ChVectorD(2.44546133329877,0.0109999999999889,-2.18990337291956)
dB = chrono.ChVectorD(-1.4432899320127e-14,1,-2.21628271290797e-14)
link_8.SetFlipped(True)
link_8.Initialize(body_51,body_49,False,cA,cB,dA,dB)
link_8.SetName("Concentric4")
exported_items.append(link_8)

link_9 = chrono.ChLinkMateGeneric()
link_9.SetConstrainedCoords(False, True, True, False, False, False)
cA = chrono.ChVectorD(2.44546133329877,0.0789500000000194,-2.18990337291956)
cB = chrono.ChVectorD(2.44546133329877,0.0109999999999889,-2.18990337291956)
dA = chrono.ChVectorD(1.44294298731751e-14,-1,2.22460938559266e-14)
dB = chrono.ChVectorD(-1.4432899320127e-14,1,-2.21628271290797e-14)
link_9.Initialize(body_51,body_49,False,cA,cB,dA,dB)
link_9.SetName("Concentric4")
exported_items.append(link_9)


# Mate constraint: Parallel4 [MateParallel] type:3 align:1 flip:False
#   Entity 0: C::E name: body_51 , SW name: robo_leg_link-9/single_bot1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_49 , SW name: robo_leg_link-9/link1-1 ,  SW ref.type:2 (2)

link_10 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.48535284089783,0.0789500000000203,-2.17695202852703)
dA = chrono.ChVectorD(1.44294298731751e-14,-1,2.22460938559266e-14)
cB = chrono.ChVectorD(2.44546133329877,0.0779999999999889,-2.18990337291956)
dB = chrono.ChVectorD(-1.4432899320127e-14,1,-2.21628271290797e-14)
link_10.SetFlipped(True)
link_10.Initialize(body_51,body_49,False,cA,cB,dA,dB)
link_10.SetName("Parallel4")
exported_items.append(link_10)


# Mate constraint: Concentric5 [MateConcentric] type:1 align:0 flip:False
#   Entity 0: C::E name: body_51 , SW name: robo_leg_link-9/single_bot1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_49 , SW name: robo_leg_link-9/link1-1 ,  SW ref.type:2 (2)

link_11 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.44546133329877,0.0109999999999889,-2.18990337291956)
dA = chrono.ChVectorD(-6.11576761455623e-15,1,4.37150315946155e-15)
cB = chrono.ChVectorD(2.44546133329877,0.0109999999999889,-2.18990337291956)
dB = chrono.ChVectorD(-1.4432899320127e-14,1,-2.21628271290797e-14)
link_11.Initialize(body_51,body_49,False,cA,cB,dA,dB)
link_11.SetName("Concentric5")
exported_items.append(link_11)

link_12 = chrono.ChLinkMateGeneric()
link_12.SetConstrainedCoords(False, True, True, False, False, False)
cA = chrono.ChVectorD(2.44546133329877,0.0109999999999889,-2.18990337291956)
cB = chrono.ChVectorD(2.44546133329877,0.0109999999999889,-2.18990337291956)
dA = chrono.ChVectorD(-6.11576761455623e-15,1,4.37150315946155e-15)
dB = chrono.ChVectorD(-1.4432899320127e-14,1,-2.21628271290797e-14)
link_12.Initialize(body_51,body_49,False,cA,cB,dA,dB)
link_12.SetName("Concentric5")
exported_items.append(link_12)


# Mate constraint: Concentric6 [MateConcentric] type:1 align:0 flip:False
#   Entity 0: C::E name: body_49 , SW name: robo_leg_link-9/link1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_50 , SW name: robo_leg_link-9/link2-2 ,  SW ref.type:2 (2)

link_13 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.41705734397534,0.0444999999999901,-2.11645940863035)
dA = chrono.ChVectorD(0.824222916094805,-6.73072708679001e-16,-0.566265471827638)
cB = chrono.ChVectorD(2.40139710304249,0.0445000005011456,-2.10570037129432)
dB = chrono.ChVectorD(0.824222916094805,-6.93889390390723e-16,-0.566265471827638)
link_13.Initialize(body_49,body_50,False,cA,cB,dA,dB)
link_13.SetName("Concentric6")
exported_items.append(link_13)

link_14 = chrono.ChLinkMateGeneric()
link_14.SetConstrainedCoords(False, True, True, False, False, False)
cA = chrono.ChVectorD(2.41705734397534,0.0444999999999901,-2.11645940863035)
cB = chrono.ChVectorD(2.40139710304249,0.0445000005011456,-2.10570037129432)
dA = chrono.ChVectorD(0.824222916094805,-6.73072708679001e-16,-0.566265471827638)
dB = chrono.ChVectorD(0.824222916094805,-6.93889390390723e-16,-0.566265471827638)
link_14.Initialize(body_49,body_50,False,cA,cB,dA,dB)
link_14.SetName("Concentric6")
exported_items.append(link_14)


# Mate constraint: Concentric7 [MateConcentric] type:1 align:0 flip:False
#   Entity 0: C::E name: body_49 , SW name: robo_leg_link-9/link1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_47 , SW name: robo_leg_link-9/link2-1 ,  SW ref.type:2 (2)

link_15 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.36671634352987,0.0444999999999877,-2.18973282587118)
dA = chrono.ChVectorD(0.824222916094805,-6.73072708679001e-16,-0.566265471827638)
cB = chrono.ChVectorD(2.35105610259585,0.0445000005068609,-2.17897378853684)
dB = chrono.ChVectorD(0.824222916094805,1.31838984174237e-16,-0.566265471827637)
link_15.Initialize(body_49,body_47,False,cA,cB,dA,dB)
link_15.SetName("Concentric7")
exported_items.append(link_15)

link_16 = chrono.ChLinkMateGeneric()
link_16.SetConstrainedCoords(False, True, True, False, False, False)
cA = chrono.ChVectorD(2.36671634352987,0.0444999999999877,-2.18973282587118)
cB = chrono.ChVectorD(2.35105610259585,0.0445000005068609,-2.17897378853684)
dA = chrono.ChVectorD(0.824222916094805,-6.73072708679001e-16,-0.566265471827638)
dB = chrono.ChVectorD(0.824222916094805,1.31838984174237e-16,-0.566265471827637)
link_16.Initialize(body_49,body_47,False,cA,cB,dA,dB)
link_16.SetName("Concentric7")
exported_items.append(link_16)


# Mate constraint: Coincident9 [MateCoincident] type:0 align:0 flip:False
#   Entity 0: C::E name: body_49 , SW name: robo_leg_link-9/link1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_50 , SW name: robo_leg_link-9/link2-2 ,  SW ref.type:2 (2)

link_17 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(2.45232534025522,0.0779999999999873,-2.26644437767193)
cB = chrono.ChVectorD(2.5527242028832,0.0780000005011454,-2.12030966127702)
dA = chrono.ChVectorD(0.824222916094805,3.46944695195361e-17,-0.566265471827638)
dB = chrono.ChVectorD(0.824222916094805,-6.93889390390723e-16,-0.566265471827638)
link_17.Initialize(body_49,body_50,False,cA,cB,dB)
link_17.SetDistance(0)
link_17.SetName("Coincident9")
exported_items.append(link_17)

link_18 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.45232534025522,0.0779999999999873,-2.26644437767193)
dA = chrono.ChVectorD(0.824222916094805,3.46944695195361e-17,-0.566265471827638)
cB = chrono.ChVectorD(2.5527242028832,0.0780000005011454,-2.12030966127702)
dB = chrono.ChVectorD(0.824222916094805,-6.93889390390723e-16,-0.566265471827638)
link_18.Initialize(body_49,body_50,False,cA,cB,dA,dB)
link_18.SetName("Coincident9")
exported_items.append(link_18)


# Mate constraint: Coincident10 [MateCoincident] type:0 align:0 flip:False
#   Entity 0: C::E name: body_49 , SW name: robo_leg_link-9/link1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_47 , SW name: robo_leg_link-9/link2-1 ,  SW ref.type:2 (2)

link_19 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(2.45232534025522,0.0779999999999873,-2.26644437767193)
cB = chrono.ChVectorD(2.41897229843635,0.0110000005068609,-2.3149911140603)
dA = chrono.ChVectorD(0.824222916094805,3.46944695195361e-17,-0.566265471827638)
dB = chrono.ChVectorD(0.824222916094805,1.31838984174237e-16,-0.566265471827637)
link_19.Initialize(body_49,body_47,False,cA,cB,dB)
link_19.SetDistance(0)
link_19.SetName("Coincident10")
exported_items.append(link_19)

link_20 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.45232534025522,0.0779999999999873,-2.26644437767193)
dA = chrono.ChVectorD(0.824222916094805,3.46944695195361e-17,-0.566265471827638)
cB = chrono.ChVectorD(2.41897229843635,0.0110000005068609,-2.3149911140603)
dB = chrono.ChVectorD(0.824222916094805,1.31838984174237e-16,-0.566265471827637)
link_20.Initialize(body_49,body_47,False,cA,cB,dA,dB)
link_20.SetName("Coincident10")
exported_items.append(link_20)


# Mate constraint: Coincident11 [MateCoincident] type:0 align:0 flip:False
#   Entity 0: C::E name: body_49 , SW name: robo_leg_link-9/link1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_47 , SW name: robo_leg_link-9/link2-1 ,  SW ref.type:2 (2)

link_21 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(2.37155149447793,0.0779999999999873,-2.21095036143282)
cB = chrono.ChVectorD(2.33819845265906,0.0110000005068609,-2.25949709782119)
dA = chrono.ChVectorD(-0.824222916094805,6.73072708679001e-16,0.566265471827638)
dB = chrono.ChVectorD(-0.824222916094805,6.24500451351651e-16,0.566265471827637)
link_21.Initialize(body_49,body_47,False,cA,cB,dB)
link_21.SetDistance(0)
link_21.SetName("Coincident11")
exported_items.append(link_21)

link_22 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.37155149447793,0.0779999999999873,-2.21095036143282)
dA = chrono.ChVectorD(-0.824222916094805,6.73072708679001e-16,0.566265471827638)
cB = chrono.ChVectorD(2.33819845265906,0.0110000005068609,-2.25949709782119)
dB = chrono.ChVectorD(-0.824222916094805,6.24500451351651e-16,0.566265471827637)
link_22.Initialize(body_49,body_47,False,cA,cB,dA,dB)
link_22.SetName("Coincident11")
exported_items.append(link_22)


# Mate constraint: Coincident12 [MateCoincident] type:0 align:0 flip:False
#   Entity 0: C::E name: body_49 , SW name: robo_leg_link-9/link1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_50 , SW name: robo_leg_link-9/link2-2 ,  SW ref.type:2 (2)

link_23 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(2.37155149447793,0.0779999999999873,-2.21095036143282)
cB = chrono.ChVectorD(2.47195035710591,0.0780000005011455,-2.06481564503791)
dA = chrono.ChVectorD(-0.824222916094805,6.73072708679001e-16,0.566265471827638)
dB = chrono.ChVectorD(-0.824222916094805,-6.24500451351651e-17,0.566265471827638)
link_23.Initialize(body_49,body_50,False,cA,cB,dB)
link_23.SetDistance(0)
link_23.SetName("Coincident12")
exported_items.append(link_23)

link_24 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.37155149447793,0.0779999999999873,-2.21095036143282)
dA = chrono.ChVectorD(-0.824222916094805,6.73072708679001e-16,0.566265471827638)
cB = chrono.ChVectorD(2.47195035710591,0.0780000005011455,-2.06481564503791)
dB = chrono.ChVectorD(-0.824222916094805,-6.24500451351651e-17,0.566265471827638)
link_24.Initialize(body_49,body_50,False,cA,cB,dA,dB)
link_24.SetName("Coincident12")
exported_items.append(link_24)


# Mate constraint: Concentric2 [MateConcentric] type:1 align:1 flip:False
#   Entity 0: C::E name: body_56 , SW name: robo_leg_link-1/leg-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_53 , SW name: robo_leg_link-1/single_bot1-1 ,  SW ref.type:2 (2)

link_1 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.25365571558301,0.0169999999999987,-1.89057779985034)
dA = chrono.ChVectorD(-0.842656193786046,2.4980018054066e-16,-0.538451984000443)
cB = chrono.ChVectorD(2.24691446603272,0.0169999999999987,-1.89488541572234)
dB = chrono.ChVectorD(0.842656193786039,3.37230243729891e-15,0.538451984000454)
link_1.SetFlipped(True)
link_1.Initialize(body_56,body_53,False,cA,cB,dA,dB)
link_1.SetName("Concentric2")
exported_items.append(link_1)

link_2 = chrono.ChLinkMateGeneric()
link_2.SetConstrainedCoords(False, True, True, False, False, False)
cA = chrono.ChVectorD(2.25365571558301,0.0169999999999987,-1.89057779985034)
cB = chrono.ChVectorD(2.24691446603272,0.0169999999999987,-1.89488541572234)
dA = chrono.ChVectorD(-0.842656193786046,2.4980018054066e-16,-0.538451984000443)
dB = chrono.ChVectorD(0.842656193786039,3.37230243729891e-15,0.538451984000454)
link_2.Initialize(body_56,body_53,False,cA,cB,dA,dB)
link_2.SetName("Concentric2")
exported_items.append(link_2)


# Mate constraint: Coincident4 [MateCoincident] type:0 align:1 flip:False
#   Entity 0: C::E name: body_53 , SW name: robo_leg_link-1/single_bot1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_54 , SW name: robo_leg_link-1/link1-1 ,  SW ref.type:2 (2)

link_3 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(2.20924887425206,0.010999999999999,-1.88740444373917)
cB = chrono.ChVectorD(2.24872785151409,0.0109999999999986,-1.89610012107589)
dA = chrono.ChVectorD(5.21804821573824e-15,1,-7.56252699352089e-15)
dB = chrono.ChVectorD(-2.32452945780892e-14,-1,-1.37199279715006e-14)
link_3.Initialize(body_53,body_54,False,cA,cB,dB)
link_3.SetDistance(0)
link_3.SetName("Coincident4")
exported_items.append(link_3)

link_4 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.20924887425206,0.010999999999999,-1.88740444373917)
dA = chrono.ChVectorD(5.21804821573824e-15,1,-7.56252699352089e-15)
cB = chrono.ChVectorD(2.24872785151409,0.0109999999999986,-1.89610012107589)
dB = chrono.ChVectorD(-2.32452945780892e-14,-1,-1.37199279715006e-14)
link_4.SetFlipped(True)
link_4.Initialize(body_53,body_54,False,cA,cB,dA,dB)
link_4.SetName("Coincident4")
exported_items.append(link_4)


# Mate constraint: Parallel3 [MateParallel] type:3 align:1 flip:False
#   Entity 0: C::E name: body_53 , SW name: robo_leg_link-1/single_bot1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_54 , SW name: robo_leg_link-1/link1-1 ,  SW ref.type:2 (2)

link_5 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.22340655916481,0.0633100000000188,-1.88513277761455)
dA = chrono.ChVectorD(-0.538451984000445,8.85402862138562e-15,0.842656193786045)
cB = chrono.ChVectorD(2.24828026227359,0.0211999999999991,-1.92868902905316)
dB = chrono.ChVectorD(0.538451984000444,-9.71445146547012e-16,-0.842656193786045)
link_5.SetFlipped(True)
link_5.Initialize(body_53,body_54,False,cA,cB,dA,dB)
link_5.SetName("Parallel3")
exported_items.append(link_5)


# Mate constraint: Coincident7 [MateCoincident] type:0 align:1 flip:False
#   Entity 0: C::E name: body_56 , SW name: robo_leg_link-1/leg-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_53 , SW name: robo_leg_link-1/single_bot1-1 ,  SW ref.type:2 (2)

link_6 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(2.24691446603272,0.0169999999999987,-1.89488541572234)
cB = chrono.ChVectorD(2.24583702361273,0.0149989999999987,-1.89319926067857)
dA = chrono.ChVectorD(-0.842656193786046,2.4980018054066e-16,-0.538451984000443)
dB = chrono.ChVectorD(0.842656193786039,3.37230243729891e-15,0.538451984000454)
link_6.Initialize(body_56,body_53,False,cA,cB,dB)
link_6.SetDistance(0)
link_6.SetName("Coincident7")
exported_items.append(link_6)

link_7 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.24691446603272,0.0169999999999987,-1.89488541572234)
dA = chrono.ChVectorD(-0.842656193786046,2.4980018054066e-16,-0.538451984000443)
cB = chrono.ChVectorD(2.24583702361273,0.0149989999999987,-1.89319926067857)
dB = chrono.ChVectorD(0.842656193786039,3.37230243729891e-15,0.538451984000454)
link_7.SetFlipped(True)
link_7.Initialize(body_56,body_53,False,cA,cB,dA,dB)
link_7.SetName("Coincident7")
exported_items.append(link_7)


# Mate constraint: Concentric4 [MateConcentric] type:1 align:1 flip:False
#   Entity 0: C::E name: body_53 , SW name: robo_leg_link-1/single_bot1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_54 , SW name: robo_leg_link-1/link1-1 ,  SW ref.type:2 (2)

link_8 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.24872785151409,0.0789500000000293,-1.89610012107589)
dA = chrono.ChVectorD(-2.3259172365897e-14,-1,-1.37155911628106e-14)
cB = chrono.ChVectorD(2.24872785151409,0.0109999999999986,-1.89610012107589)
dB = chrono.ChVectorD(2.32452945780892e-14,1,1.37199279715006e-14)
link_8.SetFlipped(True)
link_8.Initialize(body_53,body_54,False,cA,cB,dA,dB)
link_8.SetName("Concentric4")
exported_items.append(link_8)

link_9 = chrono.ChLinkMateGeneric()
link_9.SetConstrainedCoords(False, True, True, False, False, False)
cA = chrono.ChVectorD(2.24872785151409,0.0789500000000293,-1.89610012107589)
cB = chrono.ChVectorD(2.24872785151409,0.0109999999999986,-1.89610012107589)
dA = chrono.ChVectorD(-2.3259172365897e-14,-1,-1.37155911628106e-14)
dB = chrono.ChVectorD(2.32452945780892e-14,1,1.37199279715006e-14)
link_9.Initialize(body_53,body_54,False,cA,cB,dA,dB)
link_9.SetName("Concentric4")
exported_items.append(link_9)


# Mate constraint: Parallel4 [MateParallel] type:3 align:1 flip:False
#   Entity 0: C::E name: body_53 , SW name: robo_leg_link-1/single_bot1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_54 , SW name: robo_leg_link-1/link1-1 ,  SW ref.type:2 (2)

link_10 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.20694267762004,0.0789500000000302,-1.89248502180422)
dA = chrono.ChVectorD(-2.3259172365897e-14,-1,-1.37155911628106e-14)
cB = chrono.ChVectorD(2.24872785151409,0.0779999999999987,-1.89610012107589)
dB = chrono.ChVectorD(2.32452945780892e-14,1,1.37199279715006e-14)
link_10.SetFlipped(True)
link_10.Initialize(body_53,body_54,False,cA,cB,dA,dB)
link_10.SetName("Parallel4")
exported_items.append(link_10)


# Mate constraint: Concentric5 [MateConcentric] type:1 align:0 flip:False
#   Entity 0: C::E name: body_53 , SW name: robo_leg_link-1/single_bot1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_54 , SW name: robo_leg_link-1/link1-1 ,  SW ref.type:2 (2)

link_11 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.24872785151409,0.0109999999999987,-1.89610012107589)
dA = chrono.ChVectorD(5.21804821573824e-15,1,-7.56252699352089e-15)
cB = chrono.ChVectorD(2.24872785151409,0.0109999999999986,-1.89610012107589)
dB = chrono.ChVectorD(2.32452945780892e-14,1,1.37199279715006e-14)
link_11.Initialize(body_53,body_54,False,cA,cB,dA,dB)
link_11.SetName("Concentric5")
exported_items.append(link_11)

link_12 = chrono.ChLinkMateGeneric()
link_12.SetConstrainedCoords(False, True, True, False, False, False)
cA = chrono.ChVectorD(2.24872785151409,0.0109999999999987,-1.89610012107589)
cB = chrono.ChVectorD(2.24872785151409,0.0109999999999986,-1.89610012107589)
dA = chrono.ChVectorD(5.21804821573824e-15,1,-7.56252699352089e-15)
dB = chrono.ChVectorD(2.32452945780892e-14,1,1.37199279715006e-14)
link_12.Initialize(body_53,body_54,False,cA,cB,dA,dB)
link_12.SetName("Concentric5")
exported_items.append(link_12)


# Mate constraint: Concentric6 [MateConcentric] type:1 align:0 flip:False
#   Entity 0: C::E name: body_54 , SW name: robo_leg_link-1/link1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_57 , SW name: robo_leg_link-1/link2-2 ,  SW ref.type:2 (2)

link_13 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.24627116266033,0.0444999999999998,-1.9748069643608)
dA = chrono.ChVectorD(-0.538451984000444,9.71445146547012e-16,0.842656193786045)
cB = chrono.ChVectorD(2.25650175035634,0.0444999999999997,-1.99081743204274)
dB = chrono.ChVectorD(-0.538451984000443,1.11022302462516e-15,0.842656193786046)
link_13.Initialize(body_54,body_57,False,cA,cB,dA,dB)
link_13.SetName("Concentric6")
exported_items.append(link_13)

link_14 = chrono.ChLinkMateGeneric()
link_14.SetConstrainedCoords(False, True, True, False, False, False)
cA = chrono.ChVectorD(2.24627116266033,0.0444999999999998,-1.9748069643608)
cB = chrono.ChVectorD(2.25650175035634,0.0444999999999997,-1.99081743204274)
dA = chrono.ChVectorD(-0.538451984000444,9.71445146547012e-16,0.842656193786045)
dB = chrono.ChVectorD(-0.538451984000443,1.11022302462516e-15,0.842656193786046)
link_14.Initialize(body_54,body_57,False,cA,cB,dA,dB)
link_14.SetName("Concentric6")
exported_items.append(link_14)


# Mate constraint: Concentric7 [MateConcentric] type:1 align:0 flip:False
#   Entity 0: C::E name: body_54 , SW name: robo_leg_link-1/link1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_55 , SW name: robo_leg_link-1/link2-1 ,  SW ref.type:2 (2)

link_15 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.32118329828791,0.0444999999999974,-1.92693858298316)
dA = chrono.ChVectorD(-0.538451984000444,9.71445146547012e-16,0.842656193786045)
cB = chrono.ChVectorD(2.33141388598392,0.0444999999999973,-1.9429490506651)
dB = chrono.ChVectorD(-0.538451984000444,1.56125112837913e-15,0.842656193786046)
link_15.Initialize(body_54,body_55,False,cA,cB,dA,dB)
link_15.SetName("Concentric7")
exported_items.append(link_15)

link_16 = chrono.ChLinkMateGeneric()
link_16.SetConstrainedCoords(False, True, True, False, False, False)
cA = chrono.ChVectorD(2.32118329828791,0.0444999999999974,-1.92693858298316)
cB = chrono.ChVectorD(2.33141388598392,0.0444999999999973,-1.9429490506651)
dA = chrono.ChVectorD(-0.538451984000444,9.71445146547012e-16,0.842656193786045)
dB = chrono.ChVectorD(-0.538451984000444,1.56125112837913e-15,0.842656193786046)
link_16.Initialize(body_54,body_55,False,cA,cB,dA,dB)
link_16.SetName("Concentric7")
exported_items.append(link_16)


# Mate constraint: Coincident9 [MateCoincident] type:0 align:0 flip:False
#   Entity 0: C::E name: body_54 , SW name: robo_leg_link-1/link1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_57 , SW name: robo_leg_link-1/link2-2 ,  SW ref.type:2 (2)

link_17 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(2.27222895097021,0.0779999999999972,-1.82293361012755)
cB = chrono.ChVectorD(2.12282600781194,0.0779999999999998,-1.91840114689083)
dA = chrono.ChVectorD(-0.538451984000444,1.67227343084164e-15,0.842656193786045)
dB = chrono.ChVectorD(-0.538451984000443,1.11022302462516e-15,0.842656193786046)
link_17.Initialize(body_54,body_57,False,cA,cB,dB)
link_17.SetDistance(0)
link_17.SetName("Coincident9")
exported_items.append(link_17)

link_18 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.27222895097021,0.0779999999999972,-1.82293361012755)
dA = chrono.ChVectorD(-0.538451984000444,1.67227343084164e-15,0.842656193786045)
cB = chrono.ChVectorD(2.12282600781194,0.0779999999999998,-1.91840114689083)
dB = chrono.ChVectorD(-0.538451984000443,1.11022302462516e-15,0.842656193786046)
link_18.Initialize(body_54,body_57,False,cA,cB,dA,dB)
link_18.SetName("Coincident9")
exported_items.append(link_18)


# Mate constraint: Coincident10 [MateCoincident] type:0 align:0 flip:False
#   Entity 0: C::E name: body_54 , SW name: robo_leg_link-1/link1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_55 , SW name: robo_leg_link-1/link2-1 ,  SW ref.type:2 (2)

link_19 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(2.27222895097021,0.0779999999999972,-1.82293361012755)
cB = chrono.ChVectorD(2.3218614007842,0.0109999999999975,-1.79121878826992)
dA = chrono.ChVectorD(-0.538451984000444,1.67227343084164e-15,0.842656193786045)
dB = chrono.ChVectorD(-0.538451984000444,1.56125112837913e-15,0.842656193786046)
link_19.Initialize(body_54,body_55,False,cA,cB,dB)
link_19.SetDistance(0)
link_19.SetName("Coincident10")
exported_items.append(link_19)

link_20 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.27222895097021,0.0779999999999972,-1.82293361012755)
dA = chrono.ChVectorD(-0.538451984000444,1.67227343084164e-15,0.842656193786045)
cB = chrono.ChVectorD(2.3218614007842,0.0109999999999975,-1.79121878826992)
dB = chrono.ChVectorD(-0.538451984000444,1.56125112837913e-15,0.842656193786046)
link_20.Initialize(body_54,body_55,False,cA,cB,dA,dB)
link_20.SetName("Coincident10")
exported_items.append(link_20)


# Mate constraint: Coincident11 [MateCoincident] type:0 align:0 flip:False
#   Entity 0: C::E name: body_54 , SW name: robo_leg_link-1/link1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_55 , SW name: robo_leg_link-1/link2-1 ,  SW ref.type:2 (2)

link_21 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(2.32499724540225,0.077999999999997,-1.90551391711858)
cB = chrono.ChVectorD(2.37462969521625,0.0109999999999974,-1.87379909526096)
dA = chrono.ChVectorD(0.538451984000444,-9.71445146547012e-16,-0.842656193786045)
dB = chrono.ChVectorD(0.538451984000444,-8.04911692853238e-16,-0.842656193786045)
link_21.Initialize(body_54,body_55,False,cA,cB,dB)
link_21.SetDistance(0)
link_21.SetName("Coincident11")
exported_items.append(link_21)

link_22 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.32499724540225,0.077999999999997,-1.90551391711858)
dA = chrono.ChVectorD(0.538451984000444,-9.71445146547012e-16,-0.842656193786045)
cB = chrono.ChVectorD(2.37462969521625,0.0109999999999974,-1.87379909526096)
dB = chrono.ChVectorD(0.538451984000444,-8.04911692853238e-16,-0.842656193786045)
link_22.Initialize(body_54,body_55,False,cA,cB,dA,dB)
link_22.SetName("Coincident11")
exported_items.append(link_22)


# Mate constraint: Coincident12 [MateCoincident] type:0 align:0 flip:False
#   Entity 0: C::E name: body_54 , SW name: robo_leg_link-1/link1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_57 , SW name: robo_leg_link-1/link2-2 ,  SW ref.type:2 (2)

link_23 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(2.32499724540225,0.077999999999997,-1.90551391711858)
cB = chrono.ChVectorD(2.17559430224398,0.0779999999999997,-2.00098145388186)
dA = chrono.ChVectorD(0.538451984000444,-9.71445146547012e-16,-0.842656193786045)
dB = chrono.ChVectorD(0.538451984000443,-1.86656246015104e-15,-0.842656193786047)
link_23.Initialize(body_54,body_57,False,cA,cB,dB)
link_23.SetDistance(0)
link_23.SetName("Coincident12")
exported_items.append(link_23)

link_24 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.32499724540225,0.077999999999997,-1.90551391711858)
dA = chrono.ChVectorD(0.538451984000444,-9.71445146547012e-16,-0.842656193786045)
cB = chrono.ChVectorD(2.17559430224398,0.0779999999999997,-2.00098145388186)
dB = chrono.ChVectorD(0.538451984000443,-1.86656246015104e-15,-0.842656193786047)
link_24.Initialize(body_54,body_57,False,cA,cB,dA,dB)
link_24.SetName("Coincident12")
exported_items.append(link_24)


# Mate constraint: Concentric2 [MateConcentric] type:1 align:1 flip:False
#   Entity 0: C::E name: body_61 , SW name: robo_leg_link-2/leg-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_58 , SW name: robo_leg_link-2/single_bot1-1 ,  SW ref.type:2 (2)

link_1 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.13249027212167,0.0170000000000006,-1.98260972123757)
dA = chrono.ChVectorD(-0.733530180177366,2.28983498828939e-16,-0.679656880174814)
cB = chrono.ChVectorD(2.12662203068025,0.0170000000000005,-1.98804697627897)
dB = chrono.ChVectorD(0.733530180177359,3.47638584585752e-15,0.679656880174822)
link_1.SetFlipped(True)
link_1.Initialize(body_61,body_58,False,cA,cB,dA,dB)
link_1.SetName("Concentric2")
exported_items.append(link_1)

link_2 = chrono.ChLinkMateGeneric()
link_2.SetConstrainedCoords(False, True, True, False, False, False)
cA = chrono.ChVectorD(2.13249027212167,0.0170000000000006,-1.98260972123757)
cB = chrono.ChVectorD(2.12662203068025,0.0170000000000005,-1.98804697627897)
dA = chrono.ChVectorD(-0.733530180177366,2.28983498828939e-16,-0.679656880174814)
dB = chrono.ChVectorD(0.733530180177359,3.47638584585752e-15,0.679656880174822)
link_2.Initialize(body_61,body_58,False,cA,cB,dA,dB)
link_2.SetName("Concentric2")
exported_items.append(link_2)


# Mate constraint: Coincident4 [MateCoincident] type:0 align:1 flip:False
#   Entity 0: C::E name: body_58 , SW name: robo_leg_link-2/single_bot1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_59 , SW name: robo_leg_link-2/link1-1 ,  SW ref.type:2 (2)

link_3 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(2.08822649769791,0.0110000000000006,-1.98738004824548)
cB = chrono.ChVectorD(2.12862244981808,0.0110000000000006,-1.98892001652605)
dA = chrono.ChVectorD(9.29811783123569e-16,1,-6.78276879106932e-16)
dB = chrono.ChVectorD(-1.43912659567036e-14,-1,-2.24716079078036e-14)
link_3.Initialize(body_58,body_59,False,cA,cB,dB)
link_3.SetDistance(0)
link_3.SetName("Coincident4")
exported_items.append(link_3)

link_4 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.08822649769791,0.0110000000000006,-1.98738004824548)
dA = chrono.ChVectorD(9.29811783123569e-16,1,-6.78276879106932e-16)
cB = chrono.ChVectorD(2.12862244981808,0.0110000000000006,-1.98892001652605)
dB = chrono.ChVectorD(-1.43912659567036e-14,-1,-2.24716079078036e-14)
link_4.SetFlipped(True)
link_4.Initialize(body_58,body_59,False,cA,cB,dA,dB)
link_4.SetName("Coincident4")
exported_items.append(link_4)


# Mate constraint: Parallel3 [MateParallel] type:3 align:1 flip:False
#   Entity 0: C::E name: body_58 , SW name: robo_leg_link-2/single_bot1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_59 , SW name: robo_leg_link-2/link1-1 ,  SW ref.type:2 (2)

link_5 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.10175496027774,0.0633100000000205,-1.98262807925392)
dA = chrono.ChVectorD(-0.679656880174814,8.04911692853238e-16,0.733530180177366)
cB = chrono.ChVectorD(2.13397454561188,0.0212000000000012,-2.02106954731411)
dB = chrono.ChVectorD(0.679656880174813,6.66133814775094e-15,-0.733530180177366)
link_5.SetFlipped(True)
link_5.Initialize(body_58,body_59,False,cA,cB,dA,dB)
link_5.SetName("Parallel3")
exported_items.append(link_5)


# Mate constraint: Coincident7 [MateCoincident] type:0 align:1 flip:False
#   Entity 0: C::E name: body_61 , SW name: robo_leg_link-2/leg-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_58 , SW name: robo_leg_link-2/single_bot1-1 ,  SW ref.type:2 (2)

link_6 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(2.12662203068025,0.0170000000000006,-1.98804697627897)
cB = chrono.ChVectorD(2.12526203726302,0.0149990000000006,-1.98657918238843)
dA = chrono.ChVectorD(-0.733530180177366,2.28983498828939e-16,-0.679656880174814)
dB = chrono.ChVectorD(0.733530180177359,3.47638584585752e-15,0.679656880174822)
link_6.Initialize(body_61,body_58,False,cA,cB,dB)
link_6.SetDistance(0)
link_6.SetName("Coincident7")
exported_items.append(link_6)

link_7 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.12662203068025,0.0170000000000006,-1.98804697627897)
dA = chrono.ChVectorD(-0.733530180177366,2.28983498828939e-16,-0.679656880174814)
cB = chrono.ChVectorD(2.12526203726302,0.0149990000000006,-1.98657918238843)
dB = chrono.ChVectorD(0.733530180177359,3.47638584585752e-15,0.679656880174822)
link_7.SetFlipped(True)
link_7.Initialize(body_61,body_58,False,cA,cB,dA,dB)
link_7.SetName("Coincident7")
exported_items.append(link_7)


# Mate constraint: Concentric4 [MateConcentric] type:1 align:1 flip:False
#   Entity 0: C::E name: body_58 , SW name: robo_leg_link-2/single_bot1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_59 , SW name: robo_leg_link-2/link1-1 ,  SW ref.type:2 (2)

link_8 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.12862244981808,0.0789500000000312,-1.98892001652605)
dA = chrono.ChVectorD(-1.48769885299771e-14,-1,-2.34604002891103e-14)
cB = chrono.ChVectorD(2.12862244981808,0.0110000000000006,-1.98892001652605)
dB = chrono.ChVectorD(1.43912659567036e-14,1,2.24716079078036e-14)
link_8.SetFlipped(True)
link_8.Initialize(body_58,body_59,False,cA,cB,dA,dB)
link_8.SetName("Concentric4")
exported_items.append(link_8)

link_9 = chrono.ChLinkMateGeneric()
link_9.SetConstrainedCoords(False, True, True, False, False, False)
cA = chrono.ChVectorD(2.12862244981808,0.0789500000000312,-1.98892001652605)
cB = chrono.ChVectorD(2.12862244981808,0.0110000000000006,-1.98892001652605)
dA = chrono.ChVectorD(-1.48769885299771e-14,-1,-2.34604002891103e-14)
dB = chrono.ChVectorD(1.43912659567036e-14,1,2.24716079078036e-14)
link_9.Initialize(body_58,body_59,False,cA,cB,dA,dB)
link_9.SetName("Concentric4")
exported_items.append(link_9)


# Mate constraint: Parallel4 [MateParallel] type:3 align:1 flip:False
#   Entity 0: C::E name: body_58 , SW name: robo_leg_link-2/single_bot1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_59 , SW name: robo_leg_link-2/link1-1 ,  SW ref.type:2 (2)

link_10 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.08686007809217,0.0789500000000319,-1.99278964285521)
dA = chrono.ChVectorD(-1.48769885299771e-14,-1,-2.34604002891103e-14)
cB = chrono.ChVectorD(2.12862244981808,0.0780000000000006,-1.98892001652605)
dB = chrono.ChVectorD(1.43912659567036e-14,1,2.24716079078036e-14)
link_10.SetFlipped(True)
link_10.Initialize(body_58,body_59,False,cA,cB,dA,dB)
link_10.SetName("Parallel4")
exported_items.append(link_10)


# Mate constraint: Concentric5 [MateConcentric] type:1 align:0 flip:False
#   Entity 0: C::E name: body_58 , SW name: robo_leg_link-2/single_bot1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_59 , SW name: robo_leg_link-2/link1-1 ,  SW ref.type:2 (2)

link_11 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.12862244981808,0.0110000000000006,-1.98892001652605)
dA = chrono.ChVectorD(9.29811783123569e-16,1,-6.78276879106932e-16)
cB = chrono.ChVectorD(2.12862244981808,0.0110000000000006,-1.98892001652605)
dB = chrono.ChVectorD(1.43912659567036e-14,1,2.24716079078036e-14)
link_11.Initialize(body_58,body_59,False,cA,cB,dA,dB)
link_11.SetName("Concentric5")
exported_items.append(link_11)

link_12 = chrono.ChLinkMateGeneric()
link_12.SetConstrainedCoords(False, True, True, False, False, False)
cA = chrono.ChVectorD(2.12862244981808,0.0110000000000006,-1.98892001652605)
cB = chrono.ChVectorD(2.12862244981808,0.0110000000000006,-1.98892001652605)
dA = chrono.ChVectorD(9.29811783123569e-16,1,-6.78276879106932e-16)
dB = chrono.ChVectorD(1.43912659567036e-14,1,2.24716079078036e-14)
link_12.Initialize(body_58,body_59,False,cA,cB,dA,dB)
link_12.SetName("Concentric5")
exported_items.append(link_12)


# Mate constraint: Concentric6 [MateConcentric] type:1 align:0 flip:False
#   Entity 0: C::E name: body_59 , SW name: robo_leg_link-2/link1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_60 , SW name: robo_leg_link-2/link2-2 ,  SW ref.type:2 (2)

link_13 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.14019473052056,0.0445000000000022,-2.06681022656135)
dA = chrono.ChVectorD(-0.679656880174813,-6.66133814775094e-15,0.733530180177366)
cB = chrono.ChVectorD(2.15310821124388,0.0445000000000024,-2.08074729998472)
dB = chrono.ChVectorD(-0.679656880174814,4.9960036108132e-16,0.733530180177366)
link_13.Initialize(body_59,body_60,False,cA,cB,dA,dB)
link_13.SetName("Concentric6")
exported_items.append(link_13)

link_14 = chrono.ChLinkMateGeneric()
link_14.SetConstrainedCoords(False, True, True, False, False, False)
cA = chrono.ChVectorD(2.14019473052056,0.0445000000000022,-2.06681022656135)
cB = chrono.ChVectorD(2.15310821124388,0.0445000000000024,-2.08074729998472)
dA = chrono.ChVectorD(-0.679656880174813,-6.66133814775094e-15,0.733530180177366)
dB = chrono.ChVectorD(-0.679656880174814,4.9960036108132e-16,0.733530180177366)
link_14.Initialize(body_59,body_60,False,cA,cB,dA,dB)
link_14.SetName("Concentric6")
exported_items.append(link_14)


# Mate constraint: Concentric7 [MateConcentric] type:1 align:0 flip:False
#   Entity 0: C::E name: body_59 , SW name: robo_leg_link-2/link1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_62 , SW name: robo_leg_link-2/link2-1 ,  SW ref.type:2 (2)

link_15 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.20540556353833,0.0444999999999999,-2.00638872991381)
dA = chrono.ChVectorD(-0.679656880174813,-6.66133814775094e-15,0.733530180177366)
cB = chrono.ChVectorD(2.21831904426165,0.0445000000000001,-2.02032580333718)
dB = chrono.ChVectorD(-0.679656880174814,1.23512311489549e-15,0.733530180177366)
link_15.Initialize(body_59,body_62,False,cA,cB,dA,dB)
link_15.SetName("Concentric7")
exported_items.append(link_15)

link_16 = chrono.ChLinkMateGeneric()
link_16.SetConstrainedCoords(False, True, True, False, False, False)
cA = chrono.ChVectorD(2.20540556353833,0.0444999999999999,-2.00638872991381)
cB = chrono.ChVectorD(2.21831904426165,0.0445000000000001,-2.02032580333718)
dA = chrono.ChVectorD(-0.679656880174813,-6.66133814775094e-15,0.733530180177366)
dB = chrono.ChVectorD(-0.679656880174814,1.23512311489549e-15,0.733530180177366)
link_16.Initialize(body_59,body_62,False,cA,cB,dA,dB)
link_16.SetName("Concentric7")
exported_items.append(link_16)


# Mate constraint: Coincident9 [MateCoincident] type:0 align:0 flip:False
#   Entity 0: C::E name: body_59 , SW name: robo_leg_link-2/link1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_60 , SW name: robo_leg_link-2/link2-2 ,  SW ref.type:2 (2)

link_17 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(2.13874424935601,0.0779999999999988,-1.91274135039101)
cB = chrono.ChVectorD(2.00868934841057,0.0780000000000025,-2.033244515246)
dA = chrono.ChVectorD(-0.679656880174814,-5.93969318174459e-15,0.733530180177366)
dB = chrono.ChVectorD(-0.679656880174814,4.9960036108132e-16,0.733530180177366)
link_17.Initialize(body_59,body_60,False,cA,cB,dB)
link_17.SetDistance(0)
link_17.SetName("Coincident9")
exported_items.append(link_17)

link_18 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.13874424935601,0.0779999999999988,-1.91274135039101)
dA = chrono.ChVectorD(-0.679656880174814,-5.93969318174459e-15,0.733530180177366)
cB = chrono.ChVectorD(2.00868934841057,0.0780000000000025,-2.033244515246)
dB = chrono.ChVectorD(-0.679656880174814,4.9960036108132e-16,0.733530180177366)
link_18.Initialize(body_59,body_60,False,cA,cB,dA,dB)
link_18.SetName("Coincident9")
exported_items.append(link_18)


# Mate constraint: Coincident10 [MateCoincident] type:0 align:0 flip:False
#   Entity 0: C::E name: body_59 , SW name: robo_leg_link-2/link1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_62 , SW name: robo_leg_link-2/link2-1 ,  SW ref.type:2 (2)

link_19 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(2.13874424935601,0.0779999999999988,-1.91274135039101)
cB = chrono.ChVectorD(2.18194917696846,0.0110000000000002,-1.87270956014871)
dA = chrono.ChVectorD(-0.679656880174814,-5.93969318174459e-15,0.733530180177366)
dB = chrono.ChVectorD(-0.679656880174814,1.23512311489549e-15,0.733530180177366)
link_19.Initialize(body_59,body_62,False,cA,cB,dB)
link_19.SetDistance(0)
link_19.SetName("Coincident10")
exported_items.append(link_19)

link_20 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.13874424935601,0.0779999999999988,-1.91274135039101)
dA = chrono.ChVectorD(-0.679656880174814,-5.93969318174459e-15,0.733530180177366)
cB = chrono.ChVectorD(2.18194917696846,0.0110000000000002,-1.87270956014871)
dB = chrono.ChVectorD(-0.679656880174814,1.23512311489549e-15,0.733530180177366)
link_20.Initialize(body_59,body_62,False,cA,cB,dA,dB)
link_20.SetName("Coincident10")
exported_items.append(link_20)


# Mate constraint: Coincident11 [MateCoincident] type:0 align:0 flip:False
#   Entity 0: C::E name: body_59 , SW name: robo_leg_link-2/link1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_62 , SW name: robo_leg_link-2/link2-1 ,  SW ref.type:2 (2)

link_21 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(2.20535062361315,0.0779999999999994,-1.98462730804839)
cB = chrono.ChVectorD(2.24855555122559,0.0110000000000001,-1.94459551780609)
dA = chrono.ChVectorD(0.679656880174813,6.66133814775094e-15,-0.733530180177366)
dB = chrono.ChVectorD(0.679656880174814,-4.85722573273506e-16,-0.733530180177366)
link_21.Initialize(body_59,body_62,False,cA,cB,dB)
link_21.SetDistance(0)
link_21.SetName("Coincident11")
exported_items.append(link_21)

link_22 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.20535062361315,0.0779999999999994,-1.98462730804839)
dA = chrono.ChVectorD(0.679656880174813,6.66133814775094e-15,-0.733530180177366)
cB = chrono.ChVectorD(2.24855555122559,0.0110000000000001,-1.94459551780609)
dB = chrono.ChVectorD(0.679656880174814,-4.85722573273506e-16,-0.733530180177366)
link_22.Initialize(body_59,body_62,False,cA,cB,dA,dB)
link_22.SetName("Coincident11")
exported_items.append(link_22)


# Mate constraint: Coincident12 [MateCoincident] type:0 align:0 flip:False
#   Entity 0: C::E name: body_59 , SW name: robo_leg_link-2/link1-1 ,  SW ref.type:2 (2)
#   Entity 1: C::E name: body_60 , SW name: robo_leg_link-2/link2-2 ,  SW ref.type:2 (2)

link_23 = chrono.ChLinkMateXdistance()
cA = chrono.ChVectorD(2.20535062361315,0.0779999999999994,-1.98462730804839)
cB = chrono.ChVectorD(2.0752957226677,0.0780000000000024,-2.10513047290338)
dA = chrono.ChVectorD(0.679656880174813,6.66133814775094e-15,-0.733530180177366)
dB = chrono.ChVectorD(0.679656880174814,-1.26287869051112e-15,-0.733530180177366)
link_23.Initialize(body_59,body_60,False,cA,cB,dB)
link_23.SetDistance(0)
link_23.SetName("Coincident12")
exported_items.append(link_23)

link_24 = chrono.ChLinkMateParallel()
cA = chrono.ChVectorD(2.20535062361315,0.0779999999999994,-1.98462730804839)
dA = chrono.ChVectorD(0.679656880174813,6.66133814775094e-15,-0.733530180177366)
cB = chrono.ChVectorD(2.0752957226677,0.0780000000000024,-2.10513047290338)
dB = chrono.ChVectorD(0.679656880174814,-1.26287869051112e-15,-0.733530180177366)
link_24.Initialize(body_59,body_60,False,cA,cB,dA,dB)
link_24.SetName("Coincident12")
exported_items.append(link_24)

