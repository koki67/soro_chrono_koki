# -*- coding: utf-8 -*-
"""
Created on Tue Dec 17 09:32:56 2019

@author: dmulr
"""

# Import libraries
import numpy as np
from bridson import poisson_disc_samples
import math
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
from matplotlib import animation
from openopt import LCP
import sys






# In[Create compression matrix]:

#  Create compression matrix
def createCompressionLCP(HN,HT,UNA,UTA,mu,M,g_count):
    # create empty A matrix
    A=np.zeros((5*g_count,5*g_count))
    # Combine all the H matrices
    W=np.concatenate((HN,HT,-HT),axis=1)
    # Create first term of A matrix
    W=np.matmul(np.matmul(W.T,np.linalg.inv(M)),W)
    # create empty N matrix
    N=np.zeros((2*g_count,3*g_count))
    N[:g_count,:g_count],N[g_count:2*g_count,:g_count]=mu*np.eye(g_count),mu*np.eye(g_count)
    N[:g_count,g_count:2*g_count],N[g_count:2*g_count,2*g_count:3*g_count]=-np.eye(g_count),-np.eye(g_count)

    # Create empty Is matrix
    IsT=np.zeros((3*g_count,2*g_count))
    IsT[g_count:2*g_count,:g_count],IsT[2*g_count:3*g_count,g_count:2*g_count]=np.eye(g_count),np.eye(g_count)

# put the matrices together
    A=np.concatenate((W,IsT),axis=1)
    Ap=np.concatenate((N,np.zeros((2*g_count,2*g_count))),axis=1)
    A=np.concatenate((A,Ap),axis=0)

    # Create empy B column matrix
    B=np.zeros((5*g_count,1))
    B[:g_count,0],B[g_count:2*g_count,0],B[2*g_count:3*g_count,0]=UNA[:,0],UTA[:,0],-UTA[:,0]


    return A,B


# In[ create expansion matrix]
def createExpansionLCP(HN,HT,UNC,UTC,z,mu,M,en,et,g_count):
    # Create empty A matrix
    A=np.zeros((5*g_count,5*g_count))
    # COmbine the H matrices
    W=np.concatenate((HN,HT,-HT),axis=1)
    # Solve first entry of the A matrix
    W=np.matmul(np.matmul(W.T,np.linalg.inv(M)),W)
    # create empty N matrix
    N=np.zeros((2*g_count,3*g_count))
    N[:g_count,:g_count],N[g_count:2*g_count,:g_count]=mu*np.eye(g_count),mu*np.eye(g_count)
    N[:g_count,g_count:2*g_count],N[g_count:2*g_count,2*g_count:3*g_count]=-np.eye(g_count),-np.eye(g_count)

    # Create empty Is matrix
    IsT=np.zeros((3*g_count,2*g_count))
    IsT[g_count:2*g_count,:g_count],IsT[2*g_count:3*g_count,g_count:2*g_count]=np.eye(g_count),np.eye(g_count)
    #empty zero matrix
    Z=np.zeros((2*g_count,2*g_count))
    #combine A matrices
    A=np.concatenate((W,IsT),axis=1)
    Ap=np.concatenate((N,Z),axis=1)
    A=np.concatenate((A,Ap),axis=0)


    # term for B matrix
    # lambda term
    Lambda=en*z
    # little g matrix
    g=np.concatenate((UNC,UTC,-UTC),axis=0)
    # Empty B matrix
    B=np.zeros((5*g_count,1))
    
    # Define Nbar matrix
    Nbar=N
    # fill in Nbar matrix
    Nbar[:g_count,2*g_count:3*g_count],N[g_count:2*g_count,g_count:2*g_count]=(1-et)*np.eye(g_count),(1-et)*np.eye(g_count)
    # Fill in the B matrix
    B[:3*g_count]=np.matmul(W,Lambda)+g
    
    # do more matrix stuff
    B[3*g_count:5*g_count]=np.matmul(Nbar,Lambda)
    B=B.ravel()
    # Return A and B matrix
    return A,B
