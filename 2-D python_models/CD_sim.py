# -*- coding: utf-8 -*-
"""
Created on Tue Dec 17 09:37:07 2019

@author: dmulr
"""

# Import libraries
import numpy as np
from bridson import poisson_disc_samples
import math
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
from matplotlib import animation
from openopt import LCP
import sys
from CD_Objs.py import LCPSolve,createCompressionLCP,createExpansionLCP,obstacles,line2d,horizontalLine,verticalLine,circleObstacle,rigidBody2D,circle,createGlobalMatrices,updateCoordinates,dist2D,calculateGaps,geth,getH,poisson_distribution,gravity,Forces,modelStiffness





# In[Running the code]:

#Initializing objects
obj=[]

#Creating the bounding box with left, right, up, and down coordinates
left,right,up,down=-5,40,10,-10


# create internal and external boundary
nb=45
R=1.88

#x,y,phi,\dot{x},\dot{y},\dot{phi},R,rho

for i in range(nb):
    x=(R+3)*np.cos(i*2*np.pi/nb)+2.5
    y=(R+3)*np.sin(i*2*np.pi/nb)+2.5

    r0=.25
    m0=1
    i0=10
    obj.append(circle(x,y,0,0,0,0,r0,m0,i0))

#for i in range(ni):
#    obj.append(circle(R*rn(),R*rn(),0.,rn(),rn(),rn(),.5*np.random.random_sample()+.5,1.))
#x, y, phi, xv, yv, phiv, self.M, self.I)
r1=.25
i1=.1
m1=1
# Create the interior particles
poisson_distribution(obj,r1,m1,i1)

r2=2.25
m2=10
i2=50
# create ball
obj.append(circle(10,2.5,0,0,0,0,r2,m2,i2))
# Count the number of interior objects
ni=len(obj)-nb



# In[OBSTACLES]
ob_option=3
if ob_option==1:
    for i in range(4):
        obj.append(circleObstacle(6*i-4,-8,1))
        obj.append(circleObstacle(6*i-2,-14,1))
        obj.append(circleObstacle(6*i-4,-20,1))
        obj.append(circleObstacle(6*i-2,-26,1))
        obj.append(circleObstacle(6*i-4,-32,1))
        obj.append(circleObstacle(6*i-2,-38,1))


elif ob_option==2:
    for i in range(3):
        obj.append(circleObstacle(15,6*i,1))
else:
    print('none')


# In[]
#Creating Mass, q, and v matrices
M,q0,v0=createGlobalMatrices(obj)
# degrees of freedom
ndof=np.size(q0)

#Gravity turns on and off gravity
# create empty matrix
g=0
if g==1:
    Fg=gravity(obj,q0)
else:
    Fg=np.zeros_like(q0)



#Time parameters
t0,tend,nsteps=0.,1,100
time=np.linspace(t0,tend,num=nsteps)

# time step size
h=time[1]-time[0]

#position and velocities. Making the empty matrices
q,v=np.zeros((q0.size,time.size)),np.zeros((v0.size,time.size))

#Initial conditions
q[:,0],v[:,0]=q0.reshape(q0.size,),v0.reshape(v0.size,)

#Integration parameters
t_imp,gamma=1.,.5

#Impact and Friction properties
en,et,mu=.3,0,.8


# In[] Solving the system
for i in range(time.size-1):

    print(i)
    ####Predicting gap
    #P redicted q
    qbar=q[:,i]+h*gamma*v[:,i]
    K=modelStiffness(qbar,nb,ni,0.,600)
    F=Forces(obj,q0,nb,ni,i,5)
    # Create external forces
    Fext=-np.matmul(K,qbar)+Fg.flatten()+F.flatten()

    #Predicted gap. gbar is a matrix of all gaps between all bodies and ind is the index list of those bodies for which gbar<0
    updateCoordinates(obj,qbar,v[:,i])
    gbar,ind=calculateGaps(obj)
    updateCoordinates(obj,q[:,i],v[:,i])

    #Calculating vfree
    vfree=v[:,i]+h*np.matmul(np.linalg.inv(M),Fext)

    if ind.size<1:
        v[:,i+1]=vfree
    else:
######################Using Lemke's

        H,HN,HT=getH(obj,ind,ndof)
        g_count=len(ind[:,0])

        #Calculating normal and transverse gap velocities at A
        UA=np.matmul(H.T,v[:,i].reshape(ndof,1))
        UA=np.matmul(H.T,vfree.reshape(ndof,1))

        UNA,UTA=UA[:g_count],UA[g_count:2*g_count]

        #Creating the LCP during compression

        A1,B1=createCompressionLCP(HN,HT,UNA,UTA,mu,M,g_count)
        B1=B1.flatten()
        sol = LCP(A1,B1)
        r=sol.solve('lcpsolve')
        f_opt, x_opt = r.ff, r.xf
        z, w = x_opt[x_opt.size/2:], x_opt[:x_opt.size/2]
        z=z[:3*g_count].reshape(3*g_count,1)


        RNAC=z[:g_count,0]
        RTAC=z[g_count:2*g_count,0]-z[2*g_count:3*g_count,0]
        RAC=np.concatenate((RNAC,RTAC),axis=0).reshape(2*g_count,1)

        #Using the reactions to calculate the velocity at the end of compression
        UC=UA+np.matmul(np.matmul(np.matmul(H.T,np.linalg.inv(M)),H),RAC)
        UNC,UTC=UC[:g_count],UC[g_count:2*g_count]
        A2,B2=createExpansionLCP(HN,HT,UNC,UTC,z,mu,M,en,et,g_count)

#
        #Solving the second LCP to solve for the reactions during expansion
        sol = LCP(A2,B2)
        r=sol.solve('lcpsolve')
        f_opt, x_opt = r.ff, r.xf
        z, w = x_opt[x_opt.size/2:], x_opt[:x_opt.size/2]
        z=z[:3*g_count].reshape(3*g_count,1)


        RNP=z[:g_count,0]
        RTP=z[g_count:2*g_count,0]-z[2*g_count:3*g_count,0]
        RP=np.concatenate((RNP,RTP),axis=0).reshape(2*g_count,1)
        RCE=RP+en*RAC

        #Total reaction due to impulses during compression and expansion
        Rk1=RAC+RCE

        #Velocity at the end of the time step
        v[:,i+1]=vfree+(np.matmul(np.matmul(np.linalg.inv(M),H),Rk1)).reshape(ndof,)


    #     For implicit integration of q
    vkt=h*(t_imp*v[:,i+1]+(1-t_imp)*v[:,i])

    #Implicit integration of q
    q[:,i+1]=q[:,i]+vkt



#    dd.append(down)
#    du.append(up)
#    dl.append(left)
#    dr.append(right)
 #   down=down+.001*i
 #   obj[-1]=horizontalLine(down)
np.savez('temp1_1.npz',q=q,nb=nb,ni=ni,obj=obj,left=left,right=right,up=up,down=down,R=R,t0=t0,tend=tend,nsteps=nsteps)
np.savez('steadystate_exp3.npz',q0=q[:,nsteps-1],v0=v[:,nsteps-1],nb=nb,ni=ni,r0=r0,r1=r1,r2=r2,m0=m0,m1=m1,m2=m2,i0=i0,i1=i1,i2=i2,obj=obj)