# -*- coding: utf-8 -*-
"""
Created on Tue Feb 18 15:14:04 2020

@author: dmulr
"""

# Import libraries
import numpy as np
from bridson import poisson_disc_samples
import math
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
from matplotlib import animation
import sys
from cd_objs2 import * 
from config2 import *
from numpy import array, dot
from qpsolvers import solve_qp
from scipy.optimize import minimize 
from IPython.display import Image


# In[Create robots]
index=0
Q=np.zeros((3*nb))
for i in range(nb):
    x=R1*np.cos(i*2*np.pi/nb)
    y=R1*np.sin(i*2*np.pi/nb)
    Q[3*i]=x
    Q[3*i+1]=y
    obj.append(Robot(x,y,phi0b,xv0b,yv0b,phiv0b,diameter/2,mb,Ib,Fx,Fy,T,i,index))
    index=index+1
# In[Create Particles]
n=MaxValues(R1,diameter,nb)
n=n[0]

ni=np.sum(n)
# total number
nt=ni+nb
count=0
for i in range(n.size):
    for j in range(n[i]):
        R2=(diameter*n[i])/(np.pi*2)     # Radius of interiro ring
        x=R2*np.cos(j*2*np.pi/n[i])     # x position
        y=R2*np.sin(j*2*np.pi/n[i])     # z position
        obj.append(Particle(x,y,phi0p,xv0p,yv0p,phiv0p,diameter/2,mp,Ip,count,index))  
        count=count+1 
        index=index+1
# Boundary walls        
obj.append(verticalLine(left))
obj.append(verticalLine(right))
obj.append(horizontalLine(up))
obj.append(horizontalLine(down))

#plotObj(obj,left,right,up,down)
# In[Create Global Matrix]
M,q0,v0=createGlobalMatrices(nt,obj) #Creating Mass, q, and v matrices
ndof=np.size(q0) # degrees of freedom
#
# In[Gravity]
if g==1: # if gravity is on add it to forces
    Fg=gravity(obj,q0)
else: # else gravity is zero 
    Fg=np.zeros_like(q0)

# In[Empty Position and velocity matrix]
q=np.zeros((q0.size,time.size)) # empty position matrix
v=np.zeros((v0.size,time.size)) # empty velocity matrix
F=np.zeros((q0.size,time.size))
# In[Initial conditions]
q[:,0],v[:,0]=q0.reshape(q0.size,),v0.reshape(v0.size,)

# In[Transofrmation matrix T]
T=np.zeros((3*nb,3*nt))
for i in range(3*nb):
    T[i,i]=1

E=np.zeros((3*nb,time.size))
Rk=np.zeros((q0.size,time.size))
# In[Solving the system]
for i in range(time.size-1):

    print(i*tstep) # [print time]
    # qbar
    qbar=q[:,i]+tstep*gamma*v[:,i] 
    # stiffness matrix
    K,d=modelStiffness(qbar,nb,ni,l,k) 
    # Forces applied by robots
    Fb=F[:,i]

    #total forces acting on the robot
    Fext=-np.matmul(K,qbar)+Fg.flatten()
    Fin=Fext+Fb
    # update coordinates
    updateCoordinates(obj,qbar,v[:,i])
    #calculated gaps and determine which ones ar gunna collide 
    gbar,ind=calculateGaps(obj) 
    # update the positions 
    updateCoordinates(obj,q[:,i],v[:,i]) 

    #Calculating vfree
    vfree=v[:,i]+tstep*np.matmul(np.linalg.inv(M),Fin)
    

# In[if theres no collisions]
    if ind.size<1:
        v[:,i+1]=vfree
# In[If there are contacts]
    else:
       (Rk1,H)=LCP_Com_Ex(obj,ind,ndof,vfree,M,mu,en,et,v,qbar)
        # Vk+1
       v[:,i+1]=vfree+(np.matmul(np.matmul(np.linalg.inv(M),H),Rk1)).reshape(ndof,)


    # A matrix
    A=t_imp*(tstep**2)*np.linalg.inv(M)
    # B matrix
    B=(t_imp)*tstep*np.matmul(np.linalg.inv(M),H)
    # contact forces
    R_f=(np.matmul(B,Rk1)).reshape(ndof,)
    # External forces
    Fe=np.matmul(A,Fext)


    
    # Boundary Force
    E[:,i]=Q-q[0:3*nb,i]
    # controller    
    solF,sol=Controller2(A,B,Fe,R_f,qbar,v[:,i+1],tstep,nb,nt,T,Q)

    F[0:3*nb,i+1]=solF    
    updateForces(obj,F[:,i+1])
    q[:,i+1]=q[:,i]+tstep*v[:,i]+Fe+R_f
    xbar=tstep*np.ones(3*nb)
    Q=Q+xbar
np.savez('temp21.npz',q=q,nb=nb,ni=ni,obj=obj,left=left,right=right,up=up,down=down,R1=R1,t0=t0,tend=tend,time=time,F=F)