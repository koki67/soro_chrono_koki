# -*- coding: utf-8 -*-
"""
Created on Tue Feb 18 15:31:26 2020

@author: dmulr
"""
# In[Import libraries]
import numpy as np 


# In[Initializing objects]
obj=[]

# In[Creating the bounding box with left, right, up, and down coordinates]
left,right,up,down=-1,1,1,-1

# In[Robot]
nb=10 # boundary robots
diameter=.07 # diameter 
R1=(diameter*nb/(np.pi*2))+.1 # Radius of robot
mb=.12 # mass of bot
Ib=.5*mb*(diameter/2)**2 # Inertia of bot

phi0b=0 # initial angle
xv0b=0   # x velocity
yv0b=0   # y velocity
phiv0b=0     # rotational velocity

# In[Internal Particles]
mp=.08 # mass of particle
Ip=.5*mb*(diameter/2)**2 # Inertia of bot
phi0p=0 # initial angle
xv0p=0   # x velocity
yv0p=0   # y velocity
phiv0p=0     # rotational velocity

# In[Forces]
g=1   # Gravity is on 

# In[Time steps and integration properties]
#Time parameters
t0=0 # start simulation (seconds)
tend=.2# end simulation (seconds)
tstep=.001 # time step (seconds)
inter=int(tend/tstep)
time=np.linspace(t0,tend,num=inter) # time matrix
Q=np.zeros((2*nb,time.size))
#Integration parameters
t_imp=1
gamma=.5


# In[Impact and friction properties]
en=.3
et=0
mu=.8

# In[Spring Constants]

k=30
l=0

Fx=0
Fy=0
T=0