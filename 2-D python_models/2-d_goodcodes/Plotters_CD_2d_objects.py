# -*- coding: utf-8 -*-
"""
Created on Fri Feb 21 12:03:21 2020

@author: dmulr
"""
import numpy as np
import math as math
import matplotlib.pyplot as plt
import os
from matplotlib import animation
import animatplot as amp
from matplotlib import colors as colors
from scipy.spatial import Voronoi, voronoi_plot_2d,ConvexHull


def PlotForces(F,nb,nt,time):

    fig, (ax1, ax2) = plt.subplots(2, constrained_layout=True)
    for i in range(nb):
        ax1.plot(time,F[3*i,:],label=str(i))
        ax1.set_title('X Force[N]')
        plt.legend()
        ax2.plot(time,F[3*i+1,:],label=str(i))
        ax2.set_title('Y Force[N]')
        plt.legend()

def PlotError(E,nb,nt,time):
    fig, (ax1, ax2) = plt.subplots(2, constrained_layout=True)
    for i in range(nb):
        ax1.plot(time,E[2*i,:],label=str(i))
        ax1.set_title('X Error')
        plt.legend()
        ax2.plot(time,E[2*i+1,:],label=str(i))
        ax2.set_title('Y Error') 
        plt.legend()
def PlotContact(Fcontact,nb,nt,time):
    fig, (ax1, ax2) = plt.subplots(2, constrained_layout=True)
    for i in range(nb):
        ax1.plot(time,Fcontact[3*i,:],label=str(i))
        ax1.set_title('Contact force X')
        ax2.plot(time,Fcontact[3*i+1,:],label=str(i))
        ax2.set_title('Contact force Y')         

def PlotBoundaryForce(Fbound,nb,nt,time):   
    fig, (ax1, ax2) = plt.subplots(2, constrained_layout=True)
    for i in range(nb):
        ax1.plot(time,Fbound[2*i,:],label=str(i))
        ax1.set_title('Boundary force X')
        ax2.plot(time,Fbound[2*i+1,:],label=str(i))
        ax2.set_title('Boundary force Y')         
    
# In[Plot Current position]
def plotObj(obj,xf1,xf2,yf1,yf2):
    bx=[left,right,right,left,left]
    by=[down,down,up,up,down]
    ax = plt.axes(xlim=(left-.5, right+.5), ylim=(down-.5, up+.5))
    lineH = plt.Line2D(bx, by, lw=1)
    ax = plt.gca()

    # change default range so that new circles will work
 #   ax.set_xlim((xf1, xf2))
 #   ax.set_ylim((yf1, yf2))

    for i in range (0,len(obj)):
        if(obj[i].type=='particle'):
            circle1 = plt.Circle((obj[i].x,obj[i].y),obj[i].R,color='r')
            ax.add_artist(circle1)
        if(obj[i].type=='robot'):
            circle1 = plt.Circle((obj[i].x,obj[i].y),obj[i].R,color='g')
            ax.add_artist(circle1)
        if(obj[i].type=='lineObstacle'):
            l = mlines.Line2D([obj[i].x1,obj[i].x2], [obj[i].y1,obj[i].y2])
            ax.add_line(l)

        if(obj[i].type=='circleObstacle'):
            circle1 = plt.Circle((obj[i].x,obj[i].y),obj[i].R,color='b')
            ax.add_artist(circle1)

    plt.show()
    
# In[Plot frame by frame ]
    
def Frame_by_Frame(obj,left,right,up,down,q,nb,ni,nt,time):    
    script_dir = os.path.dirname("plots/")   
    results_dir = os.path.join(script_dir, 'force_chain_pos/')     
    if not os.path.isdir(results_dir):
        os.makedirs(results_dir)
    count=0
    # run through each time 
    for i in range(len(time)):
        fig = plt.figure()
        fig.set_dpi(100)
        fig.set_size_inches(12, 12)
        count=count+1
        ax=plt.gca()
        ax.set_xlim((left, right))
        ax.set_ylim((down, up))
#        bx=[left,right,right,left,left]
#        by=[down,down,up,up,down]

#        lineH = plt.Line2D(bx, by, lw=1)
        # check if robot
        for j in range(nb+ni):
            if(obj[j].type=='robot'):
                x0,y0,phi0,r=q[3*i,j],q[3*i+1,j],q[3*i+2,j],obj[j].R
                patch = plt.Circle((x0, y0), r, fc='g')
                ax.add_artist(patch)
        # chceck if particle
            if(obj[j].type=='particle'):
                x0,y0,phi0,r=q[3*i,j],q[3*i+1,j],q[3*i+2,j],obj[j].R
                patch = plt.Circle((x0, y0), r, fc='g')
                ax.add_artist(patch)
#            lineC1 = plt.Line2D((x0+r*np.cos(phi0+np.pi), x0+r*np.cos(phi0)), (y0+r*np.sin(phi0+np.pi), y0+r*np.sin(phi0)),color='k')
#            ax.add_line(lineC1)
#            lineC2 = plt.Line2D((x0+r*np.cos(phi0+np.pi+np.pi/2), x0+r*np.cos(phi0+np.pi+np.pi/2)), (y0+r*np.sin(phi0+np.pi+np.pi+np.pi/2), y0+r*np.sin(phi0+np.pi+np.pi/2)), color='k')
#            ax.add_line(lineC2)
    # change default range so that new circles will work


                
        plt.show()
        plt.title('t='+str(round(time[i],3)))
        plt.savefig(results_dir+"picture"+str(count)+".jpg")  
#            elif(obj[i].geom=='circleObstacle'):
#
#                x0,y0,r=obj[i].x,obj[i].y,obj[i].R
#                patch=plt.Circle((x0,y0),r,fc='b')
#
#                Obst.append(patch)
#            
#            elif(obj[i].geom=='ball'):
#                ii=obj[i].index
#                x0,y0,phi0,r=q[3*i,0],q[3*i+1,0],q[3*i+2,0],obj[i].R
#                patch=plt.Circle((x0,y0),r,fc='b')
#                lineC1 = plt.Line2D((x0+r*np.cos(phi0+np.pi), x0+r*np.cos(phi0)), (y0+r*np.sin(phi0+np.pi), y0+r*np.sin(phi0)),color='k')
#                lineC2 = plt.Line2D((x0+r*np.cos(phi0+np.pi+np.pi/2), x0+r*np.cos(phi0+np.pi+np.pi/2)), (y0+r*np.sin(phi0+np.pi+np.pi+np.pi/2), y0+r*np.sin(phi0+np.pi+np.pi/2)), color='k')
#                cPatches.append(patch)
#                cLines.append(lineC1)
#                cLines.append(lineC2)
#                cInd.append(ii)


        plt.close('all')        