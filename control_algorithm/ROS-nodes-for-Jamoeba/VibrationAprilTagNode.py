'''
Python script used to
test the april tags
tracking system
from : https://github.com/duckietown/apriltags3-py
'''

####################################################
from Source.Tracker import AprilTagTracker, DataPlotter
####################################################

# Say if you want to lot the data after the test
plot_data = False
animate_data = True

# Initialise the tracker
tracker = AprilTagTracker(num=19, save_data=True, save_video=True, show_mean=True, show_robots=False, trail=150,
                          show_arrow_robots=True, crop_image=False, cam_choice="brio", res_choice="1080p", focus_frames=100)

# Start the loop
tracker.start_tracking()

# Get the filename
file = tracker.filename
data_saved = tracker.save_data

# Delete the tracker
del tracker

# If you saved the data, plot it
if data_saved and (plot_data or animate_data):
    dp = DataPlotter(filename=file)
    if animate_data:
        dp.animate(speed=10.)
    if plot_data:
        dp.plot()
