'''
Python script used to test
the the vibration pattern
'''

####################################################
from Source.Photons import Photon
from Source.Application import AllSameApplication
####################################################

# Create the photon swarm
photon = Photon(number_of_robots=3)

# Create the application
app = AllSameApplication(swarm=photon)
