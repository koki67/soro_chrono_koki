'''
Python script used to
test the april tags
tracking system
from : https://github.com/duckietown/apriltags3-py
'''

####################################################
from Source.Tracker import AprilTagTracker, DataPlotter
####################################################

# Initialise the tracker
tracker = AprilTagTracker(num=8, save_data=True, save_video=True, show_mean=True, show_robots=True,
                          show_arrow_robots=True, crop_image=True, cam_choice="brio", res_choice="1080p", focus_frames=100)

# Start the loop
tracker.start_tracking()

# Get the filename
file = tracker.filename
plot = tracker.save_data

# Delete the tracker
del tracker

# If you saved the data, plot it
if plot:
	dp = DataPlotter(filename=file)
