'''
Python script for the closed loop control ROS node
'''

####################################################
from Source.Controllers import StringController
import sys
import signal
####################################################


def signal_handler(sig, frame):
	print("\n")
	sys.exit(0)


# Create the out of the loop interrupt
signal.signal(signal.SIGINT, signal_handler)

# Create a controller
ctl = StringController(kp=2., ki=0.1, kd=0., disabled=0)

# Set the target angles
ctl.set_target_angle(tar=(0, 0, 0, 0, 0, 0, 0, 0))

# Start the control loop
ctl.start_control_loop2()
