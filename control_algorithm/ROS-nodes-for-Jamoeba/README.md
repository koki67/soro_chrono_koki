# ROS-nodes-for-Jamoeba

A project with lots of tools to let you control the Jamoeba robots with ROS and Apriltags on Ubuntu.

## Get started:

Clone this repository and open it in the terminal. Type the following to initialize the `apriltags3py` submodule :

```
$ git submodule init
$ git submodule update
```
Do the same for the `apriltags` submodule. For this one, you need to build the C library. Type the following :

```
$ cd apriltags3py
$ git submodule init
$ git submodule update
$ cd apriltags
$ cmake
$ make
```

If building `apriltags` was successful, you should be able to run the codes.

## String robot

#### Closed loop control

To achieve a closed loop control with the String robot, you need to :

* Have a ROS master running. To do it, type this in a terminal :
```
$ roscore
```

* Have `StringPhotonNode.py`, `StringControlNode.py` and `StringAprilTagNode.py` running.
To do it, type every one of those lines in a different terminal :
```
$ python StringPhotonNode.py
$ python StringControlNode.py
$ python StringAprilTagNode.py
```

* In the String-Master device page, type `start` in the Set_state function to enable the robot.
Type `stop` to disable the robot. Type `jam` to jam the robot.

![Set_state function](Images/StringStateFunction.png)

#### Messages

Message can be send manually to the cloud by creating an event called `command` directly on the cloud platform event tracker :

![Creating an event](Images/StringManualEvent.png)

The message should be in this format :

```
data: "(PWM_left_robot_0, PWM_right_robot_0, PWM_left_robot_1, PWM_right_robot_1, ... , PWM_left_robot_7, PWM_right_robot_7)"
```

#### Other informations

* In `StringControlNode.py`, you can adjust different settings for your experiments.

* The String robot works with a single Photon microcontroller through the [Particle.io](https://www.particle.io/iot-platform) IoT platform.


## Vacuum robot

#### Closed loop control

To achieve a closed loop control with the Vacuum robot, you need to :

* Have a ROS master running. To do it, type this in a terminal :
```
$ roscore
```

* Have `VibrationPhotonControlNode.py` and `VibrationAprilTagNode.py` running.
To do it, type every one of those lines in a different terminal :
```
$ python VibrationPhotonControlNode.py
$ python VibrationAprilTagNode.py
```

* You can also have `VibrationOverrideNode.py` running to manually control the robots you want, but this node is optional.
You can run this script by running this line in a new terminal :
```
$ python VibrationOverrideNode.py
```

#### Robot type and states values

There is two type of robots in this swarm, Vacuum and Legged robots.

##### Vacuum robots

The Vacuum robots have the following apriltags ID : `1, 3, 5, 7, 11, 13, 15 and 17`

The states for the Vacuum robots are :

| STATE | Pump  | Vibration | Sleep |
|:-----:|:-----:|:---------:|:-----:|
| 0     | No    | No        | No    |
| 1     | Yes   | No        | No    |
| 2     | No    | Yes       | No    |
| 3     | No    | No        | Yes   |

##### Legged robots

The Legged robots have the following apriltags ID : `0, 2, 4, 6, 8, 9, 10, 12, 14 and 16`

The states for the Legged robots are :

| STATE | Leg   | Vibration | Sleep |
|:-----:|:-----:|:---------:|:-----:|
| 0     | No    | No        | No    |
| 1     | +     | No        | No    |
| 2     | -     | No        | No    |
| 3     | No    | Yes       | No    |
| 4     | No    | No        | Yes   |

#### Messages

Message can be send manually to the cloud by creating an event called `state` directly on the cloud platform event tracker :

![Creating an event](Images/VibrationManualEvent.png)

The message should be in this format :

```
{'0': 1, ... , 'TAG_ID': STATE, ... , '17': 0}
```

Also, you can send command for individual robot with this format :

```
'TAG_ID': STATE
```

#### Battery level

You can manually request the battery level of each robot by creating an event called `battery_request` directly on the cloud platform event tracker.
There is no need for data in this event. 

![Battery level event](Images/BatteryEvent.png)

You should receive events named `percent` from each connected devices with data looking like :

```
{'ID': 3, 'Charge': 75.203125}
```

## About Apriltags
AprilTags are a visual fiducial system popular in robotics research. For more information on them, click [here.](https://april.eecs.umich.edu/software/apriltag)

A simple tutorial on how to use Apriltags can be found [here](https://github.com/duckietown/apriltags3-py)
and some tag pictures can be found [here.](https://github.com/AprilRobotics/apriltag-imgs/tree/master/tag36h11)

## Other tools for research *** TODO : CHANGE THE DESCRIPTION

Execute AprilTagNode.py to make a ROS node that will publish the current state of each robots on AprilTag/state.

Execute either VibrationManualControl.py or VibrationActiveControl.py (the other ones are not working right now).

*** Note that AprilTagNode.py and ROS should be running for VibrationActiveControl.py to work. ***

### VibrationManualControl.py

A simple manual control script to try and get a feel of the 3 robots swarm.

#### Ubuntu GUI

![Ubuntu GUI](Images/VibrationManualControl_Ubuntu.png)

#### Windows GUI

![Windows GUI](Images/VibrationManualControl_Windows.png)

### VibrationActiveControl.py

A simple script that uses a proportional controller to try and keep the robot on a straight line trajectory.

## Data logging using the SerialPlot app

The `VernierTest.ino` Arduino code is used to log the force from a load cell. You can log the data through the Serial port.
A simple application to log the data (SerialPlot) can be found [here](https://hackaday.io/project/5334-serialplot-realtime-plotting-software).
