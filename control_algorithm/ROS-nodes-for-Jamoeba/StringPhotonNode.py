'''
ROS node for the String Robot
'''

####################################################
from Source.Photons import StringPhoton
###################################################

# Create the photons swarm and set the enable to true
robot = StringPhoton()
