'''
Python classes for Photon controllers
'''

####################################################
from numpy import array, ones
import rospy
from std_msgs.msg import String
from Source.Photons import Photon
from time import clock
from pid_controller.pid import PID
####################################################


####################################################
class Controller:
	"""
	Class used to control a single robot of a photon swarm
	"""
	def __init__(self, photon=Photon(), robot_id=0, gain=1.0, base_speed=75, angle=0):
		self.photon = photon
		self.id = robot_id
		self.gain = gain
		self.base_speed = base_speed
		self.angle = angle
		self.k = 0

	def ros_callback(self, data):
		""" Method to parse the desired ROS message """
		val = str(data)
		i1 = val.find('{')
		i2 = val.find('}') + 1
		dat = val[i1:i2]
		data_dict = eval(dat)

		error = self.angle - data_dict[str(self.id)][2]
		pwmp = int(self.base_speed + self.gain * error)
		pwmm = int(self.base_speed - self.gain * error)
		self.photon.set_pwm(self.id, max(min(pwmp, 150), 0), max(min(pwmm, 150), 0))
		self.photon.publish()
####################################################


####################################################
class ControllerForVideo(Controller):

	def ros_callback(self, data):
		""" Method to parse the desired ROS message """
		val = str(data)
		i1 = val.find('{')
		i2 = val.find('}') + 1
		dat = val[i1:i2]
		data_dict = eval(dat)

		error = self.angle - data_dict[str(self.id)][2]

		# Little thing to add a bit of mouvement
		x = data_dict[str(self.id)][0]
		if x > -10:
			if self.k < 20:
				self.k += 1
				pwmp = 130
				pwmm = 10
			elif self.k == 40:
				self.k = 0
				pwmp = 10
				pwmm = 130
			else:
				self.k += 1
				pwmp = 10
				pwmm = 130
		else:
			pwmp = int(self.base_speed + self.gain * error)
			pwmm = int(self.base_speed - self.gain * error)

		self.photon.set_pwm(self.id, max(min(pwmp, 150), 0), max(min(pwmm, 150), 0))
		self.photon.publish()
####################################################


####################################################
class StringController:

	def __init__(self, kp=1., ki=0., kd=0., disabled=0):
		""" Initialise the instance of the class """
		# States and angles initial position
		self.state = {"0": (0, 0, 0),
					  "1": (0, 0, 0),
					  "2": (0, 0, 0),
					  "3": (0, 0, 0),
					  "4": (0, 0, 0),
					  "5": (0, 0, 0),
					  "6": (0, 0, 0),
					  "7": (0, 0, 0),
					  "8": (0, 0, 0)}
		self.pot_angle = (0, 0, 0, 0, 0, 0, 0, 0)
		self.current_angle = array([0, 0, 0, 0, 0, 0, 0, 0])
		# wheels
        self.pwm = array([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
		self.last = clock()
		self.angle_threshold = 8
		self.disabled = disabled

		# Initialize the controllers
		self.pid = array([None, None, None, None, None, None, None, None])
		for i in range(8):
			self.pid[i] = PID(p=kp, i=ki, d=kd)

		# Set target angles
		self.target_angles = (0., 0., 0., 0., 0., 0., 0., 0.)
		self.set_target_angle()

		rospy.init_node("ControlLoop", anonymous=True)
		rospy.Subscriber("state", String, self.state_callback)
		rospy.Subscriber("angle", String, self.angle_callback)
		self.pub = rospy.Publisher('command', String, queue_size=10)

	def state_callback(self, data):
		""" Method to parse data from the state topic """
		self.state = eval(data.data)

	def angle_callback(self, data):
		""" Method to parse data from the angle topic """
		self.pot_angle = eval(data.data)

# target projection
	def set_target_angle(self, tar=(0., 0., 0., 0., 0., 0., 0., 0.)):
		""" Method to set the target """
		self.target_angles = tar
		for i in range(8):
			self.pid[i].target = tar[i]

	def start_control_loop(self):
		""" Method to start the control loop """
		error_a = ones(8)
		command = ones(8)
		while True:
			angles_are_set = sum(1 * (error_a < self.angle_threshold)) == 8  # Verify if every angles are within the tolerances

			for i in range(8):
				self.current_angle[i] = (self.state[str(i)][2] + self.pot_angle[i]) % 360  # Bring the angle between 0 and 360
				if self.current_angle[i] > 180:
					self.current_angle[i] += -360  # Bring the angle back between -180 and 180

				command[i] = -self.pid[i](feedback=self.current_angle[i])
				error_a[i] = self.pid[i].error

				if angles_are_set:
					self.pwm[2*i] = self.pwm[2*i + 1] = 150
				else:
					if abs(error_a[i]) > self.angle_threshold:
						self.pwm[2*i] = max(min(command[i], 255), -255)  # Clamped between -255 and 255
						self.pwm[2*i + 1] = -self.pwm[2*i]
					else:
						self.pwm[2*i] = self.pwm[2*i + 1] = 0

			freq = 1.0 / (clock() - self.last)  # Cap the publish frequency to let the Photons Controller read the data
			if freq <= 5:
				self.last = clock()
				to_pub = tuple(self.pwm)
				self.pub.publish(str(to_pub))

	def start_control_loop2(self):
		""" Method to start the control loop """
		error_a = ones(8)  # Initialise the error array
		command = ones(8)  # Initialise the command array
		self.angle_threshold = 20  # Change the threshold value to 20 degrees
		
		# Start the control loop
		while True:
			angles_are_set = sum(1 * (error_a < self.angle_threshold)) == 8  # Verify if every angles are within the tolerances

			for i in range(self.disabled, 8):
				self.current_angle[i] = (self.state[str(i)][2] + self.pot_angle[i]) % 360  # Bring the angle between 0 and 360
				if self.current_angle[i] > 180:
					self.current_angle[i] += -360  # Bring the angle back between -180 and 180

				command[i] = -self.pid[i](feedback=self.current_angle[i])  # Compute the command from the current state
				error_a[i] = self.pid[i].error  # Get the error

				if angles_are_set:  # If the angles of every robots are within 20 degrees of the target
					# Adjust the angles slightly without stop
					self.pwm[2*i] = 80 + command[i]
					self.pwm[2*i + 1] = 80 - command[i]
				else:  # Else, stop and get the angles of every robots within 20 degrees of the target
					if abs(error_a[i]) > self.angle_threshold:  # If the error is bigger than 20 degrees, adjust it
						self.pwm[2*i] = max(min(command[i], 255), -255)  # Clamp the command between -255 and 255
						self.pwm[2*i + 1] = -self.pwm[2*i]
					else:
						self.pwm[2*i] = self.pwm[2*i + 1] = 0  # If the angle is within 20 degrees of the target, do nothing

			freq = 1.0 / (clock() - self.last)  # Cap the publish frequency to let the Photons Controller read the data
			if freq <= 5:  # In hertz
				self.last = clock()
				to_pub = tuple(self.pwm)  # Convert the 
				self.pub.publish(str(to_pub))
####################################################
