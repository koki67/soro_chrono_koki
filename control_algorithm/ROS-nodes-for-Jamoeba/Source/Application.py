'''
Python class used to set
up any applications for the photon
Amin
'''

###################################################
from Source.Photons import Photon
from Source.Controllers import ControllerForVideo
from pyparticleio.ParticleCloud import ParticleCloud
import rospy
from std_msgs.msg import String
from tkinter import *
from time import clock
from math import atan2, sqrt, pi
from numpy import zeros
###################################################


###################################################
class Application:
	"""
	The application that will control the swarm manually
	"""
	def __init__(self, swarm=Photon(), title="Vibration manual control"):
		""" Initialisation of the class """
		self.photon = swarm
		self.window = Tk()
		self.window.title(title)
		self._create_widget()
		self.window.mainloop()

	def _create_widget(self):
		""" Method that create the widget of the app """

		self.but1 = Button(self.window)
		self.but1["text"] = "MOTORS ON/OFF"
		self.but1["command"] = self.toggle_pwm
		self.but1.grid(row=0, column=0, columnspan=6)

		self.lab1 = Label(self.window)
		self.lab1['text'] = 'ID:0'
		self.lab1.grid(row=1, column=0, columnspan=2)

		self.pwm1_1 = Scale(self.window, from_=0, to=150)
		self.pwm1_1["command"] = self.set_pwm1_1
		self.pwm1_1['background'] = 'red'
		self.pwm1_1.grid(row=2, column=0, rowspan=3)

		self.pwm1_2 = Scale(self.window, from_=0, to=150)
		self.pwm1_2["command"] = self.set_pwm1_2
		self.pwm1_2['background'] = 'red'
		self.pwm1_2.grid(row=2, column=1, rowspan=3)

		self.lab2 = Label(self.window)
		self.lab2['text'] = 'ID:1'
		self.lab2.grid(row=1, column=2, columnspan=2)

		self.pwm2_1 = Scale(self.window, from_=0, to=150)
		self.pwm2_1["command"] = self.set_pwm2_1
		self.pwm2_1['background'] = 'green'
		self.pwm2_1.grid(row=2, column=2, rowspan=3)

		self.pwm2_2 = Scale(self.window, from_=0, to=150)
		self.pwm2_2["command"] = self.set_pwm2_2
		self.pwm2_2['background'] = 'green'
		self.pwm2_2.grid(row=2, column=3, rowspan=3)

		self.lab3 = Label(self.window)
		self.lab3['text'] = 'ID:2'
		self.lab3.grid(row=1, column=4, columnspan=2)

		self.pwm3_1 = Scale(self.window, from_=0, to=150)
		self.pwm3_1["command"] = self.set_pwm3_1
		self.pwm3_1['background'] = 'blue'
		self.pwm3_1.grid(row=2, column=4, rowspan=3)

		self.pwm3_2 = Scale(self.window, from_=0, to=150)
		self.pwm3_2["command"] = self.set_pwm3_2
		self.pwm3_2['background'] = 'blue'
		self.pwm3_2.grid(row=2, column=5, rowspan=3)

		self.pump = Button(self.window)
		self.pump['text'] = 'PUMP ON/OFF'
		self.pump['command'] = self.toggle_pump
		self.pump.grid(row=6, column=0, columnspan=6)

	def toggle_pump(self):
		en = self.photon.get_en()
		if en == 0:
			self.pump['relief'] = SUNKEN
		elif en == 1:
			self.but1['relief'] = RAISED
			self.pump['relief'] = SUNKEN
		elif en == 2:
			self.pump['relief'] = RAISED
		elif en == 3:
			self.pump['relief'] = SUNKEN

		self.photon.toggle_en_pump()

	def toggle_pwm(self):
		en = self.photon.get_en()
		if en == 0:
			self.but1['relief'] = SUNKEN
		elif en == 1:
			self.but1['relief'] = RAISED
		elif en == 2:
			self.pump['relief'] = RAISED
			self.but1['relief'] = SUNKEN
		elif en == 3:
			self.but1['relief'] = SUNKEN

		self.photon.toggle_en_pwm()

	def set_pwm1_1(self, arg):
		self.photon.set_pwm(robot_id=0, right=self.pwm1_1.get())
		self.photon.publish()

	def set_pwm1_2(self, arg):
		self.photon.set_pwm(robot_id=0, left=self.pwm1_2.get())
		self.photon.publish()

	def set_pwm2_1(self, arg):
		self.photon.set_pwm(robot_id=1, right=self.pwm2_1.get())
		self.photon.publish()

	def set_pwm2_2(self, arg):
		self.photon.set_pwm(robot_id=1, left=self.pwm2_2.get())
		self.photon.publish()

	def set_pwm3_1(self, arg):
		self.photon.set_pwm(robot_id=2, right=self.pwm3_1.get())
		self.photon.publish()

	def set_pwm3_2(self, arg):
		self.photon.set_pwm(robot_id=2, left=self.pwm3_2.get())
		self.photon.publish()
###################################################


###################################################
class ActiveApplication:
	"""
	The application that will control the swarm manually
	"""
	def __init__(self, controller=ControllerForVideo(), title="Vibration active control"):
		""" Initialisation of the class """
		self.ctl = controller
		self.window = Tk()
		self.window.title(title)
		self.ros = False
		self._create_widget()
		self.window.mainloop()

	def _create_widget(self):
		""" Method that create the widget of the app """

		self.but1 = Button(self.window)
		self.but1["text"] = "MOTORS ON/OFF"
		self.but1["command"] = self.toggle_pwm
		self.but1.grid(row=0, column=0, columnspan=3)

		self.lab1 = Label(self.window)
		self.lab1['text'] = 'Gain'
		self.lab1.grid(row=1, column=0)

		self.gain = Scale(self.window, from_=0, to=3, resolution=0.1)
		self.gain['background'] = 'red'
		self.gain.grid(row=2, column=0, rowspan=3)

		self.lab2 = Label(self.window)
		self.lab2['text'] = 'Speed'
		self.lab2.grid(row=1, column=1)

		self.speed = Scale(self.window, from_=0, to=150)
		self.speed.set(75)
		self.speed['background'] = 'green'
		self.speed.grid(row=2, column=1, rowspan=3)

		self.lab3 = Label(self.window)
		self.lab3['text'] = 'Angle'
		self.lab3.grid(row=1, column=2)

		self.angle = Scale(self.window, from_=-90, to=90)
		self.angle['background'] = 'blue'
		self.angle.grid(row=2, column=2, rowspan=3)

		self.pump = Button(self.window)
		self.pump['text'] = 'Activate ROS'
		self.pump['command'] = self.start_ros
		self.pump.grid(row=6, column=0, columnspan=3)

	def toggle_pwm(self):
		en = self.ctl.photon.get_en()
		if en == 0:
			self.but1['relief'] = SUNKEN
		elif en == 1:
			self.but1['relief'] = RAISED
		elif en == 2:
			self.pump['relief'] = RAISED
			self.but1['relief'] = SUNKEN
		elif en == 3:
			self.but1['relief'] = SUNKEN

		self.ctl.photon.toggle_en_pwm()

	def start_ros(self):
		""" Start ros with the current values """
		self.ctl.gain = self.gain.get()
		self.ctl.base_speed = self.speed.get()
		self.ctl.angle = self.angle.get()
		if not self.ros:
			self.ros = False
			rospy.init_node("Photon", anonymous=True)
			rospy.Subscriber("state", String, self.ctl.ros_callback)
		rospy.spin()
###################################################


###################################################
class AllSameApplication:
	"""
	The application that will control the swarm manually
	"""

	def __init__(self, swarm=Photon(), title="Vibration manual control"):
		""" Initialisation of the class """
		self.photon = swarm
		self.window = Tk()
		self.window.title(title)
		self._create_widget()
		self.window.mainloop()

	def _create_widget(self):
		""" Method that create the widget of the app """

		self.butmot = Button(self.window)
		self.butmot["text"] = "Mot ON/OFF"
		self.butmot["command"] = self.toggle_pwm
		self.butmot.grid(row=0, column=0)

		self.butpum = Button(self.window)
		self.butpum["text"] = "Pum ON/OFF"
		self.butpum["command"] = self.toggle_pump
		self.butpum.grid(row=1, column=0)

		self.pwm = Scale(self.window, from_=0, to=150)
		self.pwm["command"] = self.set_pwm
		self.pwm.grid(row=0, column=1, rowspan=2)

	def toggle_pump(self):
		en = self.photon.get_en()
		if en == 0:
			self.butpum['relief'] = SUNKEN
		elif en == 1:
			self.butmot['relief'] = RAISED
			self.butpum['relief'] = SUNKEN
		elif en == 2:
			self.butpum['relief'] = RAISED
		elif en == 3:
			self.butpum['relief'] = SUNKEN

		self.photon.toggle_en_pump()

	def toggle_pwm(self):
		en = self.photon.get_en()
		if en == 0:
			self.butmot['relief'] = SUNKEN
		elif en == 1:
			self.butmot['relief'] = RAISED
		elif en == 2:
			self.butpum['relief'] = RAISED
			self.butmot['relief'] = SUNKEN
		elif en == 3:
			self.butmot['relief'] = SUNKEN

		self.photon.toggle_en_pwm()

	def set_pwm(self, arg):
		val = self.pwm.get()
		for i in range(self.photon.number_of_robots):
			self.photon.set_pwm(i, val, val)
		self.photon.publish()
###################################################


###################################################
class Joystick(object):
	"""
	The application that will control a photon
	"""

	def __init__(self):
		""" Method to initialise the application """
		# Set variables

		access_token = "8269d62ca4178e500166245f1835a2e96f4a0de4"

		self.particle_cloud = ParticleCloud(access_token)

		self.all_devices = self.particle_cloud.devices
		for device in self.all_devices:
			self.first_device_name = "{0}".format(device)  # To publish, you need to know one device
			break

		self.window = Tk()
		self.speed = 0
		self.heading = 0
		self.state = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
		self.at_angle = zeros(8)
		self.set_state()
		self.last = clock()

		# Set the tkinter GUI
		self._build_widgets()

	def _build_widgets(self):
		""" This method build the widget and the app """
		self.canvas = Canvas(self.window, height=500, width=500)
		self.canvas.grid(row=0, column=0, columnspan=2)
		self.canvas.bind('<1>', self.movement)  # the magic
		self.canvas.bind('<B1-Motion>', self.movement)  # similar magic
		self.canvas.bind('<ButtonRelease-1>', self.save)  # result of magic
		self.canvas.create_oval(30, 30, 470, 470, fill='white', outline='black')
		self.canvas.create_text(250, 15, anchor=CENTER, text="Photon Joystick control")
		self.indicator = self.canvas.create_oval(215, 215, 285, 285, fill='red')  # dot

	def movement(self, event):  # move the dot
		x = event.x - 250
		y = event.y - 250
		self.canvas.coords(self.indicator, event.x - 35, event.y - 35, event.x + 35, event.y + 35)

		self.speed = int(sqrt(x ** 2 + y ** 2))
		if self.speed >= 255:
			self.speed = 255
		self.heading = int(180 * atan2(y, x) / pi) + 180

		self.send_data()

	def save(self, event):  # report the dot
		self.canvas.coords(self.indicator, 215, 215, 285, 285)

		self.speed = 0

		self.send_data()

	def set_state(self):
		for i in range(8):
			self.state[2*i] = 10  # self.speed
			self.state[2*i + 1] = self.heading - self.at_angle[i]

	def send_data(self):
		freq = 1.0 / (clock() - self.last)
		if freq <= 5.0:
			self.set_state()
			self.all_devices[self.first_device_name].publish("state", str(self.state))

	def ros_callback(self, data):
		data_dict = eval(data.data)
		for i in range(8):
			self.at_angle[i] = data_dict[str(i)][2]

	def start(self):
		self.window.mainloop()
###################################################


###################################################
class OverrideApplication:
	"""
	The application that will override the vibration robots
	"""
	def __init__(self):
		""" Initialisation of the class """
		self.window = Tk()
		self.window.title("Override")

		self.num = 18
		self.leg_ids = (0, 2, 4, 6, 8, 9, 10, 12, 14, 16)
		self.vac_ids = (1, 3, 5, 7, 11, 13, 15, 17)
		self.is_override = zeros(self.num, dtype=bool)
		self.override_state = zeros(self.num)

		self._set_var()

		self._create_widget()

		# Set the ROS node
		rospy.init_node("OverrideNode", anonymous=True)
		self.pub = rospy.Publisher('override', String, queue_size=10)

		self.window.mainloop()

	def _set_var(self):
		""" Method to set the variables of the model """
		self.override_array = zeros(self.num, dtype=BooleanVar)
		for i in range(self.num):
			self.override_array[i] = BooleanVar()
			self.override_array[i].set(False)
			self.override_array[i].trace_variable('w', self.update_var)

		self.status_array = zeros(self.num, dtype=IntVar)
		for i in range(self.num):
			self.status_array[i] = IntVar()
			self.status_array[i].set(0)
			self.status_array[i].trace_variable('w', self.update_var)

		self.all_leg = BooleanVar()
		self.all_leg.set(False)
		self.all_leg.trace_variable('w', self.select_all_leg)

		self.all_vac = BooleanVar()
		self.all_vac.set(False)
		self.all_vac.trace_variable('w', self.select_all_vac)

	def _create_widget(self):
		""" Method that create the widget of the app """
		self.leg_frame = LabelFrame(self.window)
		self.leg_frame["text"] = "Legged robots"
		self.leg_frame.grid(row=0, column=0)

		self.vac_frame = LabelFrame(self.window)
		self.vac_frame["text"] = "Vacuum robots"
		self.vac_frame.grid(row=1, column=0)

		self.id_lab = zeros(self.num, dtype=Label)
		leg = vac = 1
		for (i, lab) in enumerate(self.id_lab):
			if i in self.leg_ids:
				lab = Label(self.leg_frame)
				leg += 1
				ind = leg
			else:
				lab = Label(self.vac_frame)
				vac += 1
				ind = vac

			lab['text'] = str(i)
			lab.grid(row=0, column=ind)

		self.leg_labels = zeros(7, dtype=Label)
		leg_text = ("ID:", "Override", "Stop", "Leg forward", "Leg backward", "Vibration", "Sleep")
		for (i, lab) in enumerate(self.leg_labels):
			lab = Label(self.leg_frame)
			lab['text'] = leg_text[i]
			if leg_text[i] == "ID:" or leg_text[i] == "Override":
				cs = 1
			else:
				cs = 2
			lab.grid(row=i, column=0, columnspan=cs)

		self.vac_labels = zeros(6, dtype=Label)
		vac_text = ("ID:", "Override", "Stop", "Vacuum", "Vibration", "Sleep")
		for (i, lab) in enumerate(self.vac_labels):
			lab = Label(self.vac_frame)
			lab['text'] = vac_text[i]
			if vac_text[i] == "ID:" or vac_text[i] == "Override":
				cs = 1
			else:
				cs = 2
			lab.grid(row=i, column=0, columnspan=cs)

		self.all_leg_lab = Label(self.leg_frame)
		self.all_leg_lab['text'] = "All"
		self.all_leg_lab.grid(row=0, column=1)

		self.all_leg_chk = Checkbutton(self.leg_frame)
		self.all_leg_chk['variable'] = self.all_leg
		self.all_leg_chk.grid(row=1, column=1)

		self.all_vac_lab = Label(self.vac_frame)
		self.all_vac_lab['text'] = "All"
		self.all_vac_lab.grid(row=0, column=1)

		self.all_vac_chk = Checkbutton(self.vac_frame)
		self.all_vac_chk['variable'] = self.all_vac
		self.all_vac_chk.grid(row=1, column=1)

		self.checkbut_array = zeros(self.num, dtype=Checkbutton)
		leg = vac = 1
		for (i, cb) in enumerate(self.checkbut_array):
			if i in self.leg_ids:
				cb = Checkbutton(self.leg_frame)
				leg += 1
				ind = leg
			else:
				cb = Checkbutton(self.vac_frame)
				vac += 1
				ind = vac

			cb['variable'] = self.override_array[i]
			cb.grid(row=1, column=ind)

		self.radio_array_leg = zeros((len(self.leg_ids), 5), dtype=Radiobutton)
		for j in range(5):
			for (i, rd) in enumerate(self.radio_array_leg[:, j]):
				rd = Radiobutton(self.leg_frame)
				rd['variable'] = self.status_array[self.leg_ids[i]]
				rd['value'] = j
				rd.grid(row=j+2, column=i+2)

		self.radio_array_vac = zeros((len(self.vac_ids), 4), dtype=Radiobutton)
		for j in range(4):
			for (i, rd) in enumerate(self.radio_array_vac[:, j]):
				rd = Radiobutton(self.vac_frame)
				rd['variable'] = self.status_array[self.vac_ids[i]]
				rd['value'] = j
				rd.grid(row=j+2, column=i+2)

	def update_var(self, *args):
		""" Method to update the values of the variables """
		for (i, var) in enumerate(self.override_array):
			self.is_override[i] = var.get()

		for (i, var) in enumerate(self.status_array):
			self.override_state[i] = var.get()

		data_dict = {'override': self.is_override,
					 'state': self.override_state}

		self.pub.publish(str(data_dict))

	def select_all_leg(self, *args):
		""" Method to select/deselect the override of all the legged robot """
		for (i, var) in enumerate(self.override_array):
			if i in self.leg_ids:
				var.set(self.all_leg.get())

	def select_all_vac(self, *args):
		""" Method to select/deselect the override of all the legged robot """
		for (i, var) in enumerate(self.override_array):
			if i in self.vac_ids:
				var.set(self.all_vac.get())
###################################################


if __name__ == "__main__":
	app = Joystick()
	rospy.init_node("Application", anonymous=True)
	rospy.Subscriber("state", String, app.ros_callback)
	app.start()
