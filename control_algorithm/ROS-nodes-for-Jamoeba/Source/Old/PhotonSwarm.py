'''
Python script used to communicate
with the Particle Photon microcontroller
and make a ROS node out of it
'''

####################################################
from pyparticleio.ParticleCloud import ParticleCloud
from numpy import array, sin, cos
import rospy
from std_msgs.msg import String
###################################################


class PhotonSwarm:
	def __init__(self, account='Amin', num=8):
		""" Connect to the cloud """
		# Set the access tokens and connect to the cloud
		access_token = {
			"Amin": "8269d62ca4178e500166245f1835a2e96f4a0de4",
			"Bruno": "7920605ae163b88edf6384e77193cfef42d3cb54"
		}
		self.particle_cloud = ParticleCloud(access_token[account])

		self.num_robot = num

		# Which controller are you trying to communicate to?
		self.photon_target = "v2-r2"

		""" Connect to a device to publish on the cloud"""
		self.all_devices = self.particle_cloud.devices  # Get all the devices names
		is_found = False  # Just a tag to see i the Photon target is found

		for device in self.all_devices:
			if "{0}".format(device) == self.photon_target:
				is_found = True

		if is_found:
			print(self.photon_target + " found")
		else:
			print(self.photon_target + " not found")

		self.ID_array = array(["0"])
		self.pwm = {'0': (0, 0, 0)}
		for i in range(self.num_robot):
			self.ID_array[i] = str(i)
			self.pwm[i] = (0, 0, 0)

		self.xd = 0.0
		self.yd = 0.0
		self.k = 10.0

	def callback(self, data):
		data_dict = eval(data)

		for ids in self.ID_array:
			x = data_dict[ids][0]
			y = data_dict[ids][1]
			t = data_dict[ids][2]
			st_er = array([self.xd - x],
						  [self.yd - y])
			r = array([cos(t), -sin(t)],
					  [sin(t), cos(t)])
			u = r.dot(st_er)
			ku = self.k*u[0, 0]
			kv = self.k*u[1, 0]

			if ku < 5 and kv < 5:
				en = 0
			else:
				en = 1

			self.pwm[ids] = (ku, kv, en)

		self.all_devices[self.photon_target].publish("state", str(self.pwm))


if __name__ == "__main__":
	photon = PhotonSwarm()
	rospy.init_node("Photon", anonymous=True)
	rospy.Subscriber("state", String, photon.callback)
	rospy.spin()
