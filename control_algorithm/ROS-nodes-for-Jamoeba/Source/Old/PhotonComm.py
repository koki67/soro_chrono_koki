'''
Python script used to communicate
with the Particle Photon microcontroller
and make a ROS node out of it
'''

####################################################
from pyparticleio.ParticleCloud import ParticleCloud
import rospy
from std_msgs.msg import String
###################################################

ShowData = False


class Photon:
	def __init__(self, account='Amin'):
		""" Connect to the cloud """
		# Set the access tokens and connect to the cloud
		access_token = {
			"Amin": "8269d62ca4178e500166245f1835a2e96f4a0de4",
			"Bruno": "7920605ae163b88edf6384e77193cfef42d3cb54"
		}
		self.particle_cloud = ParticleCloud(access_token[account])

		# Which controller are you trying to communicate to?
		self.photon_target = "v2-r2"

		""" Connect to a device to publish on the cloud"""
		self.all_devices = self.particle_cloud.devices  # Get all the devices names
		is_found = False  # Just a tag to see i the Photon target is found

		for device in self.all_devices:
			if "{0}".format(device) == self.photon_target:
				is_found = True

		if is_found:
			print(self.photon_target + " found")
		else:
			print(self.photon_target + " not found")

	def callback(self, data):
		if ShowData:
			print(data)

		self.all_devices[self.photon_target].publish("state", data.data)


if __name__ == "__main__":
	photon = Photon()
	rospy.init_node("Photon", anonymous=True)
	rospy.Subscriber("state", String, photon.callback)
	rospy.spin()
