'''
Python classes for our Photon swarm
'''

####################################################
from pyparticleio.ParticleCloud import ParticleCloud
import json
from numpy import zeros, sin, cos, ones, vdot, identity, pi, argmin, array
from math import atan2, sqrt
from scipy.optimize import LinearConstraint, Bounds, minimize, linprog
import rospy
from std_msgs.msg import String
from time import sleep, clock
####################################################


####################################################
class Photon:
    """
	Class used to control a whole swarm of photon vibration robots
	"""

    def __init__(self, number_of_robots=1, account='Amin'):
        """ Create an instance of a Photon microcontroller swarm """
        self.number_of_robots = number_of_robots  # Set the number of robots in your swarm

        # Set the access tokens and connect to the cloud
        access_token = {
            "Amin": "8269d62ca4178e500166245f1835a2e96f4a0de4",
            "Bruno": "7920605ae163b88edf6384e77193cfef42d3cb54"
        }
        self.particle_cloud = ParticleCloud(access_token[account])

        self.all_devices = self.particle_cloud.devices
        for device in self.all_devices:
            self.first_device_name = "{0}".format(device)  # To publish, you need to know one device
            break

        self.pwm = {'0': (0, 0, 0)}  # Create the pwm dictionary
        for i in range(self.number_of_robots):
            self.pwm[str(i)] = (0, 0, 0)

        self.pwm_signal = zeros((self.number_of_robots, 2))

        """
		For en :
		0 = PWM Off
		1 = PWM On
		2 = Pump On
		3 = Pump Off
		"""
        self.en = 0

    def _update_pwm(self):
        """ Method to update the pwm of each robots """
        # *** May need to be optimized for larger swarms
        for i in range(self.number_of_robots):
            self.pwm[str(i)] = (self.pwm_signal[i, 0], self.pwm_signal[i, 1], self.en)

    def publish(self):
        """ Method to publish the current pwm"""
        self._update_pwm()
        self.all_devices[self.first_device_name].publish("state", str(self.pwm))

    def set_pwm(self, robot_id=0, right=-1, left=-1):
        """ Method to set the PWM signals of the desired robot """
        if 0 <= right <= 150:
            self.pwm_signal[robot_id, 0] = right

        if 0 <= left <= 150:
            self.pwm_signal[robot_id, 1] = left

    def stop_robots(self):
        """ Method to stop all the robots """
        self.en = 0
        self.publish()

    def toggle_en_pwm(self):
        """ Method to toggle the enable variable for the pwm """
        if self.en == 0:
            self.en = 1
        elif self.en == 1:
            self.en = 0
        elif self.en == 2:
            self.en = 3
            self.publish()
            self.en = 1
        elif self.en == 3:
            self.en = 1

        self.publish()

    def toggle_en_pump(self):
        """ Method to toggle the enable variable for the pump """
        if self.en == 0:
            self.en = 2
        elif self.en == 1:
            self.en = 0
            self.publish()
            self.en = 2
        elif self.en == 2:
            self.en = 3
        elif self.en == 3:
            self.en = 2

        self.publish()

    def get_en(self):
        """ Method to get the enable variable """
        return self.en
####################################################


####################################################
class StringPhoton:
    """
	Class used to control a whole swarm of photon string robots
	"""

    def __init__(self, account="Amin"):
        """ Create an instance of a Photon microcontroller swarm """
        self.num = 8  # Set the number of robots in your swarm

        # Set the access tokens and connect to the cloud
        access_token = {
            "Amin": "8269d62ca4178e500166245f1835a2e96f4a0de4",
            "Bruno": "7920605ae163b88edf6384e77193cfef42d3cb54"
        }
        self.particle_cloud = ParticleCloud(access_token[account])

        self.all_devices = self.particle_cloud.devices
        for device in self.all_devices:
            self.first_device_name = "{0}".format(device)  # To publish, you need to know one device
            break

        # Set the subscriber
        self.all_devices[self.first_device_name].subscribe("angle", self.angle_callback)

        # Set the ROS node
        rospy.init_node("PhotonString", anonymous=True)
        rospy.Subscriber("command", String, self.command_callback)
        self.pub = rospy.Publisher('angle', String, queue_size=10)
        rospy.spin()

    def command_callback(self, data):
        """ Method to send the PWM signals to the cloud """
        self.all_devices[self.first_device_name].publish("command", data)

    def angle_callback(self, data):
        """" Method to receive the angle from the cloud """
        data_dict = json.loads(data.data)
        to_pub = str(data_dict["data"])
        self.pub.publish(to_pub)
####################################################


####################################################
class VibrationPhoton:
    """
    Class used to control a whole swarm of photon string robots
    """
    def __init__(self, threshold_angle=20., account="Amin", vacuum=False, stop_dist=20.):
        """ Create an instance of a Photon microcontroller swarm """
        self.num = 18  # Set the number of robots in your swarm
        self.leg_ids = (0, 2, 4, 6, 8, 9, 10, 12, 14, 16)
        self.vac_ids = (1, 3, 5, 7, 11, 13, 15, 17)
        self.t_a = threshold_angle
        self.vacuum = vacuum
        self.t = zeros(10)
        self.bounds = Bounds(list(ones(10) * 0.1), list(ones(10)))
        self.linear_constraint = LinearConstraint(identity(10), zeros(10), ones(10))
        self.x0 = zeros(10)
        self.last = clock()
        self.vac_state = True
        self.opt = "simplex"
        self.angle_mode = "mean"
        self.stop_dist_default = stop_dist
        self.stop_dist = ones(self.num)*stop_dist
        self.stopped_robot = 0
        self.at_least_one_stopped = False
        self.batt_level = ones(self.num)*-1

        # Set the override values
        self.is_override = zeros(self.num, dtype=bool)
        self.override_state = zeros(self.num)

        # Set the access tokens and connect to the cloud
        access_token = {
            "Amin": "8269d62ca4178e500166245f1835a2e96f4a0de4",
            "Bruno": "7920605ae163b88edf6384e77193cfef42d3cb54"
        }
        self.particle_cloud = ParticleCloud(access_token[account])

        self.all_devices = self.particle_cloud.devices
        for device in self.all_devices:
            self.first_device_name = "{0}".format(device)  # To publish, you need to know one device
            break

        # Subscribe to get the battery value
        self.all_devices[self.first_device_name].cloud_subscribe("percent", self.receive_battery)

        self.state = {"0": 0}
        for i in range(1, self.num):
            self.state[str(i)] = 0

    def _f(self, x, *args):
        a = sin(self.t) ** 2 - cos(self.t)
        fx = vdot(a.T, x)
        return fx

    def _f_der(self, x, *args):
        der = sin(self.t) ** 2 - cos(self.t)
        return der

    def _f_hess(self, x, *args):
        H = zeros((10, 10))
        return H

    def set_pwm(self, vac=0, leg=0):
        """ Method to change the pwm of the vibrations motors """
        pwm = {'LEG': leg, 'VAC': vac}

        # Send the values
        self.all_devices[self.first_device_name].publish("pwm", str(pwm))

    def receive_battery(self, data):
        """ Method to store the battery percentage """
        data_dict = json.loads(data.data)
        new_data = eval(data_dict["data"])
        self.batt_level[int(new_data['ID'])] = new_data['Charge']

    def show_battery(self, style="normal"):
        """ Method to store the battery level of every robots """
        if style == "normal":
            for i in range(self.num):
                print("{0:2.0f} ---> {1:.1f}%".format(i, self.batt_level[i]))
        elif style == "bars":
            for i in range(self.num):
                bar = ""
                for j in range(0, 100, 2):
                    if self.batt_level[i] - j > 0:
                        bar += "#"
                    else:
                        bar += " "
                print("{0:2.0f} ---> [{1}] {2:.1f}%".format(i, bar, self.batt_level[i]))

    def get_battery(self, style="normal"):
        """ Method to get the photons battery """
        self.all_devices[self.first_device_name].publish("battery_request", "request")
        last = clock()
        while clock() - last <= 3:
            val = 0
        self.show_battery(style=style)

    def get_alpha(self, data_dict=None):
        """ Method to get the alpha angle of the whole swarm """
        xm = 0.0
        ym = 0.0
        for i in range(self.num):
            xm += data_dict[str(i)][0]
            ym += data_dict[str(i)][1]

        dx = data_dict["18"][0] - xm / self.num
        dy = data_dict["18"][1] - ym / self.num

        return 180. * atan2(dy, dx) / pi

    def get_angle(self, data_dict=None):
        """ Method to get the alpha angle of the whole swarm """
        angle = zeros(self.num)
        for i in range(self.num):
            dx = data_dict["18"][0] - data_dict[str(i)][0]
            dy = data_dict["18"][1] - data_dict[str(i)][1]
            angle[i] = 180.*atan2(dy, dx)/pi

        return angle

    def get_dist(self, data_dict=None):
        """ Method to get the alpha angle of the whole swarm """
        dist = zeros(self.num)
        for i in range(self.num):
            dx = data_dict["18"][0] - data_dict[str(i)][0]
            dy = data_dict["18"][1] - data_dict[str(i)][1]
            dist[i] = sqrt(dx ** 2 + dy ** 2)

        return dist

    def get_min(self, dist=ones(18)):
        """ Method to get the closest legged and vacuum robot """
        leg = zeros(len(self.leg_ids))
        for (i, ids) in enumerate(self.leg_ids):
            leg[i] = dist[ids]

        vac = zeros(len(self.vac_ids))
        for (i, ids) in enumerate(self.vac_ids):
            vac[i] = dist[ids]

        return argmin(dist), self.leg_ids[argmin(leg)], self.vac_ids[argmin(vac)]

    def get_stopped_closest(self, robot_id=0, data_dict=None):
        """ Method to get the id of the closest stopped robot"""
        closest = 10000000
        closest_id = 0
        for (ids, isStopped) in enumerate(self.stop_dist == self.stop_dist_default + 5):
            if isStopped:
                dx = data_dict[str(ids)][0] - data_dict[str(robot_id)][0]
                dy = data_dict[str(ids)][1] - data_dict[str(robot_id)][1]
                dist = sqrt(dx ** 2 + dy ** 2)
                if dist < closest:
                    closest = dist
                    closest_id = ids

        return closest_id

    def get_opt_result(self, theta):
        """ Method to get the result of the optimization """
        result = zeros(10)
        theta %= 360

        # Simple comparison
        if self.opt == "comparison":
            for (i, ids) in enumerate(self.leg_ids):
                if -self.t_a <= theta[ids] <= self.t_a:
                    result[i] = 1  # Go forward
                elif theta[ids] <= -(180 - self.t_a) or theta[ids] >= 180 - self.t_a:
                    result[i] = -1  # Go backward
                else:
                    result[i] = 0  # Vibrate

        # Minimize optimization
        elif self.opt == "minimize":
            res = minimize(self._f, self.x0, method='trust-constr', jac=self._f_der, hess=self._f_hess,
                           options={'verbose': 1}, bounds=self.bounds, constraints=[self.linear_constraint])
            for i in range(len(res)):
                result[i] = int(round(res.x[i]))

        # Simplex optimization
        elif self.opt == "simplex":
            res = linprog(sin(self.t) ** 2 - cos(self.t) ** 2, method='simplex', bounds=[(0, 1)] * 10)
            for (i, ids) in enumerate(self.leg_ids):
                if self.at_least_one_stopped:
                    if (90 + self.t_a) < theta[ids] < (270 - self.t_a):
                        result[i] = -1
                    elif 0 <= theta[ids] < (90 - self.t_a) or (270 + self.t_a) < theta[ids] <= 360:
                        result[i] = 1
                    else:
                        result[i] = 0
                else:
                    result[i] = -res.x[i] * (abs(self.t[i]) > pi / 2) + res.x[i] * (abs(self.t[i]) < pi / 2)

        return result

    def test(self, test="vac"):
        """ Method to test different part of the vacuum robot """
        msg = {"20": 1}

        if test == "vac":
            for i in self.vac_ids:
                msg[str(i)] = 1

        elif test == "leg+":
            for i in self.leg_ids:
                msg[str(i)] = 1

        elif test == "leg-":
            for i in self.leg_ids:
                msg[str(i)] = 2

        elif test == "vib":
            for i in self.vac_ids:
                msg[str(i)] = 2
            for i in self.leg_ids:
                msg[str(i)] = 3

        elif test == "stop":
            for i in range(self.num):
                msg[str(i)] = 0

        elif test == "sleep":
            for i in self.vac_ids:
                msg[str(i)] = 3
            for i in self.leg_ids:
                msg[str(i)] = 4

        self.all_devices[self.first_device_name].publish("state", str(msg))

    def test_run(self):
        """ Methods to do a test run """
        tests = ("vac", "stop", "leg+", "stop", "leg-", "stop", "vib", "stop")
        times = (10., 2., 10., 2., 10., 2., 30., 2.)
        for i in range(len(tests)):
            self.test(test=tests[i])
            sleep(times[i])

    def start_vacuum(self):
        """ Method to put vacuum in the robot """
        self.test(test="vac")
        sleep(15)
        self.test(test="stop")

    def start_ros_node(self, mode="simplex", angle_mode="mean"):
        """ Method used to set the ROS node and start the looping """
        self.opt = mode
        self.angle_mode = angle_mode
        rospy.init_node("PhotonString", anonymous=True)
        rospy.Subscriber("state", String, self.state_callback)
        rospy.Subscriber("override", String, self.override_callback)
        rospy.spin()

    def state_callback(self, data):
        """ Method to send the desired signals to the photons """
        data_dict = eval(data.data)

        alpha = self.get_alpha(data_dict)
        angle = self.get_angle(data_dict)
        dist = self.get_dist(data_dict)
        closest, leg_closest, vac_closest = self.get_min(dist=dist)
        theta = zeros(self.num)

        self.stopped_robot = sum(1 * (self.stop_dist == self.stop_dist_default + 5))
        self.at_least_one_stopped = self.stopped_robot >= 1  # At least one stopped

        if self.at_least_one_stopped:  # If one robot is stopped
            self.angle_mode = "abs_closest"
        else:
            self.angle_mode = "relative"

        # Get the angles values of the legged robot and put them in a array for the optimization
        for (i, ids) in enumerate(self.leg_ids):
            if self.angle_mode == "relative":
                theta[ids] = angle[ids] - data_dict[str(ids)][2]
            elif self.angle_mode == "closest":
                theta[ids] = angle[closest] - data_dict[str(ids)][2]
            elif self.angle_mode == "mean":
                theta[ids] = alpha - data_dict[str(ids)][2]
            elif self.angle_mode == "abs_closest":
                theta[ids] = data_dict[str(self.get_stopped_closest(robot_id=ids, data_dict=data_dict))][2] - data_dict[str(ids)][2]
            self.t[i] = pi * theta[ids] / 180.

        # Get the result of the desired optimization
        result = self.get_opt_result(theta=theta)

        # Set the right values for the legged robots
        for (i, ids) in enumerate(self.leg_ids):
            if result[i] == 1:
                self.state[str(ids)] = 1  # Move forward
            elif result[i] == -1:
                self.state[str(ids)] = 2  # Move backward
            elif result[i] == 0:
                self.state[str(ids)] = 3  # Vibrate
            else:
                self.state[str(ids)] = 0  # Stop

        # Toggle self.vac_state to put the vacuum on and off
        if (clock() - self.last) > 0.5:
            self.vac_state = not self.vac_state
            self.last = clock()

        # Set the right values for the vacuum robots
        for ids in self.vac_ids:
            theta[ids] = alpha - data_dict[str(ids)][2]
            if self.vacuum and self.vac_state:
                self.state[str(ids)] = 1  # Vacuum
            else:
                self.state[str(ids)] = 2  # Vibration

        # Stop if you get close to the target
        for i in range(self.num):
            if dist[i] < self.stop_dist[i]:
                # if sum(1*(i == self.vac_ids)) == 1:  # If it's a vacuum robot
                #     self.state[str(i)] = 1  # Vacuum
                # else:
                self.state[str(i)] = 0  # Stop
                self.stop_dist[i] = self.stop_dist_default + 5
            else:
                self.stop_dist[i] = self.stop_dist_default

        # Override function
        for i in range(self.num):
            if self.is_override[i]:
                self.state[str(i)] = int(self.override_state[i])

        # Send the values
        self.all_devices[self.first_device_name].publish("state", str(self.state))

    def override_callback(self, data):
        """ Method to override the function of the  """
        data_dict = eval(data.data)
        self.is_override = data_dict["override"]
        self.override_state = data_dict["state"]
####################################################


if __name__ == "__main__":
    rob = VibrationPhoton()

    # rob.test_run()

    rob.start_ros_node()
