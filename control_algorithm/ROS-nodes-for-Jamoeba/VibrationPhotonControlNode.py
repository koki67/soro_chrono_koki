'''
Python script used to control
the vibration robot (pink)
'''

####################################################
from Source.Photons import VibrationPhoton
####################################################

# Create the Vibration robot
rob = VibrationPhoton(vacuum=False, stop_dist=10., threshold_angle=1.)

# Print the battery percentage of every robots
rob.get_battery(style="bars")

# Set the PWM for the vibration robots
rob.set_pwm(leg=75, vac=125)

# Run some tests
# rob.test_run()
# rob.start_vacuum()

# Start the control loop
rob.start_ros_node(mode="simplex", angle_mode="relative")

# Once the loop is exited, stop the robots
rob.test(test="stop")

