'''
Python script used to control
the vibration robot (pink)
'''

####################################################
from Source.Photons import VibrationPhoton
####################################################

# Create the Vibration robot
rob = VibrationPhoton(vacuum=False)
"""
The values for the test parameter can be:
"vac" -----> Start the vacuum
"leg+" ----> Turn the legs forwards
"leg-" ----> Turn the legs backwards
"vib" -----> Enabled the vibration on every robots
"stop" ----> Stop all the robots
"sleep" ---> Put all the robots to sleep
"""

rob.get_battery(style="bars")

# rob.test(test="sleep")

# rob.test_run()
