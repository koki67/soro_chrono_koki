'''
Python script used to test
the the vibration pattern
'''

####################################################
import rospy
from std_msgs.msg import String
from Source.Photons import Photon
from Source.Controllers import ControllerForVideo
###################################################

# Create the photons swarm and set the enable to true
robot = Photon(number_of_robots=1)
robot.toggle_en_pwm()

# Create the controller
ctl = ControllerForVideo(photon=robot, robot_id=0, gain=0.9, base_speed=80, angle=0)  # -15 is the straigth angle

# Start ROS
rospy.init_node("Photon", anonymous=True)
rospy.Subscriber("state", String, ctl.ros_callback)
rospy.spin()

# When exiting, disable the robot
robot.toggle_en_pwm()
robot.publish()
