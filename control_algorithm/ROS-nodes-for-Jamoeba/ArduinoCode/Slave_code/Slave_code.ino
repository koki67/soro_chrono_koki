#include <Wire.h>
#include <Servo.h>
#include "LowPower.h"

#define slave_no 1

int sensorValue = 0;
int sensorPin = A0;
int led1 = 11;
int led2 = 10;
Servo myservo; // create servo object to control a servo

void setup()
{
    SerialUSB.begin(9600);
    Wire.begin(slave_no);         // join i2c bus with slave_no:
    //1.  config pattern "0101" on board for address 0x5A
    //2.  config pattern "0011" on board for address 0x58
    //3.  config pattern "0111" on board for address 0x5C
    //4.  config pattern "0100" on board for address 0x59
    //5.  config pattern "1001" on board for address 0x5E
    //6.  config pattern "1000" on board for address 0x5D
    //7.  config pattern "1100" on board for address 0x61
    //8.  config pattern "1010" on board for address 0x5F
    //9.  config pattern "1011" on board for address 0x60
    //10. config pattern "0110" on board for address 0x5B
    Wire.onRequest(requestEvent); // register event
    Wire.onReceive(receiveEvent); // register event
    myservo.attach(9);            // attaches the servo on pin 9 to the servo object
    pinMode(led1, OUTPUT);
    pinMode(led2, OUTPUT);
    digitalWrite(led2, HIGH);
    digitalWrite(led1, HIGH);
}

void loop()
{
    sensorValue = analogRead(sensorPin); // read the value from the sensor:
}

void requestEvent()
{
    byte sendArray[2];
    sendArray[0] = (sensorValue >> 8) & 0xFF;
    sendArray[1] = sensorValue & 0xFF;
    Wire.write(sendArray, 2);
    SerialUSB.print(sendArray[0], BIN); SerialUSB.print(" | "); SerialUSB.println(sendArray[1], BIN);
}

// function that executes whenever data is received from master
// this function is registered as an event, see setup()

void receiveEvent(int howMany)
{
    byte dataArray[howMany];
    for (int i = 0; i < howMany; i++) {
        dataArray[i] = Wire.read();
        SerialUSB.print(dataArray[i], HEX);
    }

    int a = dataArray[0]; // receive byte as a character
    int b = dataArray[1]; // receive byte as a character

    if (a == 1) {
        if (b == 0) {
            digitalWrite(led1, LOW);
        }
        if (b == 1) {
            digitalWrite(led1, HIGH);
        }
    }

    if (a == 2) {
        if (b == 0) {
            digitalWrite(led2, LOW);
        }
        if (b == 1) {
            digitalWrite(led2, HIGH);
        }
    }

    if (a == 3) {
        myservo.write(dataArray[1]);
    }

    if (a == 4) { //reset
        for (int i = 1; i < 5; i++) {
            digitalWrite(led1, HIGH);
            delay(500);
            digitalWrite(led1, LOW);
            delay(500);
        }
        NVIC_SystemReset(); // processor software reset
    }

    if (a == 5) {
        digitalWrite(led2, LOW);
        for (int i = 1; i < 5; i++) {
            digitalWrite(led2, HIGH);
            delay(500);
            digitalWrite(led2, LOW);
            delay(500);
        }
        digitalWrite(led2, LOW);
        digitalWrite(led1, LOW);
        LowPower.standby();
    }
}
