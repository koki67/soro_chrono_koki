# -*- coding: utf-8 -*-
"""
Created on Mon Dec 30 09:50:29 2019

@author: qiyua
"""
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import matplotlib.pyplot as plt

def frange(start, stop, step):
    i = start
    while i<stop:
        yield i
        i += step

point_radius = 5
area = np.pi*point_radius*point_radius

a=50
c=100

n_a=10
n_c=20


x=[]
y=[]
z=[]

for u in frange(0, 2*np.pi, np.pi/n_c):
    for v in frange(0,2*np.pi,np.pi/n_a):
       
        x.append(np.cos(u)*(c+a*np.cos(v)))
        y.append(a*np.sin(v))
        z.append(np.sin(u)*(c+a*np.cos(v)))

x=np.array(x)
y=np.array(y)
z=np.array(z)

fig = plt.figure()
fig.set_size_inches(6,6)
ax = fig.add_subplot(111, projection='3d')
ax.scatter(x, y, z, s=area, c='r', marker='o',edgecolors='b')

ax.set_xlabel('X Label')
ax.set_xlim([np.min(x),np.max(x)])
ax.set_ylabel('Y Label')
ax.set_ylim([np.min(x),np.max(x)])
ax.set_zlabel('Z Label')
ax.set_zlim([np.min(x),np.max(x)])

plt.show()