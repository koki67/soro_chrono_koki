# -*- coding: utf-8 -*-
"""
Created on Fri Jul  5 12:40:47 2019

@author: dmulr
"""
# Import libraries
import numpy as np
from bridson import poisson_disc_samples
import math
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
from matplotlib import animation
from openopt import LCP
import sys






# In[Create compression matrix]:

#  Create compression matrix
def createCompressionLCP(HN,HT,UNA,UTA,mu,M,g_count):
    # create empty A matrix
    A=np.zeros((5*g_count,5*g_count))
    # Combine all the H matrices
    W=np.concatenate((HN,HT,-HT),axis=1)
    # Create first term of A matrix
    W=np.matmul(np.matmul(W.T,np.linalg.inv(M)),W)
    # create empty N matrix
    N=np.zeros((2*g_count,3*g_count))
    N[:g_count,:g_count],N[g_count:2*g_count,:g_count]=mu*np.eye(g_count),mu*np.eye(g_count)
    N[:g_count,g_count:2*g_count],N[g_count:2*g_count,2*g_count:3*g_count]=-np.eye(g_count),-np.eye(g_count)

    # Create empty Is matrix
    IsT=np.zeros((3*g_count,2*g_count))
    IsT[g_count:2*g_count,:g_count],IsT[2*g_count:3*g_count,g_count:2*g_count]=np.eye(g_count),np.eye(g_count)

# put the matrices together
    A=np.concatenate((W,IsT),axis=1)
    Ap=np.concatenate((N,np.zeros((2*g_count,2*g_count))),axis=1)
    A=np.concatenate((A,Ap),axis=0)

    # Create empy B column matrix
    B=np.zeros((5*g_count,1))
    B[:g_count,0],B[g_count:2*g_count,0],B[2*g_count:3*g_count,0]=UNA[:,0],UTA[:,0],-UTA[:,0]


    return A,B


# In[ create expansion matrix]
def createExpansionLCP(HN,HT,UNC,UTC,z,mu,M,en,et,g_count):
    # Create empty A matrix
    A=np.zeros((5*g_count,5*g_count))
    # COmbine the H matrices
    W=np.concatenate((HN,HT,-HT),axis=1)
    # Solve first entry of the A matrix
    W=np.matmul(np.matmul(W.T,np.linalg.inv(M)),W)
    # create empty N matrix
    N=np.zeros((2*g_count,3*g_count))
    N[:g_count,:g_count],N[g_count:2*g_count,:g_count]=mu*np.eye(g_count),mu*np.eye(g_count)
    N[:g_count,g_count:2*g_count],N[g_count:2*g_count,2*g_count:3*g_count]=-np.eye(g_count),-np.eye(g_count)

    # Create empty Is matrix
    IsT=np.zeros((3*g_count,2*g_count))
    IsT[g_count:2*g_count,:g_count],IsT[2*g_count:3*g_count,g_count:2*g_count]=np.eye(g_count),np.eye(g_count)
    #empty zero matrix
    Z=np.zeros((2*g_count,2*g_count))
    #combine A matrices
    A=np.concatenate((W,IsT),axis=1)
    Ap=np.concatenate((N,Z),axis=1)
    A=np.concatenate((A,Ap),axis=0)


    # term for B matrix
    # lambda term
    Lambda=en*z
    # little g matrix
    g=np.concatenate((UNC,UTC,-UTC),axis=0)
    # Empty B matrix
    B=np.zeros((5*g_count,1))

    # Define Nbar matrix
    Nbar=N
    # fill in Nbar matrix
    Nbar[:g_count,2*g_count:3*g_count],N[g_count:2*g_count,g_count:2*g_count]=(1-et)*np.eye(g_count),(1-et)*np.eye(g_count)
    # Fill in the B matrix
    B[:3*g_count]=np.matmul(W,Lambda)+g

    # do more matrix stuff
    B[3*g_count:5*g_count]=np.matmul(Nbar,Lambda)
    B=B.ravel()
    # Return A and B matrix
    return A,B


# In[]
# create classes for obstacles
class obstacles:
    count=0
    def __init__(self):
        self.index=obstacles.count
        obstacles.count += 1


# Create line
class line2d(obstacles):
    def __init__(self,x1,y1,x2,y2):
        obstacles.__init__(self)
        self.x1=x1
        self.x2=x2
        self.y1=y1
        self.y2=y2
        self.z1=0
        self.z2=0
        self.r1=np.array([[self.x1,self.y1,self.z1]])
        self.r2=np.array([[self.x2,self.y2,self.z2]])
        self.type='obstacle'
        self.geom='lineObstacle'

#Horizontal line obstacle
class horizontalLine(obstacles):
    def __init__(self,y0):
        obstacles.__init__(self)
        self.y=y0       # postion
        self.type='obstacle'  # tell it what it is
        self.geom='horizontalLine'   # geometry

# Vertical line obstacle
class verticalLine(obstacles):
    def __init__(self,x0):
        obstacles.__init__(self)
        # x intersection
        self.x=x0
        # type of object it is
        self.type='obstacle'
        # specific geometry
        self.geom='verticalLine'


# Create circular obstacle
class circleObstacle(obstacles):
    def __init__(self,x,y,R):
        obstacles.__init__(self)
        self.x=x
        self.y=y
        self.R=R
        self.z=0
        self.r=np.array([[self.x,self.y,self.z]])
        self.type='obstacle'
        self.geom='circleObstacle'

# In[] create balls
class rigidBody2D:

    count=0

    def __init__(self, x, y, phi, xv, yv, phiv, M, I):
        self.index=rigidBody2D.count
        rigidBody2D.count += 1
        self.q=np.array([[x,y,phi]]).T
        self.x,self.y,self.phi=x,y,phi
        self.v=np.array([[xv,yv,phiv]]).T
        self.M=np.zeros((3,3))
        self.M[0,0],self.M[1,1],self.M[2,2]=M,M,I


    def updatePosition(self,q):
        self.q=q
        self.x,self.y,self.phi=q[0,0],q[1,0],q[2,0]

    def updateVelocity(self,v):
        self.v=v

    def getPosition(self):
        return(self.q[0,0],self.q[1,0],self.q[2,0])

class circle(rigidBody2D):
    count=0

    def __init__(self, x, y, phi, xv, yv, phiv, R, M, I):
        circle.count += 1
        # tell it its ridged
        self.type='rigidBody2D'
        # geometry is a circle
        self.geom='circle'
        # radius
        self.R=R
        # Tell it its mass
        #self.M=rho*np.pi*(self.R)**2
        self.M=M
        self.mass=self.M
        # its inertia
        self.I=I
        rigidBody2D.__init__(self, x, y, phi, xv, yv, phiv, self.M, self.I)


class ball(rigidBody2D):
    count=0

    def __init__(self, x, y, phi, xv, yv, phiv, R, M,I):
        ball.count += 1
        # tell it its ridged
        self.type='rigidBody2D'
        # geometry is a circle
        self.geom='ball'
        # radius
        self.R=R
        # Tell it its mass
        self.M=M
        self.mass=self.M
        # its inertia
        self.I=I
        rigidBody2D.__init__(self, x, y, phi, xv, yv, phiv, self.M, self.I)

# In[]

#CREATE GLOBAL MATRIX
def createGlobalMatrices(obj):

    #Total number of 2D bodies
    nb=rigidBody2D.count
    print(nb)
    # empy M matrix
    M=np.zeros((3*nb,3*nb))
    # empty q matrix this is x, y and phi
    q=np.zeros((3*nb,1))
    #  velocity matrix so vx, vy, phi dot.
    v=np.zeros((3*nb,1))

    # fill the matrices
    for i in range (0,len(obj)):
        # only fill if a ruidged body and not a wall
        if(obj[i].type=='rigidBody2D'):
            ind=obj[i].index
            # Mass matrix
            M[3*ind:3*ind+3,3*ind:3*ind+3]=obj[i].M
            # create position matrix
            q[3*ind:3*ind+3,:]=obj[i].q
            # create velocity matrix
            v[3*ind:3*ind+3,:]=obj[i].v
    return (M,q,v)
# In[]
#UPDATE COORDINATES
def updateCoordinates(obj,qb,vb):
    for i in range (0,len(obj)):
        # classify if object is a ridged body
        if(obj[i].type=='rigidBody2D'):

            ind=obj[i].index

            # update position
            obj[i].updatePosition(qb[3*ind:3*ind+3].reshape(3,1))
            # update velocities
            obj[i].updateVelocity(vb[3*ind:3*ind+3].reshape(3,1))

# In[]
#DISTANCE BETWEEN TWO BALLS #(rD) TERM
def dist2D(x1,y1,x2,y2):
    return np.sqrt((x1-x2)**2+(y1-y2)**2)


# In[] FIND GAP FUNCTION
def calculateGaps(obj):
    # empty gap matrix
    g=np.zeros((len(obj),len(obj)))
    # empty indices matrix
    ind=[]

    for i in range (0,len(obj)):
        for j in range (i+1,len(obj)): # plus one is to ensure it only calculates each gap once

    # if the geometry is a circle and a circle
            if((obj[i].geom=='circle' and obj[j].geom=='circle') or (obj[j].geom=='circle' and obj[i].geom=='circle')):
                g[i,j]=dist2D(obj[i].x,obj[i].y,obj[j].x,obj[j].y)-(obj[i].R+obj[j].R)
    # if geomtery is a circle and a horizontal line
            if ((obj[i].geom=='circle' and obj[j].geom=='horizontalLine')):
                g[i,j]=dist2D(0,obj[i].y,0,obj[j].y)-obj[i].R

            if ((obj[i].geom=='horizontalLine' and obj[j].geom=='circle')):
                g[i,j]=dist2D(0,obj[i].y,0,obj[j].y)-obj[j].R
    # if geomtry is vertical line and a circle
            if ((obj[i].geom=='verticalLine' and obj[j].geom=='circle')):
                g[i,j]=dist2D(obj[i].x,0,obj[j].x,0)-obj[j].R

            if ((obj[i].geom=='circle' and obj[j].geom=='verticalLine')):
                g[i,j]=dist2D(obj[i].x,0,obj[j].x,0)-obj[i].R
    # If geometry is a circle and acircle obstacle
            if ((obj[i].geom=='circle' and obj[j].geom=='circleObstacle') or (obj[j].geom=='circleObstacle' and obj[i].geom=='circle')):
                g[i,j]=dist2D(obj[i].x,obj[i].y,obj[j].x,obj[j].y)-(obj[i].R+obj[j].R)

            if ((obj[i].geom=='circle' and obj[j].geom=='ball') or (obj[j].geom=='ball' and obj[i].geom=='circle')):
                g[i,j]=dist2D(obj[i].x,obj[i].y,obj[j].x,obj[j].y)-(obj[i].R+obj[j].R)
    # if a collision is dedectid meaning g is less than zero it will save the indices or the object numbers that collided.
            if(g[i,j]<0):
                # add the number of the two objects colliding
                ind.append([i,j])
    # convert to array
    ind=np.array(ind)
    # flatten it
    #ind.flatten()
    # make sure its int 64. Not sure why but it wasnt liking it in any other form
    ind=np.int64(ind)

    return g,ind

# In[]

# h MATRIX LOCAL matrix
def geth(theta,phi,a,b):
    #A matrix
    A=np.array([[-np.sin(phi), -np.cos(phi)],[np.cos(phi),-np.sin(phi)]])
    # f matrix
    f=np.array([1*np.cos(theta),1*np.sin(theta)])
    temp=np.dot(A,f)
    temp=np.array(temp)
    f1,f2=temp[0],temp[1]
    h=np.array([a,b,a*f1+b*f2])
    h=np.transpose(h)

    return(h)
# In[]
    # Big H so the global H matrix
def getH(obj,ind,ndof):
    s2=np.shape(ind)
    alpha=s2[0]
    HN,HT=np.zeros((ndof,alpha)),np.zeros((ndof,alpha))
    #ind=np.array(ind)
    #ind.flatten()
    for g in range (alpha):
        # declare indices
        i=ind[g,0]
        j=ind[g,1]

        # if interaction is with horizontal wall and circle
        if((obj[i].geom=='circle' and obj[j].geom=='horizontalLine') or (obj[j].geom=='circle' and obj[i].geom=='horizontalLine')):
            # If the contact is between a
            if(obj[i].geom=='circle' and obj[j].geom=='horizontalLine'):
                ii=obj[i].index
                phi=obj[i].phi
                jj=obj[j].index

            else:
                ii=obj[j].index
                phi=obj[j].phi
                jj=obj[i].index

            if((obj[i].y-obj[j].y)<0):
                theta=np.pi/2-phi
                HN[3*ii:3*ii+3,g]=geth(theta,phi,0,-1)
                HT[3*ii:3*ii+3,g]=geth(theta,phi,-1,0)
            else:
                theta=3*np.pi/2-phi
                HN[3*ii:3*ii+3,g]=geth(theta,phi,0,1)
                HT[3*ii:3*ii+3,g]=geth(theta,phi,1,0)

        # if interaction is with vertical wall and circle

        elif((obj[i].geom=='circle' and obj[j].geom=='verticalLine') or (obj[j].geom=='circle' and obj[i].geom=='verticalLine')):

            if(obj[i].geom=='circle' and obj[j].geom=='verticalLine'):
                ii=obj[i].index
                phi=obj[i].phi
                jj=obj[j].index
            else:
                ii=obj[j].index
                phi=obj[j].phi
                jj=obj[i].index

            if((obj[i].x-obj[j].x)>0):
                theta=np.pi-phi
                HN[3*ii:3*ii+3,g]=geth(theta,phi,1,0)
                HT[3*ii:3*ii+3,g]=geth(theta,phi,0,-1)
            else:
                theta=2*np.pi-phi
                HN[3*ii:3*ii+3,g]=geth(theta,phi,-1,0)
                HT[3*ii:3*ii+3,g]=geth(theta,phi,0,1)

        # if interaction is with circle and circle
        elif((obj[i].geom=='circle' and obj[j].geom=='circle')):
            ii=obj[i].index
            jj=obj[j].index
            #Vector from i to j
            x1=obj[j].q[0]-obj[i].q[0]
            y1=obj[j].q[1]-obj[i].q[1]
            #Calculate counter-clockwise angle between the two vectors beginning from [1,0]
            alpha1=math.atan2(y1,x1)

            theta1=alpha1-obj[i].phi
            a,b=-np.cos(alpha1),-np.sin(alpha1)
            #out=geth(theta1,obj[i].phi,a,b)
            HN[3*ii:3*ii+3,g]=geth(theta1,obj[i].phi,a,b)
            HT[3*ii:3*ii+3,g]=geth(theta1,obj[i].phi,b,-a)   ########Not sure of this

            alpha2=alpha1+np.pi
            theta2=alpha2-obj[j].phi
            HN[3*jj:3*jj+3,g]=-geth(theta2,obj[j].phi,a,b)
            HT[3*jj:3*jj+3,g]=-geth(theta2,obj[j].phi,b,-a)   ########Not sure of this


        elif((obj[i].geom=='circleObstacle' and obj[j].geom=='circle') or (obj[j].geom=='circleObstacle' and obj[i].geom=='circle')):
            if (obj[i].geom=='circle' and obj[j].geom=='circleObstacle'):

                ii=obj[i].index
                jj=obj[j].index
                phi=obj[i].phi

                x1=obj[j].x-obj[i].q[0]
                y1=obj[j].y-obj[i].q[1]

            else:
                ii=obj[j].index
                phi=obj[j].phi
                jj=obj[i].index
                x1=obj[j].q[0]-obj[i].x
                y1=obj[j].q[0]-obj[i].y
                        #Vector from i to j



            #Calculate counter-clockwise angle between the two vectors beginning from [1,0]
            alpha1=math.atan2(y1,x1)

            theta1=alpha1-obj[i].phi
            a,b=-np.cos(alpha1),-np.sin(alpha1)
            #out=geth(theta1,obj[i].phi,a,b)
            HN[3*ii:3*ii+3,g]=geth(theta1,phi,a,b)
            HT[3*ii:3*ii+3,g]=geth(theta1,phi,b,-a)   ########Not sure of this

# In[Between robots and ball]
        elif((obj[i].geom=='ball' and obj[j].geom=='circle') or (obj[j].geom=='ball' and obj[i].geom=='circle')):
            if (obj[i].geom=='circle' and obj[j].geom=='ball'):

                ii=obj[i].index
                jj=obj[j].index
                phi=obj[i].phi

                x1=obj[j].x-obj[i].q[0]
                y1=obj[j].y-obj[i].q[1]

            else:
                ii=obj[j].index
                phi=obj[j].phi
                jj=obj[i].index
                x1=obj[j].q[0]-obj[i].x
                y1=obj[j].q[0]-obj[i].y
                        #Vector from i to j



            #Calculate counter-clockwise angle between the two vectors beginning from [1,0]
            alpha1=math.atan2(y1,x1)

            theta1=alpha1-obj[i].phi
            a,b=-np.cos(alpha1),-np.sin(alpha1)
            #out=geth(theta1,obj[i].phi,a,b)
            HN[3*ii:3*ii+3,g]=geth(theta1,phi,a,b)
            HT[3*ii:3*ii+3,g]=geth(theta1,phi,b,-a)   ########Not sure of this


    H=np.concatenate((HN,HT),axis=1)

    return (H,HN,HT)
# In[]
def rn():
    return np.random.uniform(-1.,1.)

def poisson_distribution(obj,r1,mass,inertia):
    rLim=.65
    t=np.array(poisson_disc_samples(width=6, height=6, r=rLim))
    for i in range(0,np.size(t[:,0])):
        #R=rLim*np.random.rand()/2

        #x,y,phi,\dot{x},\dot{y},\dot{phi},R,rho

        obj.append(circle(t[i,0],t[i,1],0,rn(),rn(),rn(),r1,mass,inertia))

    return obj



# In[Create gravity Fg]
def gravity(obj,q0):
    Fg=np.zeros_like(q0)
    ndof=np.size(q0)
    count=int(ndof/3)

    for i in range(count):
        Fg[3*i+1,0]=-9.8*obj[i].mass

    return Fg

# In[Create External forces]
def Forces(obj,q0,nb,ni,t,type):
    F=np.zeros_like(q0)
    ndof=np.size(q0)
    count=int(ndof/3)

    ##### Force case 1
    if type==1:
        for i in range(nb):
            if t<500:
                F[3*i,0]=20*np.sin(t)
            else:
                F[3*i,0]=-20
#### Force type 2
    elif type==2:

        for i in range(nb):
            F[3*i,0]=20
    elif type==3:
        for i in range(nb):
            F[3*i+1,0]=-100*np.sin(20*t)+100*np.cos(20*t)

# Force type 4
    elif type==4:

        for i in range(nb):

            if t<50:
                F[3*i,0]=0

            else:
                F[0,0]=250

    elif type==5:

        for i in range(nb-1):
            F[3*i,0]=110
            #F[3*i+1,0]=110

        #for i in (30,31,32,33,34,35):
        #     F[3*i,0]=350


    elif type==6:
        for i in (8,9,10,20,21,22):
            F[3*i,0]=0

        for i in (12,13,14,15,16):
            F[3*i,0]=0
    else:
        print('none')
    return F
# upward y force
#[22, 23, 24, 25, 26, 27, 28]
# downward y force
#[1, 2, 3, 4, 5]
# In[Create stiffness matrix for boundary robots]
def modelStiffness(q,nb,ni,l,k):

    # number of total particles
    n=nb+ni
    # define theta variable
    theta=q
    # reshape it
    theta=np.reshape(theta,(-1,1))


    # create empty distance matrix
    d=np.zeros((nb,1))
    for i in range(nb-1):
        d[i,0]=np.sqrt((theta[3*i,0]-theta[3*i+3,0])**2+(theta[3*i+1,0]-theta[3*i+4,0])**2)
    i=nb-1
    d[i,0]=np.sqrt((theta[3*i,0]-theta[0,0])**2+(theta[3*i+1,0]-theta[1,0])**2)


    K=np.zeros((3*nb,3*nb))

    K[0,0]=k*((d[nb-1,0]-l)/d[nb-1,0]+(d[0,0]-l)/d[0,0])
    K[0,3*nb-3]=-k*((d[nb-1,0]-l)/d[nb-1,0])
    K[0,3]=-k*((d[0,0]-l)/d[0,0])
    K[1,1]=K[0,0]
    K[1,3*nb-2]=K[0,3*nb-3]
    K[1,4]=K[0,3]


    for i in range(1,nb-1):
        K[3*i,3*i]=k*((d[i-1,0]-l)/d[i-1,0]+(d[i,0]-l)/d[i,0])
        K[3*i,3*i-3]=-k*((d[i-1,0]-l)/d[i-1,0])
        K[3*i,3*i+3]=-k*((d[i,0]-l)/d[i,0])
        K[3*i+1,3*i+1]=K[3*i,3*i]
        K[3*i+1,3*i-2]=K[3*i,3*i-3]
        K[3*i+1,3*i+4]=K[3*i,3*i+3]

    i=nb-1
    K[3*i,3*i]=k*((d[i-1,0]-l)/d[i-1,0]+(d[i,0]-l)/d[i,0])
    K[3*i,3*i-3]=-k*((d[i-1,0]-l)/d[i-1,0])
    K[3*i,0]=-k*((d[i,0]-l)/d[i,0])
    K[3*i+1,3*i+1]=K[3*i,3*i]
    K[3*i+1,3*i-2]=K[3*i,3*i-3]
    K[3*i+1,1]=K[3*i,0]

    Kb=K
    temp=np.zeros((3*nb,3*ni))
    K=np.concatenate((Kb,temp),axis=1)
    temp=np.zeros((3*ni,3*(nb+ni)))
    K=np.concatenate((K,temp),axis=0)

    return K
# In[]
data=np.load('resume13_1.npz')
#np.savez('steadystate_exp3.npz',q0=q[:,nsteps-1],v0=v[:,nsteps-1],nb=nb,ni=ni,r0=r0,r1=r1,r2=r2,m0=m0,m1=m1,m2=m2,i0=i0,i1=i1,i2=i2)
#data=np.load('steadystate_exp3.npz')
q0=data['q0']
v0=data['v0']
nb=data['nb']
ni=data['ni']
r0=data['r0']
r1=data['r1']
r2=data['r2']
m0=data['m0']
m1=data['m1']
m2=data['m2']
i0=data['i0']
i1=data['i1']
i2=data['i2']
#Creating the bounding box with left, right, up, and down coordinates
left,right,up,down=-5,14,20,-2.75


# create internal and external boundary
#nb=23


#x,y,phi,\dot{x},\dot{y},\dot{phi},R,rho
obj=[]

# initial position of boundary particles
for i in range(nb-1):
    x=q0[3*i]
    y=q0[3*i+1]
    phi=q0[3*i+2]
    vx=v0[3*i]
    vy=v0[3*i+1]
    phidot=v0[3*i+2]
    R=r0
    M=m0
    I=i0
    obj.append(circle(x,y,phi,vx,vy,phidot,R,M,I))
i=nb-1
print(i)
x=q0[3*i]
y=q0[3*i+1]
phi=q0[3*i+2]
vx=v0[3*i]
vy=v0[3*i+1]
phidot=v0[3*i+2]
R=r0
M=m0
I=i0
obj.append(circle(x,y,phi,vx,vy,phidot,R,M,I))

# initial postions of internal particles
for i in range(nb,nb+ni-1):
    x=q0[3*i]
    y=q0[3*i+1]
    phi=q0[3*i+2]
    vx=v0[3*i]
    vy=v0[3*i+1]
    phidot=v0[3*i+2]
    R=r1
    M=m1
    I=i1
    obj.append(circle(x,y,phi,vx,vy,phidot,R,M,I))

i=nb+ni-1
print(i)
x=q0[3*i]
y=q0[3*i+1]
phi=q0[3*i+2]
vx=v0[3*i]
vy=v0[3*i+1]
phidot=v0[3*i+2]
R=r2
M=m2

I=i2
obj.append(circle(x,y,phi,vx,vy,phidot,R,M,I))

obj.append(verticalLine(left))
obj.append(verticalLine(right))
obj.append(horizontalLine(up))
obj.append(horizontalLine(down))


# In[OBSTACLES]
ob_option=3
if ob_option==1:
    for i in range(4):
        obj.append(circleObstacle(6*i-4,-8,1))
        obj.append(circleObstacle(6*i-2,-14,1))
        obj.append(circleObstacle(6*i-4,-20,1))
        obj.append(circleObstacle(6*i-2,-26,1))
        obj.append(circleObstacle(6*i-4,-32,1))
        obj.append(circleObstacle(6*i-2,-38,1))


elif ob_option==2:
    for i in range(3):
        obj.append(circleObstacle(15,6*i,1))
else:
    print('none')


# In[]
#Creating Mass, q, and v matrices
M,q0,v0=createGlobalMatrices(obj)
# degrees of freedom
ndof=np.size(q0)

#Gravity turns on and off gravity
# create empty matrix
g=0
if g==1:
    Fg=gravity(obj,q0)
else:
    Fg=np.zeros_like(q0)



#Time parameters
t0,tend,nsteps=0,1,100
time=np.linspace(t0,tend,num=nsteps)

# time step size
h=time[1]-time[0]

#position and velocities. Making the empty matrices
q,v=np.zeros((q0.size,time.size)),np.zeros((v0.size,time.size))

#Initial conditions
q[:,0],v[:,0]=q0.reshape(q0.size,),v0.reshape(v0.size,)

#Integration parameters
t_imp,gamma=1.,.5

#Impact and Friction properties
en,et,mu=-.01,0,.7


# In[] Solving the system
for i in range(time.size-1):
    print(i)
    ####Predicting gap
    #P redicted q
    qbar=q[:,i]+h*gamma*v[:,i]
    K1=modelStiffness(qbar,nb,ni,.125,1210)
    #K2=modelStiffness(qbar,nb,ni,0.,-1210)
    F=Forces(obj,q0,nb,ni,i,5)
    # Create external forces
    Fext=-np.matmul(K1,qbar)+Fg.flatten()+F.flatten()

    #Predicted gap. gbar is a matrix of all gaps between all bodies and ind is the index list of those bodies for which gbar<0
    updateCoordinates(obj,qbar,v[:,i])
    gbar,ind=calculateGaps(obj)
    updateCoordinates(obj,q[:,i],v[:,i])

    #Calculating vfree
    vfree=v[:,i]+h*np.matmul(np.linalg.inv(M),Fext)

    if ind.size<1:
        v[:,i+1]=vfree
    else:
######################Using Lemke's

        H,HN,HT=getH(obj,ind,ndof)
        g_count=len(ind[:,0])

        #Calculating normal and transverse gap velocities at A
        UA=np.matmul(H.T,v[:,i].reshape(ndof,1))
        UA=np.matmul(H.T,vfree.reshape(ndof,1))

        UNA,UTA=UA[:g_count],UA[g_count:2*g_count]

        #Creating the LCP during compression

        A1,B1=createCompressionLCP(HN,HT,UNA,UTA,mu,M,g_count)
        B1=B1.flatten()
        sol = LCP(A1,B1)
        r=sol.solve('lcpsolve')
        f_opt, x_opt = r.ff, r.xf
        z, w = x_opt[x_opt.size/2:], x_opt[:x_opt.size/2]
        z=z[:3*g_count].reshape(3*g_count,1)


        RNAC=z[:g_count,0]
        RTAC=z[g_count:2*g_count,0]-z[2*g_count:3*g_count,0]
        RAC=np.concatenate((RNAC,RTAC),axis=0).reshape(2*g_count,1)

        #Using the reactions to calculate the velocity at the end of compression
        UC=UA+np.matmul(np.matmul(np.matmul(H.T,np.linalg.inv(M)),H),RAC)
        UNC,UTC=UC[:g_count],UC[g_count:2*g_count]
        A2,B2=createExpansionLCP(HN,HT,UNC,UTC,z,mu,M,en,et,g_count)

#
        #Solving the second LCP to solve for the reactions during expansion
        sol = LCP(A2,B2)
        r=sol.solve('lcpsolve')
        f_opt, x_opt = r.ff, r.xf
        z, w = x_opt[x_opt.size/2:], x_opt[:x_opt.size/2]
        z=z[:3*g_count].reshape(3*g_count,1)


        RNP=z[:g_count,0]
        RTP=z[g_count:2*g_count,0]-z[2*g_count:3*g_count,0]
        RP=np.concatenate((RNP,RTP),axis=0).reshape(2*g_count,1)
        RCE=RP+en*RAC

        #Total reaction due to impulses during compression and expansion
        Rk1=RAC+RCE

        #Velocity at the end of the time step
        v[:,i+1]=vfree+(np.matmul(np.matmul(np.linalg.inv(M),H),Rk1)).reshape(ndof,)


    #     For implicit integration of q
    vkt=h*(t_imp*v[:,i+1]+(1-t_imp)*v[:,i])

    #Implicit integration of q
    q[:,i+1]=q[:,i]+vkt



#    dd.append(down)
#    du.append(up)
#    dl.append(left)
#    dr.append(right)
 #   down=down+.001*i
 #   obj[-1]=horizontalLine(down)
np.savez('temp14_1.npz',q=q,nb=nb,ni=ni,obj=obj,left=left,right=right,up=up,down=down,R=R,t0=t0,tend=tend,nsteps=nsteps)
np.savez('resume14_1.npz',q0=q[:,nsteps-1],v0=v[:,nsteps-1],nb=nb,ni=ni,r0=r0,r1=r1,r2=r2,m0=m0,m1=m1,m2=m2,i0=i0,i1=i1,i2=i2)
