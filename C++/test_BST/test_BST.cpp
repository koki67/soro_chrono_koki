// =============================================================================
// PROJECT CHRONO - http://projectchrono.org
//
// Copyright (c) 2014 projectchrono.org
// All rights reserved.
//
// Use of this source code is governed by a BSD-style license that can be found
// in the LICENSE file at the top level of the distribution and at
// http://projectchrono.org/license-chrono.txt.
//
// =============================================================================
// Authors: Alessandro Tasora
// =============================================================================
//
// FEA for thin shells of Kirchhoff-Love type, with BST triangle finite elements
//
// =============================================================================

#include <vector>

#include "chrono/physics/ChBodyEasy.h"
#include "chrono/physics/ChLinkMate.h"
#include "chrono/physics/ChSystemSMC.h"
#include "chrono/physics/ChSystemNSC.h"
#include "chrono/solver/ChIterativeSolverLS.h"
#include "chrono/timestepper/ChTimestepper.h"

#include "chrono/fea/ChElementShellBST.h"
#include "chrono/fea/ChLinkPointFrame.h"
#include "chrono/fea/ChMesh.h"
#include "chrono/fea/ChVisualizationFEAmesh.h"
#include "chrono/fea/ChMeshFileLoader.h"
#include "chrono/fea/ChContactSurfaceNodeCloud.h"
#include "chrono/fea/ChContactSurfaceMesh.h"
#include "chrono/core/ChTimer.h"
#include "chrono_irrlicht/ChIrrApp.h"

#include "chrono_mkl/ChSolverMKL.h"

#include "chrono_postprocess/ChGnuPlot.h"
#include "chrono_thirdparty/filesystem/path.h"

// Remember to use the namespace 'chrono' because all classes
// of Chrono::Engine belong to this namespace and its children...

using namespace chrono;
using namespace chrono::fea;
using namespace chrono::irrlicht;
using namespace chrono::postprocess;
using namespace irr;

// Output directory
const std::string out_dir = GetChronoOutputPath() + "FEA_SHELLS";

int main(int argc, char* argv[]) {
	///
	/// Note
	///
	/// Units are g and cm everywhere
	///
	///

    GetLog() << "Copyright (c) 2020 projectchrono.org\nChrono version: " << CHRONO_VERSION << "\n\n";
	chrono::SetChronoDataPath("C:/Chrono/Builds/chrono-develop/bin/data/"); //Qiyuan x399
	//freopen("donut.txt", "w", stdout);										// Writing to .txt file
	collision::ChCollisionInfo::SetDefaultEffectiveCurvatureRadius(100);  // Effective radius of curvature for all SCM contacts.
	collision::ChCollisionModel::SetDefaultSuggestedMargin(0.05);
	auto default_mat = chrono_types::make_shared<ChMaterialSurfaceSMC>(); //default
	default_mat->SetYoungModulus(1e2);
	default_mat->SetFriction(0.4f);
	default_mat->SetRestitution(0.5);
	default_mat->SetAdhesion(0);

	ChTimer<double> timer;

    // Create (if needed) output directory
    if (!filesystem::create_directory(filesystem::path(out_dir))) {
        std::cout << "Error creating directory " << out_dir << std::endl;
        return 1;
    }

    // Create a Chrono::Engine physical system
    ChSystemSMC my_system;
	my_system.Set_G_acc(ChVector<>(0, -981, 0));

    // Create a mesh, that is a container for groups
    // of elements and their referenced nodes.
    auto my_mesh = chrono_types::make_shared<ChMesh>();
    my_system.Add(my_mesh);

    // my_system.Set_G_acc(VNULL); // to remove gravity effect, or:
    // my_mesh->SetAutomaticGravity(false);  

    std::shared_ptr<ChNodeFEAxyz> nodePlotA;
    std::shared_ptr<ChNodeFEAxyz> nodePlotB;
    std::vector<std::shared_ptr<ChNodeFEAxyz>> nodesLoad;

    ChFunction_Recorder ref_X;
    ChFunction_Recorder ref_Y;

    ChVector<> load_force;

    // BENCHMARK n.3
    // Load and create shells from a .obj file containing a triangle mesh surface
    //
	float c = 40;					// Large torus radius
	float a = 10;					// Torus ring radius
	float init_height = 0;
	if (true) {

		double density = 1;
		double E = 6e2;
		double nu = 0.4;
		double thickness = 0.4;
		const char* donut = "donut2.obj";

		auto melasticity = chrono_types::make_shared<ChElasticityKirchhoffIsothropic>(E, nu);
		auto material = chrono_types::make_shared<ChMaterialShellKirchhoff>(melasticity);
		material->SetDensity(density);

		ChMeshFileLoader::BSTShellFromObjFile(my_mesh, donut, material, thickness);

		// FEA node contacts
		auto mcontactcloud = chrono_types::make_shared<ChContactSurfaceNodeCloud>();
		my_mesh->AddContactSurface(mcontactcloud);
		mcontactcloud->AddAllNodes(1);  // use larger point size to match beam section radius
		mcontactcloud->SetMaterialSurface(default_mat);
		my_system.Add(my_mesh);

		/*
		// FEA mesh contact surface
		auto mcontactsurf = chrono_types::make_shared<ChContactSurfaceMesh>();
		my_mesh->AddContactSurface(mcontactsurf);
		mcontactsurf->AddFacesFromBoundary(0.001);				// do this after my_mesh->AddContactSurface
		mcontactsurf->SetMaterialSurface(default_mat);          // use the SMC penalty contacts
		mcontactsurf->SurfaceAddCollisionModelsToSystem(&my_system);
		// Remember to add the mesh to the system!
		my_system.Add(my_mesh);
		*/
	}

//--------------------------------------
//        Make the big bots    
//--------------------------------------
	// Common material and ID
	auto bot_mat = chrono_types::make_shared<ChMaterialSurfaceSMC>();
	bot_mat->SetFriction(0.4f);
	bot_mat->SetRestitution(0.8);
	bot_mat->SetYoungModulus(6e2);
	int bot_id = 0;

	int bot_number = 8;
	ChVector<> bot_inertia = 0.05 * ChVector<>(1, 1, 1);  //Diagonal moment of inertia of bots
	float bot_mass = 200;
	float bot_radius = 0.8 * a-1;
	std::vector<std::shared_ptr<ChForce >> forces;

	// Create the bots
	if (true) {
		for (float u = 0; u < CH_C_2PI - CH_C_2PI / bot_number; u = u + CH_C_2PI / bot_number) {

			float x = c * cos(u);
			float y = init_height;
			float z = c * sin(u);

			auto bot = chrono_types::make_shared<ChBodyEasySphere>(bot_radius, 1, true);
			bot->SetMaterialSurface(bot_mat);
			bot->SetMass(bot_mass);
			bot->SetInertiaXX(bot_inertia);
			bot->SetPos(ChVector<>(x, y, z));
			bot->SetBodyFixed(false);
			bot->SetId(bot_id);

			// Attach a force to the bot
			auto bot_force = chrono_types::make_shared<ChForce>();
			bot_force->SetMode(ChForce().FORCE);
			bot->AddForce(bot_force);
			bot_force->SetDir(VECT_X);
			forces.push_back(bot_force);

			//Add to counter and add to system
			bot_id++;
			my_system.Add(bot);
		}
		std::cout << "Number of big bots: " << bot_id << "\n";
	}

//--------------------------------------
//        Make the interior    
//--------------------------------------

	const double bot_angle = 2 * tan(bot_radius / c); //[rad] angle taken up by each robot
	const double bot_seperation = (CH_C_2PI / bot_number) - bot_angle; //[rad] angle of free space between each robot
	const double int_radius = 2;                             //[cm] radius of interior particles
	const double int_mass = 0.95;                          //[g] mass of interior particles
	ChVector<> int_inertia = 6027e-6 * ChVector<>(1, 1, 1);    //Diagonal moment of inertia of interior particles
	const double int_c = (0.95 * CH_C_PI * c / int_radius);     //not quite sure what this parameter is exactly
	int int_id = 0;

	if (true) {
		// Create the interior
		for (double torus = 1.15*bot_angle / 2; torus < CH_C_2PI - bot_angle; torus += bot_seperation + bot_angle) {

			for (double n = 0; n < 0.95 * (a - int_radius)-1; n += 2.25 * int_radius) { //for each ring
				double int_a = (1.05 * CH_C_PI * (n / int_radius));
				if (int_a == 0) {
					int_a = 1;
				}
				for (double u = torus; u < torus + bot_seperation; u += CH_C_2PI / (0.85 * int_c)) {        //along tube direction
					for (double v = 0; v < CH_C_2PI - CH_C_2PI / int_a; v += CH_C_2PI / (0.85 * int_a)) {    //along large structure
						// Point on torus
						float x = cos(u) * (c + n * cos(v));
						float y = n * sin(v) + init_height;
						float z = sin(u) * (c + n * cos(v));

						auto int_part = chrono_types::make_shared<ChBodyEasySphere>(int_radius, 1, true);
						//auto int_part = chrono_types::make_shared<ChBodyEasyBox>(int_radius, true);
						int_part->SetMaterialSurface(default_mat);
						int_part->SetMass(int_mass);
						int_part->SetInertiaXX(int_inertia);
						int_part->SetPos(ChVector<>(x, y, z));
						int_part->SetBodyFixed(false);

						//Add to counter and add to system
						int_id++;
						my_system.AddBody(int_part);
					}
				}
			}
		}
		std::cout << "Number of interior particles: " << int_id << "\n";
	}

//--------------------------------------
//        Generating environment        
//--------------------------------------
	
// Create a floor as a simple collision primitive:
	auto mfloor = chrono_types::make_shared<ChBodyEasyBox>(1000, 10, 1000, .7, true);
	mfloor->SetPos(ChVector<>(0, -15, 0));
	mfloor->SetBodyFixed(true);
	mfloor->SetMaterialSurface(default_mat);
	my_system.Add(mfloor);

	auto masset_texture = chrono_types::make_shared<ChTexture>();
	masset_texture->SetTextureFilename(GetChronoDataFile("concrete.jpg"));
	mfloor->AddAsset(masset_texture);
	
	// two falling objects:
	auto mcube = chrono_types::make_shared<ChBodyEasyBox>(10, 10, 10, 1, true);
	mcube->SetPos(ChVector<>(30, 20, 30));
	mcube->SetMaterialSurface(default_mat);
	my_system.Add(mcube);

	auto msphere = chrono_types::make_shared<ChBodyEasySphere>(10, 1, true);
	msphere->SetPos(ChVector<>(-30, 20, -30));
	msphere->SetMaterialSurface(default_mat);
	//my_system.Add(msphere);

	//
	// VISUALIZATION
	//

    auto mvisualizeshellA = chrono_types::make_shared<ChVisualizationFEAmesh>(*(my_mesh.get()));
    //mvisualizeshellA->SetSmoothFaces(true);
    //mvisualizeshellA->SetWireframe(true);
	mvisualizeshellA->E_GLYPH_ELEM_TENS_STRAIN;
	mvisualizeshellA->SetShellResolution(2);
	//mvisualizeshellA->SetBackfaceCull(true);
    my_mesh->AddAsset(mvisualizeshellA);

    auto mvisualizeshellB = chrono_types::make_shared<ChVisualizationFEAmesh>(*(my_mesh.get()));
    mvisualizeshellB->SetFEMdataType(ChVisualizationFEAmesh::E_PLOT_NONE);
    mvisualizeshellB->SetFEMglyphType(ChVisualizationFEAmesh::E_GLYPH_NODE_DOT_POS);
    mvisualizeshellB->SetSymbolsThickness(0.5);
    my_mesh->AddAsset(mvisualizeshellB);
    
	// Create the Irrlicht visualization (open the Irrlicht device,
    // bind a simple user interface, etc. etc.)
    ChIrrApp application(&my_system, L"test_BST",                    // Window title
		core::dimension2d<u32>(1200, 800),  // Window dimensions
		false,                              // Fullscreen?
		true,                              // Shadows?
		true,                              // Anti-Aliasing?
		video::EDT_OPENGL);                 // Graphics Driver

    // Easy shortcuts to add camera, lights, logo and sky in Irrlicht scene:
    application.AddTypicalLogo();
    application.AddTypicalSky();
	application.AddLightWithShadow(irr::core::vector3df(200, 200, 200), irr::core::vector3df(0, 0, 0), 600, 0.2, 6, 50);
	application.AddLight(irr::core::vector3df(-200, -200, 0), 600, irr::video::SColorf(0.6,1,1,1));
    //application.AddTypicalLights();
    application.AddTypicalCamera(core::vector3df(100.f, 30.f, 130.f),core::vector3df(0.f, 0.f, 0.f));
	application.SetContactsDrawMode(ChIrrTools::CONTACT_FORCES);
	application.SetSymbolscale(0.0001f);
    application.AssetBindAll();
    application.AssetUpdateAll();
	application.AddShadowAll();

    //
    // THE SOFT-REAL-TIME CYCLE
    //

    // Change solver to MKL
    auto mkl_solver = chrono_types::make_shared<ChSolverMKL>();
	mkl_solver->UseSparsityPatternLearner(true);
    mkl_solver->LockSparsityPattern(true);
    my_system.SetSolver(mkl_solver);


	auto stepper = chrono_types::make_shared<ChTimestepperEulerImplicit>(&my_system);
	my_system.SetTimestepper(stepper);

	/*
	std::cout << "Using HHT timestepper" << "\n";
	auto stepper = chrono_types::make_shared<ChTimestepperHHT>(&my_system);
	my_system.SetTimestepper(stepper);
	
	stepper->SetAlpha(-0.2);
	stepper->SetMaxiters(10);
	stepper->SetAbsTolerances(1e-5, 1e-3);
	
	// Do not use POSITION mode if there are 3D rigid bodies in the system
	// (POSITION mode technically incorrect when quaternions present).
	stepper->SetMode(ChTimestepperHHT::ACCELERATION);
	stepper->SetScaling(false);
	stepper->SetStepControl(true);
	stepper->SetMinStepSize(1e-8);
	stepper->SetVerbose(false);
	*/
	
	//auto gmres_solver = chrono_types::make_shared<ChSolverGMRES>();
	//gmres_solver->SetMaxIterations(50);
    //my_system.SetSolver(gmres_solver);
	
    
    double timestep = 0.0005;
    application.SetTimestep(timestep);
    my_system.Setup();
    my_system.Update();

    ChFunction_Recorder rec_X;
    ChFunction_Recorder rec_Y;

    double mtime = 0;
	int n = 0;
	while (application.GetDevice()->run()) {
		double t = my_system.GetChTime();
		std::cout << "\n" << "Simulation Time= " << t << "s" << "\n";
		timer.start();
		application.BeginScene();
		application.DrawAll();
		application.DoStep();
		application.EndScene();
		timer.stop();
		std::cout << "Time elapsed= " << timer.GetTimeMillisecondsIntermediate() << "ms" << "\n";
		n++;
		if (t > 0.1) {
			for (int i = 0; i < bot_number; i++) {
				forces[i]->SetMforce(1e5);
				forces[i]->SetDir(ChVector<float>(1, 0, 0));
			}
		}
	}

    return 0;
}
