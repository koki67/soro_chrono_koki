#Author: Esteban Lopez
#To run this, open GnuPlot and open the file. 

#Change this directory to wherever your Cpp is outputting its data to.
cd 'C:/Users/17088/Documents/Soft Robotics Research/Wave Lab Documents/C++/EL - Plot Pendulum/build/Release'

set multiplot
set size 1,0.5
set origin 0,0.5

set title 'Position v Time'
set ylabel 'X Position'
plot 'Pendulum_Position_Data.dat' using 1:2 with lines title 'X-Pos' lc rgb 'black'

set origin 0,0
set xlabel 'Time[sec]'
set ylabel 'Y Position'
plot 'Pendulum_Position_data.dat' using 1:3 with lines title 'Y-Pos' lc rgb 'red'
