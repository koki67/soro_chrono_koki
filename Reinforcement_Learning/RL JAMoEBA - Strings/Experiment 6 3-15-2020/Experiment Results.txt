Notes on Results:
- Training took several hours when "optim_epochs" was set to 1000. This leads me to speculate that this defines how many episodes define an epoch, NOT how many epochs there will be in training.
- The above note is further supported by the iterations including many epidodes before a new iteration was created.