# -*- coding: utf-8 -*-
"""
Created on Mon Mar  2 09:39:29 2020

@author: 17088
"""

from stable_baselines import PPO1
import Strings_3_Experiment_9 as Strings_3

data_collect = True
render = True

env = Strings_3.String_3(data_collect=data_collect, experiment_name='Experiment 9')
model = PPO1.load('ppo1_Strings_3_Experiment_9.zip')

obs=env.reset()

for i in range(1000):
    action, _states = model.predict(obs)
    obs, reward, done, info = env.step(action)
    
    if render:
        env.render()
    
    if done:
        break

if data_collect:
    env.data_export() # Export the data from the simulation
env.close()
    