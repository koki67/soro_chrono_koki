# -*- coding: utf-8 -*-
"""
Created on Thu Mar  5 22:20:55 2020

@author: elopez8
"""
import Strings_3_Experiment_7 as Strings_3

from stable_baselines.common.policies import MlpPolicy #Learn what this is
from stable_baselines import PPO1

env = Strings_3.String_3(experiment_name='Experiment 7')
model = PPO1(MlpPolicy, env, verbose=1)
model.learn(total_timesteps=1000000)
model.save('ppo1_Strings_3_Experiment_7')
env.parameter_export()
env.close()


# Ignore the things below this line for now
"""
For Future Use:
Use this to make a custom network:
    https://stable-baselines.readthedocs.io/en/master/guide/custom_policy.html

import gym

from stable_baselines.deepq.policies import FeedForwardPolicy
from stable_baselines.common.vec_env import DummyVecEnv
from stable_baselines import DQN

# Custom MLP policy of two layers of size 32 each
class CustomDQNPolicy(FeedForwardPolicy):
    def __init__(self, *args, **kwargs):
        super(CustomDQNPolicy, self).__init__(*args, **kwargs,
                                           layers=[32, 32],
                                           layer_norm=False,
                                           feature_extraction="mlp")

# Create and wrap the environment
env = gym.make('LunarLander-v2')
env = DummyVecEnv([lambda: env])

model = DQN(CustomDQNPolicy, env, verbose=1)
# Train the agent
model.learn(total_timesteps=100000)
"""