Experiment 9

What is being tested:
-Adding a 'done' feature if the COM of JAMoEBA strays too far from path
-Adding a massive reward (+100) for reaching the target
-Adding a 'done' feature if the COM of JAMoEBA reaches the target

Training Parameters:
- total_timesteps=10000
- optim_epochs=5