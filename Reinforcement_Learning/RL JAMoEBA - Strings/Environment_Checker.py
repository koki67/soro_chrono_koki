# -*- coding: utf-8 -*-
"""
Created on Mon Mar  2 09:56:07 2020

@author: elopez8

Checks to see if the environment can be used by Stable-Baselines

Link:
    https://stable-baselines.readthedocs.io/en/master/guide/custom_env.html
"""

import gym
from stable_baselines.common.env_checker import check_env
import Strings_3 as Strings


#env = gym.make('CartPole-v1')                  # This one is correct! It is already a gym environment.
#env = ChronoPendulum.Env(render=False)         # Trying to get this one to work
#env = ChronoGymPendulum.ChronoPendulum()       # This is the chrono-gym one! Does not have a render call. That can be gotten from the other if needed.
#env = GoLeftEnv_Sample_Custom_Env.GoLeftEnv()   # Custom environment following Gym API
#env = ChronoGymAnt.ChronoAnt()
env = Strings.String_3()

check_env(env)
# If the environment is good, nothing will show.
# Update: You may get a few Tensorflow and colorize errors, but ignore them.