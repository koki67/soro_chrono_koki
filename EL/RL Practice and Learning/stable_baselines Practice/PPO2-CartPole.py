# -*- coding: utf-8 -*-
"""
Created on Mon Mar  2 08:52:14 2020

@author: elopez8

Link:
    https://stable-baselines.readthedocs.io/en/master/modules/ppo2.html
"""

import gym
from stable_baselines.common.policies import MlpPolicy
from stable_baselines.common import make_vec_env
from stable_baselines import PPO2

# Multiprocess environment
env = make_vec_env('CartPole-v1', n_envs=4)

model = PPO2(MlpPolicy, env, verbose=1)
model.learn(total_timesteps=25000)
model.save("ppo2_cartpole")

del model # Remove to demostrate saving and loading

model = PPO2.load("ppo2_cartpole")
env=gym.make('CartPole-v1')

# Enjoy the trained agent
obs = env.reset()
for i in range(10000):
    action, _states = model.predict(obs)
    obs, rewards, done, info = env.step(action)
    env.render()
    
    if done:
        break
    
env.close()