# -*- coding: utf-8 -*-
"""
Created on Thu Feb 27 20:49:05 2020

@author: elopez8

Link:
    https://stable-baselines.readthedocs.io/en/master/modules/ppo1.html
"""

import gym

from stable_baselines.common.policies import MlpPolicy
# from stable_baselines.common.vec_env import DummyVecEnv
from stable_baselines import PPO1

env = gym.make('CartPole-v1')
# Optional: PPO2 requires a vectorized environment to run
# the env is now wrapped automatically when passing it to the constructor
# env = DummyVecEnv([lambda: env])

model = PPO1(MlpPolicy, env, verbose=1)
model.learn(total_timesteps=10000)
model.save("ppo1_cartpole")

del model # remove to demonstrate saving and loading a model

model=PPO1.load('ppo1_cartpole')

obs = env.reset()
for i in range(10000):
    action, _states = model.predict(obs)
    obs, rewards, done, info = env.step(action)
    env.render()
    
    if done:
        break
    
env.close()