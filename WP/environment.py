import numpy as np
import argparse
import os
import sys
import matplotlib.pyplot as plt
from matplotlib.patches import Circle

class Environment:
    def __init__(self, filename, tau_theta=10000, tau_v=10000):
        self.filename       = filename
        self.n_frames       = 400
        self.dt             = 0.0002
        self.tmax           = 300
        self.n_dim          = 2
        self.k_spring       = 10
        self.k_collision    = 1000
        self.eps            = 10**-10
        self.vmax           = 5
        self.v0             = 0.7
        self.tau_theta      = tau_theta
        self.tau_v          = tau_v

        self.n_active       = 16
        self.n_passive      = 36

        # find n_obstacles
        diagonal = np.array([[-3, -1, 1, 3, 6.5, 8.5,10.5,12.5], [13,11,9, 7, 3.5, 1.5,-0.5,-2.5]])
        xmin, xmax = -5, 12
        obs_step   = 2
        print(np.arange(xmin,xmax,obs_step))
        n_vertical = 1+(xmax-xmin)//obs_step
        vertical   = np.vstack([[xmin]*n_vertical, np.arange(xmin,xmax+self.eps,obs_step)])
        horizontal = np.vstack([np.arange(xmin,xmax+self.eps,obs_step), [xmin]*n_vertical])
        self.n_obstacles = n_vertical*2+diagonal.shape[1]

        self.n_all          = self.n_active+self.n_passive+self.n_obstacles
        self.rad            = np.empty(self.n_all)
        self.rad_active     = 0.2
        self.rad_passive    = 0.2
        self.rad_obstacles  = 1.8
        self.idx_active     = [i for i in range(self.n_all) if i<self.n_active]
        self.idx_passive    = [i for i in range(self.n_all) if i>=self.n_active and i<self.n_active+self.n_passive]
        self.idx_obstacles  = [i for i in range(self.n_all) if i>=self.n_active+self.n_passive]
        self.idx_movables   = [i for i in range(self.n_all) if i<self.n_active+self.n_passive]
        self.rad[self.idx_active]       = self.rad_active
        self.rad[self.idx_passive]      = self.rad_passive
        self.rad[self.idx_obstacles]    = self.rad_obstacles
        self.p_dropout                  = 0
        self.dt_dropout                 = 10*self.dt
        self.dropout                    = np.empty(self.n_active)
        self.hist_pos                   = np.empty((self.n_frames, self.n_dim, self.n_all))
        self.hist_active_vel            = np.empty((self.n_frames, self.n_dim, self.n_active))
        self.hist_tot_vel               = np.empty((self.n_frames, self.n_dim, self.n_active))

        ### Initial Positions
        # active
        r0                  = 2
        theta0              = np.linspace(0,2*np.pi,self.n_active+1)[:-1]
        self.velocity       = np.zeros((self.n_dim, self.n_all))
        self.pos            = np.empty((self.n_dim, self.n_all))
        self.pos[:, self.idx_active] = np.array([r0*np.cos(theta0), r0*np.sin(theta0)])
        self.l0                      = np.linalg.norm(self.pos[:, 0]-self.pos[:, 1])
        self.v_hat                   = self.v0*np.ones(self.n_active)
        self.theta_hat               = np.zeros(self.n_active)+np.pi/4
        self.active_velocity         = self.v_hat*np.vstack((np.cos(self.theta_hat), np.sin(self.theta_hat)))

        # passive
        n_cols = int(np.sqrt(self.n_passive))
        n_rows = self.n_passive/n_cols
        for i in range(self.n_passive):
            self.pos[0, self.n_active+i] = (i%n_cols-(n_cols-1)/2)*self.rad_passive*2
            self.pos[1, self.n_active+i] = (i//n_cols-(n_rows-1)/2)*self.rad_passive*2

        # obstacles
        print(diagonal.shape, vertical.shape, horizontal.shape)

        self.pos[:, self.idx_obstacles] = np.concatenate((diagonal,vertical,horizontal),axis=1)

    def run_step(self):
        ##################################################################################
        ################################  SPRING  ########################################
        ##################################################################################
        r1          = self.pos[:, self.idx_active]
        r2          = self.pos[:, np.roll(self.idx_active, -1)]
        dx, dy      = r2-r1
        l           = np.linalg.norm(r2-r1, axis=0)
        f_spring    = self.k_spring*(l-self.l0)*(r2-r1)/l
        f_spring   -= np.roll(f_spring, 1, axis=1)

        ##################################################################################
        #########################  SPHERICAL COLLSION  ###################################
        ##################################################################################
        displacement = self.pos[:, :, np.newaxis]-self.pos[:, np.newaxis]
        distance     = np.linalg.norm(displacement, axis=0)
        rm           = self.rad[:, np.newaxis]+self.rad
        flag_collision = np.zeros((self.n_all, self.n_all))
        flag_collision[distance<rm] = 1
        for i in range(len(flag_collision[0])):
            flag_collision[i,i] = 0

        f_collision  = np.sum(self.k_collision*(rm-distance)*flag_collision*displacement/(self.eps+distance), axis=2)

        ##################################################################################
        #########################  RECTANGULAR COLLSION  #################################
        ##################################################################################
        A_perp          = (r2-r1)[1]/(r2-r1+self.eps)[0]
        B_perp          = -1
        C_perp          = r2[1]-A_perp*r2[0]
        d_perp          = np.abs(A_perp*self.pos[0,:,np.newaxis]+B_perp*self.pos[1,:,np.newaxis]+C_perp)/np.sqrt(A_perp**2+B_perp**2)
        sign_perp       = np.sign(A_perp*self.pos[0,:,np.newaxis]+B_perp*self.pos[1,:,np.newaxis]+C_perp)
        rm_perp         = self.rad[:, np.newaxis] + self.rad[self.idx_active]/2

        r_center        = (r2+r1)/2
        A_para          = -(r2-r1)[0]/(r2-r1+self.eps)[1]
        B_para          = -1
        C_para          = r_center[1]-A_para*r_center[0]
        d_para          = np.abs(A_para*self.pos[0,:,np.newaxis]+B_para*self.pos[1,:,np.newaxis]+C_para)/np.sqrt(A_para**2+B_para**2)
        sign_para       = np.sign(A_para*self.pos[0,:,np.newaxis]+B_para*self.pos[1,:,np.newaxis]+C_para)
        rm_para         = l/2-self.rad[self.idx_active]

        flag_rect       = np.logical_and(d_perp<rm_perp, d_para<rm_para)
        f_rect_x        = self.k_collision*flag_rect*(rm_perp-d_perp)*A_perp/np.sqrt(A_perp**2+1)*sign_perp
        f_rect_y        = self.k_collision*flag_rect*(rm_perp-d_perp)*(-1)/np.sqrt(A_perp**2+1)*sign_perp
        f_rect          = np.vstack((f_rect_x[np.newaxis], f_rect_y[np.newaxis]))
        f_collsion_from_rect = np.sum(f_rect, axis=2)
        ratio           = (l/2+d_para*sign_para)/l
        f_bounce        = -np.sum(f_rect*ratio + np.roll(f_rect, 1, axis=2)*(1-ratio), axis=1)


        self.velocity                        = np.zeros((self.n_dim, self.n_all))
        self.velocity[:, self.idx_active]   += f_bounce
        self.velocity[:, self.idx_movables] += f_collsion_from_rect[:, self.idx_movables]
        self.velocity[:, self.idx_movables] += f_collision[:, self.idx_movables]
        self.velocity[:, self.idx_active]   += f_spring
        self.velocity[:, self.idx_active]   += self.active_velocity*self.dropout

        speed                                = np.linalg.norm(self.velocity, axis=0)
        # self.velocity                        = np.minimum(speed, self.vmax)*self.velocity/(self.eps+speed)
        theta                                = np.arctan2(self.velocity[1],self.velocity[0])
        # speed                                = np.minimum(speed[self.idx_active], self.vmax)
        self.v_hat                          -= self.dt*(self.v_hat-np.maximum(self.v0/2, np.minimum(self.vmax, speed[self.idx_active])))/(self.tau_v)
        self.theta_hat                      -= self.dt*(self.theta_hat-theta[self.idx_active])/(self.tau_theta)
        self.active_velocity                 = self.v_hat*np.vstack((np.cos(self.theta_hat), np.sin(self.theta_hat)))

        self.pos                            += self.velocity*self.dt

    def simulation(self):
        self.n_t_step       = int(np.round(self.tmax/self.dt))
        self.n_vid_step     = int(np.round(self.n_t_step/self.n_frames))
        self.n_dropout_step = int(np.round(self.tmax/self.dt_dropout))
        for i in range(self.n_t_step):
            if i%self.n_dropout_step==0:
                self.dropout = 1-(self.p_dropout > np.random.rand(self.n_active))
            if i%self.n_vid_step==0:
                print('%d/%d' % (1+i//self.n_vid_step, self.n_frames))
                self.hist_pos[i//self.n_vid_step] = self.pos
                self.hist_active_vel[i//self.n_vid_step] = self.active_velocity
                self.hist_tot_vel[i//self.n_vid_step]    = self.velocity[:, self.idx_active]
            self.run_step()

    def check_success(self):
        filename    = self.filename
        # run after simulation()
        last_x = self.hist_pos[-1, 0, self.idx_movables]
        last_y = self.hist_pos[-1, 1, self.idx_movables]
        # assume obstacles are all on a straight line
        ob_pos = self.hist_pos[0, :, self.idx_obstacles[:2]].T
        m_ob   = (ob_pos[1,1]-ob_pos[1,0])/(self.eps+ob_pos[0,1]-ob_pos[0,0])
        c_ob   = ob_pos[1,0]-m_ob*ob_pos[0,0]
        # True if active robots and passive granular materials can get through a hole
        flag_success = (last_y-m_ob*last_x-c_ob>0).all()

        if not os.path.isdir(os.path.join('data')):
            os.mkdir(os.path.join('data'))

        with open(os.path.join('data', filename+'.txt'), 'w') as f:
            f.write(str(flag_success))
        return flag_success

    def make_video(self):
        filename     = self.filename
        if not os.path.isdir(os.path.join('images')):
            os.mkdir(os.path.join('images'))
        if not os.path.isdir(os.path.join('images', filename)):
            os.mkdir(os.path.join('images', filename))
        if not os.path.isdir(os.path.join('videos')):
            os.mkdir(os.path.join('videos'))
        ##################################################################################
        ############################## MAKE PLOTS ########################################
        ##################################################################################
        print('Making Plots')
        # xmin, xmax  = np.min(self.hist_pos[:,0,:]), np.max(self.hist_pos[:,0,:])
        # ymin, ymax  = np.min(self.hist_pos[:,1,:]), np.max(self.hist_pos[:,1,:])
        xmin, xmax  = -3.5, 13
        ymin, ymax  = -3.5, 13

        fig, ax     = plt.subplots(figsize=(10,10))
        def color(i):
            if i<self.n_active:
                return 'red'
            elif i<self.n_active+self.n_passive:
                return 'blue'
            else:
                return 'green'

        for i in range(self.n_frames):
            # plot objects
            circle_list = [Circle((self.hist_pos[i,0,j],self.hist_pos[i,1,j]), self.rad[j], facecolor=color(j)) for j in range(self.n_all)]
            for circle in circle_list:
                ax.add_patch(circle)
            spring = np.empty((self.n_dim, self.n_active+1))
            spring[:, :self.n_active] = self.hist_pos[i, :, :self.n_active]
            spring[:, -1]             = self.hist_pos[i, :, 0]
            # plot spring
            plt.plot(spring[0], spring[1], color='red')
            # plot velocity
            for j in range(self.n_active):
                plt.arrow(self.hist_pos[i,0,j], self.hist_pos[i,1,j], 
                          self.hist_active_vel[i,0,j], self.hist_active_vel[i,1,j], color='black', width=0.05)
                plt.arrow(self.hist_pos[i,0,j], self.hist_pos[i,1,j], 
                          self.hist_tot_vel[i,0,j], self.hist_tot_vel[i,1,j], color='orange', width=0.05)
            plt.xlim([xmin, xmax])
            plt.ylim([ymin, ymax])
            plt.savefig(os.path.join('images', filename, '%04d.png' % (i+1)))
            plt.cla()

        ##################################################################################
        ############################## MAKE VIDEO ########################################
        ##################################################################################
        print('Making Video')
        images_files = os.path.join('images', filename, '%04d.png')
        video_file      = os.path.join('videos', '%s.mp4' % filename)
        command = ['ffmpeg', '-y', '-i', images_files, '-r', '10', '-vf', 'fps=5', '-pix_fmt', 'yuv420p', video_file]
        os.system(' '.join(command))

        ##################################################################################
        ############################## SAVE HIST  ########################################
        ##################################################################################
        np.save(os.path.join('images', filename, 'env_%s.npy' % filename), self)
        np.save(os.path.join('images', filename, 'hist.npy'), self.hist_pos)

    @staticmethod
    def gather_success():
        main_filename = os.path.join('data', 'summary.txt')
        with open(main_filename, 'w') as f_main:
            all_filenames = os.listdir('data')
            for each_filename in all_filenames:
                with open(os.path.join('data', each_filename)) as f:
                    f_main.write(f.read())
                    f_main.write('\n')