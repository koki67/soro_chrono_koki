#!/usr/bin/env python
# coding: utf-8

# In[ ]:


import numpy as np
import matplotlib.pyplot as plt
import numpy.linalg as la
import matplotlib

from matplotlib.patches import Circle, Wedge, Polygon
from matplotlib.collections import PatchCollection
import matplotlib.pyplot as plt
import os
from datetime import datetime
import functions as func


# In[ ]:


def write_par():
    '''write parameters in the path named Datapath '''
    f = open(Datapath + "/parameters_run.txt", "w+")
    f.write('intrisic velocity of each active particle: \r\n' + str(c_g) + '\n')

    f.write('time step dt : \r\n' + str(dt) + '\n')
    f.write('Max iteration time : \r\n' + str(tau) + '\n')

    f.write('Number of the active particle :' + '\r\n' + str(N_p) + '\n')
    f.write('Number of the Wall  :' + '\r\n' + str(N_w) + '\n')

    f.write('Diameter of the active particle :' + '\r\n' + str(D) + '\n')
    f.write('Diameter of the wall : \r\n' + str((DW-R)*2) + '\n')
    f.write('noise level : \r\n' + str(eta) + '\n')

    f.write('LJ strength : \r\n' + str(c_LJ) + '\n')
    f.write('MAX cutoff dis for the LJ between particle and wall : \r\n' + str(DW+FWR) + '\n')
    f.write('MIN cutoff dis for the LJ between particle and wall : \r\n' + str(0.8*DW) + '\n')

    f.write('Kspring : \r\n' + str(kspring) + '\n')
    f.write('cutoff dis from equilibrum dis for the spring between particle and particle : \r\n' + str(FR) + '\n')
    
    if c_active ==0:
        f.write('NOT active''\n')
    else:
        f.write('active''\n')
        f.write('alpha(las in the code), continuous changing for theta:\r\n' + str(las)+ '\n')
        if not_adap == False:    
            f.write('parameters discribing the adaptive force(A*(alp * ad_f / (1 + alp * ad_f))), A and alp:\r\n' + str(c_active) +',' + str(alp) + '\n')
    f.close()


# In[ ]:


scalevorig = 1 # scale vs original when Jiayi gave me her code

def initialize():
        #initialize positions of everyone
    rb = func.make_hex(3,1, scalevorig*0.5)
#    rb = np.array([[0,5]])
    rb[:,1] += 5
    rb[:,0] += RW
    N_b = len(rb)
    plt.scatter(rb[:,0], rb[:,1])
    rw = func.make_hex(4,2,1)
    rw[:,0] = rw[:,0].copy()*(DW*3)
    rw[:,1] = rw[:,1].copy()*(DW*2)
    rw[:,1] += -max(rw[:,1].copy())
#     rw = np.array([[0,0]])
    N_w = len(rw)
    plt.scatter(rw[:,0], rw[:,1])
    plt.show()
#     thetas_vec = np.np.array([[0,-1]]) ## initial vector of angle of the activity
    thetas_vec = np.zeros((N_b,2))
    thetas_vec[:,1] = np.tile([-1], N_b)

    vec_n = np.zeros(2)
    #print(vec_n[1])
    ad_f = np.ones(N_b)
    actf =(alp * ad_f / (1 + alp * ad_f)) #+c_active# active driving force initially 0
       
    return rb, rw, thetas_vec, vec_n,ad_f, actf, N_b, N_w


# In[ ]:


def plot_circles(fig, ax, x, y, r, c='r', cmap=matplotlib.cm.rainbow, ticks=[0, 1]):
    patches = []
    for x1, y1 in zip(x, y):
        circle = plt.Circle((x1, y1), r)
        patches.append(circle)

    if isinstance(c, str):
        p = PatchCollection(patches, facecolor="None", edgecolor=c, alpha=1, linewidths=2)
        ax.add_collection(p)
        return ax

    else:
        p = PatchCollection(patches, cmap=cmap, alpha=0.8)
        p.set_array(np.array(c))

    ax.add_collection(p)

    # cbar = fig.colorbar(p, ax=ax,ticks=ticks)
    p.set_clim(min(ticks), max(ticks))
    return fig,p

def plot_active_force(c_active, alp, c_g):
    plt.plot(np.arange(0,10,0.1),c_active/c_g * (alp * np.arange(0,10,0.1) / (1 + alp * np.arange(0,10,0.1))))
    plt.ylabel('active velocity ($1/c_g$)', fontsize = 20)
    plt.xlabel('total velocity from last time v(t-1)', fontsize = 20)
#     plt.show()

    plt.savefig(Datapath + '/adaptive_force_profile.png')
    plt.tight_layout()
#     plt.close()

def visualization(not_adap):
    fig = plt.figure(figsize = (8,8))

    ax = fig.add_subplot(121,aspect='equal') 
    plt.title('time at %0.2f'%time+' with active = %0.1f c_g'%(c_active/c_g) ,fontsize=15)
    
    plot_circles(fig,ax,rp[:,0],rp[:,1],R, actf)
    fig,p= plot_circles(fig,ax,rw[:,0],rw[:,1],RW,la.norm(FWfeel, axis = 1), cmap = matplotlib.cm.coolwarm, ticks=[0, 100])
    cbar = fig.colorbar(p, ax=ax,orientation="horizontal",fraction=0.07,anchor=(1.0,0.0))
    

    plt.xlabel('force on wall = %d'%tot_Fwall,fontsize=15)
    ax.quiver(rp[:,0],rp[:,1], force[:,0], force[:,1], linewidth = 25, headwidth=10, units = 'xy', scale = 100)
    # ax.quiver(rp[:,0],rp[:,1], c_active*ad_f*n_vec_theta[:,0], 
    #           c_active*ad_f*n_vec_theta[:,1], linewidth = 15, headwidth=10, color= 'b')
    ax.quiver(rw[:,0], rw[:,1], FWfeel[:,0], FWfeel[:,1],
              linewidth = 25, headwidth=10, units = 'xy', scale = 100, color = 'r')

    plt.xlim(np.amin(rw[:,0]-2*RW),np.amax(rw[:,0]+2*RW))
    plt.ylim(np.amin(rw[:,1]-2*RW),np.amax(rw[:,1]+2*RW))
    # plt.xticks([])
    plt.yticks([])

    if not_adap==False and c_active !=0:
        a = plt.axes([0.6, 0.3, .2, .2])
        xx = np.arange(0,20,0.5)
        plt.plot(xx,(alp * xx / (1 + alp * xx)))

        plt.scatter(ad_f, actf, c=actf, cmap = matplotlib.cm.rainbow,
                    vmin=0, vmax=1, alpha = 1.)
        plt.colorbar()
        plt.ylabel('active velocity ',fontsize = 15)
        plt.xlabel('$\|f(t-1)*\hat{n}\|$',fontsize = 15)
        plt.yticks([])

    if len(force[:,0]) ==1:
        a = plt.axes([0.6, 0.6, .2, .2])
        plt.annotate("",xy=(pas_force[:,0], pas_force[:,1]),xytext=(0,0),
                 arrowprops=dict(facecolor='b', shrink=0.05),alpha = 0.5)
 
        plt.annotate("",xy=(c_active*actf*n_vec_theta[:,0], c_active*actf*n_vec_theta[:,1]),xytext=(0,0),
                             arrowprops=dict(facecolor='r', shrink=0.05))
        
        if c_active !=0:
            plt.ylim(-c_g/2.-c_active,c_g/2.+c_active)
            plt.xlim(-c_g/2.-c_active,c_g/2.+c_active)
        else:
            plt.ylim(-2*c_g,2*c_g)
            plt.xlim(-2*c_g,2*c_g)
        plt.xticks([])
        plt.yticks([])
        plt.title('passive(b) vs active(r)',fontsize = 15)

    return fig

    


# In[ ]:


numtoavg = 6
array = [0.01, 0.02, 0.03, 0.05, 0.075, 0.1, 0.15, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1]
array2 = [0.01, 0.02, 0.03, 0.05, 0.075, 0.1, 0.15, 0.2, 0.3, 0.4, 0.5, 0.75, 1., 1.5, 2.0, 3.0, 5.0, 7.5, 10.0]

tot_Farr = [0]*len(array) # total force * time over run
for i in range(len(tot_Farr)):
    tot_Farr[i] = [0]*len(array2)
    for j in range(len(tot_Farr[i])):
        tot_Farr[i][j] = [0]*numtoavg
Fwall_Max = [0]*len(array) # initialize force on wall max val
for i in range(len(Fwall_Max)):
    Fwall_Max[i] = [0]*len(array2)
    for j in range(len(tot_Farr[i])):
        Fwall_Max[i][j] = [0]*numtoavg
timetot = [0]*len(array) # avg wall force over on run
for i in range(len(timetot)):
    timetot[i] = [0]*len(array2)
    for j in range(len(timetot[i])):
        timetot[i][j] = [0]*numtoavg
        
for n in range(len(array)):
    for l in range(len(array2)):
        for m in range(numtoavg):
            N_s = 0 #skin active connected by spring
            tot_Fwall = 0 # total force acting on wall

            dt = 0.005

            tau = 40
            eta = 0.1 #noise 
            sqdt = np.sqrt(dt) #noise 

            #mark time where robots in obstacle course
            timestart = 0
            timeend = 0
            timestart_mark = 0
            timeend_mark = 0

            R = 0.2 * scalevorig # radius of beads
            D = 2 * R # diameter
            RW = 6 * R # radius of Wall
            DW = RW + R # distance between center of bead and center of the Wall


            FR = R# cutoff range of force between beads(repulsion if surface distance < R)
            LJdismult = 1
            LJdismultpart = 1.01
            FWR = R# cutoff range of force between bead and wall (repulsion if surface distance < 2*R)
            c_LJ = 1.

            c_cutoff = c_LJ *(12*(DW/(0.8*DW)) ** (13))
            print(c_cutoff)
            alpin = array2[l] #[0,5]
            alp = 1/alpin
            kspring = 30.
            fdscale = 1.

            #kappa = 2 
            #if =1, angle = the total force direction = actual motion
            #if =2. angle = the vector sum of actual motion + active motion





            not_adap = False
            c_active = 12.
            c_g = 6
            las = array[n] #[0,1] when las=1 the change of theta is instantaneous 
            if not_adap:
                print('its not adaptive')
            if las == 1:
                rootpath = '/Users/abert/OneDrive/Documents/bouncing-balls/AB/'
            else:
                rootpath = '/Users/abert/OneDrive/Documents/bouncing-balls/AB/'
            if not os.path.exists(rootpath): os.mkdir(rootpath)




            AVEFORCES = []
            TIMES = []


            if not_adap:
                print('its not adaptive')
            if las != 1:
                print('not instantaneous')

            rp, rw, n_vec_theta, vec_n, ad_f, actf, N_p, N_w = initialize()
            vec_theta = np.zeros(np.shape(n_vec_theta))
            Timenow = str(datetime.now().strftime("%Y_%m_%d_%H_%M_%S"))
            if m == numtoavg-1:
                if not_adap:
                    Datapath = rootpath + 'the1c_act_%02d_c_g_%02d_k_%02d_las_%01d_alpin_%01d' % (c_active, c_g, kspring, las*100, alpin*100) + str(Timenow)
                else:
                    Datapath = rootpath + 'the1adap_c_act_%02d_c_g_%02d_k_%02d_las_%d_alpin_%01d' % (c_active, c_g, kspring, las*100, alpin*100) + str(Timenow)
                if not os.path.exists(Datapath): os.mkdir(Datapath)
            else:
                if not_adap:
                    Datapath = rootpath + 'c_act_%02d_c_g_%02d_k_%02d_las_%01d_alpin_%01d' % (c_active, c_g, kspring, las*100, alpin*100) + str(Timenow)
                else:
                    Datapath = rootpath + 'adap_c_act_%02d_c_g_%02d_k_%02d_las_%d_alpin_%01d' % (c_active, c_g, kspring, las*100, alpin*100) + str(Timenow)
                if not os.path.exists(Datapath): os.mkdir(Datapath)
            print(Datapath)
            if c_active != 0 and not_adap == False:
                plot_active_force(c_active, alp, c_g)
            write_par()
            tot_Fwall = 0




            for time in np.arange(0, tau, dt):

                # initializefoce by the thermal noise force
                force = eta * (np.random.rand(N_p, 2) - 0.5) / sqdt
                FWfeel = np.zeros((N_w, 2))

                # loop around all movable particles:    
                # calculate repulsive force
                for p_id in range(0, N_p):
                    # print(rp)
                    xy = rp[p_id, :]
                    # %%%force with wall 
                    for i in range(0, N_w):
                        vec_n = -rw[i, :] + xy

                        dis = la.norm(vec_n)  # calculate distance
                        if dis < (RW + FWR)*LJdismult:  # only count when within range

                            strength = np.min([c_cutoff, c_LJ * 12 * ((FWR + RW) / dis) ** (13) - LJdismult ** (-13)])

                            force[p_id, :] += strength * vec_n / dis
                            FWfeel[i, :] += strength * vec_n / dis
                        

                    # %%%force with other body+skin particle
                    for i in range(N_p):  # all movable particles
                        if i != p_id:  # but can't interact with self
                            vec_n = -rp[i, :] + xy
                            dis = la.norm(vec_n)  # calculate distance
                            if dis - fdscale*D < fdscale*FR:
                                # print(dis, x, y, rp[i,0], rp[i,1])
                                strength = np.round(-kspring * (dis - D),4)
                            if dis < D*LJdismultpart:
                                strength += np.min([10,c_LJ/5 * 12*((D/dis) ** (13) - LJdismultpart ** (-13))])                  
                                # strength = c_r * (1-dis+D)^n_rep;
                                force[p_id, :] = force[p_id, :] + strength * vec_n / dis
                                
                add_Fwall = sum(la.norm(FWfeel, axis=1))
                tot_Fwall += add_Fwall
                
                
                if add_Fwall > Fwall_Max[n][l][m]:
                    Fwall_Max[n][l][m] = add_Fwall

                if add_Fwall > 10:
                    if timestart_mark == 0:
                            timestart = time
                            timestart_mark = 1
                    timeend = time


                force[:, 1] += - c_g
                pas_force = force.copy()
                force[:, 0] += c_active * (actf * n_vec_theta[:, 0])
                force[:, 1] += c_active * (actf * n_vec_theta[:, 1]) 
                rp = rp + dt * force

                # datasaving and plotting
                if (time / dt) % 1 == 0:

                    savedir = Datapath + '/rp_force_data/'

                    if not os.path.exists(savedir): os.mkdir(savedir)
                    np.save(savedir + '/%04d' % (time / dt), np.array([rp, force]))

                    if m == numtoavg-1:
                        fig = visualization(not_adap)
                        plotdir = Datapath + '/images/'
                        if not os.path.exists(plotdir): os.mkdir(plotdir)
                        plt.savefig(plotdir + '/%04d.png' % (time / dt))
                        #             plt.show()
                        plt.close()

                # update active forces if c_active != 0
                if c_active != 0:
                    vec_theta[:, 0] = c_active * actf * n_vec_theta[:, 0] * (1 - las) + force[:, 0] * las
                    vec_theta[:, 1] = c_active * actf * n_vec_theta[:, 1] * (1 - las) + force[:, 1] * las
                    n_vec_theta[:, 0] = vec_theta[:, 0] / la.norm(vec_theta, axis=1)
                    n_vec_theta[:, 1] = vec_theta[:, 1] / la.norm(vec_theta, axis=1)

                    ad_f = abs(np.sum(force * n_vec_theta, axis=1))              
                    if not_adap:
                        actf = np.ones(len(ad_f))
                    else:
                        actf = (alp * ad_f / (1 + alp * ad_f))

                # check if it goes through the maze
                if max(rp[:, 1]) < min(rw[:,1])-5:
                    print('all particles go through at time', time)
                    print('with the total force exerted on wall', tot_Fwall)
                    print('c_g is', c_g)

                    AVEFORCES.append(tot_Fwall / time)
                    TIMES.append(time)
                    #print(plotdir[:-8])
                    #func.make_movie(plotdir + '/', plotdir[:-8] + "/images")

                    break

            tot_Farr[n][l][m] = tot_Fwall

            timetot[n][l][m] = timeend-timestart

        f = open(Datapath + "/F_results.txt", "w+")
        f.write('las: \r\n' + str(array[n]) + '\n')
        f.write('alpin: \r\n' + str(array2[l]) + '\n')
        f.write('totF: \r\n' + str(np.sum(tot_Farr[n][l])/numtoavg) + '\n') 
        f.write('time: \r\n' + str(np.sum(timetot[n][l])/numtoavg) + '\n')
        f.write('maxF: \r\n' + str(np.sum(Fwall_Max[n][l])/numtoavg) + '\n')
        f.close()


# In[ ]:


'''''''''
changes:
- added scalability to make-hex
- scaled down arrows 10 times (10->100)
- changed k from 20 to 30
- added more obstacles
- don't touch fdscale
- replaced F_Mark system with timestart timeend system
- images only made once in averaging loop
- added special mark for files with fdata

- SWITCHED ARRAY1
'''''''''

