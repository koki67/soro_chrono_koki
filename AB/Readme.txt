-JAMoEBA Simulation Code: Instruction Guide-

For more information on the equations driving the code, concepts used, and the most recent results, see Adaptivity_in_Swarm_Navigation.pdf.

Included in this folder are codes for two different situations using the same concepts and equations, as well as some data visualisation tools.
The codes are las_alpha_trials.ipynb and adaptive_trials_obstacles.ipynb. I have also included .py versions, since you might not want to use jupyter.
Included in this folder is a file called functions.py. It must be included in the same folder as the codes in order for them to run correctly, since it contains definitions for functions the codes use.

The codes are designed to output data for a whole space of parameters. They currently vary two parameters at a time. 
las_alpha_trials.ipynb looks at las (directional/angular response to force) and alpha (activity magnitude adaptivity to force) in a non-jamming situation.
las_alpha_trials.ipynb actually uses the inverse of alpha, defined as alpin in the code. This is because higher alpin corresponds to higher adaptive behavior (see equations), making it a more intuitive adaptibility parameter.
adaptive_trials_obstacles.ipynb looks at alpha and the size of a gap through which a group of particles/robots must traverse. This is supposed to be a jamming situation.
The code constricts the gap by setting the center points of two circular obsacles and gradually increasing their radii as a multiple of the radius of a particle.

In the codes there is a parameter numtoavg which determines the number of runs used at each particular set of parameters to average data.
Averaging is necessary because the small amount of noise in the code can create significant variation in results. A number of runs greater than 5 is recommended for sharp results.

You have to set the particular values of each tested parameter you want the code to use. For example, in las_alpha_trials.ipynb, array shows all of the las values the code iterates over, while array2 shows all the alpin values. These arrays can be modified at will, but I found that the sets of parameters I chose provide a pretty complete sweep of the parameter space.

Times to clear the obstacles, average forces exerted on/by obstacles, and maximum forces are the main measured outputs.
There are also image files saved at many timesteps which can be combined into movies by a program like ImageJ.

Average data and images are saved in output files indexed by the1..., while others do not have that prefix to their file names and do not have those files. (Images could be saved for every run, but that takes a lot of space!)
Data is included in text files called F_results.
Images are saved in folders called images.

Output file names contain information about the parameters used, eg. adap_c_act_12_c_g_06_k_30_alpin_1__RW_110_2019_07_15_10_31_11
This means the robots in this run are adaptive (alpin != 0), their active force constant c_active is 12, the gravity/directional force constant is 6, the spring constant between "robots" is 30, alpin is 0.01 (multiplied by 100 for file name), the radius of obstacles as a multiple of particle radius is 11 (multiplied by 10), and the run was made on July 15, 2019, at 10:31:11.

In order to have your output files save correctly, you have to set an appropriate path for them under rootpath in the code. You will have to do this separately for each .ipynb file.

Collections of output data can be assembled into phase plots using Make_Phase_Plot_Obstacles and Make_Phase_Plot_lasalp for the jamming and non-jamming cases respectively. They essentially contain the same code, just reskinned to the ranges of inputs and outputs, names of parameters, etc.
You have to also input the sets of parameters you used in the code into the top lines so the code can know which points to take results for. If you change the output file naming at all, you will also have to change the way these plotting codes reference the output files for them to be usable.
Examples are on pages 4, 5, and 6 of Adaptivity_in_Swarm_Navigation.pdf.

This should be enough to get you up and runnning, but I obviously might have missed something, and there are many questions that can still be had. (I realize the code is not very commented.)
Shoot me an email with anything! I was sick last week, but I will be responding quickly now.