#!/usr/bin/env python
# coding: utf-8

# In[81]:


import numpy as np
import matplotlib.pyplot as plt
import numpy.linalg as la
import matplotlib
import matplotlib.pyplot as plt
import os
from datetime import datetime
import glob as g
from mpl_toolkits.mplot3d import Axes3D


# In[82]:


array = [12,12.5,13,13.5,14,14.5,15,15.5,15.7,16,16.2,16.5,16.7]
array2 = [0.01, 0.02, 0.03, 0.05, 0.07, 0.1, 0.15, 0.2, 0.3, 0.4, 0.5, 0.75, 1., 1.5, 2.0, 3.0, 5.0]
arrayr = np.array([12,12.5,13,13.5,14,14.5,15,15.5,15.75,16,16.25,16.5,16.75])
array2r = np.array([0.01, 0.02, 0.03, 0.05, 0.075, 0.1, 0.15, 0.2, 0.3, 0.4, 0.5, 0.75, 1., 1.5, 2.0, 3.0, 5.0])
totF = [0]*len(array)
for i in range(len(totF)):
    totF[i] = [0]*len(array2)
time = [0]*len(array)
for i in range(len(time)):
    time[i] = [0]*len(array2)
maxF = [0]*len(array)
for i in range(len(maxF)):
    maxF[i] = [0]*len(array2)
avgF = [0]*len(array)
for i in range(len(avgF)):
    avgF[i] = [0]*len(array2)
for n in range(len(array)):
    for l in range(len(array2)):
        f = open(g.glob('/Users/abert/Documents/Adaptive_Robot/Obstacles_Test/alp_files_las1/the1adap_c_act_12_c_g_06_k_30_alpin_' + str(int(array2[l]*100)) + '__RW_' + str(int(array[n]*10)) + '_2*/F_results*')[0])
        a = f.readlines()
        b = [0]*len(a)
        for i in range(len(a)):
            b[i] = a[i].replace('\n','')
        for i in range(5):
            b.remove('')
        a = [0]*len(b)
        for i in range(len(b)):
            a[i] = b[i].replace(': ','')
        for i in range(len(a)):
            if a[i] == 'totF':
                totF[n][l] = float(b[i+1])
            if a[i] == 'time':
                time[n][l] = float(b[i+1])
                if time[n][l] == 0:
                    time[n][l] = 0.001
            if a[i] == 'maxF':
                maxF[n][l] = float(b[i+1])
        avgF[n][l] = totF[n][l] / time[n][l] * 0.005


# In[83]:


plotarray = [0]*20
for i in range(20):
    plotarray[i] = [0]*500
for i in range(len(array)):
    for j in range(len(array2)):
        plotarray[int(arrayr[i]*4-48)][int(array2[j]*100-1)] = time[i][j]

for i in range(20):
    for j in range(500):
        markerx = 0
        markery = 0
        upperx = 0
        uppery = 0
        lowerx = 0
        lowery = 0
        for n in range(len(array)):
            if arrayr[n]*4 - 48 >= i and markerx == 0:
                upperx = int(arrayr[n]*4 - 48)
                lowerx = int(arrayr[n-1]*4 - 48)
                markerx = 1
        for n in range(len(array2)):
            if array2[n]*100 - 1 >= j and markery == 0:
                uppery = int(array2[n]*100 - 1)
                lowery = int(array2[n-1]*100 - 1)
                markery = 1
        rbl = np.sqrt((i-lowerx)**2+(j-lowery)**2)
        rul = np.sqrt((i-lowerx)**2+(j-uppery)**2)
        rbr = np.sqrt((i-upperx)**2+(j-lowery)**2)
        rur = np.sqrt((i-upperx)**2+(j-uppery)**2)
        if rbl <= rul:
            if rbl <= rbr:
                if rbl <= rur:
                    plotarray[i][j] = plotarray[lowerx][lowery]
                else:
                    plotarray[i][j] = plotarray[upperx][uppery]
            elif rbr <= rur:
                    plotarray[i][j] = plotarray[upperx][lowery]
            else:
                    plotarray[i][j] = plotarray[upperx][uppery]
        else:
            if rul <= rbr:
                if rul <= rur:
                    plotarray[i][j] = plotarray[lowerx][uppery]
                else:
                    plotarray[i][j] = plotarray[upperx][uppery]
            elif rbr <= rur:
                    plotarray[i][j] = plotarray[upperx][lowery]
            else:
                    plotarray[i][j] = plotarray[upperx][uppery]
                    
                    
def forceAspect(ax,aspect=1):
    im = ax.get_images()
    extent =  im[0].get_extent()
    ax.set_aspect(abs((extent[1]-extent[0])/(extent[3]-extent[2]))/aspect)
        
fig, ax = plt.subplots(figsize=(10,10))
im = ax.imshow(plotarray, cmap='YlGnBu')

# We want to show all ticks...
ax.set_xticks([0,100,200,300,400,500])
ax.set_yticks([0,4,8,12,16])
# ... and label them with the respective list entries
ax.set_xticklabels([0,1,2,3,4,5])
ax.set_yticklabels([6,5,4,3,2])

# Rotate the tick labels and set their alignment.
#plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
         #rotation_mode="anchor")

# Loop over data dimensions and create text annotations.
#for i in range(len(vegetables)):
    #for j in range(len(farmers)):
        #text = ax.text(j, i, harvest[i, j],
                       #ha="center", va="center", color="w")

#ax.set_title("Harvest of local farmers (in tons/year)")
#fig.tight_layout()
forceAspect(ax,aspect=1)
fig.suptitle('Time', x=0.44, y=0.85, fontsize=20)
plt.xlabel('1/alpha', fontsize=13)
plt.ylabel('Obstacle Spacing (Robot Diameters)', fontsize=13)

plt.colorbar(im, shrink = 0.82)

plt.show()


# In[ ]:


#look at those 0.001 factors, relation between average and total force...


# In[ ]:




