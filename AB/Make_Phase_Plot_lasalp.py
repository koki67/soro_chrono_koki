#!/usr/bin/env python
# coding: utf-8

# In[1]:


import numpy as np
import matplotlib.pyplot as plt
import numpy.linalg as la
import matplotlib
import matplotlib.pyplot as plt
import os
from datetime import datetime
import glob as g
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.axes_grid1 import make_axes_locatable


# In[4]:


array = [0.01, 0.02, 0.03, 0.05, 0.07, 0.1, 0.15, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1]
array2 = [0.01, 0.02, 0.03, 0.05, 0.07, 0.1, 0.15, 0.2, 0.3, 0.4, 0.5, 0.75, 1., 1.5, 2.0, 3.0, 5.0]
arrayr = np.array([0.01, 0.02, 0.03, 0.05, 0.075, 0.1, 0.15, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1])
array2r = np.array([0.01, 0.02, 0.03, 0.05, 0.075, 0.1, 0.15, 0.2, 0.3, 0.4, 0.5, 0.75, 1., 1.5, 2.0, 3.0, 5.0])
totF = [0]*len(array)
for i in range(len(totF)):
    totF[i] = [0]*len(array2)
time = [0]*len(array)
for i in range(len(time)):
    time[i] = [0]*len(array2)
maxF = [0]*len(array)
for i in range(len(maxF)):
    maxF[i] = [0]*len(array2)
avgF = [0]*len(array)
for i in range(len(avgF)):
    avgF[i] = [0]*len(array2)
for n in range(len(array)):
    for l in range(len(array2)):
        f = open(g.glob('/Users/abert/Documents/Adaptive_Robot/test/las_alp_files/the1adap_c_act_12_c_g_06_k_30_las_' + str(int(array[n]*100)) + '_alpin_' + str(int(array2[l]*100)) + '2*/F_results*')[0])
        a = f.readlines()
        b = [0]*len(a)
        for i in range(len(a)):
            b[i] = a[i].replace('\n','')
        for i in range(5):
            b.remove('')
        a = [0]*len(b)
        for i in range(len(b)):
            a[i] = b[i].replace(': ','')
        for i in range(len(a)):
            if a[i] == 'totF':
                totF[n][l] = float(b[i+1])
            if a[i] == 'time':
                time[n][l] = float(b[i+1])
                if time[n][l] == 0:
                    time[n][l] = 0.001
            if a[i] == 'maxF':
                maxF[n][l] = float(b[i+1])
        avgF[n][l] = totF[n][l] / time[n][l] * 0.001


# In[5]:


plotarray = [0]*100
for i in range(100):
    plotarray[i] = [0]*500
for i in range(len(array)):
    for j in range(len(array2)):
        plotarray[int(array[i]*100-1)][int(array2[j]*100-1)] = maxF[i][j]

for i in range(100):
    for j in range(500):
        markerx = 0
        markery = 0
        upperx = 0
        uppery = 0
        lowerx = 0
        lowery = 0
        for n in range(len(array)):
            if array[n]*100 - 1 >= i and markerx == 0:
                upperx = int(array[n]*100 - 1)
                lowerx = int(array[n-1]*100 - 1)
                markerx = 1
        for n in range(len(array2)):
            if array2[n]*100 - 1 >= j and markery == 0:
                uppery = int(array2[n]*100 - 1)
                lowery = int(array2[n-1]*100 - 1)
                markery = 1
        rbl = np.sqrt((i-lowerx)**2+(j-lowery)**2)
        rul = np.sqrt((i-lowerx)**2+(j-uppery)**2)
        rbr = np.sqrt((i-upperx)**2+(j-lowery)**2)
        rur = np.sqrt((i-upperx)**2+(j-uppery)**2)
        if rbl <= rul:
            if rbl <= rbr:
                if rbl <= rur:
                    plotarray[i][j] = plotarray[lowerx][lowery]
                else:
                    plotarray[i][j] = plotarray[upperx][uppery]
            elif rbr <= rur:
                    plotarray[i][j] = plotarray[upperx][lowery]
            else:
                    plotarray[i][j] = plotarray[upperx][uppery]
        else:
            if rul <= rbr:
                if rul <= rur:
                    plotarray[i][j] = plotarray[lowerx][uppery]
                else:
                    plotarray[i][j] = plotarray[upperx][uppery]
            elif rbr <= rur:
                    plotarray[i][j] = plotarray[upperx][lowery]
            else:
                    plotarray[i][j] = plotarray[upperx][uppery]
                    
                    
def forceAspect(ax,aspect=1):
    im = ax.get_images()
    extent =  im[0].get_extent()
    ax.set_aspect(abs((extent[1]-extent[0])/(extent[3]-extent[2]))/aspect)
        
fig, ax = plt.subplots(figsize=(10,10))
im = ax.imshow(plotarray, cmap='YlGnBu')


# We want to show all ticks...
ax.set_xticks([0,100,200,300,400,500])
ax.set_yticks([0,20,40,60,80,100])
# ... and label them with the respective list entries
ax.set_xticklabels([0,1,2,3,4,5])
ax.set_yticklabels([0,0.2,0.4,0.6,0.8,1.0])

# Rotate the tick labels and set their alignment.
#plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
         #rotation_mode="anchor")

# Loop over data dimensions and create text annotations.
#for i in range(len(vegetables)):
    #for j in range(len(farmers)):
        #text = ax.text(j, i, harvest[i, j],
                       #ha="center", va="center", color="w")

#ax.set_title("Harvest of local farmers (in tons/year)")
#fig.tight_layout()
forceAspect(ax,aspect=1)
fig.suptitle('Average Force', x=0.44, y=0.85, fontsize=20)
plt.xlabel('1/alpha', fontsize=13)
plt.ylabel('las', fontsize=13)

plt.colorbar(im, shrink = 0.82)

plt.show()


# In[145]:





# In[ ]:




